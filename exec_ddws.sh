#! /bin/bash -x
set -e
if [ $# != 1 ]; then
    echo "Usage: ${0} XMCDAWebServices/svc-name/tests/<inputs_dir>"
    exit 1
fi

script_dir="$(realpath "${0%/*}")"
input_dir="$(realpath "$1")"
IFS=/ read -r -a args_1 <<< $1
SERVICE=${args_1[1]}

TMP_DIR=tmp/out_test
idx=0
rm -rf "${TMP_DIR}"
mkdir -p "${TMP_DIR}"
while read -d '' xml; do
    xml_filename=$(basename "${xml}")
    if [ "${xml_filename#**.}" == 'xml' ]; then
        args[$((idx++))]="${xml_filename%.xml}:$xml"
    elif [ "${xml_filename#**.}" == 'csv' ]; then
        args[$((idx++))]="${xml_filename}:$xml"
    fi
done < <( find "${input_dir}" -maxdepth 1 -type f -print0 )

#echo "${args[@]}"
VERBOSE=-v
cd "${TMP_DIR}"
python3.6 ${script_dir}/python/ddws_soap_client.py ${VERBOSE}  -S -U http://webservices-test-v3.decision-deck.org/soap/%s.py -n $SERVICE -x "${args[@]}"

while read -d '' base64_encoded; do
    base64 -d ${base64_encoded} > ${base64_encoded%.base-64-encoded}
    rm ${base64_encoded}
done < <(find . -name '*.base-64-encoded' -print0)

cd -
test_dir=${1%/} # remove trailing '/', if any
test_dir=${1%/*}
input=${1##*/}
output=out${input#in}
meld ${test_dir}/${output} "${TMP_DIR}"
