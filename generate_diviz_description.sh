#! /bin/bash

if [ $# -ne 1 ]; then
  echo "Usage: $0 <directory>" 1>&2
  exit 1
fi

cwd=$(pwd)
script_absdirpath="$(cd "${0%/*}" 2>/dev/null; echo "$PWD")"

D2_ROOT=$script_absdirpath
DIVIZ_ROOT=/home/TB/projects/mcda/diviz/diviz-test-v3/descriptions

if [ -e "${1}/description-wsDD.xml" ]; then
    file="${cwd}/${1}/description-wsDD.xml"
    dir=${file%*/description-wsDD.xml}
    pushd $D2_ROOT/python
    echo -n "$dir"
    $D2_ROOT/python/generate_from_description.py -o $DIVIZ_ROOT $file xml_diviz
    echo ''
    popd
fi

# else
find ${1} -maxdepth 1 -type f -name 'description-diviz_*.xml'  | while read file;
do
    xml=${file##*/}
    # we extract the provider & service name from the description's filename
    FQservice=${xml#description-diviz_}
    provider=${FQservice%%--*}
    service=${FQservice#*--} # service--version.xml
    service=${service%.xml} # service--version
    version=${service#*--}
    service=${service%--*}
    echo -n "${file%/*} "
    cp $file $DIVIZ_ROOT/${provider}___${service}___${version}.xml
    echo ''
done
