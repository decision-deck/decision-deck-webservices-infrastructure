##########################################
# Usage:
# R --slave --vanilla --args "[inDirectory]" "[outDirectory]" < dummy.R
# Example: 
# R --slave --vanilla --args "${PWD}/in" "${PWD}/out" < dummy.R
##########################################

rm(list=ls())

##########################################
# Start by loading the RXMCDA library which allows to read and write XMCDA files
##########################################

library(RXMCDA)

errFile<-NULL
errData<-NULL
errCalc<-NULL

execFlag<-FALSE

##########################################
# Get the in and out directories from the arguments
##########################################

inDirectory <- commandArgs()[5]
outDirectory <- commandArgs()[6]

##########################################
# Set the working directory as the "in" directory
##########################################

setwd(inDirectory)

##########################################
# Try to load the mandatory input files.
# If they contain XML, the XML tree is stored in a variable.
# Then the input files are checked against the XMCDA schema.
# If something goes wrong, everything stops at this stage.
#
# For this example, we load a mandatory file containing the alternatives' IDs.
##########################################

tmpErr<-try(
		{
			treeAlternatives<-xmlTreeParse("alternatives.xml",useInternalNodes=TRUE)
		}
)
if (inherits(tmpErr, 'try-error')){
	errFile<-"Cannot read the alternatives file."
}

if (is.null(errFile)){
	
	# mandatory files were correctly loaded
	# now we check if files are valid according to xsd
	
	if (checkXSD(treeAlternatives)==0)
	{
		errFile<-"Alternatives file is not XMCDA valid."	
	}
}

##########################################
# Try to load the optional input files.
# If they contain XML, the XML tree is stored in a variable.
# If something goes wrong, we suppose that the file was not present.
# If a file is loaded, it is checked against the XMCDA schema.
#
# For this example, we load an optional file containing the seed for the random number generation.
##########################################

if (is.null(errFile)){
	
	# let us now load the optional xml files
	# if an error occurs, we suppose that the file was not present (optional files!!)
	
	treeSeed <- NULL
	
	tmpErr<-try(
			{
				treeSeed<-xmlTreeParse("seed.xml",useInternalNodes=TRUE)
			}
	)
}

if (is.null(errFile)){
	
	# we must now check if the optional files are XMCDA valid
	# for the optional files we first check whether a tree has been loaded
	
	if ((!is.null(treeSeed))){
		if (checkXSD(treeSeed)==0)
		{
			errFile<-"Seed file is not XMCDA valid."	
		}
	}	
}

##########################################
# Extract data from the XMCDA trees.
# If everything went right up to here, we extract the data from the XMCDA trees via the functions of the RXMCDA library.
#
# In this example, we extract the alternatives' IDs and the number of alternatives to be considered.
# If the optional "seed" file is loaded, we also read the value of the seed.
##########################################

if (is.null(errFile)){
	
	# files were correctly loaded and are valid according to xsd
	
	errData<-NULL
	
	flag<-TRUE
	
	numberOfAlternatives<-getNumberOfAlternatives(treeAlternatives)
	
	
	if (numberOfAlternatives$status == "OK") 
	{
		numAlt<-numberOfAlternatives[[1]]
		alternativesIDs<-getAlternativesIDs(treeAlternatives)
	}
	else {
		errData <- numberOfAlternatives$status
		flag<-FALSE
	}
	if (flag){
		if (alternativesIDs$status == "OK")
		{
			altIDs<-alternativesIDs[[1]]
		}
		else 
		{
			errData<-alternativesIDs$status
			flag<-FALSE
		} 	
	}
	
	# for the optional files we must also check whether a tree has been loaded
	
	seed<-NULL
	if (flag){
		if ((!is.null(treeSeed)))
		{
			seedParam<-getParameters(treeSeed, "seed")
			if (seedParam$status == "OK") 
			{
				seed<-seedParam[[1]]
			}
			else
			{
				errData <- seedParam$status
				flag <- FALSE
			}
		}
	}
	
##########################################
# Run the algorithm.
# If everything went right up to here, run the calculations.
#
# In this example, we generate a vector of random ranks for the alternatives.
##########################################
	
	if (is.null(errData))
	{
		# all data elements have been correctly extracted from the xml trees
		# we can proceed to the calculations now
		
		tmpErr<-try(
				{
					library(gtools)
					
					if (!is.null(seed)){
						set.seed(seed)
					}
					
					x<-1:numAlt
					
					ordre<-permute(x)
					
					altVals<-c()
					for (i in 1:numAlt)
					{
						altVals<-rbind(altVals,c(i,ordre[i]))	
					}
				}
		)
		
		if (inherits(tmpErr, 'try-error')){
			errCalc<-"Cannot generate ranks."
		} else {
			execFlag<-TRUE
		}
	}
}

##########################################
# Write the output files.
# Four possibilities appear:
#  - If everything went right with the calculation, then we write the output data into the right files: 
#    alternativesRanks.xml containing the generated ranks, and messages.xml containing an execution status.
#  - If there was an error during the calculation, we write an error message in messages.xml.
#  - If there was an error during the extraction of the data from the XMCDA trees, we write an error in messages.xml.
#  - If there was an error during the loading of the files, we write an error in messages.xml.
##########################################

if (execFlag){
	
	# execution was successfull and we can write the output files
	
	setwd(outDirectory)
	
	# first the result file
	
	outTree = newXMLDoc()
	
	newXMLNode("xmcda:XMCDA", 
			attrs=c("xsi:schemaLocation" = "http://www.decision-deck.org/2009/XMCDA-2.0.0 http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd"),
			suppressNamespaceWarning=TRUE, 
			namespace = c("xsi" = "http://www.w3.org/2001/XMLSchema-instance", "xmcda" = "http://www.decision-deck.org/2009/XMCDA-2.0.0"), 
			parent=outTree)
	
	status<-putAlternativesValues(outTree, altVals, altIDs, "alternativesRanks")
	
	status<-saveXML(outTree, file="alternativesRanks.xml")
	
	# now the messages file
	
	outTree2 = newXMLDoc()
	
	newXMLNode("xmcda:XMCDA", 
			attrs=c("xsi:schemaLocation" = "http://www.decision-deck.org/2009/XMCDA-2.0.0 http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd"),
			suppressNamespaceWarning=TRUE, 
			namespace = c("xsi" = "http://www.w3.org/2001/XMLSchema-instance", "xmcda" = "http://www.decision-deck.org/2009/XMCDA-2.0.0"), 
			parent=outTree2)
	
	status<-putLogMessage(outTree2, "OK", name = "executionStatus")
	
	status<-saveXML(outTree2, file="messages.xml")
}

if (!is.null(errCalc)){
	
	# something went wrong at the calculation step
	
	setwd(outDirectory)
	
	outTree2 = newXMLDoc()
	
	newXMLNode("xmcda:XMCDA", 
			attrs=c("xsi:schemaLocation" = "http://www.decision-deck.org/2009/XMCDA-2.0.0 http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd"),
			suppressNamespaceWarning=TRUE, 
			namespace = c("xsi" = "http://www.w3.org/2001/XMLSchema-instance", "xmcda" = "http://www.decision-deck.org/2009/XMCDA-2.0.0"), 
			parent=outTree2)
	
	status<-putErrorMessage(outTree2, errCalc, name="Error")
	
	status<-saveXML(outTree2, file="messages.xml")
	
}

if (!is.null(errData)){
	
	# something went wrong at the data extraction steps
	
	setwd(outDirectory)
	
	outTree = newXMLDoc()
	
	newXMLNode("xmcda:XMCDA", 
			attrs=c("xsi:schemaLocation" = "http://www.decision-deck.org/2009/XMCDA-2.0.0 http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd"),
			suppressNamespaceWarning=TRUE, 
			namespace = c("xsi" = "http://www.w3.org/2001/XMLSchema-instance", "xmcda" = "http://www.decision-deck.org/2009/XMCDA-2.0.0"), 
			parent=outTree)
	
	status<-putErrorMessage(outTree, errData, name = "Error")
	
	status<-saveXML(outTree, file="messages.xml")
}

if (!is.null(errFile)){
	
	# something went wrong while loading the files
	
	setwd(outDirectory)
	
	outTree = newXMLDoc()
	
	newXMLNode("xmcda:XMCDA", 
			attrs=c("xsi:schemaLocation" = "http://www.decision-deck.org/2009/XMCDA-2.0.0 http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd"),
			suppressNamespaceWarning=TRUE, 
			namespace = c("xsi" = "http://www.w3.org/2001/XMLSchema-instance", "xmcda" = "http://www.decision-deck.org/2009/XMCDA-2.0.0"), 
			parent=outTree)
	
	status<-putErrorMessage(outTree, errFile, name = "Error")
	
	status<-saveXML(outTree, file="messages.xml")
}

