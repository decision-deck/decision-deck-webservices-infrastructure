#! /bin/bash

script_absdirpath="$(cd "${0%/*}" 2>/dev/null; echo "$PWD")"

D2_ROOT=$script_absdirpath
DIVIZ_ROOT=/home/TB/projects/mcda/diviz/diviz-test-v3/descriptions

find $D2_ROOT/XMCDAWebServices \( -type f -o -type l \) -name 'description-wsDD.xml' | while read file;
do
    dir=${file%*/description-wsDD.xml}
    cd $D2_ROOT/python
    echo -n "$dir"
    $D2_ROOT/python/generate_from_description.py -o $DIVIZ_ROOT $file xml_diviz
    echo
done

echo >&2
echo "Copying specific diviz descriptions:">&2
find $D2_ROOT/XMCDAWebServices -type f -name 'description-diviz_*.xml'  | while read file;
do
    xml=${file##*/}
    # we extract the provider & service name from the description's filename
    FQservice=${xml#description-diviz_}
    provider=${FQservice%%--*}
    service=${FQservice#*--} # service--version.xml
    service=${service%.xml} # service--version
    version=${service#*--}
    service=${service%--*}
    echo -n "${file%/*}"
    cp $file $DIVIZ_ROOT/${provider}___${service}___${version}.xml
    echo
done

#cp ${DIVIZ_ROOT}/DecisionDeck___generateXMCDAReport___1.0.xml.ori ${DIVIZ_ROOT}/DecisionDeck___generateXMCDAReport___1.0.xml
