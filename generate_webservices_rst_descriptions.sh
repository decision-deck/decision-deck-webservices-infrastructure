#! /bin/bash

script_absdirpath="$(cd "${0%/*}" 2>/dev/null; echo "$PWD")"

D2_ROOT=$script_absdirpath

DD_WebRoot="/home/big/Projets/decisiondeck-web.git"

echo -n "Generating files wsd-*.rst" >&2

find $D2_ROOT/XMCDAWebServices/ -type f -name 'description-wsDD.xml'  | while read file;

do
    dir=${file%*/description-wsDD.xml}

    service=${dir##*/}

    #echo -n "." >&2 #$service"..."
    echo $service"..." >&2
    cd $D2_ROOT/python
    python ./generate_from_description.py \
      -o $DD_WebRoot/w3-ws/               \
      $dir/description-wsDD.xml           \
      doc_ws_rst

    mkdir -p $DD_WebRoot/w3-ws/files/descriptions/$service/
    cp $dir/description-wsDD.xml $DD_WebRoot/w3-ws/files/descriptions/$service/

done

echo " done" >&2
