XMCDA v2 tags order
Ordre des tags XMCDA v2

----------------------------------
Inputs / entrées :
----------------------------------

criteria
alternatives
categories
hierarchy

performanceTable

criteriaComparisons
criteriaLinearConstraints
criteriaMatrix
criteriaSets
criteriaValues

alternativesAffectations
alternativesComparisons
alternativesCriteriaValues
alternativesLinearConstraints
alternativesMatrix
alternativesSets
alternativesValues

alternativesSets
alternativesValues
categoriesComparisons
categoriesContents
categoriesLinearConstraints
categoriesMatrix
categoriesProfiles
categoriesSets
categoriesValues

methodParameters

----------------------------------
Outputs / sorties :
----------------------------------

mêmes ordres que ci-dessus / same as above, plus:

methodMessages
