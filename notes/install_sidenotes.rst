RXMCDA
------

Promethee1Ranking-RXMCDA supprimé de diviz

alternativesRankingViaQualificationDistillation-RXMCDA remplacé par ITTB

generatePerformanceTableFromBayesianNet-RXMCDA: supprimé complètement?
  à la fois dans ws_config et dans l'arbre diviz

plotAlternativesComparisons-RXMCDA remplace par ITTB

Ne sont plus à installer:

- remplacés par des ITTB:
  - alternativesRankingViaQualificationDistillation-RXMCDA
  - criteriaDescriptiveStatistics-RXMCDA
  - plotAlternativesComparisons-RXMCDA
  - plotAlternativesValues-RXMCDA
  - plotAlternativesValuesPreorder-RXMCDA
  - plotCriteriaComparisons-RXMCDA
  - plotCriteriaValues-RXMCDA
  - plotCriteriaValuesPreorder-RXMCDA
  - plotNumericPerformanceTable-RXMCDA

- Considéré comme supprimé (pas/plus dans l'arbo. diviz):
  - generatePerformanceTableFromBayesianNet-RXMCDA

Packages R
==========

biocLite
--------

à la première install de biocLite on a le message
::

  Warning: unable to access index for repository http://brainarray.mbni.med.umich.edu/bioc/src/contrib
  Old packages: 'KernSmooth', 'cluster', 'codetools', 'mgcv', 'nnet', 'rpart',
    'survival'
  Update all/some/none? [a/s/n]: 
  a

-> d'où la nécessité de faire le 'a' après la première install biocLite

compil graphviz
---------------
NB: compil graphviz::

    checking for main in -lXpm... no
    configure: WARNING: Optional Xpm library not available
    checking for X11/Xaw/Text.h... no
    configure: WARNING: Optional Xaw library not available - missing headers
    checking for zlib.h... no
    configure: WARNING: Optional z library not available - no zlib.h
    
    graphviz-2.28.0 will be compiled with the following:
    
    options:
      cgraph:        No (disabled by default - experimental)
      digcola:       Yes
      expat:         No (disabled)
      fontconfig:    No (disabled)
      freetype:      No (disabled)
      glut:          No (disabled)
      gts:           No (disabled)
      ipsepcola:     No (disabled by default - C++ portability issues)
      ltdl:          No (disabled)
      ortho:         Yes
      png:           No (disabled)
      jpeg:          No (disabled)
      sfdp:          Yes
      shared:        No (disabled)
      static:        Yes
      qt:            No (qmake not found)
    
    commands:
      dot:           Yes (always enabled)
      neato:         Yes (always enabled)
      fdp:           Yes (always enabled)
      circo:         Yes (always enabled)
      twopi:         Yes (always enabled)
      gvpr:          Yes (always enabled)
      gvmap:         Yes (always enabled)
      lefty:         No (missing Xaw headers)
      smyrna:        No (disabled by default - experimental)
      gvedit:        No (qmake not found)
    
    plugin libraries:
      dot_layout:    Yes (always enabled)
      neato_layout:  Yes (always enabled)
      core:          Yes (always enabled)
      devil:         No (disabled)
      gd:            No (disabled)
      gdiplus:       No (disabled by default - Windows only)
      gdk_pixbuf:    No (gdk_pixbuf library not available)
      ghostscript:   No (disabled)
      glitz:         No (disabled by default - incomplete)
      gtk:           No (gtk library not available)
      lasi:          No (disabled)
      ming:          No (disabled by default - incomplete)
      pangocairo:    No (disabled)
      quartz:        No (disabled by default - Mac only)
      rsvg:          No (disabled)
      visio:         No (disabled by default - experimental)
      xlib:          No (disabled or unavailable)
    
    language extensions:
      gv_sharp:      No (swig not available)
      gv_go:         No (disabled by default - experimental)
      gv_guile:      No (swig not available)
      gv_io:         No (disabled by default - no swig support yet)
      gv_java:       No (swig not available)
      gv_lua:        No (swig not available)
      gv_ocaml:      No (swig not available)
      gv_perl:       No (swig not available)
      gv_php:        No (swig not available)
      gv_python:     No (swig not available)
      gv_python23:   No (disabled by default - for multiversion installs)
      gv_python24:   No (disabled by default - for multiversion installs)
      gv_python25:   No (disabled by default - for multiversion installs)
      gv_python26:   No (disabled by default - for multiversion installs)
      gv_python27:   No (disabled by default - for multiversion installs)
      gv_R:          No (swig not available)
      gv_ruby:       No (swig not available)
      gv_tcl:        No (tcl not available)
    
      tcldot:        No (tcl not available)
      tclpathplan:   No (tcl not available)
      gdtclft:       No (tcl not available)
      tkspline:      No (tk not available)


Python
======

choix du dernier python 2.7.11

PyXMCDA
-------

par défaut sur diviz::

  > python --version
  Python 2.6.6

root@ddeck:/usr/local/src/decision-deck-webservices-infrastructure/XMCDA_WS_install/ws_config.prod# egrep -o -- 'CMD_LINE=[^ ]+' *-PyXMCDA 
CondorcetRobustnessRelation-PyXMCDA:CMD_LINE='python
ElectreTriBMInference-PyXMCDA:CMD_LINE='python
ElectreTriBMInference-PyXMCDA:CMD_LINE='/usr/local/python2.6/bin/python
RubisConcordanceRelation-PyXMCDA:CMD_LINE='python
RubisOutrankingRelation-PyXMCDA:CMD_LINE='python
csvToXMCDA-alternativesValues-PyXMCDA:CMD_LINE='python2.7
csvToXMCDA-categoriesProfiles-PyXMCDA:CMD_LINE='python2.7
csvToXMCDA-criteriaThresholds-PyXMCDA:CMD_LINE='python2.7
csvToXMCDA-criteriaValues-PyXMCDA:CMD_LINE='python2.7
csvToXMCDA-performanceTable-PyXMCDA:CMD_LINE='python2.7
csvToXMCDA-valueFunctions-PyXMCDA:CMD_LINE='python2.7
randomAlternatives-PyXMCDA:CMD_LINE='python
randomCriteria-PyXMCDA:CMD_LINE='python
randomPerformanceTable-PyXMCDA:CMD_LINE='python
stableSorting-PyXMCDA:CMD_LINE='python
thresholdsSensitivityAnalysis-PyXMCDA:CMD_LINE='python
weightedSum-PyXMCDA:CMD_LINE='python
weightsFromCondorcetAndPreferences-PyXMCDA:CMD_LINE='python


15:25:08 big@zaxxon [0] ~/tmp/sbigaret-ws-pyxmcda 
ls
CondorcetRobustWeightsAndThresholds/
CondorcetRobustnessRelation/
ElectreTriBMInference/
ElectreTriBMInference-scip/
RubisConcordanceRelation/
RubisOutrankingRelation/
csvToXMCDA-alternativesValues/
csvToXMCDA-categoriesProfiles/
csvToXMCDA-criteriaThresholds/
csvToXMCDA-criteriaValues/
csvToXMCDA-performanceTable/
csvToXMCDA-valueFunctions/
randomAlternatives/
randomCriteria/
randomPerformanceTable/
stableSorting/
thresholdsSensitivityAnalysis/
weightedSum/
weightsFromCondorcetAndPreferences/








find . -type f -name '*.py' -exec grep -h import {} \;|sort|uniq
#from itertools import product
-from mcda.types import alternatives
-from mcda.types import alternatives_affectations
-from mcda.types import categories
-from mcda.types import criteria
-from mcda.types import performance_table
-from model import leroy_linear_problem
#from optparse import OptionParser
#from xml.etree import ElementTree
-from zibopt import scip
import PyXMCDA
#import argparse, csv, sys, os
#import commands
#import getopt
+import glpk
+import lib_ampl_WAT_reverse
+import lib_ampl_reverse
#import logging
#import os
#import random
#import subprocess
#import sys
#import tempfile
+import xmcda 

ElectreTriBMInference: xmcda, glpk
CondorcetRobustWeightsAndThresholds: lib_ampl_WAT_reverse.py
ElectreTriBMInference-scip: mcda
weightsFromCondorcetAndPreferences: lib_ampl_reverse

ElectreTriBMInference-scip: à supprimer
  supprime: from zibopt import scip
            from mcda.types import alternatives
            from mcda.types import alternatives_affectations
            from mcda.types import categories
            from mcda.types import criteria
            from mcda.types import performance_table
            from model import leroy_linear_problem

reste:

- PyXMCDA: réclame lxml

- glpk qui appelle `̀glpsol`` qui doit être dans le PATH

  NB::

    diviz:/usr/local/src# glpsol --version
    GLPSOL: GLPK LP/MIP Solver, v4.47

URV
---

NB::

    diviz:/usr/local/src# java -version
    java version "1.6.0_31"
    OpenJDK Runtime Environment (IcedTea6 1.13.3) (6b31-1.13.3-1~deb6u1)
    OpenJDK 64-Bit Server VM (build 23.25-b01, mixed mode)

Avant chgt en ${Java_1_8}::

  OWA-URV:CMD_LINE='java -jar owa.jar -i $1 -o $2'
  OWAWeightsBalance-URV:CMD_LINE='/usr/local/java7/bin/java -jar OWA-WeightsBalance.jar -i $1 -o $2'
  OWAWeightsDivergence-URV:CMD_LINE='/usr/local/java7/bin/java -jar OWA-WeightsDivergence.jar -i $1 -o $2'
  OWAWeightsEntropy-URV:CMD_LINE='/usr/local/java7/bin/java -jar OWA-WeightsEntropy.jar -i $1 -o $2'
  OWAWeightsOrness-URV:CMD_LINE='/usr/local/java7/bin/java -jar OWA-WeightsOrness.jar -i $1 -o $2'
  ULOWA-URV:CMD_LINE='for f in $1/*; do sed -i -e '"'"'s|xmlns:xmcda="http://www.decision-deck.org/2009/XMCDA-2.[01].0"|xmlns:xmcda="http://www.decision-deck.org/2012/XMCDA-2.2.0"|'"'"' $f; done; java -jar ULOWA.jar -i $1 -o $2'
  defuzzificationCOG-URV:CMD_LINE='/usr/local/java7/bin/java -jar divizLingCOG.jar -i $1 -o $2'
  defuzzificationCOM-URV:CMD_LINE='/usr/local/java7/bin/java -jar divizLingCOM.jar -i $1 -o $2'
  defuzzificationOrdinal-URV:CMD_LINE='/usr/local/java7/bin/java -jar divizLingOrdinal.jar -i $1 -o $2'
  fuzzyLabelsDescriptors-URV:CMD_LINE='/usr/local/java7/bin/java -jar FuzzyLabelsDescriptors.jar -i $1 -o $2'

10 WS mais dans diviz on en a seulement 5::

      <program id="URV___OWA___1.0"/>
      <program id="URV___ULOWA___1.1"/>
      <program id="URV___fuzzyLabelsDescriptors___1.0"/>
      <program id="URV___defuzzification___1.0"/>
      <program id="URV___OWAWeightsDescriptors___1.0"/>

OWAWeightsDescriptors contient:
    - OWAWeightsBalance-URV
    - OWAWeightsDivergence-URV
    - OWAWeightsEntropy-URV
    - OWAWeightsOrness-URV

defuzzification contains:
    - defuzzificationCOG-URV
    - defuzzificationCOM-URV
    - defuzzificationOrdinal-URV


Petit problème du tar à résoudre::

  root@ddeck:/service/prod-defuzzificationCOM-URV# ls -als /home/XMCDA-services/XMCDA-prod/defuzzificationCOM-URV/divizLingCOM.jar
  3424 -rw------- 1 vasseling vasseling 3502781 Jan  3  2014 /home/XMCDA-services/XMCDA-prod/defuzzificationCOM-URV/divizLingCOM.jar

ITTB
----
On va utiliser java 8 aussi

TODO même pb que plus haut::

  root@ddeck:/service/prod-plotAlternativesComparisons-ITTB# ls -als  /home/XMCDA-services/XMCDA-prod/plotAlternativesComparisons-ITTB/plotAlternativesComparisons-ITTB.jar
  7312 -rw------- 1 vasseling vasseling 7484438 Feb 11  2013 /home/XMCDA-services/XMCDA-prod/plotAlternativesComparisons-ITTB/plotAlternativesComparisons-ITTB.jar

donc::

   @4000000056683f102bd72ec4 [ZPH1bPVaHSV6MLbC] [err] Error: Unable to access jarfile plotAlternativesComparisons-ITTB.jar

TODO

transitive reduction of criteria comparisons : WS inverse messages et résultats

J-MCDA
------
ElectreTri1GroupDisaggregationSharedProfiles
-> renamed to ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA

CMD_LINE='/usr/local/bin/xmcda_check_cplex_licence.py "$1/cplex_licence.xml" "$2" && java  -Djava.library.path=/usr/local/ibm/ILOG/CPLEX_Studio124/opl/bin/x86-64_sles10_4.1/ -classpath /usr/local/ibm/ILOG/CPLEX_Studio124/cplex/lib/cplex.jar:"`dirname "$0"`/lib/*:`dirname "$0"`/conf" org.decisiondeck.jmcda.xws.XWSExecutor -w org.decision_deck.jmcda.xws.ws.XWSSharedProfiles -i "$1" -o "$2"'


- ``csvToXMCDA-performanceTable-J-MCDA``: suppressed, replaced by PyXMCDA version
- ``cutRelation``: deprecated

- PrometheeProfiles donne des résultats identiques, mais lignes ordonnées différemment

ElectreTri1GroupDisaggregationSharedProfiles requires: cplex
la Community Edition ne suffit pas (pour l'exemple donné, ça dépasse les limites).  De plus pour le moment, de tte façon ça ne fonctionne pas. À suivre.

Avec la version academic ça fonctionne (12.6.2)

TODO le licence.xml fournit par diviz est malformé (deux xmcda)

cplex
~~~~~
::

  diviz:/usr/local/src# cplex 
  Welcome to IBM(R) ILOG(R) CPLEX(R) Interactive Optimizer 12.6.2.0
    with Simplex, Mixed Integer & Barrier Optimizers

oso
---
oblige a installer glpk en 4.47

KC-PUT
------

Liste des packages utilisés, faire sur les sources::

  egrep -ho 'library.*$'  *-PUT/*.R|sort|uniq

@40000000566a9c84268d6c44 [48HfiyiIeW640Zz1] [err] Error in png(tmp_out_file, type = "cairo") : X11 is not available

> capabilities()
       jpeg         png        tiff       tcltk         X11        aqua 
      FALSE       FALSE       FALSE       FALSE       FALSE       FALSE 
   http/ftp     sockets      libxml        fifo      cledit       iconv 
       TRUE        TRUE        TRUE        TRUE        TRUE        TRUE 
        NLS     profmem       cairo         ICU long.double     libcurl 
       TRUE       FALSE       FALSE       FALSE        TRUE       FALSE 

2 choses: png: FALSE & cairo: FALSE
apt-get install libpng12-dev
apt-get install libcairo2-dev
./configure --with-x=no --prefix=/usr/local/R-3.2.2
touch /usr/local/src/R-3.2.2/doc/NEWS.pdf

> capabilities()
       jpeg         png        tiff       tcltk         X11        aqua 
      FALSE        TRUE       FALSE       FALSE       FALSE       FALSE 
   http/ftp     sockets      libxml        fifo      cledit       iconv 
       TRUE        TRUE        TRUE        TRUE        TRUE        TRUE 
        NLS     profmem       cairo         ICU long.double     libcurl 
       TRUE       FALSE        TRUE       FALSE        TRUE       FALSE 

@40000000566aa55008b5e19c [qdXQgrDABcMGv6JD] [err] Error in if (parameters$cluster) { :
@40000000566aa55008b5e96c [qdXQgrDABcMGv6JD] [err] argument is not interpretable as logical


sur les autres::


  @40000000566aa65b366335cc [zB0ycvCrInCxF9aj] [err] Loading required package: Rglpk
  @40000000566aa65b36c40adc [zB0ycvCrInCxF9aj] [err] Loading required package: slam
  @40000000566aa65b38db84e4 [zB0ycvCrInCxF9aj] [err] Error in dyn.load(file, DLLpath = DLLpath, ...) :
  @40000000566aa65b38db8cb4 [zB0ycvCrInCxF9aj] [err] unable to load shared object '/usr/local/R-3.2.2/lib/R/library/Rglpk/libs/Rglpk.so':
  @40000000566aa65b38db909c [zB0ycvCrInCxF9aj] [err] libglpk.so.36: cannot open shared object file: No such file or directory
  @40000000566aa65b38e529a4 [zB0ycvCrInCxF9aj] [err] Error: package 'Rglpk' could not be loaded
  @40000000566aa65b38e52d8c [zB0ycvCrInCxF9aj] [err] Execution halted

à cause du LD_LIBRARY_PATH qui doit être::

    export LD_LIBRARY_PATH="/usr/local/glpk-4.57/lib"

Pb en 3.3 avec les booléens:
RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT and more I get:
Error : is.logical(strictVF) is not TRUE

En 3.0.3 il y a des dépendances introuvables

-> 3.0.2
ERROR: dependency ‘plyr’ is not available for package ‘reshape2’
* removing ‘/usr/local/R-3.0.2/lib/R/library/reshape2’
ERROR: dependency ‘plyr’ is not available for package ‘scales’
* removing ‘/usr/local/R-3.0.2/lib/R/library/scales’
ERROR: dependencies ‘plyr’, ‘reshape2’, ‘scales’ are not available for package ‘ggplot2’
* removing ‘/usr/local/R-3.0.2/lib/R/library/ggplot2’
ERROR: dependency ‘ggplot2’ is not available for package ‘rorutadis’
* removing ‘/usr/local/R-3.0.2/lib/R/library/rorutadis’

when installing rorutadis
