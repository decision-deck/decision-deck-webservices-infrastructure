# For RXMCDA & kappalab

# NB: there are probably more modules here than necessary, because the
#     original install was used by more web-services (in particular,
#     R-3.3+glpk has its own installation procedure now)

# Uses: bioconductor version 3.4
repos <- NULL
install.packages("https://bioconductor.org/packages/3.4/bioc/src/contrib/BiocGenerics_0.20.0.tar.gz", repos=repos, type="source")
install.packages("https://bioconductor.org/packages/3.4/bioc/src/contrib/BiocInstaller_1.24.0.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/KernSmooth_2.23-15.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/LowRankQP/LowRankQP_1.0.2.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/MASS/MASS_7.3-45.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/RColorBrewer_1.1-2.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/Rcpp/Rcpp_0.12.8.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/XML/XML_3.98-1.5.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/assertthat/assertthat_0.1.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/bitops_1.0-6.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/bnlearn/bnlearn_4.0.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/boot/boot_1.3-18.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/cluster/cluster_2.0.5.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/codetools/codetools_0.2-15.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/colorspace/colorspace_1.3-1.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/dichromat_2.0-0.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/digest/digest_0.6.10.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/foreign/foreign_0.8-67.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/gtable/gtable_0.2.0.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/gtools/gtools_3.5.0.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/kernlab/kernlab_0.9-25.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/labeling_0.3.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/lattice/lattice_0.20-34.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/lazyeval/lazyeval_0.2.0.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/lpSolve/lpSolve_5.6.13.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/lpSolveAPI/lpSolveAPI_5.5.2.0-17.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/magrittr_1.5.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/nnet_7.3-12.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/quadprog/quadprog_1.5-5.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/rJava/rJava_0.9-8.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/rcdd/rcdd_1.1-10.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/registry/registry_0.3.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/rpart/rpart_4.1-10.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/slam/slam_0.1-39.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/spatial_7.3-11.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/stringi/stringi_1.1.2.tar.gz", repos=repos, type="source")
install.packages("https://bioconductor.org/packages/3.4/bioc/src/contrib/Biobase_2.34.0.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/Matrix/Matrix_1.2-7.1.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/RCurl/RCurl_1.95-4.8.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/ROI/ROI_0.2-1.tar.gz", repos=repos, type="source")

# LIB
# install.packages("http://cran.r-project.org/src/contrib/Archive/Rglpk/Rglpk_0.6-2.tar.gz", repos=repos, type="source")

# ERROR Package 'XMCDA3' v1.0.0: could not find a valid URL
install.packages("http://cran.r-project.org/src/contrib/Archive/class/class_7.3-14.tar.gz", repos=repos, type="source")
install.packages("https://bioconductor.org/packages/3.4/bioc/src/contrib/graph_1.52.0.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/gridExtra/gridExtra_2.2.1.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/hitandrun/hitandrun_0.5-3.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/kappalab_0.4-7.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/munsell/munsell_0.4.3.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/nlme/nlme_3.1-128.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/plyr_1.8.4.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/stringr/stringr_1.1.0.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/tibble/tibble_1.2.tar.gz", repos=repos, type="source")

# install.packages("http://cran.r-project.org/src/contrib/Archive/ROI.plugin.glpk/ROI.plugin.glpk_0.2-1.tar.gz", repos=repos, type="source")

install.packages("http://cran.r-project.org/src/contrib/RXMCDA_1.5.5.tar.gz", repos=repos, type="source")
install.packages("https://bioconductor.org/packages/3.4/bioc/src/contrib/Rgraphviz_2.18.0.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/mgcv/mgcv_1.8-16.tar.gz", repos=repos, type="source")
install.packages("https://bioconductor.org/packages/3.4/bioc/src/contrib/pcaMethods_1.66.0.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/reshape2/reshape2_1.4.2.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/scales/scales_0.4.1.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/survival/survival_2.40-1.tar.gz", repos=repos, type="source")
# ERROR Package 'UTAR' v1.3.0: could not find a valid URL
install.packages("http://cran.r-project.org/src/contrib/Archive/ggplot2/ggplot2_2.2.0.tar.gz", repos=repos, type="source")
install.packages("http://cran.r-project.org/src/contrib/Archive/hasseDiagram/hasseDiagram_0.1.1.tar.gz", repos=repos, type="source")
# ERROR Package 'rorranking' v1.4: could not find a valid URL
# REQ glpk
install.packages("http://cran.r-project.org/src/contrib/Archive/rorutadis/rorutadis_0.4.1.tar.gz", repos=repos, type="source")

# Packages not found:
XMCDA3 1.0.0
UTAR 1.3.0
rorranking 1.4
