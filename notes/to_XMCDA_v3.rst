===================================
 Migrate from XMCDA v2 to XMCDA v3
===================================

Preamble
========

- old prod/test services:
  - are all in /usr/lib/cgi-bin
  - have their spoolers in /var/XMCDA/ (/var/XMCDA-test)
  - are installed in /home/XMCDA-services/production (/home/XMCDA-services/test)
  - have their "services" in ``/service/${service}/``

They can be left as-is
- we don't care about -test services and they can be eradicated
- we **must** keep the existing WS, at least for some time

The new prod./test services: ($ROOT_SUFFIX="prod")
  - have their cgi in /usr/lib/cgi-bin/XMCDA-${ROOT_PREFIX}
  - have their spooler in /var/XMCDA-${ROOT_SUFFIX}
  - are installed in /home/XMCDA-services/XMCDA-${ROOT_SUFFIX}
  - have their "services" in ``/service/${service}``

--> le seul clash qu'on a est sur les services,
- installation dans install_service.sh:activateService()
- désinstallation dans uninstall_service.sh:deactivateService()

devrait être: lien nommé /service/${ROOT_SUFFIX}-${SERVICE}/ p.ex.

translation
===========
Example of cmdline to translate examples::

  java -cp /home/big/Projets/XMCDA-java/xmcda.jar org.xmcda.converters.v2_2_1_v3_0.XMCDAConverter --v2 ../../tests_xmcda_v2/in1/alternatives.xml --v3 alternatives.xml

- migrate all tests from 2.x to 2.2.1::

    sed -i -e 's|2009/XMCDA-2.0.0|2012/XMCDA-2.2.1|g' *.xml
    sed -i -e 's|2009/XMCDA-2.1.0|2012/XMCDA-2.2.1|g' *.xml
    sed -i -e 's|2012/XMCDA-2.2.0|2012/XMCDA-2.2.1|g' *.xml

- conversion de v2 en v3::

    CONVERT="java -cp /home/big/Projets/XMCDA-java/xmcda.jar org.xmcda.converters.v2_2_1_v3_0.XMCDAConverter"
    ls --color=never ../../tests_xmcda_v2/in1/*.xml | while read f; do \
        echo $f; \
        $CONVERT --v2 "${f}" --v3 "${f##*/}"; \
    done



service.cmd
===========

Ex. avec ``generalWeightedSum``::

  #! /bin/bash
  CONVERT="java -cp /home/big/Projets/XMCDA-java/xmcda.jar org.xmcda.converters.v2_2_1_v3_0.XMCDAConverter"
  ls --color=never "$1" | while read f; do
    $CONVERT --v3 "$1/${f}" --v2 "$1/${f}.v2";
    mv "$1/${f}.v2" "$1/${f}"
  done
  time ( /usr/local/R-2/bin/R --slave --vanilla --args $1 $2 < $DAEMONS_ROOT/$SERVICE/generalWeightedSum.R \
    2> >(while read err; do echo "[$4] [err] $err"; done)   \
     > >(while read out; do echo "[$4] [out] $out"; done) ) \
  2> >( while read t; do echo $(date +"%s.%N")" $4 STAT $t" >> $5/$(date +%Y%m%d).stat; done )
  # NB: the last part, ">> $5/$(date ...)" is evaluated after each "read"
  # so the date is always the current one
  ret=$?
  cd "$2"
  # convert 2.x to 2.2.1
  sed -i -e 's|2009/XMCDA-2.0.0|2012/XMCDA-2.2.1|g' *.xml
  sed -i -e 's|2009/XMCDA-2.1.0|2012/XMCDA-2.2.1|g' *.xml
  sed -i -e 's|2012/XMCDA-2.2.0|2012/XMCDA-2.2.1|g' *.xml
  # convert 2.2.1 to 3.x
  ls --color=never | while read f; do
    $CONVERT --v2 "${f}" --v3 "${f}.v3"
    mv "${f}.v3" "${f}"
  done
  exit $ret


Encapsulation is made by the two elements of code surrounding ``time ... ret=$?``
They are to be applied when installing the services when XMCDA_VERSION=2 (in ws_config.xxx/svc_name).
