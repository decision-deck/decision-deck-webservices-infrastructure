===============================================================
 Objectif: faire des fichiers indépendants pour prod, test, v3
===============================================================


Les services
============
cat service_reinstall 
#! /bin/bash

. install_bin/env.sh
. install_bin/utils.sh
. install_bin/download_WS.sh
. install_bin/install_service.sh
. install_bin/uninstall_service.sh

NAME AUTHORS EMAIL VERSION SVN GIT HTTP FILES FILES_PATTERNS CMD_LINE FILES_DELIM

cat install_bin/env.sh 
----------------------
::

  # Two roots: one for production services, the other one for test
  ROOT=/var/XMCDA           | -> devient ROOT (contient daemons + JobSpooler, Problems, Solutions)
  ROOT_TEST=/var/XMCDA-test |
  DAEMONS_ROOT=$ROOT/daemons           | -> devient DAEMONS_ROOT
  DAEMONS_ROOT_TEST=$ROOT_TEST/daemons |

  # Directories where services will be installed
  WS_HOME=/home/XMCDA-services
  WS_PROD=$WS_HOME/production | -> les deux derniers deviennent WS_DIR
  WS_TEST=$WS_HOME/test       |


  # the cgi-bin directory
  CGI_BIN=/usr/lib/cgi-bin

  # needed tools
  SVN_BIN=svn
  GIT_BIN=git


-> on a comme répertoire
- ROOT=
- DAEMONS_ROOT=$ROOT/daemons
# Directories where services will be installed
WS_HOME=/home/XMCDA-services
WS_DIR=$WS_HOME/production
- cgi_bin=/usr/lib/cgi-bin
- cgi_bin_suffix=
- cgi_python_lib=  handle_XMCDA_request/handle_XMCDA_test_request
- SERVICE_DIR=/service
- SERVICE_SUFFIX=-test e.g.

En réumé:
- un répertoire pour les scripts cgi: cgi_bin
- un répertoire pour les ws (code):   WS_DIR
- un répertoire pour les daemons:     /service/$NAME


install_bin/utils.sh
--------------------
seule chose à changer: le test avec HTTP_PROD ou HTTP_TEST

install_bin/download_WS.sh
--------------------------
beaucoup de ref. à REV_PROD, HTTP_PROD, WS_PROD, IGNORE_PROD
Adresses: GIT, HTTP_PROD/TEST, SVN

install_bin/install_service.sh
------------------------------

A configurer: les environnements & commandes spécifiques.

Ex: generalWeightedSum qui nécessite un R v2.1+ (et non un R v3) parce que la lib. UTAR a besoin de LowRankQP.

Solution:
- un env/var dans le service, ex.: fichier R_2_1p contenant '/usr/local/R-2/bin/R'
- une cmdline qui dit::

    CMD_LINE='${R_2_1p} --slave --vanilla --args $1 $2 < $DAEMONS_ROOT/$SERVICE/generalWeightedSum.R'

Au départ: installXMCDA.sh
==========================

cat installXMCDA.sh::

  . install_bin/env.sh
  . install_bin/install_XMCDA_arch.sh
  installXMCDAService

Repose sur un seul env.
Il en faut plusieurs, un par environnement 'prod', 'test', etc.

-> ws_config/env.sh

divers
======
- configurer install_bin/env.sh avec $1/env.sh dans installXMCDA.sh et les scripts d'install.
  (donc inverser les deux 'sources' dans le code)
- les éléments de config PROD/TEST:
  - REV_PROD / REV_TEST
  - HTTP_PROD / HTTP_TEST

- fichiers non installés automatiquement: 
  - /usr/local/bin/xmcda_check_cplex_licence.py "$1/cplex_licence.xml"
  - /usr/lib/cgi-bin/${ROOT_SUFFIX}/handle_XMCDA_request.py

- Droits des fichiers au départ: pas root?

    genre ``chown big:big /var/XMCDA-prod`` ?

- À customiser à l'install.:
  - 


Apache2
=======
test sur e.g. http://localhost/cgi-bin/XMCDA-prod/generalWeightedSum-UTAR.py

avec::

    <VirtualHost *:80>
        ServerAdmin webmaster@localhost

        DocumentRoot /var/www
        <Directory />
                Options FollowSymLinks
                AllowOverride None
        </Directory>
        <Directory /var/www/>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride None
                Order allow,deny
                allow from all
        </Directory>

        ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
        <Directory "/usr/lib/cgi-bin">
                AllowOverride None
                Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
                Order allow,deny
                Allow from all
        </Directory>
        ...


tests
=====

Avec git: generalWeightedSum-UTAR

cd ~/Projets/DecisionDeck-Web-Services/XMCDAWebServices/generalWeightedSum-UTAR/tests
/home/big/lang/python/soap/genericXMCDAService_2.py 

cd ~/Projets/DecisionDeck-Web-Services/XMCDAWebServices/generalWeightedSum-UTAR/tests/
mkdir out
cd out
/home/big/lang/python/soap/genericXMCDAService_2.py \
   -U http://localhost/cgi-bin/XMCDA-prod/%s.py -n generalWeightedSum-UTAR -S \
   alternatives:../in1/alternatives.xml criteria:../in1/criteria.xml performanceTable:../in1/performanceTable.xml avg:../in1/avg.xml norm:../in1/norm.xml weights:../in1/weights.xml 

