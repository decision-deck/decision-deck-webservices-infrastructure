#! /usr/bin/python2.5
import sys

def header(service):
  s="""<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE program_description PUBLIC "-//Telecom Bretagne/DTD XML Praxis Program Description 3.0//EN" "http://perso.telecom-bretagne.eu/~bigaret/praxis/dtd/program_description.dtd">

<program_description export_date="2009-02-26 19:00:00" modification_date="2009-02-26 19:00:00">
  <program provider="DecisionDeck" name="%(service)s" version="1.0" />
  <from desc_id="S8888" />
  <description>%(service)s</description>
  <parameters>
    <parameter id="%(service)s" type="command" ismandatory="1">
      <name>%(service)s</name>
      <description></description>
      <position>0</position>
      <code>genericXMCDAService.py %(service)s -S </code>
    </parameter>
"""
  return s%{'service':service}

def footer():
  return """
    <parameter id="TIMEOUT" ishidden="0" type="int">
      <name>Timeout? (0 for no timeout)</name>
      <description>timeout</description>
      <position>1</position>
      <code>-t %s </code>
      <constraint>
        <description>value should be a positive or null integer</description>
        <code>(%d &gt;= 0)</code>
      </constraint>
      <vdef>60</vdef>
    </parameter>
    
    <parameter id="VERBOSE" ishidden="0" type="Excl">
      <name>Verbose mode</name>
      <description></description>
      <position>20</position>
      <vlist>
        <item id="T1">
          <description>None</description>
          <code></code>
        </item>
        <item id="T2">
          <description>Moderately verbose</description>
          <code>-v </code>
        </item>
        <item id="T3">
          <description>Very verbose</description>
          <code>-vv </code>
        </item>
        <item id="T4">
          <description>Extremely verbose (include SOAP msgs)</description>
          <code>-vvv </code>
        </item>
      </vlist>
      <vdef>T2</vdef>
    </parameter>

  </parameters>
</program_description>
"""

def param(io, mandatory, param, type, position, deps):
  io = 'input' if io=='i' else 'output'
  pid = param if io=='input' else param+'-output'
  s="""
    <parameter id="%(pid)s" ishidden="0" type="%(io)s">
      <name>%(name)s</name>
      <description>%(name)s</description>
      <position>%(position)i</position>
      %(code)s
      <types>
        <type>%(type)s</type>
      </types>
      %(vdef)s%(dep)s
    </parameter>
"""
  dep=""
  dep_param=""
  if mandatory!="m": # optional param, generate dep:
    dep='\n      <dep>(%s-dep:value="1")</dep>'%pid
    dep_param="""
    <parameter id="%(pid)s-dep" ishidden="0" type="switch">
      <name>Use %(name)s as %(io)s?</name>
      <description>Use %(name)s as %(io)s?</description>
      <position>%(position)s</position>
      <code></code>
      <vdef>1</vdef>
    </parameter>
    """%{'pid': pid, 'name':param, 'position': position+200, 'io':io}

  vdef = '<vdef/>' if io=='input' else '<vdef>%s.xml</vdef>'%param
  code = '<code>%s:%%s </code>'%param if io=='input' else '<code />'

  return ( s%{'pid':pid,
              'name':param,
              'type':type,
              'position':position,
              'io':io,
              'dep':dep,
              'vdef':vdef,
              'code':code},
           dep_param )


def main(argv=None):
  "i/o:o/m:param:type"
  if argv is None:
    argv = sys.argv

  inputs=[]
  outputs=[]
  deps=[]
  res=""
  res+=header(argv[1])
  for arg in argv[2:]:
    l=arg.split(':')
    if l[0]=='i':
      inputs.append(l)
    else:
      outputs.append(l)

  idx=100
  for i in inputs:
    res += ''.join(param(*(i+[idx,deps])))
    idx+=2

  idx=200
  for o in outputs:
    res += ''.join(param(*(o+[idx,deps])))
    idx+=2

  res+=footer()
  print res

if __name__ == "__main__":
    sys.exit(main())
