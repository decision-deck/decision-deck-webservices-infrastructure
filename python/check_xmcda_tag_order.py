#! /usr/bin/env python2.7
"""
Check that the XMCDA tags in a description are in the expected order

Usage: %s [--v2|--v3] file.xml
"""
import getopt, os, sys
from xml.etree import ElementTree as ET

from transform_description import Program

XMCDA_tags_order={
    'v2': (
        'criteria',
        'alternatives',
        'categories',
        'hierarchy',
        'performanceTable',
        'criteriaComparisons',
        'criteriaLinearConstraints',
        'criteriaMatrix',
        'criteriaSets',
        'criteriaValues',
        'alternativesAffectations',
        'alternativesComparisons',
        'alternativesCriteriaValues',
        'alternativesLinearConstraints',
        'alternativesMatrix',
        'alternativesSets',
        'alternativesValues',
        'alternativesSets',
        'alternativesValues',
        'categoriesComparisons',
        'categoriesContents',
        'categoriesLinearConstraints',
        'categoriesMatrix',
        'categoriesProfiles',
        'categoriesSets',
        'categoriesValues',
        'methodParameters',
        'other', # not a real xmcda tag
        'methodMessages',
    ),
    'v3': () # TODO
}



def check_order(program, xmcda_version):
    order = XMCDA_tags_order[xmcda_version]


    inputs_order=[]
    inputs_invalid=[]
    for input in program.inputs():
        if input.tag not in order:
            inputs_invalid.append(input)
        else:
            inputs_order.append( (order.index(input.tag), input) )

    outputs_order=[]
    outputs_invalid=[]
    for output in program.outputs():
        if output.tag not in order:
            outputs_invalid.append(output)
        else:
            outputs_order.append( (order.index(output.tag), output) )

    return [inputs_invalid, inputs_order, outputs_invalid, outputs_order]

def print_orders(fd,
                 inputs_invalid, inputs_order, outputs_invalid, outputs_order,
                 xmcda_version):
    if len(inputs_invalid)>0:
        fd.write('<h2>INVALID TAGS (inputs)<h2><ul>')
        for p in inputs_invalid:
            fd.write('<li>(id:%s name:%s tag:%s) '%(p.id, p.name, p.tag))
        fd.write('</ul>\n')
    if len(outputs_invalid)>0:
        fd.write('<h2>INVALID TAGS: (outputs)</h2><ul>')
        for p in outputs_invalid:
            fd.write('<li>(id:%s name:%s tag:%s) '%(p.id, p.name, p.tag))
        fd.write('</ul>\n')

     proposed_inputs=list(inputs_order)
    proposed_inputs.sort(lambda x,y: cmp(x[0], y[0]))
    #tags = XMCDA_tags_order[xmcda_version]
    print_diffs(fd, 'Proposed changes for inputs', inputs_order, proposed_inputs)

    proposed_outputs=list(outputs_order)
    proposed_outputs.sort(lambda x,y: cmp(x[0], y[0]))
    print_diffs(fd, 'Proposed changes for outputs', outputs_order, proposed_outputs)

def print_diffs(fd, title, current, proposed):
    if proposed == current:
        return
    fd.write('<h2>%s</h2><table border=0 cellspacing=0 cellpadding=5>\n'%title)
    fd.write('<tr><th colspan=3 align="center">Current</th><th></th><th colspan=3 align="center">Proposed</th></tr>\n')
    fd.write('<tr><th>id</th><th>name</th><th>XMCDA</th><th></th><th>id</th><th>name</th><th>XMCDA</th></tr>\n')
    row='<td>%s</td><td>%s</td><td>&lt;%s></td>'
    for idx in range(len(current)):
        p=current[idx][1]
        fd.write('<tr>')
        fd.write(row%(p.id, p.name, p.tag))
        fd.write('<td align="center">&nbsp;|&nbsp;</td>')
        p=proposed[idx][1]
        fd.write(row%(p.id, p.name, p.tag))
        fd.write('</tr>\n')
    fd.write('</table>\n')

class Usage(Exception): pass

def main(argv=None):
    xmcda_version="v2"
    if argv is None:
        argv = sys.argv
    output_dir = None
    try:
        try:
            opts, args = getopt.getopt(sys.argv[1:],
                                       "h23",
                                       ["help", "v2", "v3"
                                        ])
        except getopt.error, msg:
            raise Usage(msg)

        for opt, arg in opts:
            if opt in ("-h", "--help"):
                print __doc__
                return 0
            if opt in ('-2', '--v2'):
                xmcda_version='v2'
            if opt in ('-3', '--v3'):
                xmcda_version='v3'
                raise Usage('v3 unsupported yet')

        if len(args) != 2:
            raise Usage('Wrong number of arguments')

    except Usage, err:
        print >>sys.stderr, err.message
        print >>sys.stderr, "for help use --help"
        return 1

    description = args[0]
    dir=args[1]
    program=Program(ET.parse(description).getroot())

    f=open(os.path.join(dir, program.name+'-'+program.provider+'.html'), 'w')

    l=check_order(program, xmcda_version)
    l.append(xmcda_version)
    l.insert(0, f)

    f.write('<html><body>\n')
    f.write('<h1>%s</h1>\n'%program.name)
    print_orders(*l)
    f.write('</body></html>\n')

if __name__ == "__main__":
    sys.exit(main())
