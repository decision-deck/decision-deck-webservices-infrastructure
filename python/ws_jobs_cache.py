"""
A simple cache for web-services jobs.

"""

'''
Random/test notes

curl -i http://localhost:8080/get_ticket?hashcode=h2
curl --data 'ticket=t1' --data 'hashcode=h2' -w "\n%{http_code}" http://localhost:8080/register

http://stackoverflow.com/questions/713847/recommendations-of-python-rest-web-services-framework

'''
import web
import json
from threading import Lock

urls = (
      '/ticket', "Ticket",
      '/tickets/(.+)', 'tickets',
      '/register', "register"
    )

cache = {}

class tickets:

    # err code 202 si c'est en attente
    # https://tools.ietf.org/html/rfc7231#section-6.3.3
    def GET(self, ticket_number):
        return 'ticket number %s'%ticket_number

class Ticket:
    lock=Lock()

    def GET(self):
        i = web.input()
        hashcode = i.get('hashcode', None)
        if hashcode is None:
            raise web.BadRequest('missing argument')
        with Ticket.lock:
            ticket = cache.get(hashcode, None)
        if ticket is None:
            raise web.NotFound(repr(ticket))
        #return json.dumps({hashcode: ticket})
        return ticket

    def POST(self):
        i = web.input()
        ticket=i.get('ticket', None)
        hashcode=i.get('hashcode', None)
        if hashcode is None or ticket is None:
            raise web.BadRequest('missing argument(s)')
        with Ticket.lock:
            already_cached_ticket = cache.setdefault(hashcode, ticket)
        print already_cached_ticket, ticket, hashcode
        if already_cached_ticket == ticket:
            return ''
        return already_cached_ticket

    def DELETE(self):
        return 'DELETE'

    def PUT(self):
        return 'PUT'

class register:
    def POST(self):
        i = web.input()
        ticket=i.get('ticket', None)
        hashcode=i.get('hashcode', None)
        if hashcode is None or ticket is None:
            raise web.BadRequest('missing argument(s)')
        cache.setdefault(hashcode, [ticket])
        return "Got %s:%s"%(ticket, hashcode)

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()
