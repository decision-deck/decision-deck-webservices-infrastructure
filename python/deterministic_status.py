'''This module aims at telling if a given web-service is deterministic or not.

A module is said to be deterministic if it always returns the same
results when supplied with identical inputs.

Note that we do not take into account the rounding errors such as the
ones that may appears in floating-point arithmetic.  A webservice will
still be considered deterministic 'always' if it produces coherent,
even if slightly different, values (even on a given machine and
without changing the program, an upgrade of a library, a compiler or
interpreter etc. may introduce such variations).

There are currently four levels of determinism:
1. always
2. never
3. true, with conditions on input(s)
4. cachable

The 1st and 2nd levels are self-explanatory.  However, see level 4,
below.

The 3rd level indicates that the web-service is deterministic under
certain condition(s).  For example, some web-services give the user a
chance to supply the random seed to be applied when initializing a
random generator.

The 4th level indicates that the program is not deterministic, and
that it offers no way to make it deterministic (this includes programs
using other progs, such as linear-problems solver, which are not under
randomness happens without any control from the outside).

'''
# Possible values are:
# - True: deterministic
# - False: never deterministic
# - a function which returns True or False, depending on the inputs

class Cacheable: pass
class Unknown: pass

webservices = {
    # kappalab - Patrick Meyer
    ('kappalab', 'capacityEntropy', '1.0') : ,
    ('kappalab', 'capacityFavor', '1.0') : ,
    ('kappalab', 'capacityInteraction', '1.0') : ,
    ('kappalab', 'capacityOrness', '1.0') : ,
    ('kappalab', 'capacityShapley', '1.0') : ,
    ('kappalab', 'capacityVariance', '1.0') : ,
    ('kappalab', 'capacityVeto', '1.0') : ,
    ('kappalab', 'choquetIntegral', '1.0') : ,
    ('kappalab', 'leastSquaresCapaIdent', '1.0') : ,
    ('kappalab', 'linProgCapaIdent', '1.0') : ,
    ('kappalab', 'lsRankingCapaIdent', '1.0') : ,
    ('kappalab', 'miniVarCapaIdent', '1.0') : ,

    # RXMCDA
    ('RXMCDA', 'alternativesValuesKendall', '1.0') : ,
    ('RXMCDA', 'generateXMCDAReport', '1.0') : True,
    ('RXMCDA', 'plotAlternativesComparisons', '1.0') : True,
    ('RXMCDA', 'plotAlternativesValuesPreorder', '1.0') : True,
    ('RXMCDA', 'plotAlternativesValues', '1.0') : True,
    ('RXMCDA', 'plotCriteriaComparisons', '1.0') : True,
    ('RXMCDA', 'plotCriteriaValuesPreorder', '1.0') : True,
    ('RXMCDA', 'plotCriteriaValues', '1.0') : True,
    ('RXMCDA', 'plotNumericPerformanceTable', '1.0') : True,
    ('RXMCDA', 'rankAlternativesValues', '1.1') : ,

    ('RXMCDA', 'additiveValueFunctionsIdentification', '1.1') : ,
    ('RXMCDA', 'convertAlternativesRanksToAlternativesComparisons', '1.0') : ,
    ('RXMCDA', 'Promethee1Ranking', '1.0') : ,
    ('RXMCDA', 'randomAlternativesRanks', '1.0') : ,
    ('RXMCDA', 'randomCriteriaWeights', '1.0') : ,
    ('RXMCDA', 'randomNormalizedPerformanceTable', '1.0') : ,
    ('RXMCDA', 'invertAlternativesRanks', '1.0') : ,
    ('RXMCDA', 'plotGaiaPlane', '1.0') : ,
    ('RXMCDA', 'alternativesRankingViaQualificationDistillation', '1.0') : ,
    ('RXMCDA', 'computeAlternativesQualification', '1.0') : ,
    ('RXMCDA', 'multipleAlternativesValuesKendalls', '1.0') : ,
    ('RXMCDA', 'generatePerformanceTableFromBayesianNet', '1.0') : ,

    ('PyXMCDA', 'weightedSum', '1.0') : True,
    ('PyXMCDA', 'RubisConcordanceRelation', '1.0') : ,
    ('PyXMCDA', 'CondorcetRobustnessRelation', '1.0') : ,
    ('PyXMCDA', 'randomAlternatives', '1.0') : ,
    ('PyXMCDA', 'randomCriteria', '1.1') : ,
    ('PyXMCDA', 'randomPerformanceTable', '1.1') : ,
    ('PyXMCDA', 'ElectreTriBMInference', '1.1') : ,
    ('PyXMCDA', 'thresholdsSensitivityAnalysis', '1.0.0') : ,
    ('PyXMCDA', 'RubisOutrankingRelation', '1.1') : ,
    ('PyXMCDA', 'csvToXMCDA-alternativesValues', '1.0') : True,
    ('PyXMCDA', 'csvToXMCDA-categoriesProfiles', '1.0') : True,
    ('PyXMCDA', 'csvToXMCDA-criteriaValues', '1.2') : True,
    ('PyXMCDA', 'csvToXMCDA-criteriaThresholds', '1.2') : True,
    ('PyXMCDA', 'csvToXMCDA-performanceTable', '1.0') : True,
    ('PyXMCDA', 'csvToXMCDA-valueFunctions', '1.0') : True,
    ('PyXMCDA', 'stableSorting', '1.0') : ,

    ('jsmaa', 'smaa2', '0.2') : ,
    ('UTAR', 'ACUTA', '2.0') : ,
    ('UTAR', 'UTASTAR', '1.1') : ,
    ('UTAR', 'computeNormalisedPerformanceTable', '1.0') : ,
    ('UTAR', 'generalWeightedSum', '1.0') : ,
    ('UTAR', 'plotValueFunctions', '1.0') : True,
    ('UTAR', 'XYPlotAlternativesValues', '1.0') : True,
    ('J-MCDA', 'ElectreConcordance', '0.5.1') : ,
    ('J-MCDA', 'ElectreDiscordances', '0.5.1') : ,
    ('J-MCDA', 'ElectreOutranking', '0.5.1') : ,
    ('J-MCDA', 'cutRelation', '0.5.1') : ,
    ('J-MCDA', 'PrometheePreference', '0.5.1') : ,
    ('J-MCDA', 'PrometheeFlows', '0.5.1') : ,
    ('J-MCDA', 'PrometheeProfiles', '0.5.1') : ,
    ('J-MCDA', 'ElectreTriExploitation', '0.5.3') : ,
    ('J-MCDA', 'ElectreTri1GroupDisaggregationSharedProfiles', '0.5.3') : ,

    # URV
    ('URV', 'OWA', '1.0') : ,
    ('URV', 'ULOWA', '1.1') : ,
    ('URV', 'fuzzyLabelsDescriptors', '1.0') : ,
    ('URV', 'defuzzification', '1.0') : ,
    ('URV', 'OWAWeightsDescriptors', '1.0') : ,

    # Institut Télécom / Télécom Bretagne
    ('ITTB', 'criteriaDescriptiveStatistics', '1.0') : ,
    ('ITTB', 'plotValueFunctions', '1.1') : True,
    ('ITTB', 'plotAlternativesValues', '1.1') : True,
    ('ITTB', 'plotAlternativesValuesPreorder', '1.1') : True,
    ('ITTB', 'plotCriteriaValues', '1.1') : True,
    ('ITTB', 'plotCriteriaValuesPreorder', '1.1') : True,
    ('ITTB', 'plotAlternativesComparisons', '1.1') : True,
    ('ITTB', 'plotCriteriaComparisons', '1.0') : True,
    ('ITTB', 'plotNumericPerformanceTable', '1.1') : True,
    ('ITTB', 'transitiveReductionAlternativesComparisons', '1.0') : ,
    ('ITTB', 'transitiveReductionCriteriaComparisons', '1.0') : ,
    ('ITTB', 'cutRelation', '1.1') : ,
    ('ITTB', 'alternativesRankingViaQualificationDistillation', '1.1') : ,
    ('ITTB', 'computeNumericPerformanceTable', '1.0') : ,
    ('ITTB', 'performanceTableTransformation', '1.0') : ,
    ('ITTB', 'sortAlternativesValues', '1.0') : ,
    ('ITTB', 'UTA', '1.0') : ,
    ('ITTB', 'performanceTableFilter', '1.1') : ,
    ('ITTB', 'plotStarGraphPerformanceTable', '1.0') : True,
    ('ITTB', 'plotAlternativesAssignments', '1.0') : True,
    ('ITTB', 'plotFuzzyCategoriesValues', '1.0') : True,

    # ws-Mcc
    ('ws-Mcc', 'mccClusters', '1.0') : False,
    ('ws-Mcc', 'mccClustersRelationSummary', '1.0') : True,
    ('ws-Mcc', 'mccEvaluateClusters', '1.0') : True,
    ('ws-Mcc', 'mccPlotClusters', '1.0') : True,
    ('ws-Mcc', 'mccPreferenceRelation', '1.0') : True,

    #  LJY
    ('LJY', 'SRMPaggregation', '1.2') : ,
    ('LJY', 'SRMPdisaggregationNoInconsistency', '1.2') : ,

    #  oso
    ('oso', 'LearnMRSortMIP', '1.0') : ,
    ('oso', 'LearnMRSortMeta', '1.0') : ,

    #  TM-KC
    ('PUT', 'plotAlternativesHasseDiagram', '0.2') : True,
    ('PUT', 'RORUTADIS-ExtremeClassCardinalities', '0.1') : ,
    ('PUT', 'RORUTADIS-GroupAssignments', '0.1') : ,
    ('PUT', 'RORUTADIS-NecessaryAssignment-basedPreferenceRelation', '0.1') : ,
    ('PUT', 'RORUTADIS-PossibleAndNecessaryAssignments', '0.1') : ,
    ('PUT', 'RORUTADIS-PostFactum-InvestigatePerformanceDeterioration', '0.3') : ,
    ('PUT', 'RORUTADIS-PostFactum-InvestigatePerformanceImprovement', '0.3') : ,
    ('PUT', 'RORUTADIS-PostFactum-InvestigateValueChange', '0.3') : ,
    ('PUT', 'RORUTADIS-RepresentativeValueFunction', '0.3') : ,
    ('PUT', 'RORUTADIS-StochasticResults', '0.1') : ,
    ('PUT', 'RORUTADIS-PreferentialReducts', '0.1') : ,


    #  TM-PUT
    ('PUT', 'ElectreConcordance', '0.2.0') : ,
    ('PUT', 'ElectreConcordanceReinforcedPreference', '0.1.0') : ,
    ('PUT', 'ElectreConcordanceWithInteractions', '0.2.0') : ,
    ('PUT', 'ElectreCredibility', '0.2.0') : ,
    ('PUT', 'ElectreCredibilityWithCounterVeto', '0.1.0') : ,
    ('PUT', 'ElectreDiscordance', '0.2.0') : ,
    ('PUT', 'ElectreIVCredibility', '0.2.0') : ,
    ('PUT', 'ElectreIsDiscordanceBinary', '0.1.0') : ,
    ('PUT', 'ElectreIsFindKernel', '0.2.0') : ,
    ('PUT', 'ElectreTri-CClassAssignments', '0.2.0') : ,
    ('PUT', 'ElectreTri-rCClassAssignments', '0.2.0') : ,
    ('PUT', 'ElectreTriClassAssignments', '0.2.0') : ,
    ('PUT', 'cutRelationCrisp', '0.1.0') : ,

    #  MN-PUT
    ('PUT', 'DEACCRCrossEfficiency', '1.0') : ,
    ('PUT', 'DEACCREfficiency', '1.0') : ,
    ('PUT', 'DEACCREfficiencyBounds', '1.0') : ,
    ('PUT', 'DEACCRExtremeRanks', '1.0') : ,
    ('PUT', 'DEACCRPreferenceRelations', '1.0') : ,
    ('PUT', 'DEACCRSuperEfficiency', '1.0') : ,
    ('PUT', 'DEASMAACCREfficiencies', '1.0') : ,
    ('PUT', 'DEASMAACCRPreferenceRelations', '1.0') : ,
    ('PUT', 'DEASMAACCRRanks', '1.0') : ,
    ('PUT', 'DEASMAAvalueADDEfficiencies', '1.0') : ,
    ('PUT', 'DEASMAAvalueADDPreferenceRelations', '1.0') : ,
    ('PUT', 'DEASMAAvalueADDRanks', '1.0') : ,
    ('PUT', 'DEAvalueADDCrossEfficiency', '1.0') : ,
    ('PUT', 'DEAvalueADDEfficiency', '1.0') : ,
    ('PUT', 'DEAvalueADDEfficiencyBounds', '1.0') : ,
    ('PUT', 'DEAvalueADDExtremeRanks', '1.0') : ,
    ('PUT', 'DEAvalueADDNormalizedPerformanceMatrix', '1.0') : ,
    ('PUT', 'DEAvalueADDPreferenceRelations', '1.0') : ,
    ('PUT', 'DEAvalueADDSuperEfficiency', '1.0') : ,

    # TM-PR
    ('PUT', 'RORUTA-ExtremeRanks', '1.0') : ,
    ('PUT', 'RORUTA-ExtremeRanksHierarchical', '1.0') : ,
    ('PUT', 'RORUTA-NecessaryAndPossiblePreferenceRelations', '1.0') : ,
    ('PUT', 'RORUTA-NecessaryAndPossiblePreferenceRelationsHierarchical', '1.0') : ,
    ('PUT', 'RORUTA-PairwiseOutrankingIndices', '1.1') : ,
    ('PUT', 'RORUTA-PairwiseOutrankingIndicesHierarchical', '1.1') : ,
    ('PUT', 'RORUTA-PostFactum-PreferenceRelatedDeteriorationOrSurplusValue', '1.0') : ,
    ('PUT', 'RORUTA-PostFactum-PreferenceRelatedDeteriorationOrSurplusValueHierarchical', '1.0') : ,
    ('PUT', 'RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue', '1.0') : ,
    ('PUT', 'RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValueHierarchical', '1.0') : ,
    ('PUT', 'RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue', '1.0') : ,
    ('PUT', 'RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValueHierarchical', '1.0') : ,
    ('PUT', 'RORUTA-PostFactum-RankRelatedImprovementOrMissingValue', '1.0') : ,
    ('PUT', 'RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical', '1.0') : ,
    ('PUT', 'RORUTA-PreferentialReductsForNecessaryRelations', '1.0') : ,
    ('PUT', 'RORUTA-PreferentialReductsForNecessaryRelationsHierarchical', '1.0') : ,
    ('PUT', 'RORUTA-RankAcceptabilityIndices', '1.0') : ,
    ('PUT', 'RORUTA-RankAcceptabilityIndicesHierarchical', '1.0') : ,
    ('PUT', 'RORUTA-RankRelatedPreferentialReducts', '1.0') : ,
    ('PUT', 'RORUTA-RankRelatedPreferentialReductsHierarchical', '1.0') : ,
    ('PUT', 'RORUTA-RepresentativeValueFunction', '1.0') : ,
    ('PUT', 'RORUTA-RepresentativeValueFunctionHierarchical', '1.0') : ,

    #  MT-PUT
    ('PUT', 'ElectreComprehensiveDiscordanceIndex', '0.2.0') : ,
    ('PUT', 'ElectreCrispOutrankingAggregation', '0.2.0') : ,
    ('PUT', 'ElectreDistillation', '0.2.0') : ,
    ('PUT', 'ElectreDistillationRank', '0.1.0') : ,
    ('PUT', 'ElectreNFSOutranking', '0.2.0') : ,
    ('PUT', 'ElectreIIPreorder', '0.2.0') : ,

    #  PUT / Maciej Uniejewski
    ('PUT', 'PROMETHEE-PROMSORT_assignments', '1.0.0') : True,
    ('PUT', 'PROMETHEE-I-FlowSort_assignments', '1.0.0') : True,
    ('PUT', 'PROMETHEE-II-FlowSort_assignments', '1.0.0') : True,
    ('PUT', 'PROMETHEE-FlowSort-GDSS_assignments', '1.0.0') : True,
    ('PUT', 'plotClassAssignments-png', '1.0.0') : True,
    ('PUT', 'plotClassAssignments-LaTeX', '1.0.0') : True,
    ('PUT', 'PROMETHEE-TRI_assignments', '1.0.0') : True,

    # PUT / Sebastian Pawlak
    ('PUT', 'PROMETHEE_preference-reinforcedPreference', '1.0.0') : True,
    ('PUT', 'PROMETHEE_preference-interactions', '1.0.0') : True,
    ('PUT', 'PROMETHEE_outrankingFlows', '1.0.0') : True,
    ('PUT', 'PROMETHEE_veto', '1.0.0') : True,
    ('PUT', 'PROMETHEE_preferenceDiscordance', '1.0.0') : True,
    ('PUT', 'SRF_weights', '1.0.0') : True,
    ('PUT', 'surrogateWeights', '1.0.0') : True,
    ('PUT', 'PROMETHEE_preference', '1.0.0') : True,
    ('PUT', 'PROMETHEE_discordance', '1.0.0') : True,
    ('PUT', 'PROMETHEE_alternativesProfiles', '1.0.0') : True,

    # PUT / Magdalena Dziecielska
    ('PUT', 'PROMETHEE-II_flows', '1.0.0') : True,
    ('PUT', 'PROMETHEE-III_flows', '1.0.0') : True,
    ('PUT', 'groupClassAcceptabilities', '1.0.0') : True,
    ('PUT', 'NetFlow_scores', '1.0.0') : True,
    ('PUT', 'NetFlow-Iterative_ranking', '1.0.0') : True,
    ('PUT', 'PROMETHEE-II-GDSS_flows', '1.0.0') : True,
    ('PUT', 'PROMETHEE-I_ranking', '1.0.0') : True,

    #  PUT / Mateusz Sarbinowski
    ('PUT', 'orderedClustering', '1.0') : ,
    ('PUT', 'PROMETHEE-V', '1.0') : ,
    ('PUT', 'PROMETHEE-II_orderedClustering', '1.0') : ,
    ('PUT', 'PROMETHEE-Cluster', '1.0') : ,

    #  PUT / Anna Labijak
    ('PUT', 'ImpreciseDEACCREfficiency', '1.0') : ,
    ('PUT', 'ImpreciseDEACCRExtremeRanks', '1.0') : ,
    ('PUT', 'ImpreciseDEACCRPreferenceRelations', '1.0') : ,
    ('PUT', 'ImpreciseDEASMAACCREfficiencies', '1.0') : False,
    ('PUT', 'ImpreciseDEASMAACCRPreferenceRelations', '1.0') : False,
    ('PUT', 'ImpreciseDEASMAACCRRanks', '1.0') : False,
    ('PUT', 'ImpreciseDEASMAAValueADDEfficiency', '1.0') : False,
    ('PUT', 'ImpreciseDEASMAAValueADDPreferenceRelations', '1.0') : False,
    ('PUT', 'ImpreciseDEASMAAValueADDRanks', '1.0') : False,
    ('PUT', 'ImpreciseDEAValueADDEfficiency', '1.0') : ,
    ('PUT', 'ImpreciseDEAValueADDExtremeRanks', '1.0') : ,
    ('PUT', 'ImpreciseDEAValueADDPreferenceRelations', '1.0') : ,
}
