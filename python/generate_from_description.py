#! /usr/bin/env python3
"""
Usage:
  generate_from_description.py [options] -o <template directory> \
                               <description.xml> <template>

With 'template' being one of:
  doc_diviz_rst      web documentation for diviz programs
  doc_ws_rst         web documentation for Decision Deck web-services
  java_ws_prg        skeleton for the Java program (using XMCDA-java.jar)
  jython_ws_prg      skeleton for the Jython program (using XMCDA-java.jar)
  rjava_ws_prg       skeleton for the R program (using package XMCDA-R)
  xml_diviz          xml for diviz

Options:
  -o, --output-directory  (mandatory) The output directory. It must exist.

  -v, --verbose    Verbose mode
  -p, --preserve   If set, all files and directories will have their last
                   modified time set to the same as the description's one.
"""

import copy, getopt, sys
import os, shutil, time
import collections
from xml.etree import ElementTree as ET

xmcda_start = """<?xml version="1.0" encoding="UTF-8"?>
<xmcda xmlns="http://www.decision-deck.org/2021/XMCDA-4.0.0"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.decision-deck.org/2021/XMCDA-4.0.0 http://www.decision-deck.org/xmcda/_downloads/XMCDA-4.0.0.xsd">"""
xmcda_end = "</xmcda>"

# whether the script is verbose
verbose=False
# whether the description file stats is copied to the generated files
# NB: the permission modes of files and directories are always the same as
#     the templates' these files & directories are generated from.
preserve=False
# list of created directories; needed when 'preserve' is true
created_directories = []

from utils import prettyprint, CDATA, add_CDATA

add_CDATA()

#
def etree_Element(tag):
    element = ET.Element(tag)
    element.attrib = collections.OrderedDict()
    return element

def etree_SubElement(parent, tag):
    sub = ET.SubElement(parent, tag)
    sub.attrib = collections.OrderedDict()
    return sub

class NoSortElementTree(ET.ElementTree):
    def write(self, file_or_filename, encoding=None, xml_declaration=None, default_namespace=None, method=None):
        '''
        Disable the sorted builtin in super.write() before calling it
        '''
        builtins_sorted=sorted
        # disable sort
        ET.sorted = lambda iterable, *args, **kw: iterable
        super(NoSortElementTree, self).write(file_or_filename, encoding, xml_declaration, default_namespace, method)
        del ET.sorted

def log(msg, *args):
    global verbose
    if verbose:
        sys.stderr.write(msg % args)

def get_child_text(element, tag, type='ignored'):
    t=element.find(tag)
    ret = t.text if ( t is not None ) else None
    if t is not None and type == 'string':
        # if tag is present and it is a string but it is empty: return the empty string in that case
        ret = "" if ret is None else ret
    return ret

def set_if_not_None(element, tag, value):
    '''Sets the element's attribute to value if value is not None'''
    if value is not None:
        element.set(tag, value)

def get_program_description(program):
    return NoSortElementTree(program.to_xml())

class Program(object):
    # Attributes:
    #  provider, name, displayName, version, capitalized_name, url,
    #  documentation, parameters

    def __init__(self, tree):
        p=tree.find('program')

        self.provider      = p.get('provider')
        self.name          = p.get('name')
        self.displayName   = p.get('displayName')
        self.version       = p.get('version')
        self.url           = p.get('url')
        self.documentation = Documentation(tree.find('documentation'))

        self.capitalized_name = self.name[0].capitalize()+self.name[1:]

        self.init_parameters(tree.find('parameters'))

    def init_parameters(self, params):
        self.parameters = []

        for p in params.getchildren():
            param = Param.build(p)
            self.parameters.append(param)

    def inputs(self, includeGUI=True):
        """
        Returns all parameters that are input parameters
        """
        result = []
        inputs_type=('input',)
        if includeGUI:
            inputs_type=('input', 'input-gui')

        for p in self.parameters:
            if p.type in inputs_type:
                result.append(p)
        return result

    def inputs_gui(self):
        """
        Returns all parameters that are input parameters
        """
        result = []
        for p in self.parameters:
            if p.type == 'input-gui':
                result.append(p)
        return result

    def outputs(self):
        """
        Returns all parameters that are outputs parameters
        """
        result = []
        for p in self.parameters:
            if p.type == 'output':
                result.append(p)
        return result

    def to_xml(self):
        """
       <program provider="PyXMCDA"
                name="randomPerformanceTable"
                displayName="randomPerformanceTable"
                version="1.1"/>
       <documentation>
              <description>This web service allows to create a simple performance table by providing a list of alternatives and a list of criteria.</description>
              <contact>Thomas Veneziano (thomas.veneziano@uni.lu)</contact>
       </documentation>

       <parameters>
        """
        x_program_description = etree_Element('program_description')
        x_program = etree_SubElement(x_program_description, 'program')
        set_if_not_None(x_program, 'provider', self.provider)
        set_if_not_None(x_program, 'name', self.name)
        set_if_not_None(x_program, 'version', self.version)
        set_if_not_None(x_program, 'displayName', self.displayName)
        set_if_not_None(x_program, 'url', self.url)

        x_program_description.append(self.documentation.to_xml())

        x_params = etree_SubElement(x_program_description, 'parameters')
        for parameter in self.parameters:
            x_params.append(parameter.to_xml())

        return x_program_description

    def element_tree(self):
        return NoSortElementTree(self.to_xml())


class Documentation(object):
    """
Collects the data within ``/program_description/documentation``, example::

        <description>The description</description>
        <contact>terry.cleese (terry.cleese@parrot.com)</contact>
        <contact>john.gilliam (john.gilliam@parrot.com)</contact>
        <url>url1</url>
        <url>url2</url>
        <reference>ref1</reference>
        <reference>ref2</reference>
        <reference>ref3</reference>
        <reference>ref4</reference>
    </documentation>
"""
    # Attributes: description, contact, url, reference

    def __init__(self, xml_element):
        """
        """
        x = xml_element

        self.description = get_child_text(x, 'description')
        if self.description is not None:
            self.description = self.description.strip()
        self.contact     = [c.text for c in x.findall('contact')]
        self.url         = [u.text for u in x.findall('url')]
        self.reference   = [r.text for r in x.findall('reference')]

    def to_xml(self):
        x_desc = etree_Element('documentation')
        if self.description is not None:
            etree_SubElement(x_desc, 'description').text = self.description
        for contact in self.contact:
            etree_SubElement(x_desc, 'contact').append(CDATA(contact))
        for url in self.url:
            etree_SubElement(x_desc, 'url').text = url
        for ref in self.reference:
            etree_SubElement(x_desc, 'reference').text = ref
        return x_desc


class Param(object):
    # Attributes:
    #   id, name, displayName, optional, documentation, tag, xmcda, dependency

    def build(xml_element):
        'Factory method, returning the corresponding parameter'
        x = xml_element
        param = None
        if x.tag=='input':
            param = InputParam(x)
        elif x.tag=='output':
            param = OutputParam(x)
        else:
            raise ValueError('Invalid tag: %s'%x.tag)

        return param
    build = staticmethod(build)

    def __init__(self, xml_element):
        x = xml_element

        self.id           = x.get('id')
        self.name         = x.get('name') or self.id
        self.displayName  = x.get('displayName') or self.name
        if (self.name != self.id):
            pass #print self.id, self.name
        # optional: default is None
        # 3 possible values:
        # None: not optional (xml: no attribute or '0')
        # '1': optional, default value is True (xml: '1' or 'defaultTrue')
        #      NB: '1' is for backward-compatibility
        # '0': optional, default value is false (xml: 'defaultFalse')
        #
        _opt = x.get('isoptional')
        if _opt is None:
            _opt=''
        _opt = _opt.lower()

        self.optional = None
        if _opt=='1' or _opt.endswith('defaulttrue'):
            self.optional = '1'
        elif _opt.endswith('defaultfalse'):
            self.optional = '0'

        # now three tags: documentation, xmcda and gui (optional)
        self.documentation = Documentation(x.find('documentation'))

        # <xmcda tag=""/> (req.) + optional: <name/> & <mcdaConcept/>
        xmcda = x.find('xmcda')
        if xmcda is not None:
            self.tag = xmcda.get('tag')
            self.xmcda = xmcda.text or ''
        if xmcda is None or self.tag=='': # not an XMCDA output file
            self.tag = None
            self.xmcda = ''

        dependency  = get_child_text(x, 'dependency')
        self.dependency = None
        if dependency:
            self.dependency = Dependency(dependency)

    def xml_for_xpath(self, indent="  ", initial_indent="", full_xmcda=False):
        start, end = xml_for_xpath(self.tag, indent, initial_indent)

        tag_len = len(self.tag.split('/'))

        if self.xmcda_name:
            start=start[:-2]+'\n'
            start += initial_indent + indent*tag_len
            if self.xmcda_name_isRequired:
                start += ''#'<!-- REQUIRED  --> '
            else:
                start += ""#'<!-- suggested --> '

            start += 'name="%s"'%self.xmcda_name
            start += '>\n'

        if self.xmcda_concept:
            start=start[:-2]+'\n'
            start += initial_indent + indent*tag_len
            if self.xmcda_concept_isRequired:
                start += ""#'<!-- REQUIRED  --> '
            else:
                start += ''#'<!-- suggested --> '

            start += 'mcdaConcept="%s"'%self.xmcda_concept
            start += '>\n'

        start2, end2 = \
            xml_for_xpath(self.xpath,
                          indent=indent,
                          initial_indent=initial_indent+indent*(tag_len+1))

        if self.xml is None:
            xml=""
        else:
            xml=self.xml
        xml = indent+xml+'\n'
        if full_xmcda:
            return xmcda_start+start+start2[:-1], end2.strip()+end+xmcda_end;
        return start+start2,end2+end;

def xml_for_xpath(xpath, indent="  ", initial_indent=""):
    if not xpath:
        return "",""
    start=end=""
    tags = xpath.split('/')
    idx_tags = map(None, range(len(tags)), tags)
    for idx,tag in idx_tags:
        start+=initial_indent + indent*idx + '<%s>\n'%tag

    idx_tags.reverse()
    for idx,tag in idx_tags:
        end+=initial_indent + indent*idx + '</%s>\n'%tag

    return start, end;


class IOParam(Param):
    """
    Superclass for InputParam and OutputParam
    """
    def __init__(self, xml_element):
        super(IOParam, self).__init__(xml_element)
        self.filename = self.name + '.xml' # XXX


class InputParam(IOParam):
    def __init__(self, xml_element):
        super(InputParam, self).__init__(xml_element)

        self.gui = None
        gui = xml_element.find('gui')
        if gui is not None:
            self.gui  = GUIParam(gui)
            self.type = 'input-gui'
        else:
            self.type = 'input'

    def get_entry(self, id):
        if not self.gui:
            return None
        for e in self.gui.entries:
            if e.id==id:
                return e
        return None

    def isProgramParameter(self):
        "Tells if it corresponds to a programParameters XMCDA v3 tag"
        # resp. (v3, v2)
        return self.tag in ('programParameters', 'methodParameters')

    def to_xml(self):
        x_input = etree_Element('input')
        set_if_not_None(x_input, 'id', self.id)
        set_if_not_None(x_input, 'name', self.name)
        set_if_not_None(x_input, 'displayName', self.displayName)
        if self.optional is None:
            set_if_not_None(x_input, 'isoptional', '0')
        elif self.optional == '1':
            set_if_not_None(x_input, 'isoptional', 'defaultTrue')
        elif self.optional == '0':
            set_if_not_None(x_input, 'isoptional', 'defaultFalse')

        x_input.append(self.documentation.to_xml())

        xmcda = etree_Element('xmcda')
        set_if_not_None(xmcda, 'tag', self.tag)

        if self.xmcda:
            xmcda.append(CDATA(self.xmcda))
        x_input.append(xmcda)

        if self.gui is not None:
            x_input.append(self.gui.to_xml())

        return x_input


class OutputParam(IOParam):
    def __init__(self, xml_element):
        super(OutputParam, self).__init__(xml_element)
        self.type = 'output'

    def isProgramExecutionResult(self):
        "Tells whether it corresponds to a XMCDA v3 programExecutionResult tag"
        # resp. (v3, v2)
        return self.tag in ('programExecutionResult', 'methodMessages')

    def to_xml(self):
        x_output = etree_Element('output')
        set_if_not_None(x_output, 'id', self.id)
        set_if_not_None(x_output, 'name', self.name)
        set_if_not_None(x_output, 'displayName', self.displayName)

        x_output.append(self.documentation.to_xml())

        xmcda = etree_Element('xmcda')
        set_if_not_None(xmcda, 'tag', self.tag)
        if self.xmcda:
            xmcda.append(CDATA(self.xmcda))
        x_output.append(xmcda)

        return x_output


class Constraint(object):
    """
    Defines a constraint on a parameter's value
    (example: non-null positive integer)
    """
    # Attributes: description, code

    def __init__(self, description, code):
        self.description=description
        self.code = code

    def to_xml(self):
        x_constraint = etree_Element('constraint')
        if self.description is not None:
            etree_SubElement(x_constraint, 'description').text = self.description
        if self.code is not None:
            etree_SubElement(x_constraint, 'code').text = self.code

        return x_constraint

class Dependency(object):
    '''
    '''
    # Attributes: condition
    def __init__(self, condition):
        self.condition = condition
    def __str__(self):
        return self.condition #.replace('%', 'ph')

class GUIParam(object):
    """
    For input parameters' tag ``<gui/>``
    """
    # Attributes: entries, status

    def __init__(self, xml_element):
        #super(GUI, self).__init__(xml_element)

        x = xml_element
        self.entries = []
        self.status = x.get('status')
        if self.status not in ('alwaysGUI', 'preferGUI', 'preferFile'):
            raise ValueError('Invalid status for gui: %s'%self.status)

        for e in x.findall('entry'):
            self.entries.append(Entry.entry(e))

    def to_xml(self):
        x_gui = etree_Element('gui')
        set_if_not_None(x_gui, 'status', self.status)
        for entry in self.entries:
            x_gui.append( entry.to_xml() )
        return x_gui


class Entry(object):
    '''
    Represents an ``<entry/>`` in a GUIParam
    '''
    # Attributes:
    #  id, type, displayName
    # children:
    #   documentation, xmcda, constraint, items, defaultValue, dependency

    def entry(xml_element):
        'Factory method building and returning an entry'
        if xml_element.get('type')=='enum' and xml_element.find('items') is None:
            raise ValueError('Invalid entry: type enum with no <items>')
        if xml_element.get('type')!='enum' and xml_element.find('items') is not None:
            raise ValueError('Invalid entry: found <items> in non enum entry')
        if xml_element.find('items') is not None:
            return EnumEntry(xml_element)
        else:
            return Entry(xml_element)
    entry = staticmethod(entry)

    def __init__(self, xml_element):
        x = xml_element
        self.id   = x.get('id')
        self.type = x.get('type')
        # TODO deprecate 'float', replace with 'real'
        if self.type not in ('string', 'int', 'float', 'boolean', 'enum'):
            raise ValueError('Invalid type %s for entry w/ id: %s'%(self.type,self.id))
        self.displayName  = x.get('displayName')
        _doc = x.find('documentation')
        if _doc != None:
            self.documentation = Documentation(_doc)
        else:
            self.documentation = None
        self.xmcda = get_child_text(x, 'xmcda')
        self.defaultValue = get_child_text(x, 'defaultValue', self.type) # TODO check vdef wrt type

        self.constraint = None
        constraint = x.find('constraint')
        if constraint is not None:
            c = Constraint(get_child_text(constraint, 'description'),
                           get_child_text(constraint, 'code'))
            self.constraint = c
        dependency = x.find('dependency')
        if dependency is not None:
            self.dependency = Dependency(dependency.text)
        else:
            self.dependency = None

    def to_xml(self):
        x_entry = etree_Element('entry')
        set_if_not_None(x_entry, 'id', self.id)
        set_if_not_None(x_entry, 'type', self.type)
        set_if_not_None(x_entry, 'displayName', self.displayName)

        if self.documentation is not None:
            x_entry.append(self.documentation.to_xml())

        if self.xmcda is not None:
            x_entry.append(CDATA(self.xmcda))

        if self.constraint is not None:
            x_entry.append(self.constraint.to_xml())

        if hasattr(self, 'items') and self.items is not None:
            x_items = etree_SubElement(x_entry, 'items')
            for item in self.items:
                x_items.append(item.to_xml())

        if self.defaultValue is not None:
            dfl_value = etree_Element('defaultValue')
            dfl_value.text = self.defaultValue
            x_entry.append(dfl_value)

        if self.dependency is not None:
            x_dependency = etree_Element('dependency')
            x_dependency.text = self.dependency
            x_entry.append(x_dependency)
        return x_entry

class Item(object):
    """
    An enumeration item
    """
    def __init__(self, xml_element):
        self.id = xml_element.get('id')
        self.description = get_child_text(xml_element, 'description')
        self.value = get_child_text(xml_element, 'value')

    def to_xml(self):
        x_item = etree_Element('item')
        set_if_not_None(x_item, 'id', self.id)

        if self.description is not None:
            etree_SubElement(x_item, 'description').text = self.description

        if self.value is not None:
            etree_SubElement(x_item, 'value').text = self.value

        return x_item


class EnumEntry(Entry):
    # Attributes: items, defaultValue

    def __init__(self, xml_element):
        super(EnumEntry, self).__init__(xml_element)

        self.init_items(xml_element.find('items'))

        if self.defaultValue is None or self.defaultValue=="":
            self.defaultValue=self.items[0].id

        ### TODO check default value

    def init_items(self, items):
        """
        Builds the list of items
        """
        self.items = []
        for item in items.findall('item'):
            self.items.append(Item(item))


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def is_a_template(filename):
    return '.tmpl.' in filename or filename.endswith('.tmpl')


def filename_for_template(tmpl):
    if '.tmpl.' in tmpl:
        return '.'.join(tmpl.split('.tmpl.'))
    elif tmpl.endswith('.tmpl'):
        return tmpl[:-5] # remove trailing '.tmpl'
    else:
        raise ValueError('Argument %s is not a template'%tmpl)


def visit(args, dirname, names):
    """
    """
    global created_directories, preserve, verbose
    from stat import S_ISDIR
    from mako.template import Template
    from mako import exceptions

    # the walk is initiated from templates/<tmpl_name>/, remove it from
    # dirname
    dst_dirname = dirname[len(args['source_dir']):]
    if len(dst_dirname)>0 and dst_dirname[0]=='/':
        dst_dirname = dst_dirname[1:]
    dst_dirname = os.path.join(args['output_dir'], dst_dirname)

    args = copy.copy(args) # copy it, it should not change between calls
    description_arg = args.pop('description_arg')
    template_dir = args['source_dir']

    for name in names:
        if name.endswith('~') or name.endswith('.tmpl_utils'):
            continue
        src_filepath = os.path.join(dirname, name)
        dst_filepath = os.path.join(dst_dirname, name)

        log("Generating file: %s\n", src_filepath)

        if '{' in dst_filepath and '}' in dst_filepath:
            # substitute the name by rendering it as a template
            try:
                dst_filepath=Template(dst_filepath).render_unicode(program=args['program'])
            except:
                print(exceptions.text_error_template().render())

        if os.path.isdir(src_filepath):
            os.makedirs(dst_filepath)
            if preserve:
                shutil.copystat(description_arg, dst_filepath)
                created_directories.append(dst_filepath)
            shutil.copymode(src_filepath, dst_filepath)

            continue
        if not is_a_template(name) :
            shutil.copyfile(src_filepath, dst_filepath)
            shutil.copymode(src_filepath, dst_filepath)
            if preserve:
                shutil.copystat(description_arg, dst_filepath)
            continue

        dst_filepath = filename_for_template(dst_filepath)
        # we have a template to render
        dst_fd = open(dst_filepath, 'w')

        from mako.lookup import TemplateLookup

        lookup = TemplateLookup(directories=[template_dir],
                                input_encoding='utf-8',
                                #output_encoding='utf-8',
                                filesystem_checks=True)

        tmpl_filepath = src_filepath[len(template_dir):]
        tmpl = lookup.get_template(tmpl_filepath)

        try:
            dst_fd.write(tmpl.render_unicode(**args))
        except:
            print(exceptions.text_error_template().render())
        dst_fd.close()
        shutil.copymode(src_filepath, dst_filepath)
        if preserve:
            shutil.copystat(description_arg, dst_filepath)


def main(argv=None):
    global created_directories
    global verbose
    global preserve

    if argv is None:
        argv = sys.argv
    output_dir = None
    try:
        try:
            opts, args = getopt.getopt(sys.argv[1:],
                                       "ho:vp",
                                       ["help","output-directory=","verbose",
                                        "preserve"
                                        ])
        except getopt.error as msg:
            raise Usage(msg)

        for opt, arg in opts:
            if opt in ("-h", "--help"):
                print(__doc__)
                return 0
            if opt in ("-o", "--output-directory"):
                output_dir = arg
            elif opt in ("-v", "--verbose"):
                verbose = True
            elif opt in ("-p", "--preserve"):
                preserve = True

    except Usage as err:
        print(err.msg, sys.stderr)
        print("Try -h or --help for more information.", sys.stderr)
        return 1

    if output_dir is None:
        print("No output directory", sys.stderr)
        return 1

    mtime=time.gmtime(os.path.getmtime(args[0]))
    tree=ET.parse(args[0])
    root=tree.getroot()
    program=Program(root)

    # this is used in templates used to generate the web site, to generate
    # links to the description itself
    description_file = args[0].split('/')[-2:]
    description_file='files/descriptions/'+'/'.join(description_file)

    script_dir = os.path.realpath(sys.path[0])
    input_templates_dir = os.path.join(script_dir, 'templates', args[1])

    if not os.path.isdir(input_templates_dir):
        print("Couldn't find template '%s'" % args[1], stderr)
        return 1

    args_dict = {
        'description_arg': args[0],
        'program': program,
        'description_last_modified_time': mtime,
        'source_dir': input_templates_dir,
        'output_dir': output_dir,
        'description_file': description_file,
    }

    for dirpath, dirnames, filenames in os.walk(input_templates_dir, visit, args_dict):
        visit(args_dict, dirpath, filenames)

    for d in created_directories:
        # from /usr/lib/python2.7/shutil.py / copystat()
        st = os.stat(args[0])
        if hasattr(os, 'utime'):
            os.utime(d, (st.st_atime, st.st_mtime))

    DESC_FILE = 'description-wsDD.xml'

    _copy = shutil.copy
    if preserve:
        _copy = shutil.copy2
    if os.path.exists(os.path.join(input_templates_dir, DESC_FILE)):
        _copy(args[0], os.path.join(output_dir, DESC_FILE))

    return 0


if __name__ == "__main__":
    sys.exit(main())
