<%!
from utils import camel_to_snake_case
%>\
"""

"""
from org.xmcda import XMCDA

# TODO depending on whether the file was generated from a description based on
# XMCDA v2 or v3, only one dictionary is correct, either
# XMCDA_v2_TAG_FOR_FILENAME or XMCDA_v3_TAG_FOR_FILENAME: check them to
# determine which one should be adapted.

XMCDA_v2_TAG_FOR_FILENAME = { # TODO check
    # output name -> XMCDA v2 tag
% for output in program.outputs():
%   if output.isProgramExecutionResult():
    '${output.name}': 'methodMessages',
%   else:
    '${output.name}': '${output.tag}',
%   endif
% endfor
}

XMCDA_v3_TAG_FOR_FILENAME = { # TODO check
    # output name -> XMCDA v3 tag
% for output in program.outputs():
%   if output.isProgramExecutionResult():
    '${output.name}': 'programExecutionResult',
%   else:
    '${output.name}': '${output.tag}',
%   endif
% endfor
}


def xmcda_v3_tag(outputFilename):
    return XMCDA_v3_TAG_FOR_FILENAME[outputFilename]


def xmcda_v2_tag(outputFilename):
    return XMCDA_v2_TAG_FOR_FILENAME[outputFilename]


def convert(results, xmcda_execution_results): # TODO
    """
    Converts the outputs of the computation to XMCDA objects

    :param results:
    :param ...:
    :param xmcda_execution_results:
    :return: a map with keys being the names of the outputs, and their corresponding XMCDA v3 values (NOT including 'messages.xml')
    """
    # translate the results into XMCDA v3
% for output in program.outputs():
% if output.id=="messages":
<% continue %>
% endif
    xmcda_${camel_to_snake_case(output.name)} = XMCDA() # for XMCDA tag: ${output.tag}
    # ...

% endfor

    return {
% for output in program.outputs():
% if output.id=="messages":
<% continue %>
% endif
        '${output.name}': xmcda_${camel_to_snake_case(output.name)},
% endfor
        }
