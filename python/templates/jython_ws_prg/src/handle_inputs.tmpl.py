from java.lang import Throwable

import os
import utils
import org.xmcda.XMCDA


class Inputs:
    """
    Class Inputs gathers all the information retrieved from the XMCDA inputs
    """
    def __init__(self):
        pass # TODO -- related to check_and_extract_inputs()


def check_and_extract_inputs(xmcda, xmcda_exec_results): # TODO
    """

    :param xmcda:
    :param xmcda_exec_results:
    :return:
    """
    inputs = Inputs()
    check_inputs(inputs, xmcda, xmcda_exec_results)

    if not (xmcda_exec_results.isOk() or xmcda_exec_results.isWarning()):
        return None

    return extract_inputs(inputs, xmcda, xmcda_exec_results)


def check_inputs(inputs, xmcda, xmcda_exec_results): # TODO

    # ... check XMCDA inputs

    # Check as much things as possible before signalling an error
    # so that the user gets the maximum nb of errors we can detect

    # # example:
    # if some_error_condition:
    #     xmcda_exec_results.addError("An error message")

    if not (xmcda_exec_results.isOk() or xmcda_exec_results.isWarning()):
        return None

    return inputs

def extract_inputs(inputs, xmcda, xmcda_execution_results): # TODO
    "transform XMCDA inputs into what we need for the algorithm"

    # ...

    # we may encounter errors here as well, in which case, deal as above
    # and try to collect more errors before exiting.

    return inputs
