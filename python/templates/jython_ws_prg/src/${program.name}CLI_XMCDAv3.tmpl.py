<%!
from utils import camel_to_snake_case
%>\
"""
"""

import getopt
import os
import sys

import org.xmcda.ProgramExecutionResult
import org.xmcda.XMCDA
from java.lang import Throwable

import utils
import handle_inputs, handle_outputs
import ${program.name}


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def main(argv=None):
    input_dir = output_dir = None
    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "i:o:h",
                                       ['in=', 'out=', 'help'])
            for opt, arg in opts:
                if opt in ("-h", "--help"):
                    print __doc__
                    sys.exit(0)
                elif opt in ('-i', '--in'):
                    input_dir = arg
                elif opt in ('-o', '--out'):
                    output_dir = arg
        except getopt.error, msg:
            raise Usage(msg)
    except Usage, err:
        print >> sys.stderr, err.msg
        print >> sys.stderr, "for help use --help"
        return 2

    if None in (input_dir, output_dir):
        print >> sys.stderr, "Error: both parameters input and output must be supplied"
        print >> sys.stderr, "for help use --help"
        return 2

    prg_exec_results = os.path.join(output_dir, "messages.xml")

    x_execution_results = org.xmcda.ProgramExecutionResult()

    # this object is where XMCDA items will be put into.
    xmcda = org.xmcda.XMCDA()

## Let's handle inputs
% for input in program.inputs():
<% input.camel_name = camel_to_snake_case(input.name)%>\
% if input.optional is None:
%   if input.isProgramParameter(): # it is mandatory, always
    utils.load_XMCDA_v3(xmcda, os.path.join(input_dir, "${input.name}.xml"),
                        True, x_execution_results, "programParameters")
%   else:
    utils.load_XMCDA_v3(xmcda, os.path.join(input_dir, "${input.name}.xml"),
                        True, x_execution_results, "${input.tag}")
%   endif
% else:
    utils.load_XMCDA_v3(xmcda, os.path.join(input_dir, "${input.name}.xml"),
                        False, x_execution_results, "${input.tag}")
% endif
% endfor

    # We have problems with the inputs, it is time to stop
    if not x_execution_results.isOk() and not x_execution_results.isWarning():
        return utils.write_program_execution_results(prg_exec_results, x_execution_results, utils.XMCDA_VERSION_v3)

    # Let's check the inputs and convert them into our own structures
    inputs = handle_inputs.check_and_extract_inputs(xmcda, x_execution_results)

    if not ( x_execution_results.isOk() or x_execution_results.isWarning() ):
        return utils.write_program_execution_results(prg_exec_results, x_execution_results, utils.XMCDA_VERSION_v3)

    # Here we know that everything was loaded as expected

    # Now let's call the calculation method
    try:
        results = ${program.name}.calculate_${camel_to_snake_case(program.name)}(inputs)
    except Throwable as t:
        x_execution_results.addError(utils.get_message("The calculation could not be performed, reason: ", t))
        return utils.write_program_execution_results(prg_exec_results, x_execution_results, utils.XMCDA_VERSION_v3)

    # Fine, now let's put the results into XMCDA structures
    x_results = handle_outputs.convert(results, x_execution_results)

    # and last, write them onto the disk
    from org.xmcda.parsers.xml.xmcda_v3 import XMCDAParser
    parser = XMCDAParser()
    for output_name, x_result in x_results.items():
        output_filename = os.path.join(output_dir, "%s.xml" % output_name)
        try:
            parser.writeXMCDA(x_result, output_filename,
                              handle_outputs.xmcda_v3_tag(output_name))
        except Throwable as t:
            x_execution_results.addError(utils.get_message("Error while writing %s.xml, reason: " % output_name, t))
            # Whatever the error is, clean up the file: we do not want to leave an empty or partially-written file */
            os.remove(output_filename)

    # Let's write the file 'messages.xml' as well
    return utils.write_program_execution_results(prg_exec_results, x_execution_results, utils.XMCDA_VERSION_v3)


if __name__ == "__main__":
    sys.exit(main())
