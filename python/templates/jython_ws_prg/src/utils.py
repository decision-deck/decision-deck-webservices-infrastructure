import os, sys

from java.lang import Throwable
from org.xmcda.converters.v2_v3 import XMCDAConverter
from org.xmcda.parsers.xml import xmcda_v3
from org.xmcda.parsers.xml.xmcda_v2 import XMCDAParser

import org.xmcda
from org.xmcda import XMCDA

XMCDA_VERSION_v2 = 2
XMCDA_VERSION_v3 = 3

def read_xmcda_v2_file_and_update(xmcda_v2, parser, filename, load_tags=()):
    new_xmcda = parser.readXMCDA(filename, load_tags).getProjectReferenceOrMethodMessagesOrMethodParameters()
    xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters().addAll(new_xmcda)


def load_XMCDA_v3(xmcda, filename, mandatory, x_execution_results, load_tags=()):
    from org.xmcda.parsers.xml.xmcda_v3 import XMCDAParser
    parser = XMCDAParser()
    basefilename = os.path.basename(filename)

    if not os.path.exists(filename):
        if mandatory:
            x_execution_results.addError("Could not find the mandatory file %s" % basefilename)
            return
        else:
            # x_execution_results.addInfo("Optional file %s absent" % basefilename)
            return
    try:
        parser.readXMCDA(xmcda, filename, load_tags)
    except Throwable as exc:
        x_execution_results.addError(get_message("Unable to read & parse the file %s, reason: " % basefilename, exc))


def load_XMCDA_v2(xmcda_v2, filename, mandatory, x_execution_results, load_tags=()):
    from org.xmcda.parsers.xml.xmcda_v2 import XMCDAParser
    parser = XMCDAParser()
    basefilename = os.path.basename(filename)
    if not os.path.exists(filename):
        if mandatory:
            x_execution_results.addError("Could not find the mandatory file %s" % basefilename)
        return

    try:
        read_xmcda_v2_file_and_update(xmcda_v2, parser, filename, load_tags)
    except Throwable as t:
        x_execution_results.addError(get_message("Unable to read & parse the file %s, reason: " % basefilename, t))


def get_throwable_message(throwable):
    """
    Extracts the message from a java object Throwable, or "<unkn<on> if there is no such message
    :param throwable: a java.lang.Throwable object
    :return: the message extracted from the throwable object, or "<unknown>" if it does not exist.
    """
    if throwable.getMessage() is not None:
        return throwable.getMessage()
    # when handling XMCDA v2 files, errors may be embedded in a JAXBException
    if throwable.getCause() is not None and throwable.getCause().getMessage() is not None:
        return throwable.getCause().getMessage()
    return "unknown"


def get_message(message, throwable):
    """
    Shortcut for message + get_throwable_message(throwable).

    :param message: the message describing the error
    :param throwable: the exception raised on the java-side
    :return: a String with both the message and the reason stored in throwable
    """
    return message + get_throwable_message(throwable)


def write_program_execution_results(prg_exec_results_file, errors, xmcda_version):
    """
    Writes the XMCDA file containing the information provided to build the XMCDA tag "{@code programExecutionResult}"
    in XMCDA v3, or "{@code methodMessages}" in XMCDA v2.x.

    :param prgExecResultsFile: the file to write
    :param status: the status of the execution
    :param errors: an array of Strings; {@code null} values are ignored
    :param xmcdaVersion: indicates which {@link XMCDA_VERSION} to use when writing the file

    :return: errors.getStatus().getExitStatus(), or writes a message to stderr
             and returns org.xmcda.ProgramExecutionResult.Status.ERROR in
             case of failure when writing the execution results.
    """
    parser = xmcda_v3.XMCDAParser()
    prg_exec_results = XMCDA()
    prg_exec_results.programExecutionResultsList.add(errors)

    try:
        if xmcda_version == XMCDA_VERSION_v3:
            parser.writeXMCDA(prg_exec_results, prg_exec_results_file, 'programExecutionResult')

        elif xmcda_version == XMCDA_VERSION_v2:
            xmcda_v2 = XMCDAConverter.convertTo_v2(prg_exec_results)
            XMCDAParser.writeXMCDA(xmcda_v2, prg_exec_results_file, 'methodMessages')

        else:
            # just in case the enum has some more values in the future and the new cases has not been added TODO change comment
            raise RuntimeError("Unhandled XMCDA version " + xmcda_version.toString())
    except Throwable as exc:
        # Last resort, print something on the stderr and exit
        # We choose here not to clean up the file, in case some valuable information were successfully written
        # before a throwable is raised.
        print >> sys.stderr, get_message("Could not write messages.xml, reason: ", exc)
        return org.xmcda.ProgramExecutionResult.Status.ERROR.exitStatus()

    return errors.getStatus().exitStatus()
