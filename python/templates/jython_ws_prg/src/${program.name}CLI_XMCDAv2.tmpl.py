# coding=utf-8
<%!
from utils import camel_to_snake_case
%>\
"""
"""
import os
import sys
import getopt

import utils
import handle_inputs, handle_outputs
import ${program.name}

import org.xmcda

from java.lang import Throwable


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def main(argv=None):
    input_dir = output_dir = None
    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "i:o:h",
                                       ['in=', 'out=', 'help'])
            for opt, arg in opts:
                if opt in ("-h", "--help"):
                    print __doc__
                    sys.exit(0)
                elif opt in ('-i', '--in'):
                    input_dir = arg
                elif opt in ('-o', '--out'):
                    output_dir = arg
        except getopt.error, msg:
            raise Usage(msg)
    except Usage, err:
        print >> sys.stderr, err.msg
        print >> sys.stderr, "for help use --help"
        return 2

    if None in (input_dir, output_dir):
        print >> sys.stderr, "Error: both parameters input and output must be supplied"
        print >> sys.stderr, "for help use --help"
        return 2

    prg_exec_results = os.path.join(output_dir, "messages.xml")

    x_execution_results = org.xmcda.ProgramExecutionResult()

    # this object is where XMCDA items will be put into.
    xmcda_v3 = org.xmcda.XMCDA()

    parser = org.xmcda.parsers.xml.xmcda_v2.XMCDAParser()
    xmcda_v2 = org.xmcda.v2.XMCDA()

    # Load XMCDA v2.2.1 inputs
% for input in program.inputs():
<% input.camel_name = camel_to_snake_case(input.name)%>\
% if input.optional is None:
%   if input.isProgramParameter(): # it is mandatory, always
    utils.load_XMCDA_v2(xmcda_v2, os.path.join(input_dir, "${input.name}.xml"),
                        True, x_execution_results, "methodParameters")
%   else:
    utils.load_XMCDA_v2(xmcda_v2, os.path.join(input_dir, "${input.name}.xml"),
                        True, x_execution_results, "${input.tag}")
%   endif
% else:
    utils.load_XMCDA_v2(xmcda_v2, os.path.join(input_dir, "${input.name}.xml"),
                        False, x_execution_results, "${input.tag}")
% endif
% endfor

    # We have problems with the inputs, it is time to stop
    if x_execution_results.getStatus() not in (org.xmcda.ProgramExecutionResult.Status.OK,
                                               org.xmcda.ProgramExecutionResult.Status.WARNING):
        return utils.write_program_execution_results(prg_exec_results, x_execution_results, utils.XMCDA_VERSION_v2)

    # Convert them to XMCDA v3
    try:
        xmcda_v3 = org.xmcda.converters.v2_v3.XMCDAConverter.convertTo_v3(xmcda_v2)
    except Throwable as t:
        x_execution_results.addError(utils.get_message("Could not convert inputs to XMCDA v3, reason: ", t))
        return utils.write_program_execution_results(prg_exec_results, x_execution_results, utils.XMCDA_VERSION_v2)

    # Let's check the inputs and convert them into our own structures
    inputs = handle_inputs.check_and_extract_inputs(xmcda_v3, x_execution_results)

    if not ( x_execution_results.isOk() or x_execution_results.isWarning() ):
        return utils.write_program_execution_results(prg_exec_results, x_execution_results, utils.XMCDA_VERSION_v2)

    # Here we know that everything was loaded as expected

    # Now let's call the calculation method
    try:
        results = ${program.name}.calculate_${camel_to_snake_case(program.name)}(inputs)
    except Throwable as t:
        x_execution_results.addError(utils.get_message("The calculation could not be performed, reason: ", t))
        return utils.write_program_execution_results(prg_exec_results, x_execution_results, utils.XMCDA_VERSION_v2)

    # Fine, now let's put the results into XMCDA v3 structures
    x_results_v3 = handle_outputs.convert(results, x_execution_results)

    for output_name, x_result_v3 in x_results_v3.items():
        output_filename = os.path.join(output_dir, "%s.xml" % output_name)
        # conversion to XMCDA v2
        try:
            x_results_v2 = org.xmcda.converters.v2_v3.XMCDAConverter.convertTo_v2(x_result_v3)
        except Throwable as t:
            x_execution_results.addError(utils.get_message("Could not convert %s into XMCDA_v2, reason: " % output_name, t))
            continue
        # write the corresponding xmcda v2.2.1 file
        try:
            org.xmcda.parsers.xml.xmcda_v2.XMCDAParser.writeXMCDA(x_results_v2, output_filename,
                                                                     handle_outputs.xmcda_v2_tag(output_name))
        except Throwable as t:
            x_execution_results.addError(utils.get_message("Error while writing %s.xml, reason: " % output_name, t))
            # Whatever the error is, clean up the file: we do not want to leave an empty or partially-written file
            os.remove(output_filename)

    # Let's write the file 'messages.xml' as well
    return utils.write_program_execution_results(prg_exec_results, x_execution_results, utils.XMCDA_VERSION_v2)


if __name__ == "__main__":
    sys.exit(main())
