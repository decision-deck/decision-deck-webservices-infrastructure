"""
${program.name} command-line
"""

import sys

import ${program.name}CLI_XMCDAv2
import ${program.name}CLI_XMCDAv3


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def main(argv=None):
    if argv is None:
        argv = sys.argv

    if '--v2' in argv:
        argv.remove('--v2')
        return ${program.name}CLI_XMCDAv2.main(argv)
    elif '--v3' in argv:
        argv.remove('--v3')
        return ${program.name}CLI_XMCDAv3.main(argv)
    else:
        print >> sys.stderr, "missing mandatory option --v2 or --v3"
        return -1


if __name__ == "__main__":
    sys.exit(main())
