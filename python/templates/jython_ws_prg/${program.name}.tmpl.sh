#! /bin/bash
# Usage:
#  ${program.name}.sh [--v2|--v3] -i input_dir -o output_dir
<%text>
source common_settings.sh

${CMD} "$@"
exit $?
</%text>\
