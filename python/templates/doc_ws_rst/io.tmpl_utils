<%def name="render_io(prg_id, ios, is_input)">
% for param in ios:
- :ref:`${param.name} <${prg_id+"-"+param.id}>`\
% if param.optional is not None:
 *(optional)*\
% endif
% endfor


% for param in ios:

.. _${prg_id}-${param.id}:

${param.name+(' *(optional)*' if param.optional else '')}
${'~'*(len(param.name)+(len(' *(optional*)') if param.optional is not None else 0))}

% if param.documentation.description:
${param.documentation.description.strip()}

% endif
##% if param.optional is not None:
##
##This is an **optional** parameter.
##% endif
##- **ID:** ${param.id}
##
##- **Name:** ${param.name}
##
## -**Display name:** ${param.displayName}
##
%  for a in param.documentation.contact:
-**Contact:** ${a}

%  endfor
%  if param.documentation.url:
%   for a in param.documentation.url:
-**Web page:** ${a}

%   endfor
%  endif
%  if param.documentation.reference:
%   for a in param.documentation.reference:
-**Reference:** ${a}

%   endfor
%  endif
% if is_input:
The input value should be a valid XMCDA document whose main tag is ``<${param.tag}>``.
%   if param.xmcda:
It must have the following form::

   ${param.xmcda}
%   endif
% else:
The returned value is a XMCDA document whose main tag is ``<${param.tag}>``.
%   if param.xmcda:
It has the following form::

   ${param.xmcda}
%   endif
% endif

 % if param.type == 'input-gui':
  % if param.gui:
where:

   % for a in param.gui.entries:
- **${a.id}** is a parameter named "${a.displayName}". \
    % if a.type == 'enum':
It can have the following values:

     % for b in a.items:
  - ``${b.value}``: ${b.description}

     % endfor
    % else:
This is a ${a.type}\
     %if a.constraint:
, and the value should conform to the following constraint: ${a.constraint.description}.  More formally, the constraint is::

     ${a.constraint.code}
%      else:
.
%      endif
%     endif
    % if a.defaultValue:
  The default value is ${a.defaultValue}.

    % endif
   % endfor
  % endif
 %endif

------------------------

% endfor
</%def>
