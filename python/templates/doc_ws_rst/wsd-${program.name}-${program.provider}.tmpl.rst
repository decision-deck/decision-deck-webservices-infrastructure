## include dedicated templates
<%namespace file="io.tmpl_utils" import="*"/>\
<%namespace file="parameter.tmpl_utils" import="*"/>\
<% program_id=program.name+"-"+program.provider %>\
##
.. _${program.name}-${program.provider}:

${program.displayName}
${'='*len(program.displayName)}

:Version: ${program.version}
:Provider: ${program.provider}
% if program.name != program.displayName:
:Name: ${program.name}
% endif
##:Display name: ${program.displayName}
% if program.url:
:URL: ${program.url}
% endif
:SOAP service's name: ``${program_id}`` (see :ref:`soap-requests` for details)

Description
-----------

${program.documentation.description.strip()}

% if program.documentation.contact:
% for a in program.documentation.contact:
**Contact:** ${a}

% endfor
%endif
% if program.documentation.url:
% for a in program.documentation.url:
**Web page:** ${a}

% endfor
% endif
% if program.documentation.reference:
% for a in program.documentation.reference:
**Reference:** ${a}

% endfor
% endif

Inputs
------
(For outputs, see :ref:`below <${program_id}_outputs>`)
% if not program.inputs():
No input
% else:
${render_io(program_id, program.inputs(), True)}
% endif

.. _${program_id}_outputs:

Outputs
-------
% if not program.outputs():
No output.
% else:
${render_io(program_id, program.outputs(), False)}
% endif

Original xml description
------------------------

 - :download:`description.xml<${description_file}>`
