package org.example.${program.name}.xmcda;

import org.xmcda.ProgramExecutionResult;
import org.xmcda.XMCDA;

/**
 * @author
 */
public class InputsHandler
{
	/**
	 * This class contains every element which are needed to compute
	 * ${program.displayName}.<br>
	 * It is populated by {@link InputsHandler#checkAndExtractInputs(XMCDA, ProgramExecutionResult)}.
	 */
	public static class Inputs // TODO
	{
		// Inputs contents derived from prg inputs:
% for input in program.inputs():
		// - ${input.name} (XMCDA tag: ${input.tag})
%endfor
	}

	/**
	 *
	 * @param xmcda
	 * @param xmcda_exec_results
	 * @return
	 */
    static public Inputs checkAndExtractInputs(XMCDA xmcda, ProgramExecutionResult xmcda_exec_results) // TODO
	{
		Inputs inputs = new Inputs();

                /* ... */

		return inputs;
	}


}
