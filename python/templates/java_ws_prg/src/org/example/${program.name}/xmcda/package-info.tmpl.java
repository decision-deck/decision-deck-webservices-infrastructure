/**
 * This package contains all the necessary tools to read and write XMCDA v2 and XMCDA v3 files
 */
package org.example.${program.name}.xmcda;
