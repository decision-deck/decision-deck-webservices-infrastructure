<%! from utils import camel_to_snake_case %>\
package org.example.${program.name}.xmcda;

import org.xmcda.ProgramExecutionResult;
import org.xmcda.XMCDA;

import java.util.HashMap;
import java.util.Map;

/**
 * @author
 */
public class OutputsHandler
{
	/* TODO depending on whether the file was generated from a description
	   based on XMCDA v2 or v3, only one method is correct, either
	   xmcdaV3Tag() or XMCDAV2Tag(): check them to determine which one
	   should be adapted. */

	/**
	 * Returns the xmcda v3 tag for a given output
	 * @param outputName the output's name
	 * @return the associated XMCDA v2 tag
	 * @throws NullPointerException if outputName is null
	 * @throws IllegalArgumentException if outputName is not known
	 */
	public static final String xmcdaV3Tag(String outputName)
	{
		switch(outputName)
		{
% for output in program.outputs():
%   if output.isProgramExecutionResult():
			case "${output.name}":
				return "programExecutionResult";
%   else:
			case "${output.name}":
				return "${output.tag}";
%   endif
% endfor
			default:
				throw new IllegalArgumentException(String.format("Unknown output name '%s'",outputName));
		}
	}

	/**
	 * Returns the xmcda v2 tag for a given output
	 * @param outputName the output's name
	 * @return the associated XMCDA v2 tag
	 * @throws NullPointerException if outputName is null
	 * @throws IllegalArgumentException if outputName is not known
	 */
	public static final String xmcdaV2Tag(String outputName)
	{
		switch(outputName)
		{
% for output in program.outputs():
%   if output.isProgramExecutionResult():
			case "${output.name}":
				return "methodMessages";
%   else:
			case "${output.name}":
				return "${output.tag}";
%   endif
% endfor
			default:
				throw new IllegalArgumentException(String.format("Unknown output name '%s'",outputName));
		}
	}


	/**
	 * Converts the results of the computation step into XMCDA objects.
	 * @param results
	 * @param executionResult
	 * @return a map with keys being xmcda objects' names and values their corresponding XMCDA object
	 */
	public static Map<String, XMCDA> convert(Object results, ProgramExecutionResult executionResult) // TODO
	{
		final HashMap<String, XMCDA> results = new HashMap<>();

% for output in program.outputs():
% if output.name != 'messages':
		/* ${output.name} */
		XMCDA x_${output.name} = new XMCDA();
		// ...
		results.put("${output.name}", x_${output.name});

% endif
% endfor

		// NB: 'messages' is ignored here since because
		//     outputs.executionResult may still be updated when
		//     converting the data to XMDA v3, or when saving
		//     the files.
		return results;
	}
}
