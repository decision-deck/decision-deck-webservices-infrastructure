<%! from utils import camel_to_snake_case %>\
package org.example.${program.name}.xmcda;

import org.example.${program.name}.${program.capitalized_name};
import org.xmcda.ProgramExecutionResult;
import org.xmcda.XMCDA;

import java.io.File;
import java.util.Map;

/**
 *
 */
public class ${program.capitalized_name}CLI_XMCDAv3
{
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Utils.InvalidCommandLineException
	{
		// Parsing the options
		final Utils.Arguments params = Utils.parseCmdLineArguments(args);

		final String indir = params.inputDirectory;
		final String outdir = params.outputDirectory;

		final File prgExecResults = new File(outdir, "messages.xml");

		final ProgramExecutionResult executionResult = new ProgramExecutionResult();

		// The idea of the following code wrt. errors is to collect as many errors as possible before the
		// computation takes place, to the user's benefit since he/she then gets all of them after a single call.

		// this object is where the XMCDA objects will be put into.
		final XMCDA xmcda = new XMCDA();

% for input in program.inputs():
<% input.camel_name = camel_to_snake_case(input.name)%>\
% if input.optional is None:
%   if input.isProgramParameter(): # it is mandatory, always
		Utils.loadXMCDAv3(xmcda, new File(indir, "${input.name}.xml"), true,
		                  executionResult, "programParameters");
%   else:
		Utils.loadXMCDAv3(xmcda, new File(indir, "${input.name}.xml"), true,
		                  executionResult, "${input.tag}");
%    endif
% else:
		Utils.loadXMCDAv3(xmcda, new File(indir, "${input.name}.xml"), false,
		                  executionResult, "${input.tag}");
% endif
% endfor

		// We have problems with the inputs, its time to stop
		if ( ! (executionResult.isOk() || executionResult.isWarning() ) )
		{
			Utils.writeProgramExecutionResultsAndExit(prgExecResults, executionResult, Utils.XMCDA_VERSION.v3);
			// previous statement terminates the execution
		}

		// Let's check the inputs and convert them into our own structures
		final InputsHandler.Inputs inputs = InputsHandler.checkAndExtractInputs(xmcda, executionResult);

		if ( ! ( executionResult.isOk() || executionResult.isWarning() ) || inputs == null )
		{
			Utils.writeProgramExecutionResultsAndExit(prgExecResults, executionResult, Utils.XMCDA_VERSION.v3);
			// previous statement terminates the execution
		}

		// Here we know that everything was loaded as expected

		// Now let's call the calculation method
		final Object results;
		try
		{
			results = ${program.capitalized_name}.calculate${program.capitalized_name}(inputs);
		}
		catch (Throwable t)
		{
			executionResult.addError(Utils.getMessage("The calculation could not be performed, reason: ", t));
			Utils.writeProgramExecutionResultsAndExit(prgExecResults, executionResult, Utils.XMCDA_VERSION.v3);
			// previous statement terminates the execution
			return; // just to make the compiler happy about results being final and potentially not initialized below
		}

		// Fine, now let's put the results into XMCDA structures
		Map<String, XMCDA> x_results = OutputsHandler.convert(results, executionResult);

		// and last, write them onto the disk
		final org.xmcda.parsers.xml.xmcda_v3.XMCDAParser parser = new org.xmcda.parsers.xml.xmcda_v3.XMCDAParser();

		for ( String key : x_results.keySet() )
		{
			File outputFile = new File(outdir, String.format("%s.xml", key));
			try
			{
				parser.writeXMCDA(x_results.get(key), outputFile, OutputsHandler.xmcdaV3Tag(key));
			}
			catch (Throwable throwable)
			{
				final String err = String.format("Error while writing %s.xml, reason: ", key);
				executionResult.addError(Utils.getMessage(err, throwable));
				// Whatever the error is, clean up the file: we do not want to leave an empty or partially-written file
				outputFile.delete();
			}
		}

		// Let's write the file 'messages.xml' as well
		Utils.writeProgramExecutionResultsAndExit(prgExecResults, executionResult, Utils.XMCDA_VERSION.v3);
		// previous statement terminates the execution
	}

}
