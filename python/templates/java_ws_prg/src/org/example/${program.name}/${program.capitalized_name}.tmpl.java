/**
 *
 */
package org.example.${program.name};

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * ${program.name}
 * ${program.documentation.description}
 *
% for author in program.documentation.contact:
 * @author ${author}
% endfor
 */
public class ${program.capitalized_name}
{

	public static ... calculate${program.capitalized_name}(...params...) // TODO
	{
		/* ... */
		return ...;
	}

}
