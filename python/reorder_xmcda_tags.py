#! /usr/bin/env python3
"""
Check that the XMCDA tags in a description are in the expected order

Usage: %s [--v2|--v3] infile.xml outfile.xml
"""
import getopt, os, sys
from xml.etree import ElementTree as ET

from generate_from_description import Program
from utils import guess_indentation, prettyprint

XMCDA_tags_order={
    'v2': (
        'criteria',
        'criteriaSets',
        'alternatives',
        'alternativesSets',
        'categories',
        'categoriesSets',
        'hierarchy',
        'performanceTable',

        'criteriaComparisons',
        'criteriaLinearConstraints',
        'criteriaMatrix',
        'criterionValue',
        'criteriaValues',

        'attributeValue',

        'alternativesAffectations',
        'alternativesComparisons',
        'alternativesLinearConstraints',
        'alternativesMatrix',
        'alternativeValue',
        'alternativesValues',
        'alternativesCriteriaValues',

        'categoriesComparisons',
        'categoriesContents',
        'categoriesLinearConstraints',
        'categoriesMatrix',
        'categoriesProfiles',
        'categoriesValues',

        'methodParameters',
        None,
        'other', # not a real xmcda tag
        'methodMessages',
    ),
    'v3': (
        'criteria',
        'criteriaSets',
        'alternatives',
        'alternativesSets',
        'categories',
        'categoriesSets',
        # hierarchy
        'performanceTable',

        'criteriaFunctions',
        'criteriaLinearConstraints',
        'criteriaMatrix',
        'criteriaScales',
        'criteriaThresholds',
        'criteriaValues',

        'criteriaSetsLinearConstraints',
        'criteriaSetsMatrix',
        'criteriaSetsValues',

        'alternativesAssignments',
        'alternativesValues',
        'alternativesLinearConstraints',
        'alternativesMatrix',

        'alternativesSetsValues',
        'alternativesSetsLinearConstraints',
        'alternativesSetsMatrix',

        'alternativesCriteriaValues',

        'categoriesLinearConstraints',
        'categoriesMatrix',
        'categoriesProfiles',
        'categoriesValues',

        'categoriesSetsValues',
        'categoriesSetsLinearConstraints',
        'categoriesSetsMatrix',

        'programParameters',
        None,
        'other', # not a real xmcda tag
        'programExecutionResult',
    ),
}



def check_order(program, xmcda_version):
    order = XMCDA_tags_order[xmcda_version]

    inputs_order=[]
    inputs_invalid=[]
    tag=None
    for input in program.inputs():
        tag = input.tag
        if tag is not None and ',' in tag: # we may have tag1, tag2
            tag = [ t.strip() for t in tag.split(',') ]
        else:
            tag = [ tag ]

        for t in tag:
            if t not in order:
                inputs_invalid.append(input)
                continue

        # sort by the 1st tag
        inputs_order.append( (order.index(tag[0]), input) )

    outputs_order=[]
    outputs_invalid=[]
    for output in program.outputs():
        tag = output.tag
        if tag is not None and ',' in tag:
            tag = [ t.strip() for t in tag.split(',') ]
        else:
            tag = [ tag ]

        for t in tag:
            if t not in order:
                outputs_invalid.append(input)
                continue

        outputs_order.append( (order.index(tag[0]), output) )

    return [inputs_invalid, inputs_order, outputs_invalid, outputs_order]

def print_invalid_tags(fd, inputs_invalid, outputs_invalid):
    if len(inputs_invalid)>0:
        fd.write('INVALID TAGS (inputs):\n')
        for p in inputs_invalid:
            fd.write('- id:%s name:%s tag:%s\n'%(p.id, p.name, p.tag))
        fd.write('\n')
    if len(outputs_invalid)>0:
        fd.write('INVALID TAGS: (outputs)\n')
        for p in outputs_invalid:
            fd.write('- id:%s name:%s tag:%s\n'%(p.id, p.name, p.tag))
        fd.write('\n')

    #proposed_inputs=list(inputs_order)
    #proposed_inputs.sort(lambda x,y: cmp(x[0], y[0]))
    #print_diffs(fd, 'Proposed changes for inputs', inputs_order, proposed_inputs)

    #proposed_outputs=list(outputs_order)
    #proposed_outputs.sort(lambda x,y: cmp(x[0], y[0]))
    #print_diffs(fd, 'Proposed changes for outputs', outputs_order, proposed_outputs)

def reorder_io(program, inputs, outputs):
    sorted_inputs=list(inputs)
    sorted_inputs.sort(key=lambda x: x[0])
    sorted_outputs=list(outputs)
    sorted_outputs.sort(key=lambda x: x[0])

    program.parameters = []
    for input in sorted_inputs:
        program.parameters.append(input[1])
    for output in sorted_outputs:
        program.parameters.append(output[1])

class Usage(Exception): pass

def main(argv=None):
    xmcda_version="v2"
    if argv is None:
        argv = sys.argv
    output_dir = None
    try:
        try:
            opts, args = getopt.getopt(sys.argv[1:],
                                       "h23",
                                       ["help", "v2", "v3"
                                        ])
        except getopt.error as msg:
            raise Usage(msg)

        for opt, arg in opts:
            if opt in ("-h", "--help"):
                print( __doc__)
                return 0
            if opt in ('-2', '--v2'):
                xmcda_version='v2'
            if opt in ('-3', '--v3'):
                xmcda_version='v3'

        if len(args) != 2:
            raise Usage('Wrong number of arguments')

    except Usage as err:
        print(err.message, file=sys.stderr)
        print("for help use --help", file=sys.stderr)
        return 1

    description = args[0]
    output=args[1]
    program=Program(ET.parse(description).getroot())

    f = sys.stdout
    if output != '-':
        f=open(output, 'w')

    indent = guess_indentation(description)
    l=check_order(program, xmcda_version)
    if len(l[0]) or len(l[2]):
        print_invalid_tags(sys.stderr, l[0], l[2])
        print >>sys.stderr, 'Cannot process invalid w/ tags'
        return 2

    reorder_io(program, l[1], l[3])
    prettyprint(program.element_tree(), f, xml_declaration=True,
                indentation=indent)
    return 0

if __name__ == "__main__":
    sys.exit(main())
