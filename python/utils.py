INDENTATION='    '

def indent(elem, level=0, indentation=INDENTATION):
    """
    Adds whitespace to the tree, so that saving it as usual results in a
    prettyprinted tree.
    Mostly written by Fredrik Lundh, http://effbot.org/zone/element-lib.htm
    adapted to add newlines arround inputs and outputs
    If optional parameter 'indentation' is None it defaults to INDENTATION.
    """
    if indentation is None:
        indentation = INDENTATION
    i = "\n" + level*indentation
    if len(elem):
        if elem.tag not in ('xmcda','contact', 'code') and ( not elem.text or not elem.text.strip() ):
            elem.text = i + indentation
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for child in elem:
            indent(child, level+1, indentation)
        if not child.tail or not child.tail.strip():
            # the previous indent may have put an extra '\n' to the tail:
            # simply un-indent the two last whitespaces
            child.tail = child.tail[:-len(indentation)]
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i
    if elem.tag in ('input', 'output'):
        # add an extra newline when ending inputs or outputs
        elem.tail = '\n' + elem.tail
    if elem.tag in ('parameters', ):
        # add a newline before the first <input>
        elem.text = '\n' + elem.text


def guess_indentation(description_filepath):
    '''
    Guess the indentation used in the xml description but supposing that it is homogeneous.
    '''
    with open(description_filepath) as description:
        for line in description:
            if line.strip().startswith('<program '):
                return line[:line.index('<')]
    return None

# default encoding
# if we are not using this, we get TypeError when trying to write the files
# -> Use 'utf-8' for python2.7, 'unicode' for python 3
import sys
if sys.version_info.major==2:
    _encoding='utf-8' # py2.x
else:
    _encoding='unicode' # py3.x

def prettyprint(root, file, encoding=_encoding, xml_declaration=None, default_namespace=None, method="xml", indentation=None):
    indent(root.getroot(), indentation=indentation)
    root.write(file, encoding, xml_declaration, default_namespace, method)

# from http://stackoverflow.com/questions/1175208/elegant-python-function-to-convert-camelcase-to-snake-case
def camel_to_snake_case(name):
  import re
  s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
  return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

def capitalize_first_letter(a_string):
    return a_string[0].capitalize() + a_string[1:]

from xml.etree import ElementTree as ET
def CDATA(text=None):
    element = ET.Element('CDATA')
    element.text = text
    element.tail = ''
    return element

def add_CDATA():
    if hasattr(ET, '_original_serialize_xml'):
        return
    ET._original_serialize_xml = ET._serialize_xml

    def _serialize_xml(write, elem, qnames, namespaces, short_empty_elements, **kwargs):
        if elem.tag == 'CDATA':
            write("<![CDATA[%s]]>" % elem.text)
            return
        return ET._original_serialize_xml(write, elem, qnames, namespaces, short_empty_elements, **kwargs)

    ET._serialize_xml = ET._serialize['xml'] = _serialize_xml
