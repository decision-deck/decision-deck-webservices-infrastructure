# -*- coding: utf-8 -*-
import re, time
from xml.sax.saxutils import escape
def escape_xml(str):
  if str is None:
    return ""
  return "<![CDATA["+str+"]]>"

def dependanceForParameter(p, e=None, with_gui_activated=True):
  '''
  with_gui_parameter
  - True:  returns a dependency which is true when the GUI is activated
  - False: returns a dependency which is true when the GUI is not activated
  - None: returns a dependency which does not check whether the GUI is activated
  '''
  dep=""
  if p.optional is not None:
    dep+='(%s-dep:value="1") AND '%p.id

  # in the following, dont test if p.gui.status!='alwaysGUI': if it's always
  # GUI, the -use-file-dep will be generated, hidden and false.
  if with_gui_activated is not None and p.gui:
    dep+='(%s-use-file-dep:value="%i") AND '%(p.id, not with_gui_activated)

  if e is not None and e.dependency is not None:
    e_dep = "%s-gui-value-placeholder-"%p.id
    #:value / :type ... à insérer après r'%s\1'
    e_dep = re.sub(r'%([0-9]*)', r'%s\1'%e_dep, str(e.dependency))
    dep += '(%s) AND '%e_dep
  if p.dependency is not None:
    dep += '(%s) AND '%p.dependency
  if dep:
    return '<dep>%s</dep>\n'%dep[:-5]
  else:
    return '<dep/>'

def split_parameter(parameter):
    p = parameter

    xmcda_parts = []

    splitter = [e.id for e in p.gui.entries]
    # make sure "%10" appears before "%1" --if not, %10 in p.xmcda will
    # be matched by %1 and the split would not be correct
    # Here we just sort the list so that longer items appears first
    splitter.sort(key = lambda x: -len(x))
    splitter = '(' + '|'.join(splitter) + ')'
    splitter = re.compile(splitter)
    return splitter.split(p.xmcda)

