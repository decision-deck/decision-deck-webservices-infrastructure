#!/usr/bin/python3
# -*- coding: utf-8 -*-
# ddws_soap_client.py
# written by Olivier Sobrie, Sébastien Bigaret
# adapted from a script by Raymond Bisdorff, April 2008
#
# This script is in the public domain, it can be used freely.
#
# Portions of code were borrowed from others: you will find before them
#   their original authors' names and an URL.
#
# Requires: pysimplesoap - http://code.google.com/p/pysimplesoap/
#
"""
Usage: ddws_soap_client.py -n <service's name> [options] [command] [arguments]

Commands:

  -s, --submit-problem                submit a problem (see below);
                                      a ticket-id is printed on the standard
                                      output
  -g, --request-solution <ticket-id>  request the solution corresponding to
      --get-solution                  this ticket. The solution is written in
                                      the file 'kappalabSolution.xml', unless
                                      option '-' is given (see below)

  -S, --submit-and-wait-solution      submit a problem (see below), and waits
                                      for the solution. The solution is
                                      written on the disk the same way as
                                      for option -g

Options:
  -U, --url                    Specify an alternate url for accesing than the
                               default.
                               Default is:
                               http://webservices.decision-deck.org/soap/%s.py

                               You can specify either a string like the default
                               one, using '%s' which will be replaced by the
                               service's name, or a regular, plain url.

  -n <service's name>          It is mandatory if -U is not supplied.

  -v, --verbose                increase verbosity
  -t, --timeout <seconds>      wait for a solution during <seconds> seconds
                               at most, then exit if no solution was found.
                               Use zero (0) to disable the timeout.
                               Defaults to 60 seconds.
  -x, --original-xml           Do not pretty-print files.xml, just save the
                               received one (default is to pretty-print)
  (-h) --help                  show this help (-h works with no other options)

Arguments:  a list of <parameter_name>:<file_path>, separated by whitespaces

  For example, if you want to supply the file 'my_criteria.xml' as input
  to the parameter 'criteria', the argument is:

    criteria:my_criteria.ml

Exit status:

  0  no problem
  1  usage error
  2  timed out while waiting for a solution (with commands -g and -S)

"""
import getopt, sys, time
import os
from io import StringIO
from xml.etree import ElementTree as ET
#this was the old one, kept here a moment just in case
#ET._namespace_map['http://www.decision-deck.org/2008/UMCDA-ML-1.0']='xmcda'
ET._namespace_map['http://www.decision-deck.org/2009/XMCDA-2.0.0']='xmcda'
ET._namespace_map['http://www.decision-deck.org/2009/XMCDA-2.1.0']='xmcda'
ET._namespace_map['http://www.decision-deck.org/2012/XMCDA-2.2.0']='xmcda'
ET._namespace_map['http://www.decision-deck.org/2012/XMCDA-2.2.1']='xmcda'
ET._namespace_map['http://www.decision-deck.org/2013/XMCDA-3.0.0']='xmcda'
ET._namespace_map['http://www.decision-deck.org/2016/XMCDA-3.0.1']='xmcda'
ET._namespace_map['http://www.decision-deck.org/2017/XMCDA-3.0.2']='xmcda'
ET._namespace_map['http://www.w3.org/2001/XMLSchema-instance']='xsi'

from pysimplesoap.client import SoapClient
from pysimplesoap.simplexml import SimpleXMLElement

#service_url='http://ernst-schroeder.uni.lu/cgi-bin/%s.py'
#default_service_url='http://ddeck.lgi.ecp.fr/cgi-bin/%s.py'
default_service_url='http://webservices.decision-deck.org/soap/%s.py'
service_url=default_service_url

verbose = 0
log_suffix=''

def log(msg):
    if verbose:
        print(msg, file=sys.stderr)
def debug(msg):
    if verbose>1:
        print(msg, file=sys.stderr)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def pysimplesoap_http_proxy():
    '''
    This methods makes it possible to use a HTTP proxy which does not
    accept the HTTP CONNECT method.

    pysimplesoap relies on socks (http://socksipy.sourceforge.net/) for http
    proxies, but as far as http proxies are concerned, socks only supports
    proxies allowing the CONNECT method...  which is not allowed where we need
    it.
    '''
    proxy = os.environ.get('http_proxy')
    if proxy is None:
        return
    # cf. http://wikipython.flibuste.net/CodesReseau#Passerparunproxy
    import urllib
    proxy_support = urllib.request.ProxyHandler({'http' : proxy})
    opener = urllib.request.build_opener(proxy_support)
    urllib.request.install_opener(opener)

    # The code above is enough if httplib2 is not installed.
    # If it is installed, pysimplesoap relies on it: we need to change
    # its behaviour and tell it to use urllib.
    # The following class is directly extracted from pysimplesoap.client
    class Http(): # wrapper to use when httplib2 not available
        def request(self, url, method, body, headers):
            f = urllib.request.urlopen(urllib.request.Request(url, body, headers))
            return f.info(), f.read()

    from pysimplesoap import client
    client.Http=Http

pysimplesoap_http_proxy()

#from utils import prettyprint
#inlined utils.indent & prettyprint, so this script is executable as-is
def indent(elem, level=0):
    """
    Adds whitespace to the tree, so that saving it as usual results in a
    prettyprinted tree.
    Written by Fredrik Lundh, http://effbot.org/zone/element-lib.htm
    """
    i = "\n" + level*"\t"
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "\t"
        if not elem.tail or not elem.tail.strip():
            elem.tail = i

        #for elem in elem:
        #    indent(elem, level+1)
        #if not elem.tail or not elem.tail.strip():
        #    elem.tail = i

        for child in elem:
            indent(child, level+1)
        if not child.tail or not child.tail.strip():
            child.tail = i
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

def prettyprint(root, file, encoding='us-ascii'):
  indent(root.getroot())
  root.write(file, encoding)

def submitProblem(url, problem, params):
    debug("entering submitProblem()\n")

    host=url.split('/')[2]

    debug("Calling webservice, host:%s, url: %s\n"%(host, url))
    debug([x.get_name() for x in (params.children() or ())])

    client = SoapClient(
        location = service_url,
        action = '',
        soap_ns = 'soapenv',
        namespace = 'http://www.decision-deck.org/2009/XMCDA-2.0.0',
        trace = verbose > 2 and True or False)

    sp = client.call('hello')
    log(str(sp.helloResponse.message))

    debug("submitting problem\n")
    sp = client.call('submitProblem', params)
    reply = sp.submitProblemResponse
    debug(str(reply.message))
    log("Return Ticket: "+ str(reply.ticket))
    return str(reply.ticket)

def requestSolution(url, ticket_id, timeout=0, pretty_print_xml=True):
    host=url.split('/')[2]

    log("Request solution for problem %s" % (ticket_id))
    client = SoapClient(
        location = service_url,
        action = '',
        soap_ns = 'soapenv',
        namespace = 'http://www.decision-deck.org/2009/XMCDA-2.0.0',
        trace = verbose > 2 and True or False)

    start = time.time()
    while True:
        rs = client.call('requestSolution', ticket=ticket_id)
        reply = rs.requestSolutionResponse
        if int(getattr(reply, 'service-status')) != 1: # NOT AVAILABLE
            break

        time.sleep(0.5)
        if timeout and time.time()>start+timeout:
            log('timeout: solution not available after %i seconds: exiting'%timeout)
            return None

    filenames = []
    tags = dir(reply)
    debug('Got the following keys in the answer: '+', '.join(tags))
    for k in tags:
        v = str(getattr(reply,k))

        if k in ('ticket', 'service-status'):
            continue

        xml = None
        try:
            xml = ET.ElementTree(ET.fromstring(v))
        except Exception as e:
            log('Unable to parse answer for key: %s, writing raw file'%k)
            debug('Answer for key %s was: "%s"'%(k,v))
            filename = k
        else:
            filename = k+'.xml'

        if xml and pretty_print_xml:
            # re-read it to prettyprint it
            prettyprint(xml, filename, 'utf-8')
        else:
            f=open(filename, 'w')
            f.write(v)
            f.close()

        log('Wrote file:%s'%filename)
        filenames.append(filename)

    return filenames

def main(argv=None):
    global verbose, service_url, log_suffix

    if argv is None:
        argv = sys.argv

    # problem is True or False while parsing arguments.
    # When -s/-S is supplied, problem contains the string representation (xml)
    # of the problem
    problem=False
    ticket=None
    service_name=None
    timeout=60
    pretty_print_xml=True

    method_data_params = []
    try:

        #if len(argv)<3:
        #    if len(argv)==2 and argv[1] in ('-h', '--help'):
        #        print __doc__
        #        return 0
        #    raise Usage("Missing service and/or command")
        try:
            opts, args = getopt.getopt(sys.argv[1:],
                                       "n:hvsg:St:M:U:x",
                                       ['service-name',
                                        'help',
                                        'verbose',
                                        'submit-problem',
                                        'request-solution=', 'get-solution=',
                                        'submit-and-wait-solution',
                                        'timeout=',
                                        'add-method-data-parameter=',
                                        'url=', 'original-xml',
                                        'no-log', 'diviz', 'log-marker='
                                        ])
        except getopt.error as msg:
            raise Usage(msg)
        # process options
        for opt, arg in opts:
            if opt in ("-n", "--service-name"):
                service_name=arg
            if opt in ("-h", "--help"):
                print(__doc__)
                return 0
            if opt in ('-v', '--verbose'):
                verbose += 1
            if opt in ('-s', '--submit-problem'):
                problem=True
            if opt in ('-g', '--request-solution', '--get-solution'):
                ticket=arg
            if opt in ('-S', '--submit-and-wait-solution'):
                problem=True
                ticket=-1
            if opt in ('-t', '--timeout'):
                try:
                    timeout=int(arg)
                except ValueError as err:
                    timeout=-1
                if timeout<0:
                    raise Usage('timeout value should be a valid, positive or null integer')
            if opt in ('-M', '--add-method-data-parameter'):
                method_data_params.append(arg)

            if opt in ('-U', '--url='):
                service_url=arg

            if opt in ('-x', '--original-xml'):
                pretty_print_xml=False

            if opt in ('--no-log',):
                log_suffix = 'no-log'

            if opt in ('--diviz',):
                log_suffix = 'diviz'

            if opt in ('--log-marker',):
                log_suffix = arg

        # check the URL is plain, or that the service's name is supplied
        if service_name is None and service_url is None:
            raise Usage("Both service's name and url are not supplied")
        if service_url == default_service_url and service_name is None:
            raise Usage("The service's name should be supplied when no URL is provided")

        if service_name is not None:
          try:
              service_url = service_url%service_name
          except TypeError:
              pass
        if not problem and not ticket:
            raise Usage("")

        debug("Using url: %s"%service_url)
        params = SimpleXMLElement("<submitProblem></submitProblem>")
        if problem:
            for arg in args:
                k,v=arg.split(':',1) #TODO check ValueError
                fileIN = open(v,'r')
                content = fileIN.read()
                fileIN.close()

                child = params.add_child(k, content)
                child.add_attribute("xsi:type", "xsd:string")

            if params.children() is None:
                #raise Usage("Options -s/-S require that at least one argument")
                # a service may have reasonable default values for all of
                # its parameters (services randomly generating data e.g.)
                pass

            if log_suffix is not None and log_suffix != '':
                params.add_child('XMCDA_WS_LOG_SUFFIX', log_suffix)

    except Usage as err:
        print(err.msg, file=sys.stderr)
        print("for help use --help", file=sys.stderr)
        return 1

    client = SoapClient(
        location = service_url,
        action = '',
        soap_ns = 'soapenv',
        namespace = 'http://www.decision-deck.org/2009/XMCDA-2.0.0',
        trace = verbose > 2 and True or False)

    if problem:
        ticketNb = submitProblem(service_url, problem, params)
        print(ticketNb)
        if ticket==-1:
            ticket=ticketNb

    if ticket:
        ##ticket=sys.stdin.readline()
        ##if not ticket:
        ##    sys.stderr.write("no ticket id, either on the commandline or in stdin")
        files = requestSolution(service_url, ticket, timeout, pretty_print_xml)
        if not files:
            return 2
        for file in files:
            print(file)

if __name__ == "__main__":
    sys.exit(main())
