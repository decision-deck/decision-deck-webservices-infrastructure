#! /bin/bash

# exemple: XMCDAWebServices/RORUTADIS-PostFactum-InvestigatePerformanceDeterioration-PUT/tests/in1

if [ $# != 2 ]; then
  echo "Usage: $0 diviz_workspace test_dir" 1>&2
  exit 1
fi


diviz_workspace="$1"
IFS='/' read -r -a service <<< "$2"
svc=${service[1]}
tst=${service[3]}

wf_name="${svc}-${tst}"
wf_dir="${diviz_workspace}/${wf_name}/current"
wf_file="${wf_dir}/${wf_name}.dvz"
mkdir -p "${wf_dir}"
cat > "${wf_file}" <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE workflow PUBLIC "-//Telecom Bretagne//DTD XML Praxis Workflow 3.0//EN" "http://perso.telecom-bretagne.eu/~bigaret/praxis/dtd/workflow_3.0.dtd">

<workflow>
    <id />
    <name>${wf_name}</name>
    <user />
    <inputs>
EOF

idx=1
y=75
while IFS= read -r -d '' input; do
    cat >> "${wf_file}" <<EOF
        <input type="file" x="75" y="${y}" name="" id="${idx}">
            <infile>${input}</infile>
        </input>
EOF
  y=$((y+50))
  idx=$((idx+1))
done < <(find "${PWD}/${2}" -type f -name '*.xml' -print0)

cat >> "${wf_file}" <<EOF
    </inputs>
    <programs/>
</workflow>
EOF
