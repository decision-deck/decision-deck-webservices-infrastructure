#! /bin/bash

./downloadService.sh ACUTAR
#./downloadService.sh J-MCDA-Concordance
#./downloadService.sh J-MCDA-Discordance
#./downloadService.sh KappalabComponent                      ?????????????
./downloadService.sh RubisConcordanceRelation
./downloadService.sh XYPlotAlternativesValues
./downloadService.sh alternativesValuesKendall
./downloadService.sh capacityEntropy
./downloadService.sh capacityFavor
./downloadService.sh capacityInteraction
./downloadService.sh capacityOrness
./downloadService.sh capacityShapley
./downloadService.sh capacityVariance
./downloadService.sh capacityVeto
#./downloadService.sh choquetIntegral                         PB liens en dehors du repository
./downloadService.sh computeNormalisedPerformanceTable
./downloadService.sh generalWeightedSum
./downloadService.sh generateXMCDAReport
#./downloadService.sh jsmaa                                   TODO: archive/jar etc.
./downloadService.sh leastSquaresCapaIdent
./downloadService.sh linProgCapaIdent
./downloadService.sh lsRankingCapaIdent
./downloadService.sh miniVarCapaIdent
./downloadService.sh plotAlternativesComparisons
./downloadService.sh plotAlternativesValues
./downloadService.sh plotAlternativesValuesPreorder
./downloadService.sh plotCriteriaComparisons
./downloadService.sh plotCriteriaValues
./downloadService.sh plotCriteriaValuesPreorder
./downloadService.sh plotNumericPerformanceTable
./downloadService.sh plotValueFunctions
./downloadService.sh rankAlternativesValues
./downloadService.sh weightedSum
./downloadService.sh weightsFromCondorcetAndPreferences
./downloadService.sh weightsFromCondorcetRelation
