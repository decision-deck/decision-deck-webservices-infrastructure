# Directories where service will run
# NB: root suffix must match [a-zA-Z_] *only*
ROOT_SUFFIX=test-v3

ROOT_NAME=XMCDA-${ROOT_SUFFIX}
ROOT=/var/${ROOT_NAME}
DAEMONS_ROOT=${ROOT}/daemons

# Directories where services will be installed
WS_HOME=/home/XMCDA-services
WS_DIR=${WS_HOME}/${ROOT_NAME}

# the cgi-bin directory
CGI_BIN=/usr/lib/cgi-bin/XMCDA-${ROOT_SUFFIX}
