#! /bin/bash

msg()
{
  echo "$*" >&2
}

fail()
{
    echo "$1" >&2
    exit -1;
}

# Checks if the description is okay.
# If not, it returns a non-zero status
# Parameter:
# - <file>: the file containing the description
#
checkDescription()
{
    local CONFIG="$1"
    local err="no"
    [ -r "$1" ] &&
    local NAME AUTHORS EMAIL VERSION SVN GIT HTTP FILES
    . "${CONFIG}"

    [ -z "${NAME:-}" ] && err="y" && msg "Config: $CONFIG: missing NAME"
    [ -z "${AUTHORS:-}" ] && err="y" && msg "Config: $CONFIG: missing AUTHORS"
    [ -z "${EMAIL:-}" ] && err="y" && msg "Config: $CONFIG: missing EMAIL" >&2
    [ -z "${VERSION:-}" ] && err="y" && msg "Config: $CONFIG: missing VERSION"
    # TODO: check: GIT or HTTP or SVN, one and only one
    [ -z "${SVN:-}" ] && [ -z "${GIT:-}" ] && [ -z "${HTTP:-}" ] \
        && err="y" \
        && msg "Config: $CONFIG: either GIT, SVN or HTTP must be defined"
    [ -z "${FILES:-}" ] && err="y" && msg "Config: $CONFIG: missing FILES"
    [ "no" != ${err} ] && return 1;
    return 0;
}

# unzip/untar the supplied file
handleCompressedFile()
{
    local file tmpdir
    file=$1
    destination_dir=$2
    if [ "${file: -4}" == ".zip" ]; then
        # zip -> tar then --strip 1. If you know a better way to strip the
        # 1st dir., let me know! sebastien.bigaret@telecom-bretagne.eu
        tmpdir=$(mktemp -d)
        tmptar=$(mktemp)
        unzip -qq -d "$tmpdir" "$file"
        tar cf "$tmptar" -C "$tmpdir" .
        # --strip 2: because the previous cmd adds ./ (ex.: ./dir/content)
        tar xf "$tmptar" --strip 2 -C "$destination_dir"
        rm -rf "$tmpdir" "$tmptar"
        return
    fi
    if [ "${file: -4}" == ".tar" ]; then
        tar xf "$file" --strip 1 -C "$destination_dir"
        return
    fi
    if [ "${file: -7}" == ".tar.gz" ] || [ "${file: -4}" == ".tgz" ]; then
        tar xzf "$file" --strip 1 -C "$destination_dir"
        return
    fi
    if [ "${file: -8}" == ".tar.bz2" ]; then
        tar xjf "$file" --strip 1 -C "$destination_dir"
        return
    fi
}
