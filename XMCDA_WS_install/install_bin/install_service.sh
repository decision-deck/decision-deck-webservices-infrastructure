#! /bin/bash

installService_common()
{
    local NAME AUTHORS EMAIL VERSION SVN GIT HTTP FILES CMD_LINE
    local CONFIG="$1"
    local DAEMONS_DIR="$2"
    source "$CONFIG"
    local SERVICE="${NAME}"

    SERVICE_DIR="$DAEMONS_DIR/$SERVICE"
    mkdir -p "${SERVICE_DIR}"/env "${SERVICE_DIR}"/log/main
    chown Gxmcdalog:root "${SERVICE_DIR}"/log/main
    ln -s "${DAEMONS_DIR}"/common/run             "${SERVICE_DIR}"/
    ln -s "${DAEMONS_DIR}"/common/spool_daemon.sh "${SERVICE_DIR}"/
    ln -s "${DAEMONS_DIR}"/common/service         "${SERVICE_DIR}"/
    # run & log/run: installed in install_XMCDA_arch.sh, fct.installXMCDADaemon
    # same for common/env/
    ln -s "${DAEMONS_DIR}"/common/env/*           "${SERVICE_DIR}"/env/
    if [ -v XMCDA_VERSION ]; then
        echo "${XMCDA_VERSION}" > "$SERVICE_DIR"/env/XMCDA_VERSION
    fi
    if [ -v XMCDA_v2_REMOVE_VALUES ]; then
        echo "${XMCDA_v2_REMOVE_VALUES}" > "$SERVICE_DIR"/env/XMCDA_v2_REMOVE_VALUES
    fi

    ln -s "${DAEMONS_DIR}"/common/log/*           "${SERVICE_DIR}"/log/
    echo "${SERVICE}" > "${SERVICE_DIR}"/env/SERVICE
    echo '#! /bin/bash' > "${SERVICE_DIR}"/service.cmd
    echo -e "time ( $CMD_LINE \\">> "${SERVICE_DIR}"/service.cmd
    cat >> "${SERVICE_DIR}"/service.cmd <<EOF
  2> >(while read err; do echo "[\$4] [err] \$err"; done)   \\
   > >(while read out; do echo "[\$4] [out] \$out"; done) ) \\
2> >( while read t; do echo \$(date +"%s.%N")" \$4 STAT \$t" >> \$5/\$(date +%Y%m%d).stat; done )
# NB: the last part, ">> \$5/\$(date ...)" is evaluated after each "read"
# so the date is always the current one
ret=\$?
exit \$ret
EOF

    chmod 755 "${SERVICE_DIR}"/service.cmd
}

# Installs the files attached to this service
# Parameters:
#   1: service's configuration file
installService_files()
{
    local NAME AUTHORS EMAIL VERSION SVN GIT HTTP FILES FILES_PATTERNS CMD_LINE
    local CONFIG="$1"
    local WS_DIRECTORY
    source "$CONFIG"

    OIFS=$IFS
    IFS=${FILES_DELIM:-" "} # default: a whitespace
    msg "Installing files: $FILES"
    for file in $FILES; do
        msg "  - $file"
        ln -s "$WS_DIR/$NAME/$file" "$DAEMONS_ROOT/$NAME/"
    done
    for pattern in "${FILES_PATTERNS[@]}"; do
        WS_DIRECTORY="$WS_DIR/$NAME"
        DAEMON_DIR="$DAEMONS_ROOT/$NAME/"

        while IFS= read -d '' -r file; do
            msg "  - $file"
            ln -s "$file" "$DAEMON_DIR"
        done < <(find "$WS_DIRECTORY" -name "$pattern" -print0)
    done
    IFS=$OIFS
}

# Activates the service: put it under daemontool's control
# Parameters:
#   1: service's configuration file
#   2: path to the directory where the daemons are installed
activateService()
{
    local CONFIG="$1"
    source "$CONFIG"
    local DAEMONS_DIR=$2
    local root_service_name="${ROOT_SUFFIX}-${NAME}"

    if [ -d "/service/$root_service_name" ]; then
        msg "activate: ${root_service_name}: nothing to do, already activated"
    else
        ln -s "${DAEMONS_DIR}/${NAME}" /service/"${root_service_name}"
        msg "activate: ${root_service_name}: activated"
    fi
}

# Installs the cgi script for a service
# Parameters:
#   1: service's configuration file
installCGI()
{
    local NAME AUTHORS EMAIL VERSION SVN GIT HTTP FILES CMD_LINE
    local CONFIG="$1"
    source "$CONFIG"
    local SERVICE="$NAME"
    #local python_cgi_module="handle_XMCDA_request_${ROOT_SUFFIX}"
    #${CGI_BIN} is split by "ROOT_SUFFIX" cgi-bin/XMCDA-${ROOT_SUFFIX}
    local python_cgi_module="handle_XMCDA_request"

    msg "Installing $CGI_BIN/$SERVICE.py"

    cat > "$CGI_BIN/$SERVICE.py" <<EOF
#!/usr/bin/python
# -*- coding: utf-8 -*-
import $python_cgi_module as xmcda

SERVICE='$SERVICE'
AUTHORS=u'$AUTHORS'
VERSION='$VERSION'

hello           = lambda:      xmcda.hello(SERVICE, VERSION, AUTHORS)
submitProblem   = lambda **kw: xmcda.submitProblem(SERVICE, **kw)
requestSolution = lambda **kw: xmcda.requestSolution(SERVICE, **kw)

## CGI Python script
if __name__ == '__main__':
    from ZSI import dispatch
    dispatch.AsCGI(rpc=True)

EOF
    chmod 755 "$CGI_BIN/$SERVICE.py"
}

# argument:
# 1: the file containing the service's description
installService()
{
    local CONFIG="$1"
    checkDescription "$CONFIG" || fail "Exiting"

    installService_common "$CONFIG" "$DAEMONS_ROOT"
    installCGI "$CONFIG"
    installService_files "$CONFIG"
}
