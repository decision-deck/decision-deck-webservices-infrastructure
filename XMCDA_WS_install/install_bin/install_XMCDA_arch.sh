
# Creates the directories where the daemons (services) will be installed
createDirectories()
{
  mkdir -p "$DAEMONS_ROOT" "$DAEMONS_ROOT/bin" "$CGI_BIN" "$WS_HOME"
}

#
# Adds the users Gxmcda (grp: www-data) and Gxmcdalog (grp: Gxmcdalog)
#
addUsers()
{
  echo "Adding users Gxmcda and Gxmcdalog, + group Gxmcdalog"
  useradd Gxmcda -g www-data -d /tmp -s /bin/false
  groupadd Gxmcdalog
  useradd Gxmcdalog -g Gxmcdalog -d /tmp -s /bin/false
}

# builds a spooler at root dir.=$1
# It creates <root>/JobSpooler, <root>/Problems, <root>/Solutions and <root>/log
buildSpooler()
{
  echo "Creating spoolers in $1"
  root_dir=$1

  mkdir -p "$root_dir"/JobSpooler "$root_dir"/Problems "$root_dir"/Solutions
  mkdir -p "$root_dir"/log
  # set ownership
  chown -R www-data:www-data "$root_dir"/JobSpooler
  chown -R Gxmcda:www-data "$root_dir"/Problems
  chown -R Gxmcda:www-data "$root_dir"/Solutions
  chown -R Gxmcda:www-data "$root_dir"/log

  # set permissions
  chmod 770 "$root_dir"/JobSpooler
  chmod 700 "$root_dir"/Problems
  chmod 750 "$root_dir"/Solutions
  chmod 700 "$root_dir"/log
}

# Creates the spoolers for theg environment.
#buildSpoolers()
#{
#  buildSpooler $ROOT
#}

# Installs the "main" XMCDA daemon, which detects and distributes new jobs
# in the dedicated spoolers.
# 3 parameters:
# - the root directory
# - the daemons dir.
# - the name of the service (typically, either XMCDA or XMCDA-test)
# Used by: installXMCDADaemon
_installXMCDADaemon()
{
  root_dir=$1
  daemons_dir=$2
  daemontools_service=$3

  echo "Installing the XMCDA daemon in $daemons_dir"

  #
  # Creates the generic XMCDA service
  #
  mkdir -p "$daemons_dir"/common/env
  cp -R daemons/common/env "$daemons_dir"/common/
  mkdir -p "$daemons_dir"/common/log
  cp daemons/common/spool_daemon.sh "$daemons_dir"/common/
  cp daemons/common/service "$daemons_dir"/common/
  chmod 755 "$daemons_dir" "$daemons_dir"/common
  chmod 755 "$daemons_dir"/common/spool_daemon.sh "$daemons_dir"/common/service
  chmod 755 "$daemons_dir"/common/env
  chmod 644 "$daemons_dir"/common/env/*
  echo "$root_dir/daemons" > "$daemons_dir"/common/env/DAEMONS_ROOT
  echo "$root_dir"        > "$daemons_dir"/common/env/SPOOL_ROOT
  
  cat > "$daemons_dir"/common/run <<EOF
#! /bin/sh
exec 2>&1
exec setuidgid Gxmcda envdir ./env "$daemons_dir"/common/spool_daemon.sh
EOF
  
  cat > "$daemons_dir"/common/log/run <<EOF
#! /bin/sh
exec 2>&1
exec setuidgid Gxmcdalog multilog t ./main
EOF
  
  chmod 755 "$daemons_dir"/common/run "$daemons_dir"/common/log/run
  
  #
  # Install the XMCDA service's environment
  #
  mkdir -p "$daemons_dir"/XMCDA/env  "$daemons_dir"/XMCDA/log/main
  chown Gxmcdalog:root "$daemons_dir"/XMCDA/log/main
  
  ln -s "$daemons_dir"/common/run             "$daemons_dir"/XMCDA/
  ln -s "$daemons_dir"/common/spool_daemon.sh "$daemons_dir"/XMCDA/
  # common/service not needed here
  ln -s "$daemons_dir"/common/env/*           "$daemons_dir"/XMCDA/env/
  ln -s "$daemons_dir"/common/log/*           "$daemons_dir"/XMCDA/log/
  # an empty SERVICE env.var. indicates to spool_daemon.sh
  # that this is the "main" XMCDA service
  echo "" > "$daemons_dir"/XMCDA/env/SERVICE


  # Install and launch the service itself
  #
  ln -s "$daemons_dir"/XMCDA /service/"$daemontools_service"
}

# Installs the XMCDA daemon
installXMCDADaemon()
{
  _installXMCDADaemon "$ROOT" "$DAEMONS_ROOT" "$ROOT_NAME"
}

# Installs the generic python-cgi handlers in $CGI_BIN
installCGIGenericHandlers()
{
  if [ ! -e "${CGI_BIN}"/handle_XMCDA_request.py ]; then
      echo "Installing the generic cgi-bin handler..." >&2
      sed -e "s|XMCDA_ROOT|$ROOT|g" usr/lib/cgi-bin/handle_XMCDA_request.py > "${CGI_BIN}"/handle_XMCDA_request.py
      chmod 755 "${CGI_BIN}"/handle_XMCDA_request.py
  else
      echo "Generic cgi-bin handler already installed" >&2
  fi
#  if [ ! -e "${CGI_BIN}"/handle_XMCDA_test_request.py ]; then
#      echo "Installing the generic cgi-bin 'test' handler..." >&2
#      sed -e "s|XMCDA_ROOT|$ROOT_TEST|g" usr/lib/cgi-bin/handle_XMCDA_request.py > "${CGI_BIN}"/handle_XMCDA_test_request.py
#      chmod 755 "${CGI_BIN}"/handle_XMCDA_test_request.py
#  else
#      echo "Generic cgi-bin 'test' handler already installed" >&2
#  fi
}

# Install binaries
installBinaries()
(
    mkdir -p "${DAEMONS_ROOT}/bin"
    chmod 755 "${DAEMONS_ROOT}/bin"
    (
        cd usr/bin
        set -x
        while read -d '' file; do
            file=${file#./}
            cp -p "$PWD/$file" "${DAEMONS_ROOT}/bin/"
            chmod 755 "${DAEMONS_ROOT}/bin/${file}"
        done < <(find -L . -maxdepth 1 -type f ! -name '*~' -print0)
    )
    (
        cd usr/share
        while read -d '' file; do
            file=${file#./}
            cp -p "$PWD/$file" "${DAEMONS_ROOT}/share/"
            chmod a+r "${DAEMONS_ROOT}/share/${file}"
        done < <(find -L . -maxdepth 1 -type f ! -name '*~' -print0)
    )

)

# Checks that the environment is okay
checkEnvironment()
{
    err=0
    echo -n "Checking environment..." >&2
#    if [ ! -d "${CGI_BIN}" ]; then
#	err=1
#	echo -ne "\n- directory ${CGI_BIN} does not exist" >&2
#    fi
    if ! command -v inotifywait); then
	err=1
	echo -ne "\n- inotifywait not found" >&2
    fi
    # TODO: svn, git, wget as well
    if [ $err -ne 0 ]; then
	echo -e "\n\nPlease correct the above error(s) before installing\n" >&2
	exit -1;
    else
	echo " Ok" >&2
    fi
}

# do it!
installXMCDAService()
{
  checkEnvironment
  addUsers
  createDirectories
  buildSpooler "$ROOT"
  installXMCDADaemon
  installBinaries
  installCGIGenericHandlers

  echo -n "Testing services: they should be up (waiting 5 secs. before testing)" 
  for i in $(seq 10); do echo -n '.'; sleep 0.5; done
  echo
  svstat /service/"${ROOT_NAME}"
  #svstat /service/XMCDA-test
  echo "Check the logs if appropriate, here is what readproctitle tells:"
  pgrep -a readproctitle
}
