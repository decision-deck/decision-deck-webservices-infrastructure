#! /bin/bash
uninstallService_common()
{
    local NAME AUTHORS EMAIL VERSION SVN GIT HTTP FILES CMD_LINE
    local CONFIG="$1"
    local DAEMONS_DIR="$2"
    source "$CONFIG"
    local SERVICE=$NAME

    local SERVICE_DIR="${DAEMONS_DIR:?}/${SERVICE:?}"
    msg "Uninstalling $SERVICE: deleting $SERVICE_DIR"
    rm -rf "${SERVICE_DIR}"
}

uninstallCGI()
{
    local NAME AUTHORS EMAIL VERSION SVN GIT HTTP FILES CMD_LINE
    local CONFIG="$1"
    source "$CONFIG"
    local SERVICE="$NAME"
    local python_cgi_module="handle_XMCDA_request_${ROOT_SUFFIX}"

    msg "Uninstalling $SERVICE: removing $CGI_BIN/$SERVICE.py"

    rm "${CGI_BIN:?}/${SERVICE:?}.py"
}

uninstallHOME()
{
    local NAME AUTHORS EMAIL VERSION SVN GIT HTTP FILES CMD_LINE
    local CONFIG="$1"
    source "$CONFIG"

    msg "Uninstalling $NAME: removing $WS_DIR/$NAME"
    rm -rf "${WS_DIR:?}/${NAME:?}"
}

#
# Deactivates the service: stops the service in daemontools
# Parameter:
#   1: the service's configuration file
deactivateService()
{
    local CONFIG="$1"
    source "$CONFIG"
    local SERVICE=$NAME
    local root_service_name="${ROOT_SUFFIX:?}-${NAME:?}"

    if [ ! -h /service/"${root_service_name}" ]; then
        msg "deactivate: ${root_service_name}: already deactivated"
    else
        cd /service/"$root_service_name"
        rm  /service/"$root_service_name"
        svc -dx . log
        cd "$OLDPWD"
        msg "deactivate: ${root_service_name}: deactivated"
    fi
}

# argument:
# 1: the file containing the service's description
uninstallService()
{
    local CONFIG="$1"
    checkDescription "$CONFIG" || fail "Exiting"

    # Uninstall the production service
    deactivateService "$CONFIG"
    uninstallService_common "$CONFIG" "$DAEMONS_ROOT"
    uninstallCGI "$CONFIG"
    uninstallHOME "$CONFIG"
}
