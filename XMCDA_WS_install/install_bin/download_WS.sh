#! /bin/bash

createWSdirectories()
{
  echo -n "Creating $WS_DIR if necessary... " >&2
  if [  ! -e "$WS_DIR" ]; then
      mkdir -p "$WS_DIR";
  fi;
  echo "done.  TODO: chown !!!" >&2
}

# expects:
# NAME, GIT, REV, SUBDIR (optional)
# WS_PROD, WS_TEST
download_GIT()
{
    msg "Downloading $NAME (GIT)"

    # git archive does not work on github: clone & extract
    local tmp_dir
    # --tmpdir does not exist everywhere...
    tmp_dir=$(mktemp -d -t 'install.XMCDA.service.XXXXXXXXXX')
    msg "  Retrieving $GIT"

    # We clone it once, and use git archive on the clone
    git clone "$GIT" "$tmp_dir" -q
    cd "$tmp_dir"
	local tar_opts=("--strip" "1" "$SUBDIR")
	if [ -z "$SUBDIR" ]; then
		tar_opts=("")
	fi
    msg "  Moving ${SUBDIR:-everything} into '$WS_DIR/$NAME' (rev:${REV:-HEAD})"
    mkdir -p "$WS_DIR/$NAME/"
    if [ -z ${REV+x} ]; then
        msg "WARNING: REV unset, using HEAD"
    fi
    git checkout -q "${REV:-HEAD}"
    tar --dereference -cf - "${SUBDIR:-.}" \
        | tar xf - -C "$WS_DIR/$NAME" "${tar_opts[@]}"

    # cleanup
    cd "$OLDPWD"
    rm -rf "$tmp_dir"

}

#
# expects:
# NAME, SVN, REV,
# WS_DIR
download_SVN()
{
    local rev
    [ -z "${REV:-}" ] && rev="" || rev="-r $REV"
    $SVN_BIN checkout "$rev" "$SVN" "$WS_DIR/$NAME"
}

#
# expects:
# NAME, HTTP
# WS_DIR
download_HTTP()
{
    local rev
    #echo "#### [$IGNORE_PROD] [$IGNORE_TEST]"
    mkdir -p "$WS_DIR/$NAME/"
    filename=${HTTP##*/}
    wget --no-check-certificate -O "$WS_DIR/$NAME/$filename" "${HTTP}"
    handleCompressedFile "$WS_DIR/$NAME/$filename" "$WS_DIR/$NAME/"
}

download()
{
    local CONFIG="$1"
    local NAME AUTHORS EMAIL VERSION SVN GIT HTTP FILES CMD_LINE
    local REV
    . "$CONFIG"
    if [ -n "${GIT:-}" ]; then
        download_GIT
    elif [ -n "${SVN:-}" ]; then
        download_SVN
    elif [ -n "${HTTP:-}" ]; then
        download_HTTP
    else
        echo "Only GIT, SVN or HTTP are supported for the moment being" >&2
        return -1
    fi
}

downloadService()
{
    local DESCRIPTION_FILE=$1
    createWSdirectories
    checkDescription "$DESCRIPTION_FILE" || fail "Exiting"
    download "$DESCRIPTION_FILE"
}
