<!-- xsltproc transformation.xslt file.xml > output.xml -->  
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns="http://www.decision-deck.org/2021/XMCDA-4.0.0">

    <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <!-- root element -->
    <xsl:template match="/*">
        <xsl:element name="xmcda" namespace="http://www.decision-deck.org/2021/XMCDA-4.0.0" >
            <!-- copy attributes and overwrite the schema location -->
            <xsl:apply-templates select="@*"/>
            <xsl:attribute name="xsi:schemaLocation">http://www.decision-deck.org/2021/XMCDA-4.0.0 http://www.decision-deck.org/xmcda/_downloads/XMCDA-4.0.0.xsd</xsl:attribute>
            <xsl:apply-templates select="*[name()='alternatives']"/>
            <xsl:apply-templates select="*[name()='alternativesSets']"/>
            <xsl:apply-templates select="*[name()='criteria']"/>
            <xsl:apply-templates select="*[name()='criteriaSets']"/>
            <xsl:apply-templates select="*[name()='performanceTable']"/>
            <xsl:apply-templates select="*[name()='categories']"/>
            <xsl:apply-templates select="*[name()='categoriesSets']"/>
            <xsl:apply-templates select="*[name()='alternativesAssignments']"/>
            <xsl:apply-templates select="*[name()='alternativesCriteriaValues']"/>
            <xsl:apply-templates select="*[name()='alternativesLinearConstraints']"/>
            <xsl:apply-templates select="*[name()='alternativesMatrix']"/>
            <xsl:apply-templates select="*[name()='alternativesValues']"/>
            <xsl:apply-templates select="*[name()='alternativesSetsLinearConstraints']"/>
            <xsl:apply-templates select="*[name()='alternativesSetsMatrix']"/>
            <xsl:apply-templates select="*[name()='alternativesSetsValues']"/>
            <xsl:apply-templates select="*[name()='criteriaFunctions']"/>
            <xsl:apply-templates select="*[name()='criteriaHierarchy']"/>
            <xsl:apply-templates select="*[name()='criteriaLinearConstraints']"/>
            <xsl:apply-templates select="*[name()='criteriaMatrix']"/>
            <xsl:apply-templates select="*[name()='criteriaScales']"/>
            <xsl:apply-templates select="*[name()='criteriaThresholds']"/>
            <xsl:apply-templates select="*[name()='criteriaValues']"/>
            <xsl:apply-templates select="*[name()='criteriaSetsHierarchy']"/>
            <xsl:apply-templates select="*[name()='criteriaSetsLinearConstraints']"/>
            <xsl:apply-templates select="*[name()='criteriaSetsMatrix']"/>
            <xsl:apply-templates select="*[name()='criteriaSetsValues']"/>
            <xsl:apply-templates select="*[name()='categoriesLinearConstraints']"/>
            <xsl:apply-templates select="*[name()='categoriesMatrix']"/>
            <xsl:apply-templates select="*[name()='categoriesProfiles']"/>
            <xsl:apply-templates select="*[name()='categoriesValues']"/>
            <xsl:apply-templates select="*[name()='categoriesSetsLinearConstraints']"/>
            <xsl:apply-templates select="*[name()='categoriesSetsMatrix']"/>
            <xsl:apply-templates select="*[name()='categoriesSetsValues']"/>
            <xsl:apply-templates select="*[name()='programParameters']"/>
            <xsl:apply-templates select="*[name()='programExecutionResult']"/>
        </xsl:element>
    </xsl:template>

    <!-- "Identity" templates -->
    <!-- Move all elements into the 4.0.0 namespace: in 3.x only the root
         element is in the XMCDA-3.x namespace -->
    <xsl:template match="*">
        <!-- Using match="node()", above, would select text nodes, 
             which have no local-name -->
        <xsl:element name="{local-name()}" namespace="http://www.decision-deck.org/2021/XMCDA-4.0.0">
            <xsl:apply-templates select="@* | node()"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="@*">
        <xsl:copy>
            <xsl:apply-templates />
        </xsl:copy>
    </xsl:template>

    <!-- transform v3 to v4 -->
    <!-- xxxValues → xxxValues -->
    <xsl:template match="alternativeValue">
        <alternativeValues>
            <xsl:copy-of select="@*"/><xsl:apply-templates select="node()" />
        </alternativeValues>
    </xsl:template>
    <xsl:template match="criterionValue">
        <criterionValues>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
        </criterionValues>
    </xsl:template>
    <xsl:template match="categoryValue">
        <categoryValues>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
        </categoryValues>
    </xsl:template>
    <!-- xxxSetValues → xxxSetValues -->
    <xsl:template match="alternativesSetValue">
        <alternativesSetValues>
            <xsl:copy-of select="@*"/><xsl:apply-templates select="node()" />
        </alternativesSetValues>
    </xsl:template>
    <xsl:template match="criteriaSetValue">
        <criteriaSetValues>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
        </criteriaSetValues>
    </xsl:template>
    <xsl:template match="categoriesSetValue">
        <categoriesSetValues>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
        </categoriesSetValues>
    </xsl:template>
    <!-- criterionXxx -->
    <xsl:template match="criterionFunction">
        <criterionFunctions>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
        </criterionFunctions>
    </xsl:template>
    <xsl:template match="criterionThreshold">
        <criterionThresholds>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
        </criterionThresholds>
    </xsl:template>
    <xsl:template match="criterionScale">
        <criterionScales>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
        </criterionScales>
    </xsl:template>

    <xsl:template match="parameter">
        <programParameter>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
	</programParameter>
    </xsl:template>

    <!-- keep comments -->
    <xsl:template match="@* | node()" priority="-1">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
