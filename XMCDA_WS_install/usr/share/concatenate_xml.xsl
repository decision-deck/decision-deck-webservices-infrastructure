<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--
  Concatenates the XML file processed by this transformation with the
  one supplied as parameters.

  Example call w/ xsltproc:
    xsltproc - -stringparam with  "$(pwd)/file1.xml" \
             - -stringparam with2 "$(pwd)/file2.xml" \
             concatenate_xml.xslt file.xml

  Example call w/ the (Java) Saxon XSLT and XQuery Processor:
    java -jar saxon9he.jar file.xml concatenate_xml.xslt \
              with="$(pwd)/file-1.xml with2="$(pwd)/file-2.xml"

  NB: It is recommanded to supply the full path of the concatenated
      files most of the time, since the function doxument() uses
      the base URI of the stylesheet to resolve relative paths.
  -->

  <xsl:output omit-xml-declaration="no" indent="yes" encoding="utf-8"/>
  <xsl:strip-space elements="*"/>
  <xsl:param name="with"  select="a_file.xml"/>
  <xsl:param name="with2" select="a_file.xml"/>
  <xsl:template match="/*">
    <xsl:copy select='.'>
      <xsl:copy-of select="*"/>
      <xsl:copy-of select="document($with)/*/*"/>
      <xsl:copy-of select="document($with2)/*/*"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
