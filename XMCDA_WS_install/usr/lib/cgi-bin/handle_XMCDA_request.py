#!/usr/bin/python
# -*- coding: utf-8 -*-
# handle_XMCDA_request.py
"""
This module offers the three functionalities needed for an XMCDA service:
- hello, which gives a short information about the service
- submitProblem, for submitting a problem to the appropriate service. This
  method builds and returns a string identifying the request.
- requestSolution, for getting back the solution of a problem.

Adapted from the cgi-scripts made by Raymond Bisdorff and Patrick Meyer,
April 2008.
"""
import base64
import os, time
import hashlib
import httplib, urllib
import smtplib
from email.mime.text import MIMEText

# Adapt this to your needs: it is used by handle_empty_result() and
# handle_exception() to notify the maintainers that a problem occured.
# Note: if your MTA is not on localhost:25, adapt the function
# send_alert_email() to your needs (see comments in this method)
#
# if MAIL_FROM is None or empty, no email will be sent
# MAIL_FROM='XMCDA_handler-NO-REPLY@example.org'
MAIL_FROM=None
MAINTAINERS=['john.doe@example.org',
             'jane.roe@example.org']

# Change this to match your installation
# This is displayed within hello()
# Example:
# WELCOME_MSG="Welcome to the Decision Deck Server of IMT Atlantique / Lab-STICC - CNRS UMR 6285"
WELCOME_MSG="Welcome"

# CACHE_URL='127.0.0.1:8080' # ip:port
CACHE_URL=None

#
XMCDA_WS_ENV_NAME=''

# Return status
AVAILABLE_STATUS=0
NOT_AVAILABLE_STATUS=1
ERROR_STATUS=2

#
spool_dir='XMCDA_ROOT/JobSpooler' #'/var/XMCDA/JobSpooler'
solutions_dir='XMCDA_ROOT/Solutions' #'/var/XMCDA/Solutions'

#
BASE_64_OUTPUT_SUFFIX_MARKER='.base-64-encoded'

## hello port implementation
def hello(service, version, authors):
    """
    Returns the hello message.
    """
    return {"message":
            """
**************************************************************************
%(welcome)s
This is the web-service: %(service)s
  version %(version)s
  by %(authors)s
**************************************************************************
""" % { 'welcome': WELCOME_MSG,
        'service': service, 'version': version, 'authors': authors}}

def _cache_ticket_for_submitted_problem(service, ticket, kw):
    """
    Helper function for submitProblem()

    If there is a cache, ask it for an already submitted identical problem.

    Returns None if there is no cache, or if any error happened while
    requesting the cache for a ticket number.
    """
    global CACHE_URL, XMCDA_WS_ENV_NAME
    if CACHE_URL is None:
        return None

    # hashcode: service name + SHA512 of all files, sorted alphabetically
    hashcode = service
    hashcode += ':' + XMCDA_WS_ENV_NAME
    log_suffix = kw.get('XMCDA_WS_LOG_SUFFIX')
    if log_suffix is not None:
        if log_suffix.endswith('no-cache'):
            return None
        hashcode += '@' + log_suffix
    hashcode+=':'

    sorted_filenames = kw.keys()
    sorted_filenames.sort()
    for k in sorted_filenames:
        hashcode += hashlib.sha512(kw[k]).hexdigest()

    # If there is a cache and this calculation has already been computed
    # (or if it has already been submitted), let's get the corresponding
    # ticket number
    conn = None
    cached_ticket = None
    try:
        conn = httplib.HTTPConnection(CACHE_URL)
        params = urllib.urlencode({'ticket': ticket, 'hashcode': hashcode})
        headers = {"Content-type": "application/x-www-form-urlencoded",
                   "Accept": "text/plain"}
        conn.request("POST", "/ticket", params, headers)
        response = conn.getresponse()
        cached_ticket = response.read()
    except:
        pass
    finally:
        if conn is not None:
            try: conn.close()
            except: pass
    return cached_ticket

## submitProblem port implementation
def submitProblem(service, **kw):
    """
    Submits a problem and return the ticket identifying the corresponding job
    """
    import random, string, codecs

    try: # try saving of submitted problem file
        chars = string.letters+string.digits
        dirname = None
        while dirname is None or os.path.exists(dirname):
            ticket = ''.join([random.choice(chars) for i in range(16)])
            dirname = os.path.join(spool_dir,
                                    '%s-%s'%(service, ticket))

        cached_ticket=_cache_ticket_for_submitted_problem(service, ticket, kw)
        if cached_ticket is not None and cached_ticket != ticket:
            msg = 'The problem submission was successful!'
            return { 'message': msg, 'ticket': cached_ticket }

        os.mkdir(dirname)
        os.chmod(dirname, 0770)

        for k,v in kw.items():
            if k=='XMCDA_WS_LOG_SUFFIX':
                with open(os.path.join(dirname, '___FOR_XMCDA_SERVICE_LOG_SUFFIX'), 'w') as _log_suffix:
                    _log_suffix.write(v)
                    continue
            if '.' not in k:
                k = k + '.xml'
            filename = os.path.join(dirname, k)
            enc = 'UTF-8'
            fileOUT = codecs.open(filename,'w',encoding=enc)
            if v is not None:
                if type(v) is unicode:
                    fileOUT.write(v)
                else:
                    fileOUT.write(unicode(v,enc))
            fileOUT.close()

        # Create the marker to indicate to the spool we've finished
        # NB: we really mean + here, not os.path.join
        open(dirname+'___FOR_XMCDA_SERVICE_SPOOL_HANDLE_ME_IM_READY', 'w').close()

        msg = 'The problem submission was successful!'
        return { 'message': msg, 'ticket': ticket }
    except Exception, e:
        # import traceback
        # tb = traceback.format_exc()
        try:
            filename is None or os.unlink(filename)
        except:
            pass
        return { 'message': "The problem submission was unsuccessful",
                 'ticket': ticket, }
                 # 'exception': str(e), 'exception-traceback': tb, }

def requestSolution(service, **kw):
    answer = {}
    ticket = 'unknown' # in case it could not be retrieved
    status = NOT_AVAILABLE_STATUS
    try: # check availability of solution file
        ticket = kw['ticket']
        dirname = os.path.join(solutions_dir, '%s-%s'%(service, ticket))

        answer_files = ()
        idx = 0
        while not os.path.isfile(os.path.join(dirname, 'FINISHED')) and idx<20:
            time.sleep(0.5) # wait at most 10s
            idx+=1

        # the file FINISHED is a simple marker indicating that the
        # computation is finished (see 'service' in
        # XMCDA_WS_install/daemons/common/).  Since the results may be
        # asked more than once (either because of network problems, or
        # because the cache has returned this ticket to different
        # requesters, it should not be deleted
        if os.path.isfile(os.path.join(dirname, 'FINISHED')):
            answer_files = os.listdir(dirname)
            answer_files.remove('FINISHED') # do not transmit the marker!
            status=AVAILABLE_STATUS

        if status == AVAILABLE_STATUS:
            for filename in answer_files:
                filepath=os.path.join(dirname, filename)
                if os.path.isfile(filepath):
                    fileIN = open(filepath,'r')
                    answer_key = filename
                    if answer_key.endswith('.xml'):
                        answer_key = answer_key[:-4]
                        answer[answer_key] = fileIN.read()
                    else:
                        # not xml, base64-encode it
                        answer_key += BASE_64_OUTPUT_SUFFIX_MARKER
                        answer[answer_key] = base64.encodestring(fileIN.read())
                    fileIN.close()

            if len(answer) == 0:
                handle_empty_result(service, ticket, answer)

    except Exception, e:
        status = ERROR_STATUS
        import traceback
        handle_exception(service, ticket, traceback.format_exc(), answer)

    answer['ticket'] = ticket
    answer['service-status'] = status
    return answer

def handle_empty_result(service, ticket, answer):
    """
    Inserts an xmcda-formatted 'messages' in the answer, and sends an email
    to alert the maintainers.
    """
    content="""<?xml version='1.0'?>
<xmcda:XMCDA xmlns:xmcda="http://www.decision-deck.org/2012/XMCDA-2.2.1"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.decision-deck.org/2012/XMCDA-2.2.1 http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.1.xsd">
  <methodMessages>
    <errorMessage>
      <text>Execution terminated, but no result were produced:  you probably hit a bug in the service.</text>
    </errorMessage>
  </methodMessages>
</xmcda:XMCDA>
"""
    answer["messages"]=content

    # and send an alert
    msg="""
A service was called but there is no file in the solutions directory.
The service has probably encountered a bug that made it abruptly fail, you may want to check the logs.

  service: %s
  ticket:  %s
  handler: %s
"""%(service, ticket, __name__)
    send_alert_email('%s: empty solutions directory'%service, msg)

def handle_exception(service, ticket, traceback, answer):
    """
    Inserts an xmcda-formatted 'service-failure' in the answer, and
    sends an email to alert the maintainers.
    """
    content="""<?xml version='1.0'?>
<xmcda:XMCDA xmlns:xmcda="http://www.decision-deck.org/2012/XMCDA-2.2.1"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.decision-deck.org/2012/XMCDA-2.2.1 http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.1.xsd">
  <methodMessages>
    <errorMessage>
      <text>The service handler has encountered an error.  You may have got back all, part of or none of the expected results (nb of results sent: %i).  An email has been automatically sent to the maintainers. We apologize for the inconvenience, please retry a bit later when the problem is solved.
      </text>
    </errorMessage>
  </methodMessages>
</xmcda:XMCDA>
"""%len(answer)
    answer["service-failure"]=content # do not override messages.xml, if any

    # and send an alert
    msg="""handle_XMCDA_request raised an exception.

  service: %s
  ticket:  %s
  handler: %s

%s
"""%(service, ticket, __name__, traceback)
    send_alert_email('%s: exception raised'%__name__, msg)

def send_alert_email(subject, content):
    """"
    Sends an email to the MAINTAINERS with the supplied subject and content.
    """
    if MAIL_FROM is None or MAIL_FROM=='':
        return
    msg = MIMEText(content)
    msg['From'] = MAIL_FROM
    msg['To'] = ', '.join(MAINTAINERS)
    msg['Subject'] = subject
    try:
        # Modify the following line if your MTA is not on localhost:25
        # cf. http://docs.python.org/library/smtplib.html
        s = smtplib.SMTP('localhost')
        s.sendmail(MAIL_FROM, MAINTAINERS, msg.as_string())
    except:
        # couldn't send mail... what could we do?
        pass


## CGI Python script
if __name__ == '__main__':
    import os, sys
    from ZSI import dispatch
    dispatch.AsCGI(rpc=True)
