#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
import handle_XMCDA_request as xmcda

SERVICE='generalWeightedSum'
AUTHORS=u'Boris Leistedt, 2009'
VERSION='1.0'

hello           = lambda:      xmcda.hello(SERVICE, VERSION, AUTHORS)
submitProblem   = lambda **kw: xmcda.submitProblem(SERVICE, **kw)
requestSolution = lambda **kw: xmcda.requestSolution(SERVICE, **kw)

## CGI Python script
if __name__ == '__main__':
    from ZSI import dispatch
    dispatch.AsCGI(rpc=True)
