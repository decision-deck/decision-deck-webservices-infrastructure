#! /bin/bash

# xmcda_concatenate <directory> <file-1.xml:file-2.xml:[file-3.xml:]result.xml
#   where file-N.xml are input files and reult.xml the output,
#   and where result.xml can be the same as one of the input files
xmcda_concatenate()
(
    set -x
    dir=$1
    shift
    cd "$dir"
    XSL="${DAEMONS_ROOT:?}/share/concatenate_xml.xsl"
    declare -a inputs
    declare -A outputs
    cd "$dir"
    for arg in "$@"; do
        IFS=':' read -ra array <<< "$arg"

        if [ ${#array[@]} -lt 3 ] || [ ${#array[@]} -gt 4 ]; then
            echo "Invalid parameter ${arg}: 3 or 4 elements expected" >&2
            continue
        fi
        
        output="${array[${#array[@]}-1]}"
        temp_output=$(mktemp --tmpdir=. "${output}.XXX")
        unset 'array[${#array[@]}-1]'

        if [ ! -e "${array[0]}" ]; then
            unset 'array[0]'
            array=( "${array[@]}" )  # re-index array
        fi
        
        if [ ${#array[@]} -eq 1 ]; then
            # there is only one file left, simply rename it
            cp "${array[0]}" "${temp_output}"
        elif ! xsltproc --nonet --novalid -o "${temp_output}" \
             --stringparam with "$(pwd)/${array[1]}" \
             ${array[2]+--stringparam with2 "$(pwd)/${array[2]}"} \
             "${XSL}" "${array[0]}";
        then
            echo "Could not concatenate ${array[*]}" >&2
            continue
        fi
        inputs+=( "${array[@]}" )
        outputs+=(["${temp_output}"]="${output}")
    done
    rm -f "${inputs[@]}"
    # leave inputs as is, we choose not to delete them for now
    # Note: some may be overwritten when renaming the outputs, below
    for output in "${!outputs[@]}"; do
        mv "${output}" "${outputs[$output]}"
    done
)

xmcda_concatenate "$@"
