#! /bin/bash

# xmcda_extract <directory> <input.xml:output.xml:xpath> <input.xml:...> ...
#   where input.xml and output.xml can be the same file
xmcda_extract()
(
    #set -x
    dir=$1
    shift
    cd "$dir"
    XSL="xmcda_filter.xsl"
    declare -a inputs
    declare -A outputs
    for arg in "$@"; do
        IFS=':' read -ra array <<< "$arg"
        if [ ${#array[@]} -ne 3 ]; then
            echo "Invalid parameter ${arg}: 3 elements expected" >&2
            continue
        fi

        cd "$dir"
        input="${array[0]}"
        output="${array[1]}"
        tag="${array[2]}"

        cat > "${XSL}" <<EOF
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:template match="/*[local-name()='XMCDA']">
        <xsl:copy>
            <xsl:copy-of select="${tag}" />
        </xsl:copy>
    </xsl:template>
    <xsl:template match="*"/>
</xsl:stylesheet>
EOF
        temp_output=$(mktemp --tmpdir=. "${output}.XXX")
        if ! xsltproc --nonet --novalid -o "${temp_output}" \
             "${XSL}" "${input}";
        then
            echo "Could not split ${input} -> ${output}" >&2
            rm -f "${XSL}" "${temp_output}"
            continue
        fi
        inputs+=("${input}")
        outputs+=(["${temp_output}"]="${output}")
    done
    rm -f "${XSL}"
    # leave inputs as is, we choose not to delete them for now
    # Note: some may be overwritten when renaming the outputs, below
    for temp_output in "${!outputs[@]}"; do
        mv "${temp_output}" "${outputs[$temp_output]}"
    done
)

xmcda_extract "$@"
