#! /bin/bash -x
#
# Converts all files in directory xmcda v2 <> xmcda v3
#

if [ "${0##*/}" = "v2-to-v3-dir.sh" ]; then
  from=v2
  to=v3
elif [ "${0##*/}" = "v3-to-v2-dir.sh" ]; then
  from=v3
  to=v2
else
    echo "Invalid script name, use the aliases v2-to-v3-dir or v3-to-v2-dir instead" >&2
    exit 1
fi

script_path="${0%/*}"

usage()
{
    echo "Usage: ${0} <input dir> <output dir> [options]" >&2
    echo "Options are passed as-is to the XMCDA conversion program" >&2
}

if [ $# -lt 2 ]; then usage; exit -1; fi
if [ "$1" = "-h" -o "$1" = "--help" ]; then usage; exit 0; fi

if ! [ -d "${1}" -a -r "${1}" -a -x "${1}" ]; then
    echo "${1} is not a directory, it does not exist or it is not readable" >&2
    exit 1
fi

mkdir -p "${2}"
if ! [ -d "${2}" -a -r "${2}" -a -w "${2}" -a -x "${2}" ]; then
    echo "Output directory ${2} does not exist, it is not readable or it could not be created" >&2
    exit 1
fi

input=$(realpath "${1}")
output=$(realpath "${2}")
shift 2

# No need to explicitly convert 2.x to 2.2.2
# XMCDA-java handles all v2: 2.0.0, 2.1.0, 2.2.0, 2.2.1 and 2.2.2
#sed -i -e 's|2009/XMCDA-2\.0\.0|2017/XMCDA-2.2.2|g' "$@"
#sed -i -e 's|2009/XMCDA-2\.1\.0|2017/XMCDA-2.2.2|g' "$@"
#sed -i -e 's|2012/XMCDA-2\.2\.0|2017/XMCDA-2.2.2|g' "$@"
#sed -i -e 's|2012/XMCDA-2\.2\.1|2017/XMCDA-2.2.2|g' "$@"

# but for v3, the converter asks for the latest, namely 3.0.2
sed -i -e 's|201[36]/XMCDA-3\.0\.[01]|2017/XMCDA-3.0.2|g' "${input}"/*.xml

# Collect the xml files to be converted
# and copy non-xml files into the destination directory, if any
# (there may be non-xml files in a program's IO, such as csv, images, …)
cd "${input}"
idx=0
while read -d '' file; do
    echo $file
    if [ ${file##*.} = 'xml' ]; then
        infiles[$idx]="${input}/${file}"
        outfiles[$((idx++))]="${output}/${file}"
    fi
    # ignore other files
done < <(find . -type f -maxdepth 1 -print0)
cd -

"${0%/*}"/xmcda_convert.sh \
    --$from "${infiles[@]}" --$to "${outfiles[@]}" "$@"
