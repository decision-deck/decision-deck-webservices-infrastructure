#! /bin/bash -x
# post-processes outputs for XMCDA v3 services

# Remember: this is for XMCDA v3 service, the final outputs are v3.x
# but program are v2.x or v3.x, as stated in the $2

# Arguments:
# 1: the directory where outputs are
# 2: the XMCDA version handled by this service, x.y.z as in: 2.2.1

OUTPUTS_DIR="$1"
XMCDA_VERSION="$2"

IFS='.' read -r -a xmcda <<< "${XMCDA_VERSION}"
if [ "${xmcda[0]}" = 2 ]; then
    # Conversion v2.x -> v3

    cd "$1"
    for f in *.xml; do
        cp -p "$f" "$1/output_${f%.xml}".xml_v2
    done
    cd -
    
    OUTPUTS_DIR_v3="${OUTPUTS_DIR}.v3"  #"$(mktemp --tmpdir -d post-process-outputs.sh.v3.XXXX)"
    mkdir -p "${OUTPUTS_DIR_v3}"
    time \
      "${DAEMONS_ROOT}"/bin/v2-to-v3-dir.sh "${OUTPUTS_DIR}" "${OUTPUTS_DIR_v3:?}" --input-tags-only

    cp -a "${OUTPUTS_DIR_v3:?}"/. "${OUTPUTS_DIR}"/
    /bin/rm -r "${OUTPUTS_DIR_v3:?}"

fi
if [ "${xmcda[0]}" = 3 ]; then
    # 3.x to 3.1.1
    sed -e 's|201[0-9]/XMCDA-3\.[0-9]\.[0-9]|2019/XMCDA-3.1.1|g' \
        -i "${OUTPUTS_DIR}"/*.xml
    sed -e 's|XMCDA-3\.[0-9]\.[0-9]\.xsd|XMCDA-3.1.1.xsd|g' \
        -i "${OUTPUTS_DIR}"/*.xml
fi

IFS=',' read -ra XMCDA_EXTRACT_ARGS <<< "${XMCDA_EXTRACT_ARGS}"
"${DAEMONS_ROOT}"/bin/xmcda_extract.sh "$1" "${XMCDA_EXTRACT_ARGS[@]}"
find "$1" -type f -exec chmod 640 {} \;
