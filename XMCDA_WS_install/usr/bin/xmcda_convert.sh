#! /bin/bash
# Invokes the conversion of XMCDA v2/v3
#
time "${0%/*}"/ng \
     org.xmcda.converters.v2_v3.XMCDAConverter \
     "$@"
if [ $? -ne 0 ]; then
    echo "Failed to invoke nailgun, falling back on pure java" >&2 
    time "${prefix}"/java-1.8/bin/java \
         -cp ${prefix}/bin/XMCDA-java-latest-dev.jar \
         org.xmcda.converters.v2_v3.XMCDAConverter \
        "$@"
fi
