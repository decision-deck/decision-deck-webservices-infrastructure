#! /bin/bash -x
# pre-process inputs for XMCDA v3 & v4 services
# The files are converted / modified in-place

# Context: this is for XMCDA v3 & v4 services, so inputs are v3.x or v4.x
# but programs are v2.x or v3.x/v4.x, as stated in the 2nd param $2

# Arguments:
# 1: the directory where inputs are
# 2: the XMCDA version handled by this service, x.y.z as in: 2.2.1

# environment variables
#
# - DAEMONS_ROOT
#
# - XMCDA_v3_CONCATENATE_INPUTS: a comma-separated list of files, all of except
#       the last one are to be concatenated into a file named after the last
#       element of the list.  It may be empty or unset.
#
# - XMCDA_v2_INPUTS_CONVERT: extra parameters for the v3>v2 conversion script.
#       It may be empty or unset.
#
# - XMCDA_v2_REMOVE_VALUES: whether <values> should be removed from XMCDA v2
#       after the conversion (see possible values, below).

INPUTS_DIR="$1"
XMCDA_VERSION="$2"

echo "target XMCDA version: ${XMCDA_VERSION}" >&2

IFS='.' read -r -a xmcda <<< "${XMCDA_VERSION}"

# we receive: XMCDA v3 or v4

# If the webservice does not speak XMCDA v4, then convert to v3 since what follows wil handle
# xmcda v3→v2 or v3→v3.x if needed
if [ "${xmcda[0]}" != 4 ]; then
    "${DAEMONS_ROOT}"/bin/v4-to-v3.sh "${INPUTS_DIR}"
fi

# from now on, we expect XMCDA V3
if [ "${xmcda[0]}" = 2 ]; then
  IFS=',' read -ra XMCDA_v3_CONCATENATE_INPUTS <<< "${XMCDA_v3_CONCATENATE_INPUTS}"
  "${DAEMONS_ROOT}"/bin/xmcda_concatenate.sh "${INPUTS_DIR}" "${XMCDA_v3_CONCATENATE_INPUTS[@]}"

  #
  # Conversion v3.x -> v2.2.2
  time "${DAEMONS_ROOT}"/bin/v3-to-v2-dir.sh \
      "${INPUTS_DIR}" "${INPUTS_DIR}.v2" ${XMCDA_v2_INPUTS_CONVERT} --no-values --input-tags-only
  cp -Rp "${INPUTS_DIR}.v2"/* "${INPUTS_DIR}"/
  /bin/rm -r "${INPUTS_DIR}.v2"

  # finally convert XMCDA v2.2.2 to the expected v${XMCDA_VERSION}
  if [ "${xmcda[1]}" = 0 ] || [ "${xmcda[1]}" = 1 ]; then 
      xmcda_year=2009  # XMCDA v2.0.0 or XMCDA v2.1.0
  elif [ "${XMCDA_VERSION}" = "2.2.0" ]; then
      xmcda_year=2012
  elif [ "${XMCDA_VERSION}" = "2.2.1" ]; then
      xmcda_year=2012
  elif [ "${XMCDA_VERSION}" = "2.2.2" ]; then
      xmcda_year=2017
  elif [ "${XMCDA_VERSION}" = "2.2.3" ]; then
      xmcda_year=2019
  fi

  sed -e "s|2019/XMCDA-2.2.3|${xmcda_year}/XMCDA-${XMCDA_VERSION}|g" \
      -i "${INPUTS_DIR}"/*.xml

  # Unset or 'y': remove all <values>
  # 'n': leave files as-is (the v3→v2 conversion (above) uses <values>)
  # otherwise: remove <values> in xml files in $XMCDA_v2_REMOVE_VALUES
  #            (separator is '|')
  if [ "${XMCDA_v2_REMOVE_VALUES:-y}" = "y" ]; then
      sed -E 's|</?values>||g' -i "${INPUTS_DIR}"/*.xml
  elif [ "${XMCDA_v2_REMOVE_VALUES}" != "n" ]; then
      IFS="|" read -r -a xml_files <<< "${XMCDA_v2_REMOVE_VALUES}"
      for xml_file in "${xml_files[@]}"; do
          sed -E 's|</?values>||g' -i "${INPUTS_DIR}/${xml_file}"
      done
  fi
  
  find "${INPUTS_DIR}" -maxdepth 1 -name '*.xml' -print0 | while read -d '' -r xml_file; do
      xml_file="${xml_file##*/}"
      cp -p "${INPUTS_DIR}/$xml_file" "$3/input_${xml_file%.xml}".xml_v2
  done
fi

if [ "${xmcda[0]}" = 3 ]; then
  # convert v3.x to the expected XMCDA_VERSION
  if [ "${XMCDA_VERSION}" = "3.0.0" ]; then
      xmcda_year=2013
  elif [ "${XMCDA_VERSION}" = "3.0.1" ]; then
      xmcda_year=2016
  elif [ "${XMCDA_VERSION}" = "3.0.2" ]; then
      xmcda_year=2017
  elif [ "${XMCDA_VERSION}" = "3.1.0" ]; then
      xmcda_year=2018
  elif [ "${XMCDA_VERSION}" = "3.1.1" ]; then
      xmcda_year=2019
  fi
  sed -E "s|201[0-9]/XMCDA-3\.[0-9]+\.[0-9]+|${xmcda_year}/XMCDA-${XMCDA_VERSION}|g" \
      -i "${INPUTS_DIR}"/*.xml
fi

# last, if the webservice expects XMCDA v4, make sure each XMCDA v3 file is converted
if [ "${xmcda[0]}" == 4 ]; then
    "${DAEMONS_ROOT}"/bin/v3-to-v4.sh "${INPUTS_DIR}"
fi
