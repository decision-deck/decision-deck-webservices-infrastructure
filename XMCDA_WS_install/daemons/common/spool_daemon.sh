#!/bin/bash
# Sebastien Bigaret
# based on a script originally written by Raymond Bisdorff and Patrick Meyer

# This scripts expects the following variables to be set:
# DAEMONS_ROOT
# SPOOL_ROOT
# SERVICE
# A empty SERVICE corresponds to the main XMCDA spooler

# Markers
CGI_SCRIPT_MARKER="FOR_XMCDA_SERVICE_SPOOL_HANDLE_ME_IM_READY"
SERVICE_SPOOL_MARKER="FOR_SERVICE_SPOOL_HANDLE_ME_IM_READY"

# The FIFO used to read the notifications produced by inotifywait(1)
FIFO_DIR=/tmp/XMCDA_spool_daemons_fifos
mkdir -p $FIFO_DIR
FIFO_DIR=$(mktemp -d "${FIFO_DIR}/${SERVICE:-XMCDA-main-spooler}.XXX")
FIFO="${FIFO_DIR}/fifo-${SERVICE:-XMCDA-main-spooler}"
mkfifo $FIFO
chmod 700 $FIFO

#  clean up when stopping the spool server
cleanup()
{
    echo "Stopping the ${SERVICE:-XMCDA-main-spooler} spool daemon"
    kill $INOTIFYWAIT_PID
    #echo $FIFO_DIR
    rm -rf $FIFO_DIR
    #kill -s TERM $(jobs -p)
    exit
}

trap cleanup INT TERM KILL


#  start the spool logging
echo "Starting ${SERVICE:-XMCDA} spool daemon"


XMCDA_pool_handle_job()
{
  local log_suffix
  job_dir=$1

  # check: in case we are called in the inotify-loop while the
  # previous step (using find) already handled this one
  if [ -d "$job_dir" ]; then
      # Get the service name
      service=${job_dir##*/}
      service=${service%-*}
      # slice the ticket out of job_file
      ticket=${job_dir##*-}
      #ticket=${ticket%.xml}

      echo "Handling request for service: $service with ticket: $ticket"

      mkdir -p "${SPOOL_ROOT}/Problems/${service}"

      log_suffix=''
      if [ -r "${job_dir}/___FOR_XMCDA_SERVICE_LOG_SUFFIX" ]; then
	  read -r -t 1 log_suffix < "${job_dir}/___FOR_XMCDA_SERVICE_LOG_SUFFIX"
      fi
      log_suffix=${log_suffix:+"-$log_suffix"}

      mv "${job_dir}" "${SPOOL_ROOT}/Problems/${service}/${ticket}"
      # TODO test the $?
      # mark it as ready for the dedicated spooler
      echo $(date +"%s.%N")" $ticket SUBMIT" >> "${SPOOL_ROOT}/log/${service}/$(date +%Y%m%d).stat${log_suffix}"
      touch "${SPOOL_ROOT}/Problems/${service}/${ticket}___${SERVICE_SPOOL_MARKER}"
  fi
}


scan_XMCDA_pool()
{
  inotifywait -q -m -e create --format "%w%f" "$SPOOL_ROOT/JobSpooler/" > $FIFO &
  INOTIFYWAIT_PID=$!

  find "$SPOOL_ROOT/JobSpooler/" -mindepth 1 -maxdepth 1 -type d | while read job_dir;
    do
      if [ -d "$job_dir" ]; then
        XMCDA_pool_handle_job $job_dir;
    fi
  done

  while read f; do
    if [ "${f##*___}" = "${CGI_SCRIPT_MARKER}" ]; then
      rm $f
      XMCDA_pool_handle_job "${f%___*}"
    fi
  done < "$FIFO"
}

service_pool_handle_job()
{
  job_dir=$1
  ticket=${job_dir##*/}
  echo "[$ticket] service_pool_handle_job begin"

  # 1st, check that this is really for us, it may not be the case.
  # Example: $SERVICE=="example" and file is "example-provider-ticketNb"
  local svc

  svc=${job_dir%/*}
  svc=${svc##*/}

  if [ ! "$SERVICE" == "${svc}" ]; then
      # this is not for us! ignore it
      return;
  fi

  if [ ! -d "$job_dir" ]; then
      echo "[$ticket] not a directory, exiting ($job_dir)"
      return;
  fi
  echo "[$ticket] checking status"
  if [ ! -f ${job_dir}.lock -a ! -f ${job_dir}.started ]; then

      echo "[$ticket] Handling request"

      if [ -x $DAEMONS_ROOT/$SERVICE/service ]; then

          inputDir=$job_dir
          outputDir=${SPOOL_ROOT}/Solutions/${SERVICE}-${ticket}
          mkdir $outputDir
         if [ $? -ne 0 ]; then
            echo "[$ticket] Couldn't make dir $outputDir"
            id -a
            return
          fi
          chmod 750 $outputDir
          touch "${inputDir}.lock"
          touch "${inputDir}.started"
          # flock not available on Debian... hmmm...
          #flock -xn $inputFile.lock <cmd>
          # pwd: $DAEMONS_ROOT/$SERVICE
          echo "[$ticket] $DAEMONS_ROOT/$SERVICE/service $inputDir $outputDir ${inputDir}.lock ${ticket} &"
          $DAEMONS_ROOT/$SERVICE/service "${inputDir}" "${outputDir}" "${inputDir}.lock" "${ticket}" &
          # TODO check return status, make file solution.xml w/ error
          ## echo $job_file 'spooled !' >> $SPOOL_ROOT/spool-log
      else
          echo "[$ticket] No script for service $SERVICE (in $DAEMONS_ROOT). Ignoring"
      fi
  else
    if [ ! -f "${job_dir}.lock" -a -f "${job_dir}.started" ]; then
        echo "[$ticket] Ticket finished, removing $job_dir"
        rm -rf "${job_dir}" "${job_dir}.started"
    fi
  fi
  echo "[$ticket] service_pool_handle_job end"
}

scan_service_pool()
{
  mkdir -p "${SPOOL_ROOT}/Problems/${SERVICE}"
  mkdir -p "${SPOOL_ROOT}/log/${SERVICE}"
  # moved_to should be registered: if not, the directory which was moved
  # by the main XMCDA spooler into Problems/ is not registered and as a
  # consequence, the create events within that dir is not registered
  # NB: this was true when we were using '-r', I leave the comment as a
  #     reminder.
  # NB2: not using '-r' avoids race conditions which can cause events to be
  #      missed(see the man page)
  inotifywait -m -e create -e delete --format "%e %w%f" "$SPOOL_ROOT/Problems/${SERVICE}" > $FIFO &
  INOTIFYWAIT_PID=$!

  find "$SPOOL_ROOT/Problems/${SERVICE}" -mindepth 1 -maxdepth 1 -type d | while read job_dir;
  do
    service_pool_handle_job "${job_dir}"
  done

  while read event file; do
    #echo "while $event / $file"

    if [ "$event" = "CREATE" -a "${file##*___}" = "${SERVICE_SPOOL_MARKER}" ]; then
      echo "C $file"
      rm $file
      service_pool_handle_job "${file%___*}"
    fi
    if [ "$event" = "DELETE" -a "${file##*.}" = "lock" ]; then
      echo "D $f ${file%.lock}"
      service_pool_handle_job "${file%.lock}"
    fi
  done < "$FIFO"
}


# Launches the main loop
if [ "$SERVICE" == "" ]; then
  scan_XMCDA_pool
else
  scan_service_pool
fi
