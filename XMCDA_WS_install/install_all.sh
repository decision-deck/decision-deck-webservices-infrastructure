#! /bin/bash
CMD=./service_reinstall

# Total: 98

#### TB & ECP: 93

# kappalab Patrick Meyer 12
$CMD ws_config/capacityEntropy-kappalab
$CMD ws_config/capacityFavor-kappalab
$CMD ws_config/capacityInteraction-kappalab
$CMD ws_config/capacityOrness-kappalab
$CMD ws_config/capacityShapley-kappalab
$CMD ws_config/capacityVariance-kappalab
$CMD ws_config/capacityVeto-kappalab
$CMD ws_config/choquetIntegral-kappalab
$CMD ws_config/leastSquaresCapaIdent-kappalab
$CMD ws_config/linProgCapaIdent-kappalab
$CMD ws_config/lsRankingCapaIdent-kappalab
$CMD ws_config/miniVarCapaIdent-kappalab

# R-XMCDA Patrick Meyer 17
$CMD ws_config/alternativesValuesKendall-RXMCDA
$CMD ws_config/computeAlternativesQualification-RXMCDA
$CMD ws_config/convertAlternativesRanksToAlternativesComparisons-RXMCDA
$CMD ws_config/generateXMCDAReport-RXMCDA
$CMD ws_config/invertAlternativesRanks-RXMCDA
$CMD ws_config/plotGaiaPlane-RXMCDA
$CMD ws_config/Promethee1Ranking-RXMCDA
$CMD ws_config/rankAlternativesValues-RXMCDA
$CMD ws_config/multipleAlternativesValuesKendalls-RXMCDA

# R-XMCDA Helene Schmitz: 4
$CMD ws_config/additiveValueFunctionsIdentification-RXMCDA
$CMD ws_config/randomAlternativesRanks-RXMCDA
$CMD ws_config/randomCriteriaWeights-RXMCDA
$CMD ws_config/randomNormalizedPerformanceTable-RXMCDA

# UTAR Boris Leistedt: 6
$CMD ws_config/ACUTA-UTAR
$CMD ws_config/computeNormalisedPerformanceTable-UTAR
$CMD ws_config/generalWeightedSum-UTAR
$CMD ws_config/UTASTAR-UTAR
$CMD ws_config/XYPlotAlternativesValues-UTAR

# J-MCDA Olivier Cailloux 7
$CMD ws_config/ElectreConcordance-J-MCDA
$CMD ws_config/ElectreDiscordances-J-MCDA
$CMD ws_config/ElectreOutranking-J-MCDA
$CMD ws_config/ElectreTriExploitation-J-MCDA
$CMD ws_config/PrometheeFlows-J-MCDA
$CMD ws_config/PrometheePreference-J-MCDA
$CMD ws_config/PrometheeProfiles-J-MCDA

# jsmaa 1
$CMD ws_config/smaa2-jsmaa

# CppXMCDA Jun Zheng: 0
#$CMD ws_config/inconsistencyResolution-CppXMCDA
#$CMD ws_config/IRIS-CppXMCDA
#$CMD ws_config/IRIS-testInconsistency-CppXMCDA

# PyXMCDA Thomas & Olivier & Sébastien 15
$CMD ws_config/RubisConcordanceRelation-PyXMCDA
$CMD ws_config/weightedSum-PyXMCDA
#$CMD ws_config/weightsFromCondorcetAndPreferences
$CMD ws_config/CondorcetRobustnessRelation-PyXMCDA
$CMD ws_config/ElectreTriBMInference-PyXMCDA
$CMD ws_config/randomAlternatives-PyXMCDA
$CMD ws_config/randomCriteria-PyXMCDA
$CMD ws_config/randomPerformanceTable-PyXMCDA
$CMD ws_config/thresholdsSensitivityAnalysis-PyXMCDA
$CMD ws_config/csvToXMCDA-alternativesValues-PyXMCDA
$CMD ws_config/csvToXMCDA-categoriesProfiles-PyXMCDA
$CMD ws_config/csvToXMCDA-criteriaThresholds-PyXMCDA
$CMD ws_config/csvToXMCDA-criteriaValues-PyXMCDA
$CMD ws_config/csvToXMCDA-performanceTable-PyXMCDA
$CMD ws_config/csvToXMCDA-valueFunctions-PyXMCDA
$CMD ws_config/stableSorting-PyXMCDA
$CMD ws_config/RubisOutrankingRelation-PyXMCDA

# URV Aida Valls, Jordi Canals 2
$CMD ws_config/OWA-URV
$CMD ws_config/ULOWA-URV

# ws-Mcc Alexandru Olteanu 5
$CMD ws_config/mccClusters-ws-Mcc
$CMD ws_config/mccClustersRelationSummary-ws-Mcc
$CMD ws_config/mccEvaluateClusters-ws-Mcc
$CMD ws_config/mccPlotClusters-ws-Mcc
$CMD ws_config/mccPreferenceRelation-ws-Mcc

# ITTB Institut Télécom / Télécom Bretagne 21
$CMD ws_config/UTA-ITTB
$CMD ws_config/alternativesRankingViaQualificationDistillation-ITTB
$CMD ws_config/computeNumericPerformanceTable-ITTB
$CMD ws_config/criteriaDescriptiveStatistics-ITTB
$CMD ws_config/cutRelation-ITTB
$CMD ws_config/performanceTableFilter-ITTB
$CMD ws_config/performanceTableTransformation-ITTB
$CMD ws_config/plotAlternativesAssignments-ITTB
$CMD ws_config/plotAlternativesComparisons-ITTB
$CMD ws_config/plotAlternativesValues-ITTB
$CMD ws_config/plotAlternativesValuesPreorder-ITTB
$CMD ws_config/plotCriteriaComparisons-ITTB
$CMD ws_config/plotCriteriaValues-ITTB
$CMD ws_config/plotCriteriaValuesPreorder-ITTB
$CMD ws_config/plotFuzzyCategoriesValues-ITTB
$CMD ws_config/plotNumericPerformanceTable-ITTB
$CMD ws_config/plotStarGraphPerformanceTable-ITTB
$CMD ws_config/plotValueFunctions-ITTB
$CMD ws_config/sortAlternativesValues-ITTB
$CMD ws_config/transitiveReductionAlternativesComparisons-ITTB
$CMD ws_config/transitiveReductionCriteriaComparisons-ITTB

exit 0

#### uni.lu: 5

# CppXMCDA Jun Zheng: 3 
$CMD ws_config/inconsistencyResolution-CppXMCDA
$CMD ws_config/IRIS-CppXMCDA
$CMD ws_config/IRIS-testInconsistency-CppXMCDA

# PyXMCDA Thomas 2
$CMD ws_config/weightsFromCondorcetAndPreferences-PyXMCDA
exit 0;

#### Test
$CMD ws_config/ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA
