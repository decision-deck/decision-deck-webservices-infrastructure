#! /bin/bash
# Usage: installXMCDA.sh <directory>

cmd=$0

usage()
{
cat >&2 <<EOF
Usage: $cmd <configuration_file>
EOF
}

if [ $# -ne "1" ]; then
   usage
   exit -1
fi


. install_bin/env.sh
. "${1}"/env.sh
. install_bin/install_XMCDA_arch.sh
installXMCDAService
