#! /usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""
curl -i http://localhost:8080/get_ticket?hashcode=svc:h1
curl --data 'ticket=t1' --data 'hashcode=svc1:h1' -w "\n%{http_code}" http://localhost:8080/register


http://stackoverflow.com/questions/713847/recommendations-of-python-rest-web-services-framework


"""
import web
import json
import time
from threading import Lock
import sys

DETERMINISTIC_PRGS=(

# <!-- ws-Mcc / Alex -->
    # ND    ws-Mcc mccClusters (1.0)
    ( 'ws-Mcc', 'mccClustersRelationSummary' ),
    ( 'ws-Mcc', 'mccEvaluateClusters' ),
    ( 'ws-Mcc', 'mccPlotClusters' ),
    ( 'ws-Mcc', 'mccPreferenceRelation' ),
    #tous D:always Deterministic, sauf mccClusters: (ND): Never deterministic.

# <!-- RXMCDA / Patrick -->
    ( 'kappalab', 'capacityEntropy' ),
    ( 'kappalab', 'capacityFavor' ),
    ( 'kappalab', 'capacityInteraction' ),
    ( 'kappalab', 'capacityOrness' ),
    ( 'kappalab', 'capacityShapley' ),
    ( 'kappalab', 'capacityVariance' ),
    ( 'kappalab', 'capacityVeto' ),
    ( 'kappalab', 'choquetIntegral' ),
    ( 'kappalab', 'leastSquaresCapaIdent' ),
    ( 'kappalab', 'linProgCapaIdent' ),
    ( 'kappalab', 'lsRankingCapaIdent' ),
    ( 'kappalab', 'miniVarCapaIdent' ),

# <!-- RXMCDA -->
    ( 'RXMCDA', 'alternativesValuesKendall' ),
    ( 'RXMCDA', 'generateXMCDAReport' ),
    ( 'RXMCDA', 'plotAlternativesComparisons' ),
    ( 'RXMCDA', 'plotAlternativesValuesPreorder' ),
    ( 'RXMCDA', 'plotAlternativesValues' ),
    ( 'RXMCDA', 'plotCriteriaComparisons' ),
    ( 'RXMCDA', 'plotCriteriaValuesPreorder' ),
    ( 'RXMCDA', 'plotCriteriaValues' ),
    ( 'RXMCDA', 'plotNumericPerformanceTable' ),
    ( 'RXMCDA', 'rankAlternativesValues' ),

    ( 'RXMCDA', 'additiveValueFunctionsIdentification' ),
    ( 'RXMCDA', 'convertAlternativesRanksToAlternativesComparisons' ),
    ( 'RXMCDA', 'Promethee1Ranking' ),
    # ND ( 'RXMCDA', 'randomAlternativesRanks' ),
    # ND ( 'RXMCDA', 'randomCriteriaWeights' ),
    # ND ( 'RXMCDA', 'randomNormalizedPerformanceTable' ),
    ( 'RXMCDA', 'invertAlternativesRanks' ),
    ( 'RXMCDA', 'plotGaiaPlane' ),
    ( 'RXMCDA', 'alternativesRankingViaQualificationDistillation' ),
    ( 'RXMCDA', 'computeAlternativesQualification' ),
    ( 'RXMCDA', 'multipleAlternativesValuesKendalls' ),
  #    ??? je croyais qu'on avait tué celui-là ???     RXMCDA generatePerformanceTableFromBayesianNet' ),

    ( 'PyXMCDA', 'weightedSum' ),
    ( 'PyXMCDA', 'RubisConcordanceRelation' ),
    ( 'PyXMCDA', 'CondorcetRobustnessRelation' ),
    # ND ( 'PyXMCDA randomAlternatives',  -> corrigé, was: D initalement
    # ND ( 'PyXMCDA randomCriteria' ),
    # ND ( 'PyXMCDA randomPerformanceTable' ),
    ( 'PyXMCDA', 'ElectreTriBMInference' ),
    ( 'PyXMCDA', 'thresholdsSensitivityAnalysis' ),
    ( 'PyXMCDA', 'RubisOutrankingRelation' ),
    ( 'PyXMCDA', 'csvToXMCDA-alternativesValues' ),
    ( 'PyXMCDA', 'csvToXMCDA-categoriesProfiles' ),
    ( 'PyXMCDA', 'csvToXMCDA-criteriaValues' ),
    ( 'PyXMCDA', 'csvToXMCDA-criteriaThresholds' ),
    ( 'PyXMCDA', 'csvToXMCDA-performanceTable' ),
    ( 'PyXMCDA', 'csvToXMCDA-valueFunctions' ),
    ( 'PyXMCDA', 'stableSorting' ),

    # ?? je ne sais pas ... probablement non déterministe ??   jsmaa smaa2' ),

    ( 'UTAR', 'ACUTA' ),
    ( 'UTAR', 'UTASTAR' ),
    ( 'UTAR', 'computeNormalisedPerformanceTable' ),
    ( 'UTAR', 'generalWeightedSum' ),
    ( 'UTAR', 'plotValueFunctions' ),
    ( 'UTAR', 'XYPlotAlternativesValues' ),

# <!-- J-MCDA / Olivier Cailloux -->
    ( 'J-MCDA', 'ElectreConcordance' ),
    ( 'J-MCDA', 'ElectreDiscordances' ),
    ( 'J-MCDA', 'ElectreOutranking' ),
    ( 'J-MCDA', 'cutRelation' ),
    ( 'J-MCDA', 'PrometheePreference' ),
    ( 'J-MCDA', 'PrometheeFlows' ),
    ( 'J-MCDA', 'PrometheeProfiles' ),
    ( 'J-MCDA', 'ElectreTriExploitation' ),
    ( 'J-MCDA', 'ElectreTri1GroupDisaggregationSharedProfiles' ),

#   <!-- URV / Aïda Valls et alii -->
    ( 'URV', 'OWA' ),
    ( 'URV', 'ULOWA' ),
    ( 'URV', 'fuzzyLabelsDescriptors' ),
    ( 'URV', 'defuzzification' ),
    ( 'URV', 'OWAWeightsDescriptors' ),

#   <!-- Institut Télécom / Télécom Bretagne -->
    ( 'ITTB', 'criteriaDescriptiveStatistics' ),
    ( 'ITTB', 'plotValueFunctions' ),
    ( 'ITTB', 'plotAlternativesValues' ),
    ( 'ITTB', 'plotAlternativesValuesPreorder' ),
    ( 'ITTB', 'plotCriteriaValues' ),
    ( 'ITTB', 'plotCriteriaValuesPreorder' ),
    ( 'ITTB', 'plotAlternativesComparisons' ),
    ( 'ITTB', 'plotCriteriaComparisons' ),
    ( 'ITTB', 'plotNumericPerformanceTable' ),
    ( 'ITTB', 'transitiveReductionAlternativesComparisons' ),
    ( 'ITTB', 'transitiveReductionCriteriaComparisons' ),
    ( 'ITTB', 'cutRelation' ),
    ( 'ITTB', 'alternativesRankingViaQualificationDistillation' ),
    ( 'ITTB', 'computeNumericPerformanceTable' ),
    ( 'ITTB', 'performanceTableTransformation' ),
    ( 'ITTB', 'sortAlternativesValues' ),
    ( 'ITTB', 'UTA' ),
    ( 'ITTB', 'performanceTableFilter' ),
    ( 'ITTB', 'plotStarGraphPerformanceTable' ),
    ( 'ITTB', 'plotAlternativesAssignments' ),
    ( 'ITTB', 'plotFuzzyCategoriesValues' ),

#   <!-- ws-Mcc -->
# (see above)

#<!--  LJY -->
    ( 'LJY', 'SRMPaggregation' ),
    ( 'LJY', 'SRMPdisaggregationNoInconsistency' ),

#   <!--  oso / Olivier Sobrie -->
    ( 'oso', 'LearnMRSortMIP' ),
    #ND ( 'oso', 'LearnMRSortMeta' ),

# PUT

  # In the following classifications & comments are from Miłosz Kadzinski
  # (up to PUT-2017)
  # ref. mail 2017/06/20 16:19:34 +0200 Subject: Re: [Important] Fwd: Fw: Deterministic programs

  # Quoting Miłosz here:
  # Basically, there are 3 groups of modules which not
  # deterministic: 1) based on sampling algorithms (Monte Carlo simulation) -
  # I mark them with C as they could be possibly deterministic if seed would
  # be implemented, 2) based on same heuristic approaches (they don't provide
  # exact solutions, but approximate ones) as the problem is too difficult to
  # be solved exactly - I mark them with N, 3) there exist many optimal
  # solutions to the same problem, and each run could possibly indicate a
  # different one (this is the case with representative value functions) - I
  # mark them with N. Another issue with the modules that use solvers (as you
  # know when the solver library is changed, sometimes a different solution
  # can be given even though in theory it should not happen), but with this we
  # can do nothing, so I mark them with A.

  # NB S.Bigaret about when solver is changed: this is what we do for other WS
  #    using solvers as well, we will simply invalidate the cache when solvers
  #    are updated

  # C means deterministic, with (C)onditions on input(s)
  #   (such as an optional parameter 'randomSeed' being set)

# <!--  PUT / Krzysztof Ciomek / https://github.com/kciomek/XMCDA-ws.git -->
    ( 'PUT', 'plotAlternativesHasseDiagram' ), #0.2)
    ( 'PUT', 'RORUTADIS-ExtremeClassCardinalities' ), #0.1)
    ( 'PUT', 'RORUTADIS-GroupAssignments' ), #0.1)
    ( 'PUT', 'RORUTADIS-NecessaryAssignment-basedPreferenceRelation' ), #0.1)
    ( 'PUT', 'RORUTADIS-PossibleAndNecessaryAssignments' ), #0.1)
  # these are based on approximate (heuristic) methods
    # ND ( 'PUT', 'RORUTADIS-PostFactum-InvestigatePerformanceDeterioration' ), #0.3)
    # ND ( 'PUT', 'RORUTADIS-PostFactum-InvestigatePerformanceImprovement' ), #0.3)
    # ND ( 'PUT', 'RORUTADIS-PostFactum-InvestigateValueChange' ), #0.3)

  # there exist many compatible value functions with the same optimum
    # ND ( 'PUT', 'RORUTADIS-RepresentativeValueFunction' ), #0.3)

  # if seed would be implemented
    # C ( 'PUT', 'RORUTADIS-StochasticResults' ), #0.1)

    ( 'PUT', 'RORUTADIS-PreferentialReducts' ), #0.1)


# <!--  PUT / Tomasz Mieszkowski / https://github.com/xor-xor/electre_diviz.git -->
    ( 'PUT', 'ElectreConcordance' ), #0.2.0)
    ( 'PUT', 'ElectreConcordanceReinforcedPreference' ), #0.1.0)
    ( 'PUT', 'ElectreConcordanceWithInteractions' ), #0.2.0)
    ( 'PUT', 'ElectreCredibility' ), #0.2.0)
    ( 'PUT', 'ElectreCredibilityWithCounterVeto' ), #0.1.0)
    ( 'PUT', 'ElectreDiscordance' ), #0.2.0)
    ( 'PUT', 'ElectreIVCredibility' ), #0.2.0)
    ( 'PUT', 'ElectreIsDiscordanceBinary' ), #0.1.0)
    ( 'PUT', 'ElectreIsFindKernel' ), #0.2.0)
    ( 'PUT', 'ElectreTri-CClassAssignments' ), #0.2.0)
    ( 'PUT', 'ElectreTri-rCClassAssignments' ), #0.2.0)
    ( 'PUT', 'ElectreTriClassAssignments' ), #0.2.0)
    ( 'PUT', 'cutRelationCrisp' ), #0.1.0)

# <!--  PUT / Małgorzata Napieraj / https://github.com/mnapieraj/DEA.git -->
    ( 'PUT', 'DEACCRCrossEfficiency' ), #1.0)
    ( 'PUT', 'DEACCREfficiency' ), #1.0)
    ( 'PUT', 'DEACCREfficiencyBounds' ), #1.0)
    ( 'PUT', 'DEACCRExtremeRanks' ), #1.0)
    ( 'PUT', 'DEACCRPreferenceRelations' ), #1.0)
    ( 'PUT', 'DEACCRSuperEfficiency' ), #1.0)
  # if seed would be implemented
    # C ( 'PUT', 'DEASMAACCREfficiencies' ), #1.0)
    # C ( 'PUT', 'DEASMAACCRPreferenceRelations' ), #1.0)
    # C ( 'PUT', 'DEASMAACCRRanks' ), #1.0)
    # C ( 'PUT', 'DEASMAAvalueADDEfficiencies' ), #1.0)
    # C ( 'PUT', 'DEASMAAvalueADDPreferenceRelations' ), #1.0)
    # C ( 'PUT', 'DEASMAAvalueADDRanks' ), #1.0)

    ( 'PUT', 'DEAvalueADDCrossEfficiency' ), #1.0)
    ( 'PUT', 'DEAvalueADDEfficiency' ), #1.0)
    ( 'PUT', 'DEAvalueADDEfficiencyBounds' ), #1.0)
    ( 'PUT', 'DEAvalueADDExtremeRanks' ), #1.0)
    ( 'PUT', 'DEAvalueADDNormalizedPerformanceMatrix' ), #1.0)
    ( 'PUT', 'DEAvalueADDPreferenceRelations' ), #1.0)
    ( 'PUT', 'DEAvalueADDSuperEfficiency' ), #1.0)

# <!-- PUT / Paweł Rychly / https://github.com/pawelrychly/ror-ranking-services.git -->
    ( 'PUT', 'RORUTA-ExtremeRanks' ), #1.0)
    ( 'PUT', 'RORUTA-ExtremeRanksHierarchical' ), #1.0)
    ( 'PUT', 'RORUTA-NecessaryAndPossiblePreferenceRelations' ), #1.0)
    ( 'PUT', 'RORUTA-NecessaryAndPossiblePreferenceRelationsHierarchical' ), #1.0)

  # if seed would be implemented
    # C ( 'PUT', 'RORUTA-PairwiseOutrankingIndices' ), #1.1)
    # C ( 'PUT', 'RORUTA-PairwiseOutrankingIndicesHierarchical' ), #1.1)

  # approximate algorithms
    # ND ( 'PUT', 'RORUTA-PostFactum-PreferenceRelatedDeteriorationOrSurplusValue' ), #1.0)
    # ND ( 'PUT', 'RORUTA-PostFactum-PreferenceRelatedDeteriorationOrSurplusValueHierarchical' ), #1.0)
    # ND ( 'PUT', 'RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue' ), #1.0)
    # ND ( 'PUT', 'RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValueHierarchical' ), #1.0)
    # ND ( 'PUT', 'RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue' ), #1.0)
    # ND ( 'PUT', 'RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValueHierarchical' ), #1.0)
    # ND ( 'PUT', 'RORUTA-PostFactum-RankRelatedImprovementOrMissingValue' ), #1.0)
    # ND ( 'PUT', 'RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical' ), #1.0)

    ( 'PUT', 'RORUTA-PreferentialReductsForNecessaryRelations' ), #1.0)
    ( 'PUT', 'RORUTA-PreferentialReductsForNecessaryRelationsHierarchical' ), #1.0)

  # if seed would be implemented
    # C ( 'PUT', 'RORUTA-RankAcceptabilityIndices' ), #1.0)
    # C ( 'PUT', 'RORUTA-RankAcceptabilityIndicesHierarchical' ), #1.0)

    ( 'PUT', 'RORUTA-RankRelatedPreferentialReducts' ), #1.0)
    ( 'PUT', 'RORUTA-RankRelatedPreferentialReductsHierarchical' ), #1.0)

  # possible many optimal solutions
    # ND ( 'PUT', 'RORUTA-RepresentativeValueFunction' ), #1.0)
    # ND ( 'PUT', 'RORUTA-RepresentativeValueFunctionHierarchical' ), #1.0)

    # <!--  PUT / Michał Tomczyk / https://github.com/MTomczyk/ElectreDiviz -->
    ( 'PUT', 'ElectreComprehensiveDiscordanceIndex' ), #0.2.0)
    ( 'PUT', 'ElectreCrispOutrankingAggregation' ), #0.2.0)
    ( 'PUT', 'ElectreDistillation' ), #0.2.0)
    ( 'PUT', 'ElectreDistillationRank' ), #0.1.0)
    ( 'PUT', 'ElectreNFSOutranking' ), #0.2.0)
    ( 'PUT', 'ElectreIIPreorder' ), #0.2.0)

# New WS PUT-2017

# <!-- PUT / Maciej Uniejewski -->
  #- Maciej
  #  deterministic with only one small exception: Module plotClassAssignments-png (image)
    ( 'PUT', 'PROMETHEE-PROMSORT_assignments' ),
    ( 'PUT', 'PROMETHEE-I-FlowSort_assignments' ),
    ( 'PUT', 'PROMETHEE-II-FlowSort_assignments' ),
    ( 'PUT', 'PROMETHEE-FlowSort-GDSS_assignments' ),
    ( 'PUT', 'plotClassAssignments-png' ),
    ( 'PUT', 'plotClassAssignments-LaTeX' ),
    ( 'PUT', 'PROMETHEE-TRI_assignments' ),

# <!-- PUT / Magdalena Dziecielska -->
  # Magdalena: all my programs are deterministic.
    ( 'PUT', 'PROMETHEE-II_flows' ),
    ( 'PUT', 'PROMETHEE-III_flows' ),
    ( 'PUT', 'groupClassAcceptabilities' ),
    ( 'PUT', 'NetFlow_scores' ),
    ( 'PUT', 'NetFlow-Iterative_ranking' ),
    ( 'PUT', 'PROMETHEE-II-GDSS_flows' ),
    ( 'PUT', 'PROMETHEE-I_ranking' ),

# <!-- PUT / Mateusz Sarbinowski -->
  # Mateusz: I make Promethee_cluster deterministic
  #  Note sbigaret: to be verified: probably undeterministic unless random seed is given
    ( 'PUT', 'orderedClustering' ),
    ( 'PUT', 'PROMETHEE-V' ),
    ( 'PUT', 'PROMETHEE-II_orderedClustering' ),
    # ? ( 'PUT', 'PROMETHEE-Cluster' ),

# <!-- PUT / Sebastian Pawlak -->
  # Sebastian: I can tell that all my programs are deterministic.
    ( 'PUT', 'PROMETHEE_preference-reinforcedPreference' ),
    ( 'PUT', 'PROMETHEE_preference-interactions' ),
    ( 'PUT', 'PROMETHEE_outrankingFlows' ),
    ( 'PUT', 'PROMETHEE_veto' ),
    ( 'PUT', 'PROMETHEE_preferenceDiscordance' ),
    ( 'PUT', 'SRF_weights' ),
    ( 'PUT', 'surrogateWeights' ),
    ( 'PUT', 'PROMETHEE_preference' ),
    ( 'PUT', 'PROMETHEE_discordance' ),
    ( 'PUT', 'PROMETHEE_alternativesProfiles' ),

)

DETERMINISTIC_PRGS=tuple( ( service+'-'+provider
                            for provider, service in DETERMINISTIC_PRGS ) )


urls = (
      '/ticket', "Ticket",
      '/tickets/(.+)', 'tickets',
      '/register', "register"
    )

cache = {} # hashcode -> (ticket, time_in_seconds)
cache_lock = Lock()

import datetime
log=open( "cache.{:%Y-%m-%d_%H%M}.log".format(datetime.datetime.now()), 'w' )
#log=sys.stdout

def get_ticket(hashcode, ticket):
    global log
    now = time.time()
    service_name, env=hashcode.split(':',2)[:2]

    log.write('R\t%s\t%s\t%s\t%s\t%.3f\n' % (service_name, env, ticket, hashcode, now)) # REQUEST
    cache_lock.acquire()
    already_cached_ticket, cache_time = cache.setdefault(hashcode,(ticket,now))

    if now - cache_time > 3600: # 1h
        log.write('E\t%s\t%s\n'%(ticket, already_cached_ticket)) # EXPIRED
        already_cached_ticket = ticket
        cache[hashcode] = (ticket, now)
    cache_lock.release()

    if already_cached_ticket == ticket:
        log.write('M\t%s\n' % ticket) # MISS
        log.flush()
        return ticket

    log.write('H\t%s\t%s\n' % (ticket, already_cached_ticket)) # HIT
    log.flush()
    return already_cached_ticket


class tickets:

    # err code 202 si c'est en attente
    # https://tools.ietf.org/html/rfc7231#section-6.3.3
    def GET(self, ticket_number):
        return 'ticket number %s'%ticket_number

class Ticket:

    def GET(self):
        i = web.input()
        hashcode = i.get('hashcode', None)
        service_name=hashcode.split(':')[0]
        print 'GET', hashcode, service_name

        if service_name not in DETERMINISTIC_PRGS:
            print 'GET returns None'
            return None

            if hashcode is None:
                raise web.BadRequest('missing argument')
        ticket, cache_time = cache.get(hashcode, None)
        if ticket is None:
            raise web.NotFound(repr(ticket))
        #return json.dumps({hashcode: ticket})
        print 'GET returns ' + ticket
        return ticket

    def POST(self):
        i = web.input()
        ticket=i.get('ticket', None)
        hashcode=i.get('hashcode', None)
        if hashcode is None or ticket is None:
            raise web.BadRequest('missing argument(s)')

        service_name=hashcode.split(':')[0]

        return get_ticket(hashcode, ticket)

    def DELETE(self):
        return 'DELETE'

    def PUT(self):
        return 'PUT'

class register:
    def POST(self):
        i = web.input()
        ticket=i.get('ticket', None)
        hashcode=i.get('hashcode', None)
        if hashcode is None or ticket is None:
            raise web.BadRequest('missing argument(s)')
        cache.setdefault(hashcode, [ticket])
        return "Got %s:%s"%(ticket, hashcode)

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()
