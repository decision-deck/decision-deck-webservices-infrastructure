echo "WARNING: Check & understad  what this script does before editing & launching it" >&2
exit 1

SUFFIX=prod
cd "/service/XMCDA-${SUFFIX}"
rm "/service/XMCDA-${SUFFIX}"
svc -dx . log

rm -r "/usr/lib/cgi-bin-XMCDA${SUFFIX}" "/var/XMCDA-${SUFFIX}" "/home/XMCDA-services/${SUFFIX}"
