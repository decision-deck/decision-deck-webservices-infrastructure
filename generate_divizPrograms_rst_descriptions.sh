#! /bin/bash

script_absdirpath="$(cd "${0%/*}" 2>/dev/null; echo "$PWD")"

D2_ROOT=$script_absdirpath

DD_WebRoot="/home/pat/Documents/currentResearch/DecisionDeck/svn-DecisionDeck-web"
DD_WebRoot="/home/TB/projects/mcda/decision-deck/decisiondeck-web.git"

echo -n "Generating files pd-*.rst" >&2

find $D2_ROOT/XMCDAWebServices/ -type f -name 'description-wsDD.xml'  | while read file;
do
    dir=${file%*/description-wsDD.xml}

    service=${dir##*/}

    echo -n "." >&2
    #echo "$service..." >&2

    cd $D2_ROOT/python
    python ./generate_from_description.py \
      -o $DD_WebRoot/w3-diviz/            \
      $dir/description-wsDD.xml           \
      doc_diviz_rst

done
echo " done" >&2
