<?xml version="1.0" encoding="UTF-8"?>
<program_description>
  <program provider="PyXMCDA"
       name="csvToXMCDA-alternativesValues"
       displayName="csvToXMCDA-alternativesValues"
       version="2.0" />
     <documentation>
        <description>Transforms a file containing alternatives values from a comma-separated values (CSV) file to two XMCDA compliant files, containing the corresponding alternatives ids and their alternativesValues.</description>
        <contact>Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)</contact>
        <url>https://gitlab.com/sbigaret/ws-pyxmcda</url>
     </documentation>

     <parameters>
        <input displayName="alternativesValues (csv)" name="alternativesValues.csv" id="csv" isoptional="0">
            <documentation>
                <description>The alternatives and their alternatives values as a CSV file.  The first line is made of two cells, the first one being empty, and the second one will be the content of the attribute "mcdaConcept" in the tag "&lt;alternativesValues>", if supplied.  The following lines are made of at least two cells, with the first cell being an alternative' id (and name, see below), and the remaining cells their associated values.

Example::

    ,ranks
    a1 (eggs),1,2,3,4,5
    a2 (spam),3.14159
    a3 (parrot),int:0xDEAD

The first column contains the alternatives' ids. Additionally, the alternatives' names are also extracted when the cells are formatted like `id (name)`.  Set the parameter "First column" to "id" to deactivate the extraction of alternatives' names.

By default the values are considered as float numbers.  This can be changed using the parameter "Default content".  It is possible to specify the type of a value by prepending it with a prefix:

- `float:` for floats (ex.: `1`, `1.2`, `1.2e3`)
- `integer:` for integers (decimal representation: `127`, hexadecimal: `0x7f`, octal: `0o177`, binary: `0b1111111`)
- `string:` for strings (note that a string with a colon should always be prefixed by `string:`, no matter what the default prefix is).
- `boolean:` for booleans: 1 or 'true' (case insensitive) are True values, everything else is false.
- `na:` for N/A (everything after the colon is ignored)

Example::

    ,values
    a1,float:1.0
    a2,integer:2
    a3,3.03
    a4,string:a label
    a4b,string:another label
    a5,na:content ignored for N/A
    a6,boolean:1

</description>
            </documentation>
            <xmcda tag="other" /> <!-- Not a real xmcda tag -->
        </input>

        <input id="parameters" name="parameters" displayName="parameters" isoptional="0">
          <documentation>
            <description>Parameters of the method</description>
          </documentation>
          <xmcda tag="programParameters"><![CDATA[
    <programParameters>
        <parameter id="csv_delimiter">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </parameter>
        <parameter id="default_prefix">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </parameter>
        <parameter id="name_in_id">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>
]]></xmcda>

          <gui status="preferGUI">
            <entry id="%1" type="string" displayName="CSV delimiter">
              <documentation>
                <description>Indicates the delimiter to use. Leave blank for auto-detection.  It is especially handful when the auto-detection fails to determine the csv delimiter.</description>
              </documentation>
              <constraint>
                <description>One character maximum</description>
                <code><![CDATA[%1.length() < 2]]></code>
              </constraint>
              <defaultValue></defaultValue> <!-- default is blank: auto-detection -->
            </entry>
            <entry id="%2" type="enum" displayName="Default content">
              <documentation>
                <description>.</description>
              </documentation>
              <items>
                  <item id="float">
                      <description>float</description>
                      <value>float</value>
                  </item>
                  <item id="string">
                      <description>string</description>
                      <value>label</value>
                  </item>
                  <item id="integer">
                      <description>integer</description>
                      <value>integer</value>
                  </item>
                  <item id="boolean">
                      <description>boolean</description>
                      <value>boolean</value>
                  </item>
              </items>
              <defaultValue>float</defaultValue>
            </entry>
            <entry id="%3" type="enum" displayName="First column">
              <documentation>
                <description>Content of the first column.</description>
              </documentation>
              <items>
                  <item id="id_only">
                      <description>id</description>
                      <value>false</value>
                  </item>
                  <item id="id_and_name">
                      <description>id (name)</description>
                      <value>true</value>
                  </item>
	      </items>
              <defaultValue>id_and_name</defaultValue>
            </entry>
          </gui>
        </input>

        <output displayName="alternatives" name="alternatives" id="alternatives">
            <documentation>
                <description>The equivalent alternatives ids.</description>
            </documentation>
            <xmcda tag="alternatives" />
        </output>

        <output displayName="alternatives values" name="alternativesValues" id="alternativesValues">
            <documentation>
                <description>The equivalent alternatives values.</description>
            </documentation>
            <xmcda tag="alternativesValues" />
        </output>

        <output displayName="messages" name="messages" id="messages">
            <documentation>
                <description>Status messages.</description>
            </documentation>
            <xmcda tag="programExecutionResult" />
        </output>

     </parameters>
</program_description>
