<?xml version='1.0' encoding='utf-8'?>
<program_description>
    <program provider="kappalab" name="lsRankingCapaIdent" version="1.0" displayName="lsRankingCapaIdent" />
    <documentation>
        <description>Identifies a Mobius capacity by means of an approach called TOMASO in the particular ranking framework. More precisely, given a preorder on the alternatives, and possibly additional linear constraints expressing preferences, importance of criteria, etc., this algorithm determines, if it exists, a capacity minimising the  "gap" between the given ranking and the one derived from the Choquet integral, and compatible with the additional linear constraints. The problem is solved by quadratic programming.</description>
        <contact><![CDATA[Patrick Meyer (patrick.meyer@telecom-bretagne.eu)]]></contact>
        <url />
        <reference>P. Meyer, M. Roubens (2005), Choice, Ranking and Sorting in Fuzzy Multiple Criteria Decision Aid, in: J. Figueira, S. Greco, and M. Ehrgott, Eds, Multiple Criteria Decision Analysis: State of the Art Surveys, volume 78 of International Series in Operations Research and Management Science, chapter 12, pages 471-506. Springer Science + Business Media, Inc., New York. </reference>
    </documentation>
    <parameters>

        <input id="criteria" name="criteria" displayName="criteria" isoptional="0">
            <documentation>
                <description>A list of criteria. Criteria can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), criteria are considered as active.</description>
            </documentation>
            <xmcda tag="criteria"><![CDATA[
	<criteria>
		<criterion>
			<active>[...]</active>
			[...]
		</criterion>
	    [...]
	</criteria>
]]></xmcda>
        </input>

        <input id="alternatives" name="alternatives" displayName="alternatives" isoptional="0">
            <documentation>
                <description>A list of alternatives. Alternatives can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), alternatives are considered as active.</description>
            </documentation>
            <xmcda tag="alternatives"><![CDATA[
	<alternatives>
		<alternative id="..." [...]>
			<active>[...]</active>
		</alternative>
		[...]
	</alternatives>
]]></xmcda>
        </input>

        <input id="performanceTable" name="performanceTable" displayName="performanceTable" isoptional="0">
            <documentation>
                <description>A performance table. The evaluations should be numeric values, i.e. &lt;real&gt;, &lt;integer&gt; or &lt;rational&gt;.</description>
            </documentation>
            <xmcda tag="performanceTable" />
        </input>

        <input id="shapleyPreorder" name="shapleyPreorder" displayName="shapleyPreorder" isoptional="1">
            <documentation>
                <description>A valued relation on criteria expressing importance constraints on the critera. A numeric &lt;value&gt; indicates a minimal preference threshold for each &lt;pair&gt;. One &lt;pair&gt; represents an affirmation of the type "the Shapley importance index of criterion g1 is greater than the Shapley importance index of criterion g2 with preference threshold delta".</description>
            </documentation>
            <xmcda tag="criteriaMatrix" />
        </input>

        <input id="interactionPreorder" name="interactionPreorder" displayName="interactionPreorder" isoptional="1">
            <documentation>
                <description>A valued relation on pairs of criteria expressing constraints on value of the the Shapley interaction index. A numeric &lt;value&gt; indicates a minimal preference threshold for each &lt;pair&gt; of the relation. One &lt;pair&gt; represents a constraint of the type "the Shapley interaction index of the pair (g1,g2) of criteria is greater than the Shapley interaction index of the pair (g3,g4) of criteria with preference threshold delta".</description>
            </documentation>
            <xmcda tag="criteriaSets,criteriaSetsMatrix" />
        </input>

        <input id="shapleyInterval" name="shapleyInterval" displayName="shapleyInterval" isoptional="1">
            <documentation>
                <description>A list of &lt;criterionValue&gt; containing the constraints relative to the quantitative importance of the criteria. Each &lt;criterionValue&gt; contains an an &lt;interval&gt;. Each &lt;criteriaValue&gt; represents an affirmation of the type "the Shapley importance index of criterion g1 lies in the interval [a,b]".</description>
            </documentation>
            <xmcda tag="criteriaValues" />
        </input>

        <input id="interactionInterval" name="interactionInterval" displayName="interactionInterval" isoptional="1">
            <documentation>
                <description>A list of &lt;criterionValue&gt; containing the constraints relative to the type and the magnitude of the Shapley interaction index for pairs of criteria. Each &lt;criterionValue&gt; contains an an &lt;interval&gt;. Each &lt;criteriaValue&gt; represents an affirmation of the type "the Shapley interaction index of the pair (g1,g2) of criteria lies in the interval [a,b]".</description>
            </documentation>
            <xmcda tag="criteriaSets,criteriaSetsValues" />
        </input>

        <input id="alternativesPreorder" name="alternativesPreorder" displayName="alternativesPreorder" isoptional="0">
            <documentation>
                <description>A valued relation relative to the preorder of the alternatives. A numeric &lt;value&gt; indicates a minimal preference threshold for each &lt;pair&gt; of the relation. One &lt;pair&gt; represents a constraint of the type "alternative a is preferred to alternative b with preference threshold delta".</description>
            </documentation>
            <xmcda tag="alternativesMatrix" />
        </input>

        <input id="kAdditivity" name="kAdditivity" displayName="kAdditivity" isoptional="0">
            <documentation>
                <description>Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).</description>
            </documentation>
            <xmcda tag="programParameters"><![CDATA[
	<programParameters>
		<parameter name="kAdditivity"> <!-- name: REQUIRED -->
			<values>
				<value>
					<integer>%1</integer>
				</value>
			</values>
		</parameter>
	</programParameters>
]]></xmcda>
            <gui status="preferFile">
                <entry id="%1" type="int" displayName="kAdditivity">
                    <documentation>
                        <description>Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).</description>
                    </documentation>
                    <constraint>
                        <description>The value should be a positive integer, less than or equal to the number of criteria.</description>
                        <code><![CDATA[ %1 > 0 ]]></code>
                    </constraint>
                    <defaultValue>1</defaultValue>
                </entry>
            </gui>
        </input>

        <input id="separationThreshold" name="separationThreshold" displayName="separationThreshold" isoptional="0">
            <documentation>
                <description>Threshold value indicating the minimal difference in terms of the Choquet integral between two neighbor alternatives in the given ranking.</description>
            </documentation>
            <xmcda tag="programParameters"><![CDATA[
	<programParameters>
		<parameter name="separationThreshold"> <!-- name: REQUIRED -->
			<values>
				<value>
					<real>%1</real>
				</value>
			</values>
		</parameter>
	</programParameters>
]]></xmcda>
            <gui status="preferGUI">
                <entry id="%1" type="float" displayName="separationThreshold">
                    <documentation>
                        <description>Threshold value indicating the minimal difference in terms of the Choquet integral between two neighbor alternatives in the given ranking.</description>
                    </documentation>
                    <constraint>
                        <description>The value should be a strictly positive float, less than the highest possible overall value.</description>
                        <code><![CDATA[ %1 > 0 ]]></code>
                    </constraint>
                </entry>
            </gui>
        </input>

        <output id="mobiusCapacity" name="mobiusCapacity" displayName="mobiusCapacity">
            <documentation>
                <description>The Mobius transform of a capacity.</description>
            </documentation>
            <xmcda tag="criteriaSets,criteriaSetsValues"/>
        </output>

        <output id="messages" name="messages" displayName="messages">
            <documentation>
                <description>A list of messages generated by the algorithm.</description>
            </documentation>
            <xmcda tag="programExecutionResult" />
        </output>

    </parameters>
</program_description>
