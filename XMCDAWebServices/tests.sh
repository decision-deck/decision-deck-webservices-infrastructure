test_it ()
{
  ./test.sh $1 webservices-test.decision-deck.org y
}

CMD=test_it

# UTAR
$CMD ACUTA-UTAR
$CMD UTASTAR-UTAR
$CMD XYPlotAlternativesValues-UTAR
$CMD computeNormalisedPerformanceTable-UTAR
$CMD generalWeightedSum-UTAR

# RXMCDA
$CMD Promethee1Ranking-RXMCDA
$CMD alternativesValuesKendall-RXMCDA
$CMD computeAlternativesQualification-RXMCDA
$CMD convertAlternativesRanksToAlternativesComparisons-RXMCDA
$CMD generateXMCDAReport-RXMCDA
$CMD invertAlternativesRanks-RXMCDA
$CMD multipleAlternativesValuesKendalls-RXMCDA
$CMD plotGaiaPlane-RXMCDA
$CMD rankAlternativesValues-RXMCDA
  # Helene Schmidt
$CMD additiveValueFunctionsIdentification-RXMCDA
$CMD randomAlternativesRanks-RXMCDA
$CMD randomCriteriaWeights-RXMCDA
$CMD randomNormalizedPerformanceTable-RXMCDA
  # kappalab: only needs lib. RXMCDA & kappalab
$CMD capacityEntropy-kappalab
$CMD capacityFavor-kappalab
$CMD capacityInteraction-kappalab
$CMD capacityOrness-kappalab
$CMD capacityShapley-kappalab
$CMD capacityVariance-kappalab
$CMD capacityVeto-kappalab
$CMD choquetIntegral-kappalab
$CMD leastSquaresCapaIdent-kappalab
$CMD linProgCapaIdent-kappalab
$CMD lsRankingCapaIdent-kappalab
$CMD miniVarCapaIdent-kappalab

# PyMCDA

  # Thomas & Olivier
$CMD CondorcetRobustnessRelation-PyXMCDA
$CMD ElectreTriBMInference-PyXMCDA
$CMD RubisConcordanceRelation-PyXMCDA
$CMD RubisOutrankingRelation-PyXMCDA
$CMD randomAlternatives-PyXMCDA
$CMD randomCriteria-PyXMCDA
$CMD randomPerformanceTable-PyXMCDA
$CMD stableSorting-PyXMCDA
$CMD thresholdsSensitivityAnalysis-PyXMCDA
$CMD weightedSum-PyXMCDA
  # csvToXMCDA
$CMD csvToXMCDA-alternativesValues-PyXMCDA
$CMD csvToXMCDA-categoriesProfiles-PyXMCDA
$CMD csvToXMCDA-criteriaThresholds-PyXMCDA
$CMD csvToXMCDA-criteriaValues-PyXMCDA
$CMD csvToXMCDA-performanceTable-PyXMCDA
$CMD csvToXMCDA-valueFunctions-PyXMCDA

# URV
$CMD OWA-URV
$CMD OWAWeightsBalance-URV
$CMD OWAWeightsDivergence-URV
$CMD OWAWeightsEntropy-URV
$CMD OWAWeightsOrness-URV
$CMD ULOWA-URV
$CMD fuzzyLabelsDescriptors-URV
$CMD defuzzificationCOG-URV
$CMD defuzzificationCOM-URV
$CMD defuzzificationOrdinal-URV

# ITTB

$CMD UTA-ITTB
$CMD alternativesRankingViaQualificationDistillation-ITTB
$CMD computeNumericPerformanceTable-ITTB
$CMD criteriaDescriptiveStatistics-ITTB
$CMD cutRelation-ITTB
$CMD performanceTableFilter-ITTB
$CMD performanceTableTransformation-ITTB
$CMD plotAlternativesAssignments-ITTB
$CMD plotAlternativesComparisons-ITTB
$CMD plotAlternativesValues-ITTB
$CMD plotAlternativesValuesPreorder-ITTB
$CMD plotCriteriaComparisons-ITTB
$CMD plotCriteriaValues-ITTB
$CMD plotCriteriaValuesPreorder-ITTB
$CMD plotFuzzyCategoriesValues-ITTB
$CMD plotNumericPerformanceTable-ITTB
$CMD plotStarGraphPerformanceTable-ITTB
$CMD plotValueFunctions-ITTB
$CMD sortAlternativesValues-ITTB
$CMD transitiveReductionAlternativesComparisons-ITTB
$CMD transitiveReductionCriteriaComparisons-ITTB

#J-MCDA

$CMD ElectreConcordance-J-MCDA
$CMD ElectreDiscordances-J-MCDA
$CMD ElectreOutranking-J-MCDA
$CMD ElectreTriExploitation-J-MCDA
$CMD PrometheeFlows-J-MCDA
$CMD PrometheePreference-J-MCDA
$CMD PrometheeProfiles-J-MCDA

#ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA

# oso

$CMD LearnMRSortMIP-oso
$CMD LearnMRSortMeta-oso

# ws-Mcc

$CMD mccClusters-ws-Mcc
$CMD mccClustersRelationSummary-ws-Mcc
$CMD mccEvaluateClusters-ws-Mcc
$CMD mccPlotClusters-ws-Mcc
$CMD mccPreferenceRelation-ws-Mcc

# Krzysztof Ciomek (PUT)

$CMD plotAlternativesHasseDiagram-PUT
$CMD RORUTADIS-ExtremeClassCardinalities-PUT
$CMD RORUTADIS-GroupAssignments-PUT
$CMD RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT
$CMD RORUTADIS-PossibleAndNecessaryAssignments-PUT
$CMD RORUTADIS-PostFactum-InvestigatePerformanceDeterioration-PUT
$CMD RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT
$CMD RORUTADIS-PostFactum-InvestigateValueChange-PUT
$CMD RORUTADIS-RepresentativeValueFunction-PUT
$CMD RORUTADIS-StochasticResults-PUT
$CMD RORUTADIS-PreferentialReducts-PUT

# Michal Tomczyk's (PUT)

$CMD ElectreComprehensiveDiscordanceIndex-PUT
$CMD ElectreCrispOutrankingAggregation-PUT
$CMD ElectreDistillation-PUT
$CMD ElectreDistillationRank-PUT
$CMD ElectreIIPreorder-PUT
$CMD ElectreNFSOutranking-PUT

# Napieraj Malgorzata's (PUT)

$CMD DEACCRCrossEfficiency-PUT
$CMD DEACCREfficiency-PUT
$CMD DEACCREfficiencyBounds-PUT
$CMD DEACCRExtremeRanks-PUT
$CMD DEACCRPreferenceRelations-PUT
$CMD DEACCRSuperEfficiency-PUT
$CMD DEASMAACCREfficiencies-PUT
$CMD DEASMAACCRPreferenceRelations-PUT
$CMD DEASMAACCRRanks-PUT
$CMD DEASMAAvalueADDEfficiencies-PUT
$CMD DEASMAAvalueADDPreferenceRelations-PUT
$CMD DEASMAAvalueADDRanks-PUT
$CMD DEAvalueADDCrossEfficiency-PUT
$CMD DEAvalueADDEfficiency-PUT
$CMD DEAvalueADDEfficiencyBounds-PUT
$CMD DEAvalueADDExtremeRanks-PUT
$CMD DEAvalueADDNormalizedPerformanceMatrix-PUT
$CMD DEAvalueADDPreferenceRelations-PUT
$CMD DEAvalueADDSuperEfficiency-PUT

# Tomasz Mieszkowski (PUT)

$CMD ElectreConcordance-PUT
$CMD ElectreConcordanceReinforcedPreference-PUT
$CMD ElectreConcordanceWithInteractions-PUT
$CMD ElectreCredibility-PUT
$CMD ElectreCredibilityWithCounterVeto-PUT
$CMD ElectreDiscordance-PUT
$CMD ElectreIVCredibility-PUT
$CMD ElectreIsDiscordanceBinary-PUT
$CMD ElectreIsFindKernel-PUT
$CMD ElectreTri-CClassAssignments-PUT
$CMD ElectreTri-rCClassAssignments-PUT
$CMD ElectreTriClassAssignments-PUT
$CMD cutRelationCrisp-PUT

# Paweł Rychły's (PUT)

$CMD RORUTA-ExtremeRanks-PUT
$CMD RORUTA-ExtremeRanksHierarchical-PUT
$CMD RORUTA-NecessaryAndPossiblePreferenceRelations-PUT
$CMD RORUTA-NecessaryAndPossiblePreferenceRelationsHierarchical-PUT
$CMD RORUTA-PairwiseOutrankingIndices-PUT
$CMD RORUTA-PairwiseOutrankingIndicesHierarchical-PUT
$CMD RORUTA-PostFactum-PreferenceRelatedDeteriorationOrSurplusValue-PUT
$CMD RORUTA-PostFactum-PreferenceRelatedDeteriorationOrSurplusValueHierarchical-PUT
$CMD RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT
$CMD RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValueHierarchical-PUT
$CMD RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT
$CMD RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValueHierarchical-PUT
$CMD RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT
$CMD RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT
$CMD RORUTA-PreferentialReductsForNecessaryRelations-PUT
$CMD RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT
$CMD RORUTA-RankAcceptabilityIndices-PUT
$CMD RORUTA-RankAcceptabilityIndicesHierarchical-PUT
$CMD RORUTA-RankRelatedPreferentialReducts-PUT
$CMD RORUTA-RankRelatedPreferentialReductsHierarchical-PUT
$CMD RORUTA-RepresentativeValueFunction-PUT
$CMD RORUTA-RepresentativeValueFunctionHierarchical-PUT

# SMAA

$CMD smaa2-jsmaa

# Sebastian Pawlak (PUT)
$CMD PROMETHEE_preference-reinforcedPreference-PUT
$CMD PROMETHEE_preference-interactions-PUT
$CMD PROMETHEE_outrankingFlows-PUT
$CMD PROMETHEE_veto-PUT
$CMD PROMETHEE_preferenceDiscordance-PUT
$CMD SRF_weights-PUT
$CMD surrogateWeights-PUT
$CMD PROMETHEE_preference-PUT
$CMD PROMETHEE_discordance-PUT
$CMD PROMETHEE_alternativesProfiles-PUT

# Magdalena Dzięcielska (PUT)

$CMD PROMETHEE-II_flows-PUT
$CMD PROMETHEE-III_flows-PUT
$CMD groupClassAcceptabilities-PUT
$CMD NetFlow_scores-PUT
$CMD NetFlow-Iterative_ranking-PUT
$CMD PROMETHEE-II-GDSS_flows-PUT
$CMD PROMETHEE-I_ranking-PUT

# Maciej Uniejewski (PUT)
$CMD PROMETHEE-PROMSORT_assignments-PUT
$CMD PROMETHEE-I-FlowSort_assignments-PUT
$CMD PROMETHEE-II-FlowSort_assignments-PUT
$CMD PROMETHEE-FlowSort-GDSS_assignments-PUT
$CMD plotClassAssignments-png-PUT
$CMD plotClassAssignments-LaTeX-PUT
$CMD PROMETHEE-TRI_assignments-PUT

# Mateusz Sarbinowski
$CMD orderedClustering-PUT
$CMD PROMETHEE-V-PUT
$CMD PROMETHEE-II_orderedClustering-PUT
$CMD PROMETHEE-Cluster-PUT

# Anna Labijak
$CMD ImpreciseDEA-CCR_efficiencies-PUT
$CMD ImpreciseDEA-CCR_extremeRanks-PUT
$CMD ImpreciseDEA-CCR_preferenceRelations-PUT
$CMD ImpreciseDEA-CCR-SMAA_efficiencies-PUT
$CMD ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT
$CMD ImpreciseDEA-CCR-SMAA_ranks-PUT
$CMD ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT
$CMD ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT
$CMD ImpreciseDEA-ValueAdditive-SMAA_ranks-PUT
$CMD ImpreciseDEA-ValueAdditive_efficiencies-PUT
$CMD ImpreciseDEA-ValueAdditive_extremeRanks-PUT
$CMD ImpreciseDEA-ValueAdditive_preferenceRelations-PUT

# Witold Kups (PUT)
# - Java
$CMD AHP_criteriaWeights-PUT ${TEST_SERVER} ${VERBOSE}
$CMD AHP_priorities-PUT ${TEST_SERVER} ${VERBOSE}
$CMD TOPSIS_normalizationAndWeighting-PUT ${TEST_SERVER} ${VERBOSE}
$CMD TOPSIS_extremeAlternatives-PUT ${TEST_SERVER} ${VERBOSE}
$CMD TOPSIS_ranking-PUT ${TEST_SERVER} ${VERBOSE}
# - R
$CMD DEMATEL_influences-PUT ${TEST_SERVER} ${VERBOSE}
$CMD DEMATEL_relation-PUT ${TEST_SERVER} ${VERBOSE}
$CMD VIKOR_ranking-PUT ${TEST_SERVER} ${VERBOSE}
$CMD VIKOR_SRVectors-PUT ${TEST_SERVER} ${VERBOSE}
