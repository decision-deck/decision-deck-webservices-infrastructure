<?xml version="1.0" encoding="ISO-8859-1"?>
<program_description xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://www.decision-deck.org/ws/_downloads/description.xsd">
    <program provider="ITTB" name="plotPerformanceTable_starGraph" displayName="plotStarGraphPerformanceTable" version="2.0"/>
    <documentation>
        <description>This web service generates, for each alternative, a plot representing the performance table as a  star graph. Colors can be used. You can specify how to display the star graphs: by line, by column or in a grid. The star graphs can also be ordered by name or by id (of the alternatives).</description>
        <contact>Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)</contact>
    </documentation>

    <parameters>

        <input id="alternatives" name="alternatives" displayName="alternatives" isoptional="0">
            <documentation>
                <description>A list of alternatives. Alternatives can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), alternatives are considered as active. </description>
            </documentation>
            <xmcda tag="alternatives"><![CDATA[
	<alternatives>
		<alternative>
			<active>[...]</active>
			[...]
		</alternative>
		[...]
	</alternatives>
]]></xmcda>
        </input>

        <input id="criteria" name="criteria" displayName="criteria" isoptional="0">
            <documentation>
                <description>A list of criteria. Criteria can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), criteria are considered as active.</description>
            </documentation>
               <xmcda tag="criteria"><![CDATA[
	<criteria>
		<criterion>
			<active>[...]</active>
		</criterion>
		[...]
	</criteria>
]]></xmcda>
        </input>

        <input id="criteriaScales" name="criteriaScales" displayName="criteria scales" isoptional="1">
            <documentation>
                <description>Criteria scales. Preference direction for the selected criteria can be provided (min or max). In this web service, the default value is set to max.</description>
            </documentation>
               <xmcda tag="criteriaScales"><![CDATA[
        <criteriaScales>
                <criterionScale>
                        <criterionID>...</criterionID>
                        <scales>
                                <scale>
                                        <quantitative>
                                                <preferenceDirection>min</preferenceDirection>
                                        </quantitative>
                                </scale>
                        </scales>
                </criterionScale>
		[...]
	</criteriaScales>
]]></xmcda>
        </input>

        <input id="performanceTable" name="performanceTable" displayName="performanceTable" isoptional="0">
            <documentation>
                <description>A performance table. The evaluations should be only real or integer numeric values, i.e. &lt;real&gt; or &lt;integer&gt;.</description>
            </documentation>
            <xmcda tag="performanceTable"/>
        </input>

        <input id="parameters" name="parameters" displayName="parameters" isoptional="0">
            <documentation>
                <description/>
            </documentation>
            <xmcda tag="programParameters"><![CDATA[
	<programParameters>
		<parameter id="preference_direction" name="Use preference direction">
			<values>
				<value>
					<label>%1</label>
				</value>
			</values>
		</parameter>
		<parameter id="unique_plot" name="Unique plot">
			<values>
				<value>
					<label>%2</label>
				</value>
			</values>
		</parameter>
		<parameter id="plots_display" name="Plots' display">
			<values>
				<value>
					<label>%3</label>
				</value>
			</values>
		</parameter>
		<parameter id="order_by" name="Order by">
			<values>
				<value>
					<label>%4</label>
				</value>
			</values>
		</parameter>
		<parameter id="order" name="order">
			<values>
				<value>
					<label>%5</label>
				</value>
			</values>
		</parameter>
		<parameter id="use_color" name="Colors in the plots">
			<values>
				<value>
					<label>%6</label>
				</value>
			</values>
		</parameter>
		<parameter id="selected_color" name="Selected color">
			<values>
				<value>
					<label>%7</label>
				</value>
			</values>
		</parameter>
	</programParameters>
]]></xmcda>
            <gui status="preferGUI">
                <entry id="%1" type="enum" displayName="Use preference directions?">
                    <documentation>
                        <description>Taking (or not) into account preference directions in the generated star graphs.</description>
                    </documentation>
                    <items>
                        <item id="true">
                            <description>Yes</description>
                            <value>true</value>
                        </item>
                        <item id="false">
                            <description>No</description>
                            <value>false</value>
                        </item>
                    </items>
                    <defaultValue>false</defaultValue>
                </entry>

                <entry id="%2" type="enum" displayName="Unique or multiple plot(s)?">
                    <documentation>
                        <description>In a unique plot, only one image is generated containing all star graphs. Multiple plots can be obtained. The default value is true.</description>
                    </documentation>
                    <items>
                        <item id="true">
                            <description>Unique</description>
                            <value>true</value>
                        </item>
                        <item id="false">
                            <description>Multiple</description>
                            <value>false</value>
                        </item>
                    </items>
                    <defaultValue>true</defaultValue>
                </entry>

                <entry id="%3" type="enum" displayName="Plots arrangement">
                    <documentation>
                        <description>In the case of a unique plot, you can specify how to display the star graphs: by line, by column or in a grid. The default value is by column.</description>
                    </documentation>

                    <!-- %3 is active if the entry id=%2 takes the value true -->
                    <dependency><![CDATA[%2:type="true"]]></dependency>
                    <items>
                        <item id="column">
                            <description>Column</description>
                            <value>column</value>
                        </item>
                        <item id="line">
                            <description>Line</description>
                            <value>line</value>
                        </item>
                        <item id="grid">
                            <description>Grid</description>
                            <value>grid</value>
                        </item>
                    </items>
                    <defaultValue>column</defaultValue>
                </entry>

                <entry id="%4" type="enum" displayName="Order abscissa by:">
                    <documentation>
                        <description>Choose between "name" or "id".</description>
                    </documentation>
                    <items>
                        <item id="order_by_name">
                            <description>name</description>
                            <value>name</value>
                        </item>
                        <item id="order_by_id">
                            <description>id</description>
                            <value>id</value>
                        </item>
                    </items>
                    <defaultValue>order_by_id</defaultValue>
                </entry>

                <entry id="%5" type="enum" displayName="order">
                    <documentation>
                        <description>The parameter which says if the star graphs are sorted out in the alphabetical order or its inverse, according to the title.</description>
                    </documentation>
                    <items>
                        <item id="increasing">
                            <description>Ascending order</description>
                            <value>increasing</value>
                        </item>
                        <item id="decreasing">
                            <description>Descending order</description>
                            <value>decreasing</value>
                        </item>
                    </items>
                    <defaultValue>increasing</defaultValue>
                </entry>


                <entry id="%6" type="enum" displayName="Use Colors?">
                    <documentation>
                        <description>The use of colors: true for colored star graphs.</description>
                    </documentation>
                    <items>
                        <item id="true">
                            <description>Yes</description>
                            <value>true</value>
                        </item>
                        <item id="false">
                            <description>No</description>
                            <value>false</value>
                        </item>
                    </items>
                    <defaultValue>false</defaultValue>
                </entry>

                <entry id="%7" type="enum" displayName="Choose color:">
                    <documentation>
                        <description>Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".</description>
                    </documentation>
                    <!-- %7 is active if the entry id=%6 takes the value true -->
                    <dependency><![CDATA[%6:type="true"]]></dependency>
                    <items>
                        <item id="black">
                            <description>Black</description>
                            <value>black</value>
                        </item>
                        <item id="red">
                            <description>Red</description>
                            <value>red</value>
                        </item>
                        <item id="blue">
                            <description>Blue</description>
                            <value>blue</value>
                        </item>
                        <item id="green">
                            <description>Green</description>
                            <value>green</value>
                        </item>
                        <item id="yellow">
                            <description>Yellow</description>
                            <value>yellow</value>
                        </item>
                        <item id="magenta">
                            <description>Magenta</description>
                            <value>magenta</value>
                        </item>
                        <item id="cyan">
                            <description>Cyan</description>
                            <value>cyan</value>
                        </item>
                    </items>
                    <defaultValue>black</defaultValue>
                </entry>

            </gui>
        </input>

        <output id="png" name="starGraph.png" displayName="star graph (png)">
            <documentation>
                <description>The star graph representing the performance table, as a PNG image.</description>
            </documentation>
            <xmcda tag="alternativeValue"/>
        </output>

        <output id="messages" name="messages" displayName="messages">
            <documentation>
                <description>A list of messages generated by the algorithm.</description>
            </documentation>
            <xmcda tag="programExecutionResult"/>
        </output>

    </parameters>
</program_description>
