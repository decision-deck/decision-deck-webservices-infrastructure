from pathlib import Path
from collections import namedtuple, OrderedDict
import matplotlib.pyplot as plt
import numpy as np
from numpy import array
import sys


Figure = namedtuple('Figure', 'criteria_values, fig_options,'
                              'ax_options, plot_options')


figure = Figure(
    criteria_values=OrderedDict(
        [("Toyota Prius (a1)", 1.0), ("Ford Fiesta", 2.0), ("Toyota Prius (a3)", 3.0)]
    ),
    fig_options={"constrained_layout": True},
    ax_options={"xlabel": "criteria", "ylabel": "values", "title": "Criteria Values"},
    plot_options=[
        {"color": "#000000", "width": 0.75},
        {"color": "#000000", "width": 0.75},
        {"color": "#000000", "width": 0.75},
    ],
)



def plot_function(figure):
    fig, ax = plt.subplots(**figure.fig_options)
    labels = [*figure.criteria_values.keys()]
    x = np.arange(len(labels))
    for xx, yy, options in zip(x, figure.criteria_values.values(),
                               figure.plot_options):
        ax.bar(xx, yy, **options)
    ax.set_xlabel(figure.ax_options['xlabel'])
    ax.set_ylabel(figure.ax_options['ylabel'])
    ax.set_title(figure.ax_options['title'])
    ax.set_xticks(x)
    ax.set_xticklabels(labels, rotation=-45, ha="left", rotation_mode="anchor")
    return fig


def save_fig(fig, dirname, filename):
    fig.savefig(Path(dirname, filename))


def main(directory='.'):
    f = plot_function(figure)
    save_fig(f, directory, "criteriaValues.png")


if __name__ == "__main__":
    sys.exit(main())
