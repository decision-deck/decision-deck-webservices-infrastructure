<?xml version="1.0" encoding="ISO-8859-1"?>
<program_description xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://www.decision-deck.org/ws/_downloads/description.xsd">
    <program provider="ITTB" name="plotAlternativesMatrix" displayName="plotAlternativesMatrix" version="2.0"/>
       <documentation>
           <description>This web service generates a graph representing a partial preorder on the alternatives. The generated graph can be valued. It can also be transitive. Several shapes for the nodes are proposed and colors can be used.</description>
              <contact>Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)</contact>
       </documentation>

       <parameters>

           <input id="alternatives" name="alternatives" displayName="alternatives" isoptional="0">

               <documentation>
                   <description>A list of alternatives. Alternatives can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), alternatives are considered as active. </description>
               </documentation>
               <xmcda tag="alternatives"><![CDATA[
	<alternatives>
		<alternative>
			<active>[...]</active>
			[...]
		</alternative>
		[...]
	</alternatives>
]]></xmcda>
           </input>

           <input id="alternativesMatrix" name="alternativesMatrix" displayName="alternatives matrix" isoptional="0">
               <documentation>
                   <description>A valued relation relative to comparisons of the alternatives. A numeric &lt;value&gt; indicates a the valuation for each &lt;pair&gt; of the relation.</description>
               </documentation>
               <xmcda tag="alternativesMatrix"><![CDATA[
	<alternativesMatrix>
		<row>
			<alternativeID>a01</alternativeID>
			<column>
				<alternativeID>[...]</alternativeID>
				<values>
					<value>
						<real>[...]</real>
					</value>
				</values>
			</column>
			[...]
		</row>
		[...]
	</alternativesMatrix>
]]></xmcda>

           </input>

           <input id="parameters" name="parameters" displayName="parameters" isoptional="0">
               <documentation>
                   <description>
                       Generates a graph taking into account the proposed options.
                   </description>
               </documentation>
               <xmcda tag="programParameters"><![CDATA[
	<programParameters>
		<parameter id="valued_graph" name="Valued graph">
			<values>
				<value>
					<label>%1</label>
				 </value>
			</values>
		</parameter>
		<parameter id="transitive_reduction" name="Transitive reduction">
			<values>
				<value>
					<label>%2</label>
				 </value>
			 </values>
		</parameter>
		<parameter id="node_shape" name="Node shape">
			<values>
				<value>
					<label>%3</label>
				 </value>
			 </values>
		</parameter>
		<parameter id="use_color" name="Colors in the graph">
			<values>
				<value>
					<label>%4</label>
				 </value>
			 </values>
		</parameter>
		<parameter id="selected_color" name="Selected color">
			<values>
				<value>
					<label>%5</label>
				 </value>
			 </values>
		</parameter>
	</programParameters>
]]></xmcda>
               <gui status="preferGUI">
                   <entry id="%1" type="enum" displayName="Graph type:">
                       <documentation>
                       <description>
                           Choose between true (values appear with their transitions) or false.
                       </description>
                           </documentation>
                       <items>
                           <item id="true">
                               <description>Arcs and values</description>
                               <value>true</value>
                           </item>
                           <item id="false">
                               <description>Arcs only</description>
                               <value>false</value>
                           </item>
                       </items>
                       <defaultValue>false</defaultValue>
                   </entry>
				<entry id="%2" type="enum" displayName="With transitive reduction?">

				    <!-- %2 is active if the entry id=%1 takes the value false -->
				    <dependency><![CDATA[%1:type="false"]]></dependency>
				    <items>
                           <item id="true">
                               <description>Yes</description>
                               <value>true</value>
                           </item>
                           <item id="false">
                               <description>No</description>
                               <value>false</value>
                           </item>
                       </items>
                       <defaultValue>false</defaultValue>
                   </entry>
                   <entry id="%3" type="enum" displayName="Shape of the nodes?">
                       <documentation>
                           <description>
                               Choose between rectangle, square, ellipse, circle or diamond.
                           </description>
                       </documentation>
                       <items>
                           <item id="rectangle">
                               <description>Rectangle</description>
                               <value>Rectangle</value>
                           </item>
                           <item id="square">
                               <description>Square</description>
                               <value>Square</value>
                           </item>
                           <item id="ellipse">
                               <description>Ellipse</description>
                               <value>Ellipse</value>
                           </item>
                           <item id="circle">
                               <description>Circle</description>
                               <value>Circle</value>
                           </item>
                           <item id="diamond">
                               <description>Diamond</description>
                               <value>Diamond</value>
                           </item>
                       </items>
                       <defaultValue>rectangle</defaultValue>
                   </entry>

                   <entry id="%4" type="enum" displayName=" Use colors?">
                       <documentation>
                       <description>
                           The use of colors: true for a colored graph.
                       </description>
                       </documentation>
                       <items>
                           <item id="true">
                               <description>Yes</description>
					           <value>true</value>
                           </item>
                           <item id="false">
                               <description>No</description>
                       		   <value>false</value>
                           </item>
                       </items>
                       <defaultValue>false</defaultValue>
                   </entry>

                   <entry id="%5" type="enum" displayName="Choose color:">

                       <documentation>
                           <description>String that indicates the color in the generated graph.Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".</description>
                       </documentation>
                       <!-- %5 is active if the entry id=%4 takes the value true -->
                       <dependency><![CDATA[%4:type="true"]]></dependency>
                       <items>
                           <item id="Black">
                               <description>Black</description>
                               <value>Black</value>
                           </item>
                           <item id="Red">
                               <description>Red</description>
                               <value>Red</value>
                           </item>
                           <item id="Blue">
                               <description>Blue</description>
                               <value>Blue</value>
                           </item>
                           <item id="Green">
                               <description>Green</description>
                               <value>Green</value>
                           </item>
                           <item id="Yellow">
                               <description>Yellow</description>
                               <value>Yellow</value>
                           </item>
                           <item id="Magenta">
                               <description>Magenta</description>
                               <value>Magenta</value>
                           </item>
                           <item id="Cyan">
                               <description>Cyan</description>
                               <value>Cyan</value>
                           </item>
                       </items>
                       <defaultValue>Black</defaultValue>
                   </entry>

               </gui>
           </input>

            <output id="graph.dot" name="graph.dot" displayName="graph (dot)">
                     <documentation>
                         <description>The dot file used to generate the graph.</description>
                     </documentation>
               <xmcda tag="other"/>
           </output>

           <output id="graph" name="graph.png" displayName="graph (png)">
                     <documentation>
                         <description>The image representing the graph.</description>
                     </documentation>
               <xmcda tag="other"/>
           </output>

           <output id="messages" name="messages" displayName="messages">
               <documentation>
                   <description>A list of messages generated by the algorithm.</description>
               </documentation>
               <xmcda tag="programExecutionResult"/>
           </output>

       </parameters>
</program_description>
