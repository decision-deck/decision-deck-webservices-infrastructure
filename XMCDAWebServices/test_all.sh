#! /bin/bash
shopt -s extglob

#if [ $# -ne 1 ]; then
#    echo "Usage: ${0} ..."
#fi

CMD=check
#callServiceSTD="/home/big/Projets/DecisionDeck-Web-Services/python/genericXMCDAService.py -x"
#callServiceLU="/home/big/Projets/DecisionDeck-Web-Services/python/genericXMCDAService.py -U http://ernst-schroeder.uni.lu/cgi-bin/%s.py"
#callServiceSTD="/home/big/Projets/DecisionDeck-Web-Services/python/genericXMCDAService.py -x -U http://srv-labs-012.enst-bretagne.fr/cgi-bin/%s-test.py"

# Le répertoire de sortie dans chaque répertoire service
#OUT_SUFFIX=.ecp
OUT_SUFFIX=.tb
#OUT_SUFFIX=.$1


OUT_SUFFIX=.ecp; callServiceSTD="/home/big/Projets/DecisionDeck-Web-Services/python/genericXMCDAService.py -x"; callService=$callServiceSTD
OUT_SUFFIX=.tb; callServiceSTD="/home/big/Projets/DecisionDeck-Web-Services/python/genericXMCDAService.py -x -U http://srv-labs-012.enst-bretagne.fr/soap/%s.py"; callService=$callServiceSTD

callService=$callServiceSTD

RM=/bin/rm
RMDIR="/bin/rm -rf"

check ()
{
  if [ ! -d $1 -o ! \( -d $1/in -o -d $1/in1 \) ]; then
    echo "Missing dir: $1 or $1/in" 1>&2;
    return 1
  fi
  test_logs=$(pwd)/test.logs
  mkdir -p "${test_logs}"
  log_base="${test_logs}/$1"

  pushd $1 >/dev/null 2>&1

  for in in ?(in|in[0-9]); do
    OUT="out${in##in}${OUT_SUFFIX}"
    if [ -d $OUT ]; then $RMDIR $OUT; fi
    mkdir "$OUT"
    cd "$in"
    # *, not *.xml because some services accept other formats (ex.: csvToXMCDA-* accept .csv)
    files=*
    cmdArg=""
    for f in $files; do
        cmdArg="$cmdArg ${f%.xml}:../${in}/${f}"
    done
    cd "../$OUT"
    $callService -n $1 -v -S $cmdArg > "${log_base}.stdout" 2> "${log_base}.stderr"
    ret=$?
    cd ..
    diff -rw "out${in##in}" "$OUT" > "${log_base}.${in}.diff" 2>&1
    echo " $ret $?  $1"
  done
  popd >/dev/null 2>&1
}

echo "(${OUT_SUFFIX})"

# Total: 61

#### ECP: 56

# kappalab Patrick Meyer 12
$CMD capacityEntropy-kappalab
$CMD capacityFavor-kappalab
$CMD capacityInteraction-kappalab
$CMD capacityOrness-kappalab
$CMD capacityShapley-kappalab
$CMD capacityVariance-kappalab
$CMD capacityVeto-kappalab
$CMD choquetIntegral-kappalab
$CMD leastSquaresCapaIdent-kappalab
$CMD linProgCapaIdent-kappalab
$CMD lsRankingCapaIdent-kappalab
$CMD miniVarCapaIdent-kappalab

# R-XMCDA Patrick Meyer 9
$CMD Promethee1Ranking-RXMCDA
$CMD alternativesValuesKendall-RXMCDA
$CMD computeAlternativesQualification-RXMCDA
$CMD convertAlternativesRanksToAlternativesComparisons-RXMCDA
$CMD generateXMCDAReport-RXMCDA
$CMD invertAlternativesRanks-RXMCDA
$CMD multipleAlternativesValuesKendalls-RXMCDA
$CMD plotGaiaPlane-RXMCDA
$CMD rankAlternativesValues-RXMCDA

# R-XMCDA Helene Schmitz: 4
$CMD additiveValueFunctionsIdentification-RXMCDA
$CMD randomAlternativesRanks-RXMCDA
$CMD randomCriteriaWeights-RXMCDA
$CMD randomNormalizedPerformanceTable-RXMCDA

# UTAR Boris Leistedt: 5
$CMD ACUTA-UTAR
$CMD computeNormalisedPerformanceTable-UTAR
$CMD generalWeightedSum-UTAR
$CMD UTASTAR-UTAR
$CMD XYPlotAlternativesValues-UTAR

# J-MCDA Olivier Cailloux 7
$CMD ElectreConcordance-J-MCDA
$CMD ElectreDiscordances-J-MCDA
$CMD ElectreOutranking-J-MCDA
$CMD ElectreTriExploitation-J-MCDA
$CMD PrometheeFlows-J-MCDA
$CMD PrometheePreference-J-MCDA
$CMD PrometheeProfiles-J-MCDA

# jsmaa 1
$CMD smaa2-jsmaa

# CppXMCDA Jun Zheng: 0
#$CMD inconsistencyResolution-CppXMCDA
#$CMD IRIS-CppXMCDA
#$CMD IRIS-testInconsistency-CppXMCDA

# PyXMCDA Thomas & Olivier 11
$CMD CondorcetRobustnessRelation-PyXMCDA
$CMD ElectreTriBMInference-PyXMCDA
$CMD RubisConcordanceRelation-PyXMCDA
$CMD RubisOutrankingRelation-PyXMCDA
$CMD randomAlternatives-PyXMCDA
$CMD randomCriteria-PyXMCDA
$CMD randomPerformanceTable-PyXMCDA
$CMD stableSorting-PyXMCDA
$CMD thresholdsSensitivityAnalysis-PyXMCDA
$CMD weightedSum-PyXMCDA
$CMD weightsFromCondorcetAndPreferences-PyXMCDA

# PyXMCDA csvToXMCDA 6
$CMD csvToXMCDA-alternativesValues-PyXMCDA
$CMD csvToXMCDA-categoriesProfiles-PyXMCDA
$CMD csvToXMCDA-criteriaThresholds-PyXMCDA
$CMD csvToXMCDA-criteriaValues-PyXMCDA
$CMD csvToXMCDA-performanceTable-PyXMCDA
$CMD csvToXMCDA-valueFunctions-PyXMCDA

# URV: 10
$CMD OWA-URV
$CMD OWAWeightsBalance-URV
$CMD OWAWeightsDivergence-URV
$CMD OWAWeightsEntropy-URV
$CMD OWAWeightsOrness-URV
$CMD ULOWA-URV
$CMD fuzzyLabelsDescriptors-URV
$CMD defuzzificationCOG-URV
$CMD defuzzificationCOM-URV
$CMD defuzzificationOrdinal-URV

# ITTB 21
$CMD UTA-ITTB
$CMD alternativesRankingViaQualificationDistillation-ITTB
$CMD computeNumericPerformanceTable-ITTB
$CMD criteriaDescriptiveStatistics-ITTB
$CMD cutRelation-ITTB
$CMD performanceTableFilter-ITTB
$CMD performanceTableTransformation-ITTB
$CMD plotAlternativesAssignments-ITTB
$CMD plotAlternativesComparisons-ITTB
$CMD plotAlternativesValues-ITTB
$CMD plotAlternativesValuesPreorder-ITTB
$CMD plotCriteriaComparisons-ITTB
$CMD plotCriteriaValues-ITTB
$CMD plotCriteriaValuesPreorder-ITTB
$CMD plotFuzzyCategoriesValues-ITTB
$CMD plotNumericPerformanceTable-ITTB
$CMD plotStarGraphPerformanceTable-ITTB
$CMD plotValueFunctions-ITTB
$CMD sortAlternativesValues-ITTB
$CMD transitiveReductionAlternativesComparisons-ITTB
$CMD transitiveReductionCriteriaComparisons-ITTB

# oso 2
$CMD LearnMRSortMIP-oso
$CMD LearnMRSortMeta-oso

# PUT-KC 11
$CMD plotAlternativesHasseDiagram-PUT
$CMD RORUTADIS-ExtremeClassCardinalities-PUT
$CMD RORUTADIS-GroupAssignments-PUT
$CMD RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT
$CMD RORUTADIS-PossibleAndNecessaryAssignments-PUT
$CMD RORUTADIS-PostFactum-InvestigatePerformanceDeterioration-PUT
$CMD RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT
$CMD RORUTADIS-PostFactum-InvestigateValueChange-PUT
$CMD RORUTADIS-RepresentativeValueFunction-PUT
$CMD RORUTADIS-StochasticResults-PUT
$CMD RORUTADIS-PreferentialReducts-PUT

# PUT-MT 6
$CMD ElectreComprehensiveDiscordanceIndex-PUT
$CMD ElectreCrispOutrankingAggregation-PUT
$CMD ElectreDistillation-PUT
$CMD ElectreDistillationRank-PUT
$CMD ElectreIIPreorder-PUT
$CMD ElectreNFSOutranking-PUT

# PUT-NM 19
$CMD DEACCRCrossEfficiency-PUT
$CMD DEACCREfficiency-PUT
$CMD DEACCREfficiencyBounds-PUT
$CMD DEACCRExtremeRanks-PUT
$CMD DEACCRPreferenceRelations-PUT
$CMD DEACCRSuperEfficiency-PUT
$CMD DEASMAACCREfficiencies-PUT
$CMD DEASMAACCRPreferenceRelations-PUT
$CMD DEASMAACCRRanks-PUT
$CMD DEASMAAvalueADDEfficiencies-PUT
$CMD DEASMAAvalueADDPreferenceRelations-PUT
$CMD DEASMAAvalueADDRanks-PUT
$CMD DEAvalueADDCrossEfficiency-PUT
$CMD DEAvalueADDEfficiency-PUT
$CMD DEAvalueADDEfficiencyBounds-PUT
$CMD DEAvalueADDExtremeRanks-PUT
$CMD DEAvalueADDNormalizedPerformanceMatrix-PUT
$CMD DEAvalueADDPreferenceRelations-PUT
$CMD DEAvalueADDSuperEfficiency-PUT

# PUT-PR 22
$CMD RORUTA-ExtremeRanks-PUT
$CMD RORUTA-ExtremeRanksHierarchical-PUT
$CMD RORUTA-NecessaryAndPossiblePreferenceRelations-PUT
$CMD RORUTA-NecessaryAndPossiblePreferenceRelationsHierarchical-PUT
$CMD RORUTA-PairwiseOutrankingIndices-PUT
$CMD RORUTA-PairwiseOutrankingIndicesHierarchical-PUT
$CMD RORUTA-PostFactum-PreferenceRelatedDeteriorationOrSurplusValue-PUT
$CMD RORUTA-PostFactum-PreferenceRelatedDeteriorationOrSurplusValueHierarchical-PUT
$CMD RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT
$CMD RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValueHierarchical-PUT ### pas seulement du round
$CMD RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT
$CMD RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValueHierarchical-PUT
$CMD RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT
$CMD RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT
$CMD RORUTA-PreferentialReductsForNecessaryRelations-PUT
$CMD RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT
$CMD RORUTA-RankAcceptabilityIndices-PUT
$CMD RORUTA-RankAcceptabilityIndicesHierarchical-PUT
$CMD RORUTA-RankRelatedPreferentialReducts-PUT
$CMD RORUTA-RankRelatedPreferentialReductsHierarchical-PUT
$CMD RORUTA-RepresentativeValueFunction-PUT
$CMD RORUTA-RepresentativeValueFunctionHierarchical-PUT

exit 0;

#### uni.lu: 5
callService=$callServiceLU

# CppXMCDA Jun Zheng: 3
$CMD inconsistencyResolution-CppXMCDA
$CMD IRIS-CppXMCDA
$CMD IRIS-testInconsistency-CppXMCDA

# PyXMCDA Thomas 2
$CMD weightsFromCondorcetAndPreferences-PyXMCDA

echo "(${OUT_SUFFIX})"
exit 0;
