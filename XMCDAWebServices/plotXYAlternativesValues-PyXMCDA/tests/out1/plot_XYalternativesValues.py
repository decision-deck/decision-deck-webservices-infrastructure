import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerLine2D
import numpy as np
from numpy import array
from collections import namedtuple, OrderedDict
import sys
from pathlib import Path


Figure = namedtuple('Figure', 'alternativesValues, alternatives, fig_options,'
                    'plot_options')


figure=Figure(alternativesValues=OrderedDict([('a1', (1.0, 1.0)), ('a2', (2.0, 2.0)), ('a3', (3.0, 3.0))]), alternatives={'a1': 'a1: Toyota Prius', 'a2': 'a2: Ford Fiesta', 'a3': 'a3: Toyota Prius'}, fig_options={'subplot_kw': {'xlabel': 'alternativesValues 1', 'ylabel': 'alternativesValues 2', 'title': 'Alternatives Values XY'}, 'constrained_layout': True}, plot_options={'color': 'r', 'marker': 'o', 'linestyle': 'solid'})


def euclidean(a, b):
    return np.sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2)


def angle(p, p_ref):
    r = np.sqrt((p[0] - p_ref[0])**2 + (p[1] - p_ref[1])**2)
    if r == 0:
        return 0.
    return np.sign(p[1] - p_ref[1])*np.arccos((p[0] - p_ref[0])/r)


def place_text(p, p1, p2, margin=0., vmargin=0.):
    a1 = angle(p1, p)
    a2 = angle(p2, p)
    if np.fabs(a1 - a2) < np.pi:
        a = (a1 + a2)/2 + np.pi
    else:
        a = (a1 + a2)/2
    a = a % (2*np.pi)
    dy = vmargin if a <= np.pi else -vmargin
    x = p[0] + margin * np.cos(a)
    y = p[1] + margin * np.sin(a) + dy
    return (x, y)


def remove_markers_legend(handle, orig):
    handle.update_from(orig)
    handle.set_marker("")
    handle.set_linestyle("")


def plot_function(figure):
    fig, ax = plt.subplots(**figure.fig_options)
    p = None
    x_lims = [float("inf"), -float("inf")]
    y_lims = [float("inf"), -float("inf")]
    # Plot points and segments
    for k, (x, y) in figure.alternativesValues.items():
        x_lims[0] = np.min([x_lims[0], x])
        x_lims[1] = np.max([x_lims[1], x])
        y_lims[0] = np.min([y_lims[0], y])
        y_lims[1] = np.max([y_lims[1], y])
        if p is None:
            p = (x, y)
            continue
        ax.plot([p[0], x], [p[1], y], **figure.plot_options)
        p = (x, y)
    ax.plot([], [])
    # Annotate figure
    scale = (x_lims[1] + y_lims[1] - x_lims[0] - y_lims[0])/2
    buff = []
    last_p = None
    for i in range(len(figure.alternativesValues)):
        p = [*figure.alternativesValues.values()][i]
        if i < len(figure.alternativesValues)-1:
            p2 = [*figure.alternativesValues.values()][i+1]
        else:
            p2 = None
        if p2 is None:
            if last_p is None:
                p2 = (0, 0)
                last_p = (0, 0)
            else:
                p2 = last_p
        if euclidean(p, p2) < 0.0001:
            # points superposed -> delay annotation
            buff.append([*figure.alternativesValues.keys()][i])
        else:
            if last_p is None:
                last_p = p2
            for j, k in enumerate(buff):
                ax.annotate(k,
                            place_text(p, last_p, p2, margin=0.05*scale),
                            horizontalalignment='center',
                            verticalalignment='center',
                            fontsize='small')
            ax.annotate([*figure.alternativesValues.keys()][i],
                        place_text(p, last_p, p2, margin=0.05*scale),
                        horizontalalignment='center',
                        verticalalignment='center',
                        fontsize='small')

            buff = []
            last_p = p
    ax.set_xlim(x_lims[0] - scale * 0.1, x_lims[1] + scale * 0.1)
    ax.set_ylim(y_lims[0] - scale * 0.1, y_lims[1] + scale * 0.1)
    leg = []
    for k in figure.alternativesValues.keys():
        leg.append(figure.alternatives[k])
    ax.legend(leg, bbox_to_anchor=(1,1), loc='best', fontsize='small',
              handler_map={plt.Line2D:HandlerLine2D(update_func=remove_markers_legend)})

    return fig


def save_fig(fig, dirname, filename):
    fig.savefig(Path(dirname, filename))


def main(directory='.'):
    f = plot_function(figure)
    save_fig(f, directory, "XYalternativesValues.png")


if __name__ == "__main__":
    sys.exit(main())
