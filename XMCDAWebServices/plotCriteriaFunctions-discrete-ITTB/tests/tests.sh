#! /bin/sh

JAR=../build/plotValueFunctions-ITTB.jar

OUT=out
TMPF=$("tempfile")

unset DISPLAY

for IN in in[0-9]*; do
  echo -n "$IN / out${IN#in} "
  /bin/rm -rf $OUT
  mkdir $OUT
  java -jar $JAR $IN/methodParameters.xml $IN/criteria.xml $IN/valueFunctions.xml $OUT/valueFunctionsPlot.xml $OUT/messages.xml
  diff -Bqrw --exclude=alternativesValuesPlot.xml out out${IN#in} > $TMPF 2>&1
  if [ $? -ne 0 ]; then
    echo "FAILED"
    cat $TMPF >&2
  else
    echo "SUCCESS"
  fi
  /bin/rm $TMPF
done


# How to extract the images from the XMCDA output:
# grep '<image>' out/alternativesComparisonsPlot.xml | sed -e 's|.*<image>\(.*\)</image>.*|\1|g'|base64 -d > 2.png

#i=0; grep '<image>' out${IN#in}/performanceTablePlot.xml |while read line; do echo $line | sed -e 's|.*<image>\(.*\)</image>.*|\1|g'|base64 -d > 1_$i.png; i=$((i+1)); done
# IN=in1
#./tests.sh $IN
# grep '<image>' out${IN#in}/valueFunctionsPlot.xml |sed -e 's|.*<image>\(.*\)</image>.*|\1|g'|base64 -d > 1.png; grep '<image>' out/valueFunctionsPlot.xml |sed -e 's|.*<image>\(.*\)</image>.*|\1|g'|base64 -d > 2.png
# nd xzgv 1.png; nd xzgv 2.png
#cp -p out/* out${IN#in}/
