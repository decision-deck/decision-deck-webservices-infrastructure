#! /bin/sh

JAR=../build/performanceTableFilter-ITTB.jar

OUT=out
TMPF=$("tempfile")

for IN in in[0-9]*; do
  echo -n "$IN / out${IN#in} "
  /bin/rm -rf $OUT
  mkdir $OUT
  java -jar $JAR $IN/methodParameters.xml $IN/alternatives.xml $IN/criteria.xml $IN/performanceTable.xml $OUT/filteredPerformanceTable.xml $OUT/messages.xml
  diff -Bqrw out out${IN#in} > $TMPF 2>&1
  if [ $? -ne 0 ]; then
    echo "FAILED"
    cat $TMPF >&2
  else
    echo "SUCCESS"
  fi
  /bin/rm $TMPF
done
