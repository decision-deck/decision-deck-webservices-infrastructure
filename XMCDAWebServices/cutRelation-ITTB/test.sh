#! /bin/bash

TSTDIR=tests
OUT="$TSTDIR/out"

#JAR=advancedCutRelations-ITTB.jar
JAR=cutRelation-new.jar

for IN in "$TSTDIR"/in[0-9]*; do
  echo $IN
  /bin/rm -rf "$OUT"; mkdir "$OUT"
  java -Djava.awt.headless=true -jar $JAR  \
    $IN/options.xml $IN/alternatives.xml $IN/relation.xml \
    $OUT/outputRelation.xml $OUT/messages.xml
  echo $?
  diff -r "$TSTDIR"/"out"${IN##*/in} $OUT
done
