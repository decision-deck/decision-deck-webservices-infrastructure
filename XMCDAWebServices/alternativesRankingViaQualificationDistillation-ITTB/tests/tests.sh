#! /bin/sh

JAR=../build/alternativesRankingViaQualificationDistillation-ITTB.jar

OUT=out
TMPF=$("tempfile")
for IN in in[0-9]*; do
  echo -n "$IN / out${IN#in} "
  /bin/rm -rf $OUT
  mkdir $OUT
  java -jar "$JAR" $IN/alternatives.xml $IN/outrankingRelation.xml $OUT/downwardsDistillation.xml $OUT/upwardsDistillation.xml $OUT/intersectionDistillation.xml $OUT/messages.xml
  diff -qrw out out${IN#in} > $TMPF 2>&1
  if [ $? -ne 0 ]; then
    echo "FAILED"
    cat $TMPF >&2
  else
    echo "SUCCESS"
  fi
  /bin/rm $TMPF
done
