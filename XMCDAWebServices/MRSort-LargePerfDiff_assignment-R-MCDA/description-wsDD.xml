<?xml version='1.0' encoding='UTF-8'?>
<program_description>
	<program provider="R-MCDA" name="MRSort-LargePerfDiff_assignment" version="1.0" displayName="MRSort with large performance differences" />
	<documentation>
		<description>MRSort is a simplified ELECTRE TRI sorting method, where alternatives are assigned to an ordered set of categories. In this case, we also take into account large performance differences, both negative (vetoes) and positive (dictators).</description>
		<contact><![CDATA[Alexandru Olteanu (alexandru.olteanu@univ-ubs.fr)]]></contact>
		<reference>P. MEYER, A-L. OLTEANU, Integrating large positive and negative performance differences into multicriteria majority-rule sorting models, Computers and Operations Research, 81, pp. 216 - 230, 2017.</reference>
	</documentation>
	<parameters>

		<input id="inalt" name="alternatives" displayName="alternatives" isoptional="0">
			<documentation>
				<description>A complete list of alternatives to be considered by the MR-Sort method.</description>
			</documentation>
			<xmcda tag="alternatives" />
		</input>

		<input id="incateg" name="categories" displayName="categories" isoptional="0">
			<documentation>
				<description>A list of categories to which the alternatives will be assigned.</description>
			</documentation>
			<xmcda tag="categories" />
		</input>

		<input id="inperf" name="performanceTable" displayName="performance table" isoptional="0">
			<documentation>
				<description>The evaluations of the alternatives on the set of criteria.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</input>

		<input id="incatprofpt" name="categoriesProfilesPerformanceTable" displayName="categories profiles performance table" isoptional="0">
			<documentation>
				<description>The evaluations of the category profiles.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</input>

		<input id="invetoprofpt" name="vetoProfilesPerformanceTable" displayName="veto profiles performance table" isoptional="defaultTrue">
			<documentation>
				<description>The evaluations of the veto profiles.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</input>

		<input id="indictprofpt" name="dictatorProfilesPerformanceTable" displayName="dictator profiles performance table" isoptional="defaultTrue">
			<documentation>
				<description>The evaluations of the dictator profiles.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</input>

		<input id="incrit" name="criteria" displayName="criteria scales" isoptional="0">
			<documentation>
				<description>A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.</description>
			</documentation>
			<xmcda tag="criteriaScales" />
		</input>

		<input id="weights" name="criteriaWeights" displayName="criteria weights" isoptional="0">
			<documentation>
				<description>The criteria weights.</description>
			</documentation>
			<xmcda tag="criteriaValues" />
		</input>

		<input id="incatprof" name="categoriesProfiles" displayName="categories profiles" isoptional="0">
			<documentation>
				<description>The categories delimiting profiles.</description>
			</documentation>
			<xmcda tag="categoriesProfiles" />
		</input>

		<input id="invetoprof" name="vetoProfiles" displayName="veto profiles" isoptional="defaultTrue">
			<documentation>
				<description>The categories veto profiles.</description>
			</documentation>
			<xmcda tag="categoriesProfiles" />
		</input>

		<input id="indictprof" name="dictatorProfiles" displayName="dictator profiles" isoptional="defaultTrue">
			<documentation>
				<description>The categories dictator profiles.</description>
			</documentation>
			<xmcda tag="categoriesProfiles" />
		</input>

		<input id="incategval" name="categoriesRanks" displayName="categories ranks" isoptional="0">
			<documentation>
				<description>A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.</description>
			</documentation>
			<xmcda tag="categoriesValues" />
		</input>

		<input id="majorityThreshold" name="majorityThreshold" displayName="majority threshold" isoptional="0">
			<documentation>
				<description>The majority threshold.</description>
			</documentation>
			<xmcda tag="programParameters" />
		</input>

		<input id="assignmentRule" name="assignmentRule" displayName="assignment rule" isoptional="0">
			<documentation>
				<description>The type of assignment rule. Can be anything from the list M (majority rule), V (veto), D (dictator), v (veto weakened by dictator), d (dictator weakened by veto), dV (dominating veto and weakened dictator), Dv (dominating dictator and weakened veto) and dv (conflicting veto and dictator).</description>
			</documentation>
			<xmcda tag="programParameters" />
			<gui status="preferGUI">
				<entry id="%1" type="enum" displayName="Assignment rule">
					<documentation>
						<description>Indicates the type of assignment rule to be used.</description>
					</documentation>
					<items>
						<item id="M">
							<description>Majority rule</description>
							<value>M</value>
						</item>
						<item id="V">
							<description>Veto</description>
							<value>V</value>
						</item>
						<item id="D">
							<description>Dictator</description>
							<value>D</value>
						</item>
						<item id="v">
							<description>Veto weakened by dictator</description>
							<value>v</value>
						</item>
						<item id="d">
							<description>Dictator weakened by veto</description>
							<value>d</value>
						</item>
						<item id="dV">
							<description>Dominating Veto and weakened Dictator</description>
							<value>dV</value>
						</item>
						<item id="Dv">
							<description>Dominating Dictator and weakened veto</description>
							<value>Dv</value>
						</item>
						<item id="dv">
							<description>Conflicting Veto and Dictator</description>
							<value>dv</value>
						</item>
					</items>
					<defaultValue>M</defaultValue>
				</entry>
			</gui>
		</input>

		<output id="outaffect" name="alternativesAssignments" displayName="alternatives assignments">
			<documentation>
				<description>The alternatives assignments to categories.</description>
			</documentation>
			<xmcda tag="alternativesAssignments" />
		</output>

		<output id="msg" name="messages" displayName="messages">
			<documentation>
				<description>Messages from the execution of the webservice. Possible errors in the input data will be given here.</description>
			</documentation>
			<xmcda tag="programExecutionResult" />
		</output>

	</parameters>
</program_description>
