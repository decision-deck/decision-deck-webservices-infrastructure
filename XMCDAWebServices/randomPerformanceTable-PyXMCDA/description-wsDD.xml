<?xml version="1.0" encoding="UTF-8"?>
<program_description>
  <program provider="PyXMCDA"
       name="randomPerformanceTable"
       displayName="randomPerformanceTable"
       version="1.0" />

  <documentation>
    <description><![CDATA[This service generate a random performance table from inputs.
Any scale type can be used and the service have different minimum requirements:
    - Quantitative scales need at least the 'minimum' and 'maximum' values to be set
    - Qualitative scales need at least the valued labels set
    - Nominal scales need at least the labels set

Distributions can be defined per criterion for the quantitative scales in the criteriaDistributions input.
Default is a uniform distribution between scale minimum and maximum bounds.
To set another distribution, user needs to add a criterionValues with following identified values:
    - Uniform distribution: value 'uniform' with id 'distribution', numerical values with id 'min' and 'max' to override scale boundaries (optional)
    - Normal distribution: value 'normal' with id 'distribution', numerical values with id 'mu' and 'std' for average and standard value, numerical values with id 'min' and 'max' to override scale boundaries (optional)
N.B: 'min' and 'max' values will be ignored if they are outside of the scale limits.

Other types of scales use a uniform random sampler to choose labels (distributions inputs for these scales are ignored).

Example of normal distribution (with optional min/max values)::

    <criterionValues>
        <criterionID>g1</criterionID>
        <values>
            <value id="distribution">
                <label>normal</label>
            </value>
            <value id="mu">
                <real>25000</real>
            </value>
            <value id="std">
                <real>5000</real>
            </value>
            <value id="min">
                <real>0</real>
            </value>
            <value id="max">
                <real>40000</real>
            </value>
        </values>
    </criterionValues>

Example of uniform distribution (with optional min/max values)::

    <criterionValues>
    <criterionID>g2</criterionID>
        <values>
            <value id="distribution">
                <label>uniform</label>
            </value>
            <value id="min">
                <real>5</real>
            </value>
            <value id="max">
                <real>15</real>
            </value>
        </values>
    </criterionValues>
]]>
    </description>
    <contact>Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)</contact>
    <url>https://gitlab.com/nduminy/ws-pyxmcda</url>
  </documentation>

  <parameters>
      <input displayName="alternatives" name="alternatives" id="alternatives" isoptional="0">
        <documentation>
            <description>The alternatives (active field disregarded for this service).</description>
        </documentation>
        <xmcda tag="alternatives" />
      </input>

      <input displayName="criteria scales" name="criteriaScales" id="criteriaScales" isoptional="0">
        <documentation>
            <description>
                The criteria scales.
            </description>
        </documentation>
        <xmcda tag="criteriaScales" />
      </input>

      <input displayName="criteria distributions" name="criteriaDistributions" id="criteriaDistributions" isoptional="1">
        <documentation>
            <description>The distributions to use for each criterion.</description>
        </documentation>
        <xmcda tag="criteriaValues" />
      </input>

    <input id="parameters" name="parameters" displayName="parameters" isoptional="1">
          <documentation>
            <description>Parameters of the method</description>
          </documentation>
          <xmcda tag="programParameters"><![CDATA[
    <programParameters>
        <programParameter id="seed" name="Seed">
            <values>
                <value>
                    <integer>%1</integer>
                </value>
            </values>
        </programParameter>
    </programParameters>
]]></xmcda>
          <gui status="preferGUI">
            <entry id="%1" type="int" displayName="Seed">
              <documentation>
                <description>Seed to be used for random numbers.</description>
              </documentation>
              <defaultValue>0</defaultValue>
            </entry>
          </gui>
        </input>

    <output displayName="performance table" name="performanceTable" id="performanceTable">
        <documentation>
            <description>The performance table.</description>
        </documentation>
        <xmcda tag="performanceTable" />
    </output>

    <output displayName="messages" name="messages" id="messages">
        <documentation>
            <description>Status messages.</description>
        </documentation>
        <xmcda tag="programExecutionResult" />
    </output>
  </parameters>
</program_description>
