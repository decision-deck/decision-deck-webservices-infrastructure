int sz = 1155;int no_obj = 14;
real[] obj_x={305.50421037,-101.904768137,-432.218598619,-31.4014650511,82.7607000969,-361.715295533,82.9518184469,-31.0919974989,-247.553130385,-120.679935496,-247.362012035,129.295211687,-361.405827981,305.104497796};
real[] obj_y={125.0,330.120094216,-4.88449556902,233.015592497,269.988556251,-101.988997288,389.988404058,427.324821193,-65.0160335342,-307.236351564,54.9838142731,-303.711328054,92.3202314087,-125.972488973};
real[] arc_type={0,0,0,1,0,2,2,1,2,1,3,1,1,1,0,0,0,0,0,1,2,0,1,1,1,2,2,0,2,2,0,2,0,3,0,1,0,1,0,0,1,2,1,1,1,1,0,2,0,0,2,0,1,3,1,2,2,0,2,0,3,0,1,0,1,0,1,1,1,1,1,0,0,1,1,1,2,0,3,0,1,1,1,1,1,3,1,1,3,3,0};
string[] labels={"a11","a03","a07","a12","a10","a08","a05","a04","a01","a06","a13","a02","a09","a14"};
import settings;
import plain;

outformat="png";
int sz = 700;
size(sz,sz);
pen dashed=linetype(new real[] {8,8});
int circle_size = 20;

real  x1,x2,y1,y2;
int ct = 0;
if(no_obj > 1)
{
	for(int i=0;i<no_obj-1;++i)
	{
		for(int j=i+1;j<no_obj;++j)
		{
			x1 = obj_x[i];
			y1 = obj_y[i];
			x2 = obj_x[j];
			y2 = obj_y[j];
			if(arc_type[ct] == 0)
			{
				draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size--(x2,y2)-dir((x1,y1)--(x2,y2))*circle_size,rgb(0.3,0,0.3));
				draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size--(x1,y1)+dir((x1,y1)--(x2,y2))*circle_size * 3,rgb(0.4,0,0.4) + 3);
				draw((x2,y2)+dir((x2,y2)--(x1,y1))*circle_size * 3--(x2,y2)+dir((x2,y2)--(x1,y1))*circle_size,rgb(0.4,0,0.4) + 3);
			}
			if(arc_type[ct] == 1)
			{
				draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size--(x2,y2)-dir((x1,y1)--(x2,y2))*circle_size,rgb(0.3,0,0.3));
				draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size--(x1,y1)+dir((x1,y1)--(x2,y2))*(circle_size * 3 - 15),rgb(1,0,0) + 3);
				draw((x2,y2)+dir((x2,y2)--(x1,y1))*circle_size * 3--(x2,y2)+dir((x2,y2)--(x1,y1))*(circle_size + 15),rgb(0,0,1) + 3);
				draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size--(x1,y1)+dir((x1,y1)--(x2,y2))*circle_size * 3,rgb(1,0,0),Arrow(20));
				draw((x2,y2)+dir((x2,y2)--(x1,y1))*circle_size * 3--(x2,y2)+dir((x2,y2)--(x1,y1))*circle_size,rgb(0,0,1),Arrow(20));
			}
			if(arc_type[ct] == 2)
			{
				draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size--(x2,y2)-dir((x1,y1)--(x2,y2))*circle_size,rgb(0.3,0,0.3));
				draw((x2,y2)+dir((x2,y2)--(x1,y1))*circle_size--(x2,y2)+dir((x2,y2)--(x1,y1))*(circle_size * 3 - 15),rgb(1,0,0) + 3);
				draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size * 3--(x1,y1)+dir((x1,y1)--(x2,y2))*(circle_size + 15),rgb(0,0,1) + 3);
				draw((x2,y2)+dir((x2,y2)--(x1,y1))*circle_size--(x2,y2)+dir((x2,y2)--(x1,y1))*circle_size * 3,rgb(1,0,0),Arrow(20));
				draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size * 3--(x1,y1)+dir((x1,y1)--(x2,y2))*circle_size,rgb(0,0,1),Arrow(20));
			}
			if(arc_type[ct] == 3)
				draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size--(x2,y2)-dir((x1,y1)--(x2,y2))*circle_size,rgb(0,0,0)+2);
			ct = ct + 1;
		}
	}
}
for(int i=0;i<no_obj;++i)
{
	x1 = obj_x[i];
	y1 = obj_y[i];
	fill(circle((x1,y1),circle_size),rgb(1,1,1));
	draw(circle((x1,y1),circle_size),rgb(0.0,0.0,0.0)+2);
	label(labels[i],(x1,y1),rgb(0.0,0.0,0.0)+fontsize(24));
}
shipout(bbox(Fill(rgb(1,1,1))));
