int sz = 1155;int no_arcs = 20;
int no_cluster_arcs = 15;
real[] arcs_x1={-101.904768137,-101.904768137,-101.904768137,-101.904768137,-31.4014650511,-31.4014650511,-31.4014650511,82.7607000969,82.7607000969,82.9518184469,-432.218598619,-432.218598619,-432.218598619,-432.218598619,-361.715295533,-361.715295533,-361.715295533,-247.553130385,-247.553130385,-247.362012035};
real[] arcs_x2={-31.4014650511,82.7607000969,82.9518184469,-31.0919974989,82.7607000969,82.9518184469,-31.0919974989,82.9518184469,-31.0919974989,-31.0919974989,-361.715295533,-247.553130385,-247.362012035,-361.405827981,-247.553130385,-247.362012035,-361.405827981,-247.362012035,-361.405827981,-361.405827981};
real[] arcs_y1={330.120094216,330.120094216,330.120094216,330.120094216,233.015592497,233.015592497,233.015592497,269.988556251,269.988556251,389.988404058,-4.88449556902,-4.88449556902,-4.88449556902,-4.88449556902,-101.988997288,-101.988997288,-101.988997288,-65.0160335342,-65.0160335342,54.9838142731};
real[] arcs_y2={233.015592497,269.988556251,389.988404058,427.324821193,269.988556251,389.988404058,427.324821193,389.988404058,427.324821193,427.324821193,-101.988997288,-65.0160335342,54.9838142731,92.3202314087,-65.0160335342,54.9838142731,92.3202314087,54.9838142731,92.3202314087,92.3202314087};
real[] cluster_arcs_x1={305.50421037,305.50421037,305.50421037,305.50421037,305.50421037,0.262857571411,0.262857571411,0.262857571411,0.262857571411,-330.05097291,-330.05097291,-330.05097291,-120.679935496,-120.679935496,129.295211687};
real[] cluster_arcs_x2={0.262857571411,-330.05097291,-120.679935496,129.295211687,305.104497796,-330.05097291,-120.679935496,129.295211687,305.104497796,-120.679935496,129.295211687,305.104497796,129.295211687,305.104497796,305.104497796};
real[] cluster_arcs_y1={125.0,125.0,125.0,125.0,125.0,330.087493643,330.087493643,330.087493643,330.087493643,-4.9170961418,-4.9170961418,-4.9170961418,-307.236351564,-307.236351564,-303.711328054};
real[] cluster_arcs_y2={330.087493643,-4.9170961418,-307.236351564,-303.711328054,-125.972488973,-4.9170961418,-307.236351564,-303.711328054,-125.972488973,-307.236351564,-303.711328054,-125.972488973,-303.711328054,-125.972488973,-125.972488973};
real[] cluster_arcs_z1={50,50,50,50,50,102.122874885,102.122874885,102.122874885,102.122874885,102.122874885,102.122874885,102.122874885,50,50,50};
real[] cluster_arcs_z2={102.122874885,102.122874885,50,50,50,102.122874885,50,50,50,50,50,50,50,50,50};
real[] arc_type={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
real[] cluster_arc_type={2,1,2,3,1,1,2,1,1,2,3,1,1,1,3};
int no_points = 14;
real[] points_x={305.50421037,-101.904768137,-31.4014650511,82.7607000969,82.9518184469,-31.0919974989,-432.218598619,-361.715295533,-247.553130385,-247.362012035,-361.405827981,-120.679935496,129.295211687,305.104497796};
real[] points_y={125.0,330.120094216,233.015592497,269.988556251,389.988404058,427.324821193,-4.88449556902,-101.988997288,-65.0160335342,54.9838142731,92.3202314087,-307.236351564,-303.711328054,-125.972488973};
string[] labels={"a11","a03","a12","a10","a05","a04","a07","a08","a01","a13","a09","a06","a02","a14"};
import settings;
import plain;

outformat="png";
int sz = 700;
size(sz,sz);
pen dashed=linetype(new real[] {8,8});
int circle_size = 20;

real  x1,x2,y1,y2,c1_circle_size,c2_circle_size;
for(int i=0;i<no_arcs;++i)
{
	x1 = arcs_x1[i];
	y1 = arcs_y1[i];
	x2 = arcs_x2[i];
	y2 = arcs_y2[i];
	if(arc_type[i] == 0)
	{
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size--(x2,y2)-dir((x1,y1)--(x2,y2))*circle_size,rgb(0.3,0,0.3));
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size--(x1,y1)+dir((x1,y1)--(x2,y2))*circle_size * 3,rgb(0.4,0,0.4) + 3);
		draw((x2,y2)+dir((x2,y2)--(x1,y1))*circle_size * 3--(x2,y2)+dir((x2,y2)--(x1,y1))*circle_size,rgb(0.4,0,0.4) + 3);
	}
	if(arc_type[i] == 1)
	{
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size--(x2,y2)-dir((x1,y1)--(x2,y2))*circle_size,rgb(0.3,0,0.3));
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size--(x1,y1)+dir((x1,y1)--(x2,y2))*(circle_size * 3 - 15),rgb(1,0,0) + 3);
		draw((x2,y2)+dir((x2,y2)--(x1,y1))*circle_size * 3--(x2,y2)+dir((x2,y2)--(x1,y1))*(circle_size + 15),rgb(0,0,1) + 3);
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size--(x1,y1)+dir((x1,y1)--(x2,y2))*circle_size * 3,rgb(1,0,0),Arrow(20));
		draw((x2,y2)+dir((x2,y2)--(x1,y1))*circle_size * 3--(x2,y2)+dir((x2,y2)--(x1,y1))*circle_size,rgb(0,0,1),Arrow(20));
	}
	if(arc_type[i] == 2)
	{
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size--(x2,y2)-dir((x1,y1)--(x2,y2))*circle_size,rgb(0.3,0,0.3));
		draw((x2,y2)+dir((x2,y2)--(x1,y1))*circle_size--(x2,y2)+dir((x2,y2)--(x1,y1))*(circle_size * 3 - 15),rgb(1,0,0) + 3);
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size * 3--(x1,y1)+dir((x1,y1)--(x2,y2))*(circle_size + 15),rgb(0,0,1) + 3);
		draw((x2,y2)+dir((x2,y2)--(x1,y1))*circle_size--(x2,y2)+dir((x2,y2)--(x1,y1))*circle_size * 3,rgb(1,0,0),Arrow(20));
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size * 3--(x1,y1)+dir((x1,y1)--(x2,y2))*circle_size,rgb(0,0,1),Arrow(20));
	}
	if(arc_type[i] == 3)
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*circle_size--(x2,y2)-dir((x1,y1)--(x2,y2))*circle_size,rgb(0,0,0)+2);
}
for(int i=0;i<no_points;++i)
{
	x1 = points_x[i];
	y1 = points_y[i];
	fill(circle((x1,y1),circle_size),rgb(1,1,1));
	draw(circle((x1,y1),circle_size),rgb(0.0,0.0,0.0)+2);
	label(labels[i],(x1,y1),rgb(0.0,0.0,0.0)+fontsize(24));
}
for(int i=0;i<no_cluster_arcs;++i)
{
	x1 = cluster_arcs_x1[i];
	y1 = cluster_arcs_y1[i];
	x2 = cluster_arcs_x2[i];
	y2 = cluster_arcs_y2[i];
	c1_circle_size = cluster_arcs_z1[i] + 2* circle_size;
	c2_circle_size = cluster_arcs_z2[i] + 2* circle_size;
	if(cluster_arc_type[i] == 0)
	{
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*c1_circle_size--(x2,y2)-dir((x1,y1)--(x2,y2))*c2_circle_size,rgb(0.3,0,0.3));
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*c1_circle_size--(x1,y1)+dir((x1,y1)--(x2,y2))*(c1_circle_size + circle_size*2),rgb(0.4,0,0.4) + 3);
		draw((x2,y2)+dir((x2,y2)--(x1,y1))*(c2_circle_size + circle_size*2)--(x2,y2)+dir((x2,y2)--(x1,y1))*c2_circle_size,rgb(0.4,0,0.4) + 3);
	}
	if(cluster_arc_type[i] == 1)
	{
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*c1_circle_size--(x2,y2)-dir((x1,y1)--(x2,y2))*c2_circle_size,rgb(0.3,0,0.3));
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*c1_circle_size--(x1,y1)+dir((x1,y1)--(x2,y2))*(c1_circle_size + circle_size*2-15),rgb(1,0,0) + 3);
		draw((x2,y2)+dir((x2,y2)--(x1,y1))*(c2_circle_size + circle_size*2)--(x2,y2)+dir((x2,y2)--(x1,y1))*(c2_circle_size+15),rgb(0,0,1) + 3);
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*c1_circle_size--(x1,y1)+dir((x1,y1)--(x2,y2))*(c1_circle_size + circle_size*2),rgb(1,0,0),Arrow(20));
		draw((x2,y2)+dir((x2,y2)--(x1,y1))*(c2_circle_size + circle_size*2)--(x2,y2)+dir((x2,y2)--(x1,y1))*c2_circle_size,rgb(0,0,1),Arrow(20));
	}
	if(cluster_arc_type[i] == 2)
	{
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*c1_circle_size--(x2,y2)-dir((x1,y1)--(x2,y2))*c2_circle_size,rgb(0.3,0,0.3));
		draw((x2,y2)+dir((x2,y2)--(x1,y1))*c2_circle_size--(x2,y2)+dir((x2,y2)--(x1,y1))*(c2_circle_size + circle_size*2-15),rgb(1,0,0) + 3);
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*(c1_circle_size + circle_size*2)--(x1,y1)+dir((x1,y1)--(x2,y2))*(c1_circle_size+15),rgb(0,0,1) + 3);
		draw((x2,y2)+dir((x2,y2)--(x1,y1))*c2_circle_size--(x2,y2)+dir((x2,y2)--(x1,y1))*(c2_circle_size + circle_size*2),rgb(1,0,0),Arrow(20));
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*(c1_circle_size + circle_size*2)--(x1,y1)+dir((x1,y1)--(x2,y2))*c1_circle_size,rgb(0,0,1),Arrow(20));
	}
	if(cluster_arc_type[i] == 3)
		draw((x1,y1)+dir((x1,y1)--(x2,y2))*c1_circle_size--(x2,y2)-dir((x1,y1)--(x2,y2))*c2_circle_size,rgb(0,0,0)+2);
}
shipout(bbox(Fill(rgb(1,1,1))));
