<?xml version="1.0" encoding="ISO-8859-1"?>
<program_description xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://www.decision-deck.org/ws/_downloads/description.xsd">
    <program provider="ITTB" name="plotCriteriaValues" displayName="plotCriteriaValues" version="2.0"/>
       <documentation>
           <description>This web service generates a barplot or a pie plot representing a numeric quantity for each criterion, like, e.g., an importance value. Compared to the web service plotCriteriaValues, some parameters are added. Colors can be used and the title of the plot can be typed. In the case of a bar chart, the axis-labels can also be typed. The criteria's evaluations are supposed to be real or integer numeric values.</description>
              <contact>Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)</contact>
       </documentation>

       <parameters>

              <input id="criteria" name="criteria" displayName="criteria" isoptional="0">
                     <documentation>
                            <description>A list of criteria. Criteria can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), criteria are considered as active.</description>
                     </documentation>
               <xmcda tag="criteria"><![CDATA[
	<criteria>
		<criterion>
			<active>[...]</active>
			[...]
		</criterion>
		[...]
	</criteria>
]]></xmcda>
              </input>

           <input id="criteriaValues" name="criteriaValues" displayName="criteriaValues" isoptional="0">

                     <documentation>
                         <description>A list of &lt;criterionValue&gt; representing a certain numeric quantity for each criterion, like, e.g., an importance value.</description>
                     </documentation>
               <xmcda tag="criteriaValues"/>
              </input>

           <input id="parameters" name="parameters" displayName="parameters" isoptional="0">
               <documentation>
                   <description>Plot type method: choose between "Bar chart" and "Pie chart". The default plot is a bar chart.</description>
               </documentation>
               <xmcda tag="programParameters"><![CDATA[
	<programParameters>
		<parameter id="chart_type" name="Chart type">
			<values>
				<value>
					<label>%1</label>
				</value>
			</values>
		</parameter>
		<parameter id="order_by" name="Order by">
			<values>
				<value>
					<label>%2</label>
				</value>
			</values>
		</parameter>
		<parameter id="order" name="Order">
			<values>
				<value>
					<label>%3</label>
				</value>
			</values>
		</parameter>
		<parameter id="use_color" name="Colors in the chart">
			<values>
				<value>
					<label>%4</label>
				</value>
			</values>
		</parameter>
		<parameter id="initial_color" name="Initial color">
			<values>
				<value>
					<label>%5</label>
				</value>
			</values>
		</parameter>
		<parameter id="final_color" name="Final color">
			<values>
				<value>
					<label>%6</label>
				</value>
			</values>
		</parameter>
		<parameter id= "chart_title" name="Chart title">
			<values>
				<value>
					<label>%7</label>
				</value>
			</values>
		</parameter>
		<parameter id="domain_axis" name="Domain axis label">
			<values>
				<value>
					<label>%8</label>
				</value>
			</values>
		</parameter >
		<parameter id="range_axis" name="Range axis label">
			<values>
				<value>
					<label>%9</label>
				</value>
			</values>
		</parameter>
	</programParameters>
]]></xmcda>
               <gui status="preferGUI">
                   <entry id="%1" type="enum" displayName="Chart type:">
                       <documentation>
                       <description>Type of the plot: choose between "Bar chart" and "Pie chart". The default plot is a bar chart.</description>
                           </documentation>
                       <items>
                           <item id="bar_chart">
                               <description>bar chart</description>
                               <value>barChart</value>
                           </item>
                           <item id="pie_chart">
                               <description>pie chart</description>
                               <value>pieChart</value>
                           </item>
                       </items>
                       <defaultValue>bar_chart</defaultValue>
                   </entry>
                   <entry id="%2" type="enum" displayName="Order by:">
                       <documentation>
                           <description>Choose between "name", "id" or "values".</description>
                       </documentation>
                       <items>
                           <item id="order_by_name">
                               <description>name</description>
                               <value>name</value>
                           </item>
                           <item id="order_by_id">
                               <description>id</description>
                               <value>id</value>
                           </item>
                           <item id="order_by_values">
                               <description>values</description>
                               <value>values</value>
                           </item>
                       </items>
                       <defaultValue>order_by_values</defaultValue>
                   </entry>
                   <entry id="%3" type="enum" displayName="Order:">
                       <documentation>
                           <description>The parameter which says if the highest or lowest value is to be placed first.</description>
                       </documentation>
                       <items>
                           <item id="increasing">
                               <description>increasing</description>
                               <value>increasing</value>
                           </item>
                           <item id="decreasing">
                               <description>decreasing</description>
                               <value>decreasing</value>
                           </item>
                       </items>
                       <defaultValue>increasing</defaultValue>
                   </entry>
                   <entry id="%4" type="enum" displayName="Colors:">
                       <documentation>
                       <description>The use of colors: true for a colored plot.</description>
                       </documentation>
                       <items>
                           <item id="true">
                               <description>gradient</description>
                               <value>true</value>
                           </item>
                           <item id="false">
                               <description>black and white</description>
                               <value>false</value>
                           </item>
                       </items>
                       <defaultValue>false</defaultValue>
                   </entry>
                   <entry id="%5" type="enum" displayName="Initial color:">
                       <documentation>
                           <description>String that indicates the initial color in the generated barplot or pieplot.Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".</description>
                       </documentation>
                       <!-- %5 is active if the entry id=%4 takes the value true -->
                       <dependency><![CDATA[%4:type="true"]]></dependency>
                       <items>
                           <item id="black">
                               <description>black</description>
                               <value>black</value>
                           </item>
                           <item id="red">
                               <description>red</description>
                               <value>red</value>
                           </item>
                           <item id="blue">
                               <description>blue</description>
                               <value>blue</value>
                           </item>
                           <item id="green">
                               <description>green</description>
                               <value>green</value>
                           </item>
                           <item id="yellow">
                               <description>yellow</description>
                               <value>yellow</value>
                           </item>
                           <item id="magenta">
                               <description>magenta</description>
                               <value>magenta</value>
                           </item>
                           <item id="cyan">
                               <description>cyan</description>
                               <value>cyan</value>
                           </item>
                       </items>
                       <defaultValue>black</defaultValue>
                   </entry>
                   <entry id="%6" type="enum" displayName="Final color:">
                       <documentation>
                           <description>String that indicates the final color in the generated barplot or pieplot. choose between "Black", "White", "Red", "Blue", "Green", "Yellow", "Magenta", "Cyan" and "White".</description>
                       </documentation>
                       <!-- %6 is active if the entry id=%4 takes the value true -->
                       <dependency><![CDATA[%4:type="true"]]></dependency>
                       <items>
                           <item id="black">
                               <description>black</description>
                               <value>black</value>
                           </item>
                           <item id="red">
                               <description>red</description>
                               <value>red</value>
                           </item>
                           <item id="blue">
                               <description>blue</description>
                               <value>blue</value>
                           </item>
                           <item id="green">
                               <description>green</description>
                               <value>green</value>
                           </item>
                           <item id="yellow">
                               <description>yellow</description>
                               <value>yellow</value>
                           </item>
                           <item id="magenta">
                               <description>magenta</description>
                               <value>magenta</value>
                           </item>
                           <item id="cyan">
                               <description>cyan</description>
                               <value>cyan</value>
                           </item>
                           <item id="white">
                               <description>white</description>
                               <value>white</value>
                           </item>
                       </items>
                       <defaultValue>black</defaultValue>
                   </entry>
                   <entry id="%7" type="string" displayName="Chart title:">
                       <documentation>
                           <description>String for the title of the plot. The default value is an empty field.</description>
                       </documentation>
                       <defaultValue></defaultValue>
                   </entry>
                   <entry id="%8" type="string" displayName="X axis label:">
                       <documentation>
                           <description>String for the horizontal axis-label.The default value is an empty field.</description>
                       </documentation>
                       <defaultValue></defaultValue>
                       <!-- %8 is active if the entry id=%1 takes the value Bar Chart -->
                       <dependency><![CDATA[%1:type="bar_chart"]]></dependency>
                   </entry>

                   <entry id="%9" type="string" displayName="Y axis label:">
                       <documentation>
                       <description>String for the vertical axis-label.The default value is an empty field.</description>
                        </documentation>
                       <defaultValue></defaultValue>
                       <!-- %9 is active if the entry id=%1 takes the value Bar Chart -->
                       <dependency><![CDATA[%1:type="bar_chart"]]></dependency>
                   </entry>
               </gui>
           </input>

           <output id="png" name="criteriaValues.png" displayName="criteria values (png)">
                     <documentation>
                         <description>The requested barplot or pieplot as a PNG image.</description>
                     </documentation>
               <xmcda tag="criterionValue"/>
           </output>

           <output id="messages" name="messages" displayName="messages">
               <documentation>
                   <description>A list of messages generated by the algorithm.</description>
               </documentation>
               <xmcda tag="programExecutionResult"/>
           </output>

       </parameters>
</program_description>
