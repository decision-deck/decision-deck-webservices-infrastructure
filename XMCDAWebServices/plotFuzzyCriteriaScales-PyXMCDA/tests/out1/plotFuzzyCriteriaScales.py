import matplotlib.pyplot as plt
from numpy import array
from collections import namedtuple, OrderedDict
import sys
from pathlib import Path


Plot = namedtuple('Plot', 'points, options')   # points is a list of (segment as numpy array)

Figure = namedtuple('Figure', 'axes, options, show_grid')   # axes is dictionary {title: plots}


figure = Figure(
    axes=[
        (
            "c01/fuzzy scales",
            OrderedDict(
                [
                    (
                        "Very Low",
                        Plot(
                            points=[
                                array([[0.0, 0.0], [0.2, 1.0]]),
                                array([[0.2, 1.0], [0.5, 0.0]]),
                            ],
                            options={
                                "color": "#000000",
                                "marker": ".",
                                "linestyle": "solid",
                            },
                        ),
                    ),
                    (
                        "Low",
                        Plot(
                            points=[
                                array([[0.0, 0.0], [0.2, 1.0]]),
                                array([[0.2, 1.0], [0.5, 0.0]]),
                            ],
                            options={
                                "color": "#000000",
                                "marker": ",",
                                "linestyle": "solid",
                            },
                        ),
                    ),
                    (
                        "Medium",
                        Plot(
                            points=[
                                array([[0.2, 0.0], [0.5, 1.0]]),
                                array([[0.5, 1.0], [0.6, 0.0]]),
                            ],
                            options={
                                "color": "#000000",
                                "marker": "o",
                                "linestyle": "solid",
                            },
                        ),
                    ),
                    (
                        "Almost High",
                        Plot(
                            points=[
                                array([[0.5, 0.0], [0.6, 1.0]]),
                                array([[0.6, 1.0], [0.7, 0.0]]),
                            ],
                            options={
                                "color": "#000000",
                                "marker": "v",
                                "linestyle": "solid",
                            },
                        ),
                    ),
                    (
                        "High",
                        Plot(
                            points=[
                                array([[0.6, 0.0], [0.7, 1.0]]),
                                array([[0.7, 1.0], [0.8, 1.0]]),
                                array([[0.8, 1.0], [0.9, 0.0]]),
                            ],
                            options={
                                "color": "#000000",
                                "marker": "^",
                                "linestyle": "solid",
                            },
                        ),
                    ),
                    (
                        "Very High",
                        Plot(
                            points=[
                                array([[0.8, 0.0], [0.9, 1.0]]),
                                array([[0.9, 1.0], [1.0, 0.0]]),
                            ],
                            options={
                                "color": "#000000",
                                "marker": "<",
                                "linestyle": "solid",
                            },
                        ),
                    ),
                    (
                        "Perfect",
                        Plot(
                            points=[array([[0.9, 0.0], [1.0, 1.0]])],
                            options={
                                "color": "#000000",
                                "marker": ">",
                                "linestyle": "solid",
                            },
                        ),
                    ),
                ]
            ),
        ),
        (
            "c02/fuzzy scales",
            OrderedDict(
                [
                    (
                        "Very Low",
                        Plot(
                            points=[
                                array([[0.0, 0.0], [0.2, 1.0]]),
                                array([[0.2, 1.0], [0.5, 0.0]]),
                            ],
                            options={
                                "color": "#000000",
                                "marker": ".",
                                "linestyle": "solid",
                            },
                        ),
                    ),
                    (
                        "Low",
                        Plot(
                            points=[
                                array([[0.0, 0.0], [0.2, 1.0]]),
                                array([[0.2, 1.0], [0.5, 0.0]]),
                            ],
                            options={
                                "color": "#000000",
                                "marker": ",",
                                "linestyle": "solid",
                            },
                        ),
                    ),
                    (
                        "Medium",
                        Plot(
                            points=[
                                array([[0.2, 0.0], [0.5, 1.0]]),
                                array([[0.5, 1.0], [0.6, 0.0]]),
                            ],
                            options={
                                "color": "#000000",
                                "marker": "o",
                                "linestyle": "solid",
                            },
                        ),
                    ),
                    (
                        "Almost High",
                        Plot(
                            points=[
                                array([[0.5, 0.0], [0.6, 1.0]]),
                                array([[0.6, 1.0], [0.7, 0.0]]),
                            ],
                            options={
                                "color": "#000000",
                                "marker": "v",
                                "linestyle": "solid",
                            },
                        ),
                    ),
                    (
                        "High",
                        Plot(
                            points=[
                                array([[0.6, 0.0], [0.7, 1.0]]),
                                array([[0.7, 1.0], [0.8, 1.0]]),
                                array([[0.8, 1.0], [0.9, 0.0]]),
                            ],
                            options={
                                "color": "#000000",
                                "marker": "^",
                                "linestyle": "solid",
                            },
                        ),
                    ),
                    (
                        "Very High",
                        Plot(
                            points=[
                                array([[0.8, 0.0], [0.9, 1.0]]),
                                array([[0.9, 1.0], [1.0, 0.0]]),
                            ],
                            options={
                                "color": "#000000",
                                "marker": "<",
                                "linestyle": "solid",
                            },
                        ),
                    ),
                    (
                        "Perfect",
                        Plot(
                            points=[array([[0.9, 0.0], [1.0, 1.0]])],
                            options={
                                "color": "#000000",
                                "marker": ">",
                                "linestyle": "solid",
                            },
                        ),
                    ),
                ]
            ),
        ),
    ],
    options={
        "ncols": 2,
        "nrows": 1,
        "subplot_kw": {"xlabel": "series", "ylabel": "fuzzy numbers"},
        "squeeze": False,
        "constrained_layout": True,
        "figsize": (10, 2.2),
    },
    show_grid=False,
)



def save_fig(fig, dirname, filename):
    fig.savefig(Path(dirname, filename))


def plot_fuzzy_numbers(figure):
    fig, axes = plt.subplots(**figure.options)
    for i, (k, plots) in enumerate(figure.axes):
        ax = axes[int(i/2), i%2]
        if figure.show_grid:
            ax.grid()
        ax.set_title(k)
        for label, plot in plots.items():
            plot_fuzzy_number(plot.points, ax, label, **plot.options)
        ax.legend(bbox_to_anchor=(1,1), loc='best', fontsize='small')
    if len(figure.axes) % 2 == 1 and len(figure.axes) != 1:
        axes[-1,1].remove()
    return fig


def plot_fuzzy_number(points, ax, label, **kwargs):
    for i, seg in enumerate(points):
        myLabel = None
        if i == 0:
            myLabel = label
        ax.plot(seg[:,0], seg[:,1], label=myLabel, **kwargs)


def main(directory='.'):
    f = plot_fuzzy_numbers(figure)
    save_fig(f, directory, "fuzzyCriteriaScales.png")



if __name__ == "__main__":
    sys.exit(main())






