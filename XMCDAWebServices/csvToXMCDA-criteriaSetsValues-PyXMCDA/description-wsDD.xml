<?xml version="1.0" encoding="UTF-8"?>
<program_description>
  <program provider="PyXMCDA"
       name="csvToXMCDA-criteriaSetsValues"
       displayName="csvToXMCDA-criteriaSetsValues"
       version="1.0" />
     <documentation>
        <description>Transforms a file containing criteria sets values from a comma-separated values (CSV) file to three XMCDA compliant files, containing the corresponding criteria/criteriaSets ids and their criteriaSetsValues.</description>
        <contact>Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)</contact>
        <url>https://gitlab.com/nduminy/ws-pyxmcda</url>
     </documentation>

     <parameters>
        <input displayName="criteria values (csv)" name="criteriaSetsValues.csv" id="csv" isoptional="0">
            <documentation>
                <description>The criteria and their criteria values as a CSV file.  The first line is made of two cells, the first one being empty, and the second one will be the content of the attribute "mcdaConcept" in the tag "&lt;criteriaValues>", if supplied.  The following lines are made of at least two cells, with the first cell being an criterion' id (and name, see below), and the remaining cells their associated values.

The CSV is formatted by pairs of lines, each describing a criteriaSetValues.

Example::

    set1 (performance),1.8,19
    ,g1,g2 (time),g3

The first line contains the criteriaSet id on the first cell. Additionally, the criteriaSet name is also extracted when the cell is formatted like `id (name)`.  Set the parameter "First column" to "id" to deactivate the extraction of criteriaSet name.

The following cells on the first line is the enumeration of all values associated to this criteriaSet.
By default the values are considered as float numbers.  This can be changed using the parameter "Default content".  It is possible to specify the type of a value by prepending it with a prefix:

- `float:` for floats (ex.: `1`, `1.2`, `1.2e3`)
- `integer:` for integers (decimal representation: `127`, hexadecimal: `0x7f`, octal: `0o177`, binary: `0b1111111`)
- `string:` for strings (note that a string with a colon should always be prefixed by `string:`, no matter what the default prefix is).
- `boolean:` for booleans: 1 or 'true' (case insensitive) are True values, everything else is false.
- `na:` for N/A (everything after the colon is ignored)

The second line contains the criteria' ids. Additionally, the criteria' names are also extracted when the cells are formatted like `id (name)`.  Set the parameter "First column" to "id" to deactivate the extraction of criteria' names.

Complete example::

    performance,1.8
    ,g1,g2,g3
    safety,3.8,s:Perfect,b:false
    ,g3,g4 (maintenance),g5 (risk)

</description>
            </documentation>
            <xmcda tag="other" /> <!-- Not a real xmcda tag -->
        </input>

        <input id="parameters" name="parameters" displayName="parameters" isoptional="0">
          <documentation>
            <description>Parameters of the method</description>
          </documentation>
          <xmcda tag="programParameters"><![CDATA[
    <programParameters>
        <programParameter id="csv_delimiter">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="default_prefix">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="name_in_id">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </programParameter>
    </programParameters>
]]></xmcda>
          <gui status="preferGUI">
            <entry id="%1" type="string" displayName="CSV delimiter">
              <documentation>
                <description>Indicates the delimiter to use. Leave blank for auto-detection.  It is especially handful when the auto-detection fails to determine the csv delimiter.</description>
              </documentation>
              <constraint>
                <description>One character maximum</description>
                <code><![CDATA[%1.length() < 2]]></code>
              </constraint>
              <defaultValue></defaultValue> <!-- default is blank: auto-detection -->
            </entry>
            <entry id="%2" type="enum" displayName="Default content">
              <documentation>
                <description>.</description>
              </documentation>
              <items>
                  <item id="float">
                      <description>float</description>
                      <value>float</value>
                  </item>
                  <item id="string">
                      <description>string</description>
                      <value>label</value>
                  </item>
                  <item id="integer">
                      <description>integer</description>
                      <value>integer</value>
                  </item>
                  <item id="boolean">
                      <description>boolean</description>
                      <value>boolean</value>
                  </item>
              </items>
              <defaultValue>float</defaultValue>
            </entry>
            <entry id="%3" type="enum" displayName="First column">
              <documentation>
                <description>Content of the first column.</description>
              </documentation>
              <items>
                  <item id="id_only">
                      <description>id</description>
                      <value>false</value>
                  </item>
                  <item id="id_and_name">
                      <description>id (name)</description>
                      <value>true</value>
                  </item>
	      </items>
              <defaultValue>id_and_name</defaultValue>
            </entry>
          </gui>
        </input>

        <output displayName="criteria" name="criteria" id="criteria">
            <documentation>
                <description>The contained criteria.</description>
            </documentation>
            <xmcda tag="criteria" />
        </output>

        <output displayName="criteria sets" name="criteriaSets" id="criteriaSets">
            <documentation>
                <description>The contained criteria sets.</description>
            </documentation>
            <xmcda tag="criteriaSets" />
        </output>

        <output displayName="criteria sets values" name="criteriaSetsValues" id="criteriaSetsValues">
            <documentation>
                <description>The equivalent criteria sets values.</description>
            </documentation>
            <xmcda tag="criteriaSetsValues" />
        </output>

        <output displayName="messages" name="messages" id="messages">
            <documentation>
                <description>Status messages.</description>
            </documentation>
            <xmcda tag="programExecutionResult" />
        </output>

     </parameters>
</program_description>
