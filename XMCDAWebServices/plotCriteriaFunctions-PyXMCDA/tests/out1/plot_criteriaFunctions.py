import matplotlib.pyplot as plt
from numpy import array
from collections import namedtuple, OrderedDict
import sys
from pathlib import Path


Plot = namedtuple('Plot', 'points, options, stem, xticks')

Figure = namedtuple('Figure', 'axes, options, show_grid')


figure = Figure(axes=OrderedDict([('c01', Plot(points=array([[[ 0., 15.],
        [ 1., 16.]]]), options={'color': 'r', 'marker': '', 'linestyle': 'solid'}, stem=False, xticks=[])), ('c02', Plot(points=array([[[ 0., 35.],
        [ 1., 35.]]]), options={'color': 'r', 'marker': '', 'linestyle': 'solid'}, stem=False, xticks=[])), ('c03', Plot(points=array([[[ 10.,  55.]],

       [[ 15., -10.]],

       [[ 21.,  23.]],

       [[ 32.,  38.]]]), options={'color': 'r', 'marker': 'o', 'linestyle': 'solid'}, stem=True, xticks=[])), ('c04', Plot(points=array([[[ 0., -5.]],

       [[ 1.,  5.]],

       [[ 2., 20.]]]), options={'color': 'r', 'marker': 'o', 'linestyle': 'solid'}, stem=True, xticks=[array([0., 1., 2.]), ['Poor', 'Medium', 'Top']])), ('c05', Plot(points=array([[[ 0. ,  1. ],
        [ 0.3,  2.5]],

       [[ 0.3,  2.5],
        [ 0.6,  3. ]],

       [[ 0.6,  3. ],
        [ 1. , -0.2]]]), options={'color': 'r', 'marker': 'o', 'linestyle': 'solid'}, stem=False, xticks=[]))]), options={'ncols': 2, 'nrows': 3, 'subplot_kw': {'xlabel': 'x', 'ylabel': 'y'}, 'squeeze': False, 'constrained_layout': True, 'figsize': (10, 6.6000000000000005)}, show_grid=False)


def save_fig(fig, dirname, filename):
    fig.savefig(Path(dirname, filename))


def plot_functions(figure):
    fig, axes = plt.subplots(**figure.options)
    for i, k in enumerate(figure.axes.keys()):
        ax = axes[int(i/2), i%2]
        if figure.show_grid:
            ax.grid()
        ax.set_title(k)
        if figure.axes[k].stem:
            if len(figure.axes[k].xticks) == 2:
                ax.set_xticks(figure.axes[k].xticks[0])
                ax.set_xticklabels(figure.axes[k].xticks[1])
            stem_function(figure.axes[k].points, ax, **figure.axes[k].options)
        else:
            plot_function(figure.axes[k].points, ax, **figure.axes[k].options)
    if len(figure.axes) % 2 == 1 and len(figure.axes) != 1:
        axes[-1,1].remove()
    return fig


def plot_function(points, ax, **kwargs):
    for seg in points:
        ax.plot(seg[:,0], seg[:,1], **kwargs)


def stem_function(points, ax, **kwargs):
    markerline, stemlines, baseline = ax.stem(points[:,0,0], points[:,0,1])
    plt.setp(stemlines, 'color', kwargs['color'])
    plt.setp(stemlines, 'linestyle', kwargs['linestyle'])
    plt.setp(baseline, 'color', 'k')
    plt.setp(baseline, 'linestyle', 'solid')
    plt.setp(markerline, 'color', kwargs['color'])
    plt.setp(markerline, 'marker', kwargs['marker'])



def main(directory='.'):
    f = plot_functions(figure)
    save_fig(f, directory, "criteriaFunctions.png")



if __name__ == "__main__":
    sys.exit(main())






