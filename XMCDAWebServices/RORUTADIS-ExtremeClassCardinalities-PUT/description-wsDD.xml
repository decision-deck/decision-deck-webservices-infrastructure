<?xml version='1.0' encoding='utf-8'?>
<program_description>
	<program provider="PUT" name="RORUTADIS-ExtremeClassCardinalities" version="0.1" displayName="RORUTADIS-ExtremeClassCardinalities" />
	<documentation>
		<description>Robust Ordinal Regression for value-based sorting: RORUTADIS-ExtremeClassCardinalities calculates minimal and maximal possible cardinality of each category (class). It is possible to provide an additional optional preference information: example alternatives assignments, assignment pairwise comparisons and desired class cardinalities. Service developed by Krzysztof Ciomek (Poznan University of Technology, under supervision of Milosz Kadzinski).</description>
		<contact><![CDATA[
			Krzysztof Ciomek (k.ciomek@gmail.com),
			Milosz Kadzinski (milosz.kadzinski@cs.put.poznan.pl)
		]]></contact>
		<url>https://github.com/kciomek/rorutadis</url>
		<reference />
	</documentation>
	<parameters>

		<input id="criteria" name="criteria" displayName="criteria" isoptional="0">
			<documentation>
				<description>A list of criteria (&lt;criteria&gt; tag) with information about preference direction (&lt;criteriaValues mcdaConcept="preferenceDirection"&gt;, 0 - gain, 1 - cost) and number of characteristic points (&lt;criteriaValues mcdaConcept="numberOfCharacteristicPoints"&gt;, 0 for the most general marginal utility function or integer grater or equal to 2) of each criterion.</description>
			</documentation>
			<xmcda tag="criteria"><![CDATA[
<criteria>
	<criterion id="[...]" />
	[...]
</criteria>

<criteriaValues mcdaConcept="preferenceDirection">
	<criterionValue>
		<criterionID>[...]</criterionID>
		<values><value><integer>[...]</integer></value></values>
	</criterionValue>
	[...]
</criteriaValues>

<criteriaValues mcdaConcept="numberOfCharacteristicPoints">
	<criterionValue>
		<criterionID>[...]</criterionID>
		<values><value><integer>[0|integer greater or equal to 2]</integer></value></values>
	</criterionValue>
	[...]
</criteriaValues>
]]></xmcda>
		</input>

		<input id="alternatives" name="alternatives" displayName="alternatives" isoptional="0">
			<documentation>
				<description>A list of alternatives.</description>
			</documentation>
			<xmcda tag="alternatives"><![CDATA[
<alternatives>
	<alternative id="[...]">
		<active>[...]</active>
	</alternative>
	[...]
</alternatives>
]]></xmcda>
		</input>

		<input id="categories" name="categories" displayName="categories" isoptional="0">
			<documentation>
				<description>A list of categories (classes). List must be sorted from the worst category to the best.</description>
			</documentation>
			<xmcda tag="categories"><![CDATA[
<categories>
	<category id="[...]" />
	[...]
</categories>
]]></xmcda>
		</input>

		<input id="performanceTable" name="performanceTable" displayName="performanceTable" isoptional="0">
			<documentation>
				<description>The performances of the alternatives.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</input>

		<input id="assignmentExamples" name="assignmentExamples" displayName="assignmentExamples" isoptional="1">
			<documentation>
				<description>A list of assignment examples of alternatives to intervals of categories (classes) or to a specific category (class).</description>
			</documentation>
			<xmcda tag="alternativesAssignments"><![CDATA[
<alternativesAssignments>
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoryID>[...]</categoryID>
	</alternativeAffectation>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesInterval>
			<lowerBound>
				<categoryID>[...]</categoryID>
			</lowerBound>
			<upperBound>
				<categoryID>[...]</categoryID>
			</upperBound>
		</categoriesInterval>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesSet>
			<categoryID>[...]</categoryID>
			[...]
		</categoriesSet>
	</alternativeAssignment>
	[...]
</alternativesAffectations>
]]></xmcda>
		</input>

		<input id="assignmentComparisons" name="assignmentComparisons" displayName="assignmentComparisons" isoptional="1">
			<documentation>
				<description>Two lists of assignment pairwise comparisons. A comparison from list with attribute mcdaConcept="atLeastAsGoodAs" indicates that some alternative should be assigned to class at least as good as class of some other alternative (k = 0) or at least better by k classes (k &gt; 0). A comparison from list with attribute mcdaConcept="atMostAsGoodAs" indicates that some alternative should be assigned to class at most better by k classes (k &gt; 0) then some other alternative.</description>
			</documentation>
			<xmcda tag="alternativesComparisons"><![CDATA[
<alternativesMatrix mcdaConcept="atLeastAsGoodAs">
	<row>
		<alternativeID>[...]</alternativeID>
		<column>
			<alternativeID>[...]</alternativeID>
			<values><value><integer>k</integer></value></values>
		</column>
		[...]
	</row>
	[...]
</alternativesMatrix>
<alternativesMatrix mcdaConcept="atMostAsGoodAs">
	<row>[...]</row>
	[...]
</alternativesMatrix>
]]></xmcda>
		</input>

		<input id="categoriesCardinalities" name="categoriesCardinalities" displayName="categoriesCardinalities" isoptional="1">
			<documentation>
				<description>A list of category (class) cardinality constraints. It allows to define minimal and/or maximal desired category (class) cardinalities.</description>
			</documentation>
			<xmcda tag="categoriesValues"><![CDATA[
<categoriesValues>
	<categoryValue>
		<categoryID>[...]</categoryID>
		<value>
			<interval>
				<lowerBound><integer>[...]</integer></lowerBound>
				<upperBound><integer>[...]</integer></upperBound>
			</interval>
		</value>
	</categoryValue>
	[...]
</categoriesValues>
]]></xmcda>
		</input>

		<input id="strictlyMonotonicValueFunctions" name="strictlyMonotonicValueFunctions" displayName="strictlyMonotonicValueFunctions" isoptional="0">
			<documentation>
				<description>Whether marginal value functions strictly monotonic (true) or weakly monotonic (false).</description>
			</documentation>
			<xmcda tag="methodParameters"><![CDATA[
<programParameters>
	<parameter name="strictlyMonotonicValueFunctions">
		<values>
			<value>
				<boolean>%1</boolean>
			</value>
		</values>
	</parameter>
</programParameters>
]]></xmcda>
			<gui status="preferGUI">
				<entry id="%1" type="boolean" displayName="strictlyMonotonicValueFunctions">
					<documentation>
						<description>Whether marginal value functions strictly monotonic or not.</description>
					</documentation>
					<defaultValue>false</defaultValue>
				</entry>
			</gui>
		</input>

		<output id="categoriesExtremeCardinalities" name="categoriesExtremeCardinalities" displayName="categoriesExtremeCardinalities">
			<documentation>
				<description>Category extreme cardinalities.</description>
			</documentation>
			<xmcda tag="categoriesValues"><![CDATA[
<categoriesValues>
	<categoryValue>
		<categoryID>[...]</categoryID>
		<values>
			<value>
				<interval>
					<lowerBound><real>[integer]</real></lowerBound>
					<upperBound><real>[integer]</real></upperBound>
				</interval>
			</value>
		</values>
	</categoryValue>
	[...]
</categoriesValues>
			]]></xmcda>
		</output>

		<output id="messages" name="messages" displayName="messages">
			<documentation>
				<description>Messages generated by the program.</description>
			</documentation>
			<xmcda tag="programExecutionResult" />
		</output>

	</parameters>
</program_description>
