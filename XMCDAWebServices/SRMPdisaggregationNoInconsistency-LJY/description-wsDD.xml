<?xml version='1.0' encoding='utf-8'?>
<program_description>
	<program provider="LJY" name="SRMPdisaggregationNoInconsistency" version="1.2" displayName="S-RMP Disaggregation without inconsistency" />
	<documentation>
		<description>Elicit a S-RMP model from a set of pairwise comparisons without inconsistency. The algorithm will find out a S-RMP model which is able to exactly represent all the input pairwise comparisons (if it exists). The elicitation may be infeasible due to the unknown input inconsistency.</description>
		<contact><![CDATA[Jinyan Liu (jinyan.liu@ecp.fr)]]></contact>
		<url>http://www.lgi.ecp.fr/pmwiki.php/PagesPerso/JinyanLiu</url>
		<reference>Zheng, J., Rolland, A. &amp; Mousseau, V., 2012. Inferring a reference based multicriteria ranking model from pairwise comparisons. European Journal of Operational Research.</reference>
		<reference>Jun Zheng. Preference elicitation for aggregation models based on reference points: algorithms and procedures. These PhD. May, 2012.</reference>
	</documentation>
	<parameters>

		<input id="inc" name="criteria" displayName="criteria" isoptional="0">
			<documentation>
				<description>A list of criteria involved in the ranking problem. For each criterion, the preference direction should be provided. Besides, the maximum and minimum should also be included.</description>
			</documentation>
			<xmcda tag="criteria" />
		</input>

		<input id="ina" name="alternatives" displayName="alternatives" isoptional="0">
			<documentation>
				<description>A complete list of alternatives to be considered in the ranking problem. At least, all the alternatives appear in the provided pairwise comparisons should be included.</description>
			</documentation>
			<xmcda tag="alternatives"><![CDATA[
			]]></xmcda>
		</input>

		<input id="inperf" name="performanceTable" displayName="performance table" isoptional="0">
			<documentation>
				<description>Values of criteria for each alternatives defined in the alternatives list.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</input>

		<input id="inac" name="alternativesComparisons" displayName="pairwise comparisons between alternatives" isoptional="0">
			<documentation>
				<description>The pairwise comparisons of alternatives provided by the decision maker.</description>
			</documentation>
			<xmcda tag="alternativesComparisons" />
		</input>

		<output id="outrp" name="profileConfigs" displayName="reference points">
			<documentation>
				<description>A set of reference points (special alternatives) with their performance values on each criteria. For each reference point, the id number indicates its lexicographic order.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</output>

		<output id="outw" name="weights" displayName="criteria weights">
			<documentation>
				<description>The weights of criteria. For each criteria, the value of weight is strictly positive and less than 0.49. The sum of weights is normalized to 1.</description>
			</documentation>
			<xmcda tag="criteriaValues" />
		</output>

		<output id="outlexi" name="lexicography" displayName="lexicographic order">
			<documentation>
				<description>the lexicographic order of the reference points we use during aggregation.</description>
			</documentation>
			<xmcda tag="alternativesValues" />
		</output>

		<output id="msg" name="messages" displayName="messages">
			<documentation>
				<description>Some status messages.</description>
			</documentation>
			<xmcda tag="methodMessages" />
		</output>

	</parameters>
</program_description>
