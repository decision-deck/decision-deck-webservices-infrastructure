#! /bin/bash
# shellcheck disable=SC2164
#   SC2164: Use 'cd ... || exit' or 'cd ... || return' in case cd fails.

# usage: test.sh <serviceName> <hostname[:port]> [verbose]
#  if env. variable SERVICE_SUFFIX is set (e.g.: -test), it is appended to
#   <serviceName> when calling the service
shopt -s extglob
#set -x

server="webservices-decision-deck.org"
verbose=""
if [ $# -lt 2 ]; then
    cat >&2 <<EOF
Usage: ${0} service_name hostname [verbose]" >&2
       verbose: is activated by any non-empty string" >&2

Example:
    ${0} randomCriteria-PyXMCDA webservices.decision-deck.org y
EOF
    exit 1
fi

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

service_name=${1}
server=${2}
verbose=${3:-}
callService="${script_dir}/../python/ddws_soap_client.py --no-log -x -U http://${server}/soap/%s.py";

# Le répertoire de sortie dans chaque répertoire service
OUT_SUFFIX=.tb

cd "${script_dir}" || { echo "Cannot cd to ${script_dir}"; exit 1; }

OUT_SUFFIX=.tb;
DIFF="diff"

c14n_dir()
{
    find "${1:?}" -type f -name '*.xml' -print0 | \
        while IFS= read -r -d $'\0' xmlfile
    do
        if xmllint --c14n "$xmlfile" > "${xmlfile}".c14n; then
            mv "${xmlfile}".c14n "${xmlfile}"
        else
            rm "${xmlfile}".c14n
        fi
    done
}
decode_base64()
{
    find "${1:?}" -type f -name '*.base-64-encoded' -print0 | \
        while IFS= read -r -d $'\0' b64;
    do
        f="${b64%.base-64-encoded}"
        base64 -d "$b64" > "$f"
        rm "$b64"
    done
}

get_diff ()
{
  ref="$1"
  out="$2"
  tests_dir="$3"

  # ignore list: can be within a given test...
  diff_ignore_list="$ref"/test.ignore.list
  if [ ! -f "${diff_ignore_list}" ];
  then
      # ... or defined globally for all tests
      diff_ignore_list=$(realpath "$tests_dir"/test.ignore.list)
  fi

  if [ -f "${diff_ignore_list}" ]; then
      diff_ignore_list="--exclude-from ${diff_ignore_list}"
  else
      diff_ignore_list=""
  fi
  if [ -f  "$(pwd)/test.ignore.list" ]; then
      diff_ignore_list+="  --exclude-from=$(pwd)/test.ignore.list"
  fi


  # Ignore the version of XMCDA v3
  # We're not sure yet whether the latest version of XMCDA should be
  # returned always or if we should return the service's native version
  diff_options="-I XMCDA-3\.[0-9]\.[0-9]"

  #
  diff_options+=" --exclude=pre-process-inputs.stdout"
  diff_options+=" --exclude=pre-process-inputs.stderr"
  diff_options+=" --exclude=post-process-outputs.stdout"
  diff_options+=" --exclude=post-process-outputs.stderr"
  diff_options+=" --exclude=input_*.base-64-encoded"
  diff_options+=" --exclude=output_*.base-64-encoded"
  diff_options+=" --exclude=*.v3.xml"
  diff_options+=" --exclude=*.xml_v2"
  diff_options+=" --exclude test.ignore.list"

  # the filtering script can be defined for a given test...
  if [ -x "$ref"/test.filter.file ];
  then
      "$ref"/test.filter.file "$ref" "$out" $diff_ignore_list
  elif [ -x "$tests_dir"/test.filter.file ];
  then
      # ...or globally for all tests
      "$tests_dir"/test.filter.file "$ref" "$out" \
                                    "$diff_ignore_list $diff_options"
  else
      ${DIFF} -Bruw ${diff_options} ${diff_ignore_list} "$ref" "$out"
  fi
}

check ()
{
  local test_dir func_ret test_output_dir tests_input_dir
  local IN OUT STDOUT STDERR files cmdArg _diff ret_diff
  set "${1%/}" # remove trailing slash from $1, if any
  if [ ! -d "$1" -o ! \( -d "$1"/in -o -d "$1"/in1 -o -d "$1"/tests/in -o -d "$1"/tests/in1 -o -d "$1"/tests/in1 \) ]; then
    echo "Missing dir: $1 or $1/[tests/]in" 1>&2;
    return 1
  fi

  test_dir=.
  if [ -d "$1"/tests/in1 ] || [ -d "$1"/tests/in ] || [ -d "$1"/tests/in1 ];
  then
    test_dir=tests
  fi

  func_ret=0

  test_output_dir=$(pwd)/tests.out
  mkdir -p "${test_output_dir}"

  # one output directory per execution
  test_output_dir=$(mktemp -p "${test_output_dir}" -d "ddws_test_$1.XXXX")

  tests_input_dir="$1/${test_dir}" # e.g.service-provider/tests

  pushd "${tests_input_dir}" >/dev/null
  tests_input_dir=$(pwd) # absolute path

  while read -r -d '' IN; do
  #for IN in in1; do
    IN="${IN#./}"
    OUT="out${IN##in}.real${OUT_SUFFIX}"
    OUT=${test_output_dir}/${OUT}
    mkdir -p "${OUT}"

    EXPECTED="out${IN##in}.expected${OUT_SUFFIX}"
    EXPECTED=${test_output_dir}/${EXPECTED}
    mkdir -p "${EXPECTED}"

    pushd "$IN" > /dev/null
    STDOUT="stdout${IN##in}"
    STDERR="stderr${IN##in}"

    # *, not *.xml because some services accept other formats (ex.: csvToXMCDA-* accept .csv)
    files=*
    cmdArg=""
    for f in ${files}; do
        cmdArg="${cmdArg} ${f%.xml}:$(pwd)/${f}"
    done
    popd > /dev/null
    pushd "${OUT}" > /dev/null
    $callService -n "$1" -v -S $cmdArg > "${test_output_dir}/${STDOUT}" 2> "${test_output_dir}/${STDERR}"
    ret=$?
    popd > /dev/null
    decode_base64 "${test_output_dir}"
    if [  "$verbose" != "" ];
    then
      echo "--- stdout ---"
      cat "${test_output_dir}/${STDOUT}"
      echo "--- stderr ---"
      cat "${test_output_dir}/${STDERR}"
    fi

    #cd ..

    if [ $ret -ne 0 ]; then
      func_ret=$((func_ret+1))
      echo "  $ret  ?  $1  $IN"
      continue;
    fi
    # copy expected, then canonicalise all xml files
    cp -Rp "${tests_input_dir}/out${IN##in}"/. "${EXPECTED}"/

    c14n_dir "${EXPECTED}"
    c14n_dir "${OUT}"

    _diff="diff-$1-in${IN##in}"
    get_diff "${EXPECTED}" "$OUT" "${tests_input_dir}" > "${test_output_dir}/${_diff}" 2>&1
    ret_diff=$?

    if [  "$verbose" != "" ];
    then
      echo "--- diff ---"
      cat "${test_output_dir}/${_diff}"
    fi
    echo "  $ret  $ret_diff  $1  $IN"
    if [ $ret_diff -ne 0 ]; then
      func_ret=$((func_ret+1))
      echo -e "\033[91;1mFAILED\033[0m: ${IN}"
    else
      echo -e "\033[32mSUCCESS\033[0m: ${IN}"
    fi
  done < <(find . -maxdepth 1 -name 'in1\.[0-9]*' -print0 | sort -z -t/ -k2.3n)
  popd >/dev/null
  return $func_ret
}


check "${service_name}"
