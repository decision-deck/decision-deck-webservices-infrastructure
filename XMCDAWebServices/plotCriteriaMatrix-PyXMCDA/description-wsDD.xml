<?xml version="1.0" encoding="UTF-8"?>
<program_description>
  <program provider="PyXMCDA"
       name="plotCriteriaMatrix"
       displayName="plotCriteriaMatrix"
       version="1.1" />

  <documentation>
    <description>
        Generate graph representing provided criteriaMatrix as well as the dot files generating this graph.
        Values associated to transitions can be shown without any preprocessing.
        Graphviz dot program and format are used by this service.
    </description>
    <contact>Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)</contact>
    <url>https://gitlab.com/nduminy/ws-pyxmcda</url>
  </documentation>

  <parameters>
      <input displayName="criteria" name="criteria" id="criteria" isoptional="1">
        <documentation>
            <description>The criteria to be plotted. All are plotted if not provided.</description>
        </documentation>
        <xmcda tag="criteria" />
      </input>

      <input displayName="criteria matrix" name="criteriaMatrix" id="criteriaMatrix" isoptional="0">
        <documentation>
            <description>The criteria matrix to be plotted.</description>
        </documentation>
        <xmcda tag="criteriaMatrix" />
      </input>

    <input id="parameters" name="parameters" displayName="parameters" isoptional="1">
          <documentation>
            <description>Parameters of the method</description>
          </documentation>
          <xmcda tag="programParameters"><![CDATA[
    <programParameters>
        <programParameter id="color" name="Color">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="shape" name="Shape">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="show_values" name="Show values">
            <values>
                <value>
                    <boolean>%4</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="transitive_reduction" name="Transitive reduction">
            <values>
                <value>
                    <boolean>%5</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="transitive_closure" name="Transitive closure">
            <values>
                <value>
                    <boolean>%6</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="directed_graph" name="Directed graph">
            <values>
                <value>
                    <boolean>%7</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="naming_conventions" name="Naming conventions">
            <values>
                <value>
                    <label>%8</label>
                </value>
            </values>
        </programParameter>
    </programParameters>
]]></xmcda>
          <gui status="preferGUI">
            <entry id="%1" type="enum" displayName="Color">
              <documentation>
                <description>Color used in the plots.</description>
              </documentation>
              <items>
                  <item id="black">
                      <description>Black</description>
                      <value>black</value>
                  </item>
                  <item id="white">
                      <description>White</description>
                      <value>white</value>
                  </item>
                  <item id="red">
                      <description>Red</description>
                      <value>red</value>
                  </item>
                  <item id="green">
                      <description>Green</description>
                      <value>green</value>
                  </item>
                  <item id="blue">
                      <description>Blue</description>
                      <value>blue</value>
                  </item>
                  <item id="magenta">
                      <description>Magenta</description>
                      <value>magenta</value>
                  </item>
                  <item id="yellow">
                      <description>Yellow</description>
                      <value>yellow</value>
                  </item>
                  <item id="cyan">
                      <description>Cyan</description>
                      <value>cyan</value>
                  </item>
                  <item id="orange">
                      <description>Orange</description>
                      <value>orange</value>
                  </item>
                  <item id="pink">
                      <description>Pink</description>
                      <value>pink</value>
                  </item>
                  <item id="brown">
                      <description>Brown</description>
                      <value>brown</value>
                  </item>
                  <item id="gray">
                      <description>Gray</description>
                      <value>gray</value>
                  </item>
              </items>
              <defaultValue>black</defaultValue>
            </entry>
            <entry id="%2" type="enum" displayName="Shape">
              <documentation>
                <description>Shape use to represent categories.</description>
              </documentation>
              <items>
                  <item id="box">
                      <description>Rectangle</description>
                      <value>box</value>
                  </item>
                  <item id="circle">
                      <description>Circle</description>
                      <value>circle</value>
                  </item>
                  <item id="diamond">
                      <description>Diamond</description>
                      <value>diamond</value>
                  </item>
                  <item id="ellipse">
                      <description>Ellipse</description>
                      <value>ellipse</value>
                  </item>
                  <item id="oval">
                      <description>Oval</description>
                      <value>oval</value>
                  </item>
                  <item id="polygon">
                      <description>Polygon</description>
                      <value>polygon</value>
                  </item>
                  <item id="triangle">
                      <description>Triangle</description>
                      <value>triangle</value>
                  </item>
              </items>
              <defaultValue>oval</defaultValue>
            </entry>
            <entry id="%3" type="enum" displayName="Image file extension">
              <documentation>
                <description>File extension of generated image figure.</description>
              </documentation>
              <items>
                  <item id="bmp">
                      <description>.bmp (Windows Bitmap Format)</description>
                      <value>bmp</value>
                  </item>
                  <item id="dia">
                      <description>.dia (DIA Format)</description>
                      <value>dia</value>
                  </item>
                  <item id="fig">
                      <description>.fig (FIG graphics format)</description>
                      <value>fig</value>
                  </item>
                  <item id="gif">
                      <description>.gif (Graphics Interchange Format)</description>
                      <value>gif</value>
                  </item>
                  <item id="hpgl">
                      <description>.hpgl (Hewlett Packard Graphic Language 2)</description>
                      <value>hpgl</value>
                  </item>
                  <item id="ico">
                      <description>.ico (Icon Image File Format)</description>
                      <value>ico</value>
                  </item>
                  <item id="jpg">
                      <description>.jpg (Joint Photographic Experts Group)</description>
                      <value>jpg</value>
                  </item>
                  <item id="jpeg">
                      <description>.jpeg (Joint Photographic Experts Group)</description>
                      <value>jpeg</value>
                  </item>
                  <item id="jpe">
                      <description>.jpe (Joint Photographic Experts Group)</description>
                      <value>jpe</value>
                  </item>
                  <item id="pdf">
                      <description>.pdf (Portable Document Format)</description>
                      <value>pdf</value>
                  </item>
                  <item id="png">
                      <description>.png (Portable Network Graphics)</description>
                      <value>png</value>
                  </item>
                  <item id="ps">
                      <description>.ps (PostScript)</description>
                      <value>ps</value>
                  </item>
                  <item id="ps2">
                      <description>.ps2 (PostScript for PDF)</description>
                      <value>ps2</value>
                  </item>
                  <item id="svg">
                      <description>.svg (Scalable Vector Graphics)</description>
                      <value>svg</value>
                  </item>
                  <item id="svgz">
                      <description>.svgz (Compressed Scalable Vector Graphics)</description>
                      <value>svgz</value>
                  </item>
                  <item id="tif">
                      <description>.tif (Tagged Image File Format)</description>
                      <value>tif</value>
                  </item>
                  <item id="tiff">
                      <description>.tiff (Tagged Image File Format)</description>
                      <value>tiff</value>
                  </item>
              </items>
              <defaultValue>png</defaultValue>
            </entry>
            <entry id="%4" type="boolean" displayName="Show values">
              <documentation>
                <description>Defines if values are shown (between parentheses) on the plots or not.</description>
              </documentation>
              <defaultValue>false</defaultValue>
            </entry>
            <entry id="%5" type="boolean" displayName="Transitive reduction">
              <documentation>
                <description>Defines if transitive reduction is applied or not.</description>
              </documentation>
              <defaultValue>false</defaultValue>
              <dependency><![CDATA[%7:type="true"]]></dependency>
            </entry>
            <entry id="%6" type="boolean" displayName="Transitive closure">
              <documentation>
                <description>Defines if transitive closure is applied or not.</description>
              </documentation>
              <defaultValue>false</defaultValue>
              <dependency><![CDATA[%7:type="true"]]></dependency>
            </entry>
            <entry id="%7" type="enum" displayName="Directed graph">
              <documentation>
                <description>Defines if graph representing alternatives matrix is directed or not.</description>
              </documentation>
              <items>
                <item id="true">
                  <description>Directed</description>
                  <value>true</value>
                </item>
                <item id="false">
                  <description>Undirected</description>
                    <value>false</value>
                </item>
              </items>
              <defaultValue>true</defaultValue>
            </entry>
            <entry id="%8" type="enum" displayName="Naming conventions">
              <documentation>
                <description>How categories and alternatives are labelled on the graph.</description>
              </documentation>
              <items>
                  <item id="id">
                      <description>Only ids are shown</description>
                      <value>id</value>
                  </item>
                  <item id="name">
                      <description>Only names are shown (disambiguated by appending the ids, if needed)</description>
                      <value>name</value>
                  </item>
                  <item id="name (id)">
                      <description>Names and ids are shown in that order</description>
                      <value>name (id)</value>
                  </item>
                  <item id="id (name)">
                      <description>Ids and names are shown in that order</description>
                      <value>id (name)</value>
                  </item>
              </items>
              <defaultValue>name</defaultValue>
            </entry>
          </gui>
        </input>

    <output displayName="criteria matrix plot" name="glob:criteriaMatrix.{bmp,dia,fig,gif,hpgl,ico,jpg,jpeg,jpe,pdf,png,ps,ps2,svg,svgz,tif,tiff}" id="criteriaMatrixPlot">
        <documentation>
            <description>Image containing criteria matrix graph. Format corresponds to the one given in parameters (default is .png).</description>
        </documentation>
    </output>

    <output displayName="criteria matrix plot script" name="criteriaMatrix.dot" id="criteriaMatrixPlotScript">
        <documentation>
            <description>Generated graphviz dot script that made the image. Given to enable users to later customize the appearance of the graph.</description>
        </documentation>
    </output>

    <output displayName="messages" name="messages" id="messages">
        <documentation>
            <description>Status messages.</description>
        </documentation>
        <xmcda tag="programExecutionResult" />
    </output>
  </parameters>
</program_description>
