<?xml version='1.0' encoding='utf-8'?>
<program_description>
       <program provider="PyXMCDA" name="RubisOutrankingRelation" version="1.1" displayName="RubisOutrankingRelation" />
       <documentation>
              <description>This web service allows to compute an outranking relation as defined in the Rubis methodology.</description>
              <contact><![CDATA[Thomas Veneziano (thomas.veneziano@uni.lu)]]></contact>
              <reference>R. Bisdorff, P. Meyer, M. Roubens, Rubis: a bipolar-valued outranking method for the best choice decision problem, 4OR, 6 (2), June 2008, Springer (doi:10.1007/s10288-007-0045-5).</reference>
       </documentation>
       <parameters>

              <input id="criteria" name="criteria" displayName="criteria" isoptional="0">
                     <documentation>
                            <description>A list of criteria. Criteria can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), criteria are considered as active.
Using thresholds is optional, only the constant ones with mcdaConcept equals to "indifference", "preference" or "veto" will be considered.
Preference directions can be specified with scales; if absent, they default to 'max'.</description>
                     </documentation>
                     <xmcda tag="criteria"><![CDATA[
<criteria>
	<criterion id="[...]">
		<active>[...]</active>
	</criterion>
	[...]
</criteria>

<criteriaScales>
	<criterionScale>
		<criterionID>...</criterionID>
		<scales>
			<scale>
				<quantitative>
					<preferenceDirection>min</preferenceDirection>
				</quantitative>
			</scale>
		</scales>
	</criterionScale>
</criteriaScales>

<criteriaThresholds>
	<criterionThreshold>
		<criterionID>PrixAchat</criterionID>
		<thresholds>
			<threshold mcdaConcept="weakVeto"> <!-- REQUIRED, must be indifference, preference, veto or weakVeto -->
				<constant>
					<real>7000.0</real>
				</constant>
			</threshold>
			<threshold mcdaConcept="veto">
				<constant>
					<real>10000.0</real>
				</constant>
			</threshold>
		</thresholds>
	</criterionThreshold>
</criteriaThresholds>
]]></xmcda>
              </input>

              <input id="alternatives" name="alternatives" displayName="alternatives" isoptional="0">
                     <documentation>
                            <description>A list of alternatives. Alternatives can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), alternatives are considered as active.</description>
                     </documentation>
                     <xmcda tag="alternatives"><![CDATA[
<alternatives>
	<alternative>
		<active>[...]</active>
	</alternative>
	[...]
</alternatives>
]]></xmcda>
              </input>

              <input id="performanceTable" name="performanceTable" displayName="performanceTable" isoptional="0">
                     <documentation>
                            <description>A performance table. The evaluations should be numeric values, i.e. &lt;real&gt;, &lt;integer&gt; or &lt;rational&gt;.</description>
                     </documentation>
                     <xmcda tag="performanceTable" />
              </input>

              <input id="criteriaWeights" name="criteriaWeights" displayName="criteriaWeights" isoptional="0">
                     <documentation>
                            <description>The set of criteria weights.</description>
                     </documentation>
                     <xmcda tag="criteriaValues" />
              </input>

              <input id="valuationDomain" name="valuationDomain" displayName="valuationDomain" isoptional="1">
                     <documentation>
                            <description>Indicates the minimal and the maximal values of the valuation domain for computing the outranking relation. By default the valuation domain is {0,0.5,1}. The median indetermination value is computed as the average of the minimal and the maximal values.</description>
                     </documentation>
                     <xmcda tag="programParameters"><![CDATA[
<programParameters name="valuationDomain"> <!-- name REQUIRED  -->
	<parameter name="min"> <!-- name REQUIRED  -->
		<values>
			<value>
				<real>%1</real>
			</value>
		</values>
	</parameter>
	<parameter name="max"> <!-- name REQUIRED  -->
		<values>
			<value>
				<real>%2</real>
			</value>
		</values>
	</parameter>
</programParameters>
]]></xmcda>
                     <gui status="preferGUI">
                            <entry id="%1" type="float" displayName="min">
                                   <documentation>
                                          <description>Indicates the minimal value of the valuation domain.</description>
                                   </documentation>
                                   <defaultValue>0</defaultValue>
                            </entry>
                            <entry id="%2" type="float" displayName="max">
                                   <documentation>
                                          <description>Indicates the maximal value of the valuation domain (which should be greater than the minimal value).</description>
                                   </documentation>
                                   <defaultValue>1</defaultValue>
                            </entry>
                     </gui>
              </input>

              <output id="alternativesComparisons" name="alternativesComparisons" displayName="outrankingRelation">
                     <documentation>
                            <description>The ogniknartu relation.</description>
                     </documentation>
                     <xmcda tag="alternativesMatrix" />
              </output>

              <output id="messages" name="messages" displayName="messages">
                     <documentation>
                            <description>A list of messages generated by the algorithm.</description>
                     </documentation>
                     <xmcda tag="programExecutionResult" />
              </output>

       </parameters>
</program_description>
