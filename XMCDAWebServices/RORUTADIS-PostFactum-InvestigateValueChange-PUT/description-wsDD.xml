<?xml version='1.0' encoding='utf-8'?>
<program_description>
	<program provider="PUT" name="RORUTADIS-PostFactum-InvestigateValueChange" version="0.3" displayName="RORUTADIS-PostFactum-InvestigateValueChange" />
	<documentation>
		<description>Robust Ordinal Regression for value-based sorting: RORUTADIS-PostFactum-InvestigateValueChange service calculates missing value of an alternative utility for that alternative to be possibly (or necessarily) assigned to at least some specific class. It is possible to provide an additional optional preference information: example alternatives assignments, assignment pairwise comparisons and desired class cardinalities. Service developed by Krzysztof Ciomek (Poznan University of Technology, under supervision of Milosz Kadzinski).</description>
		<contact><![CDATA[
			Krzysztof Ciomek (k.ciomek@gmail.com),
			Milosz Kadzinski (milosz.kadzinski@cs.put.poznan.pl)
		]]></contact>
		<url>https://github.com/kciomek/rorutadis</url>
		<reference />
	</documentation>
	<parameters>

		<input id="criteria" name="criteria" displayName="criteria" isoptional="0">
			<documentation>
				<description>A list of criteria (&lt;criteria&gt; tag) with information about preference direction (&lt;criteriaValues mcdaConcept="preferenceDirection"&gt;, 0 - gain, 1 - cost) and number of characteristic points (&lt;criteriaValues mcdaConcept="numberOfCharacteristicPoints"&gt;, 0 for the most general marginal utility function or integer grater or equal to 2) of each criterion.</description>
			</documentation>
			<xmcda tag="criteria"><![CDATA[
<criteria>
	<criterion id="[...]" />
	[...]
</criteria>

<criteriaValues mcdaConcept="preferenceDirection">
	<criterionValue>
		<criterionID>[...]</criterionID>
		<value><integer>[...]</integer></value>
	</criterionValue>
	[...]
</criteriaValues>

<criteriaValues mcdaConcept="numberOfCharacteristicPoints">
	<criterionValue>
		<criterionID>[...]</criterionID>
		<value><integer>[0|integer greater or equal to 2]</integer></value>
	</criterionValue>
	[...]
</criteriaValues>
]]></xmcda>
		</input>

		<input id="alternatives" name="alternatives" displayName="alternatives" isoptional="0">
			<documentation>
				<description>A list of alternatives.</description>
			</documentation>
			<xmcda tag="alternatives"><![CDATA[
<alternatives>
	<alternative id="...">
		<active>[...]</active>
	</alternative>
</alternatives>
]]></xmcda>
		</input>

		<input id="categories" name="categories" displayName="categories" isoptional="0">
			<documentation>
				<description>A list of categories (classes). List must be sorted from the worst category to the best.</description>
			</documentation>
			<xmcda tag="categories"><![CDATA[
<categories>
	<category id="[...]" />
	[...]
</categories>
]]></xmcda>
		</input>

		<input id="performanceTable" name="performanceTable" displayName="performanceTable" isoptional="0">
			<documentation>
				<description>The performances of the alternatives.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</input>

		<input id="assignmentExamples" name="assignmentExamples" displayName="assignmentExamples" isoptional="1">
			<documentation>
				<description>A list of assignment examples of alternatives to intervals of categories (classes) or to a specific category (class).</description>
			</documentation>
			<xmcda tag="alternativesAssignments"><![CDATA[
<alternativesAssignments>
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoryID>[...]</categoryID>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesInterval>
			<lowerBound>
				<categoryID>[...]</categoryID>
			</lowerBound>
			<upperBound>
				<categoryID>[...]</categoryID>
			</upperBound>
		</categoriesInterval>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesSet>
			<categoryID>[...]</categoryID>
			[...]
		</categoriesSet>
	</alternativeAssignment>
	[...]
</alternativesAssignments>
]]></xmcda>
		</input>

		<input id="assignmentComparisons" name="assignmentComparisons" displayName="assignmentComparisons" isoptional="1">
			<documentation>
				<description>Two lists of assignment pairwise comparisons. A comparison from list with attribute mcdaConcept="atLeastAsGoodAs" indicates that some alternative should be assigned to class at least as good as class of some other alternative (k = 0) or at least better by k classes (k &gt; 0). A comparison from list with attribute mcdaConcept="atMostAsGoodAs" indicates that some alternative should be assigned to class at most better by k classes (k &gt; 0) then some other alternative.</description>
			</documentation>
			<xmcda tag="alternativesMatrix"><![CDATA[
<alternativesMatrix mcdaConcept="atLeastAsGoodAs">
	<row>
		<alternativeID>[...]</alternativeID>
		<column>
			<alternativeID>[...]</alternativeID>
			<values>
				<value>
					<integer>k</integer>
				</value>
			</values>
		</column>
	</row>
	[...]
</alternativesMatrix>

<alternativesMatrix mcdaConcept="atMostAsGoodAs">
	<row>[...]</row>
	[...]
</alternativesMatrix>
]]></xmcda>
		</input>

		<input id="categoriesCardinalities" name="categoriesCardinalities" displayName="categoriesCardinalities" isoptional="1">
			<documentation>
				<description>A list of category (class) cardinality constraints. It allows to define minimal and/or maximal desired category (class) cardinalities.</description>
			</documentation>
			<xmcda tag="categoriesValues"><![CDATA[
<categoriesValues>
	<categoryValue>
		<categoryID>[...]</categoryID>
		<values>
			<value>
				<interval>
					<lowerBound><integer>[...]</integer></lowerBound>
					<upperBound><integer>[...]</integer></upperBound>
				</interval>
			</value>
		</values>
	</categoryValue>
	[...]
</categoriesValues>
]]></xmcda>
		</input>

		<input id="methodParameters" name="methodParameters" displayName="methodParameters" isoptional="0">
			<documentation>
				<description>Method parameters.</description>
			</documentation>
			<xmcda tag="programParameters"><![CDATA[
<programParameters>
	<parameter name="strictlyMonotonicValueFunctions">
		<values>
			<value><boolean>%1</boolean></value>
		</values>
	</parameter>
	<parameter name="alternative">
		<values>
			<value><label>%2</label></value>
		</values>
	</parameter>
	<parameter name="necessary">
		<values>
			<value><boolean>%3</boolean></value>
		</values>
	</parameter>
	<parameter name="atLeastToClass">
		<values>
			<value><label>%4</label></value>
		</values>
	</parameter>
</programParameters>
]]></xmcda>
			<gui status="preferGUI">
				<entry id="%1" type="boolean" displayName="strictlyMonotonicValueFunctions">
					<documentation>
						<description>Whether marginal value functions strictly monotonic or not.</description>
					</documentation>
					<defaultValue>false</defaultValue>
				</entry>
				<entry id="%2" type="string" displayName="alternative">
					<documentation>
						<description>An identifier of alternative for assignment investigation.</description>
					</documentation>
					<constraint>
						<description>This identifier cannot be empty</description>
						<code><![CDATA[ ! %2.isEmpty() ]]></code>
					</constraint>
				</entry>
				<entry id="%3" type="boolean" displayName="necessary">
					<documentation>
						<description>Whether necessary or possible assignments to consider.</description>
					</documentation>
					<defaultValue>false</defaultValue>
				</entry>
				<entry id="%4" type="string" displayName="atLeastToClass">
					<documentation>
						<description>An identifier of category of assignment to investigate.</description>
					</documentation>
					<constraint>
						<description>This identifier cannot be empty</description>
						<code><![CDATA[ ! %4.isEmpty() ]]></code>
					</constraint>
				</entry>
			</gui>
		</input>

		<output id="marginalValueFunctions" name="marginalValueFunctions" displayName="marginalValueFunctions">
			<documentation>
				<description>Marginal value functions.</description>
			</documentation>
			<xmcda tag="criteriaFunctions"><![CDATA[
<criteriaFunctions>
	<criterionFunction>
		<criterionID>g01</criterionID>
		<functions>
			<function>
				<discrete>
					<point>
						<abscissa><real>[...]</real></abscissa>
						<ordinate><real>[...]</real></ordinate>
					</point>
					[...]
				</discrete>
			</function>
		</functions>
	</criterionFunction>
	<criterionFunction>[...]</criterionFunction>
	[...]
</criteriaFuctions>
]]></xmcda>
		</output>

		<output id="alternativesValuesTable" name="alternativesValuesTable" displayName="alternativesValuesTable">
			<documentation>
				<description>Marginal utility values of alternatives (for result function where missing utility is the smallest).</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</output>

		<output id="assignments" name="assignments" displayName="assignments">
			<documentation>
				<description>Alternative assignments (for result function where missing utility is the smallest).</description>
			</documentation>
			<xmcda tag="alternativesAssignments"><![CDATA[
<alternativesAffectations>
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoryID>[...]</categoryID>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesInterval>
			<lowerBound>
				<categoryID>[...]</categoryID>
			</lowerBound>
			<upperBound>
				<categoryID>[...]</categoryID>
			</upperBound>
		</categoriesInterval>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesSet>
			<categoryID>[...]</categoryID>
			[...]
		</categoriesSet>
	</alternativeAssignment>
	[...]
</alternativesAssignments>
]]></xmcda>
		</output>

		<output id="values" name="values" displayName="value">
			<documentation>
				<description>Value of missing utility.</description>
			</documentation>
			<xmcda tag="alternativesValues"><![CDATA[
<alternativesValues mcdaConcept="investigationResultValue">
	<alternativeValue>
		<alternativeID>[...]</alternativeID>
		<values>
			<value>
				<real>[...]</real>
			</value>
		</values>
	</alternativeValue>
</alternativesValues>
]]></xmcda>
		</output>

		<output id="thresholds" name="thresholds" displayName="thresholds">
			<documentation>
				<description>Lower threshold for each category/class (for result function where missing utility is the smallest).</description>
			</documentation>
			<xmcda tag="categoriesValues"><![CDATA[
<categoriesValues mcdaConcept="thresholds">
	<categoryValue>
		<categoryID>[...]</categoryID>
		<values>
			<value>
				<real>[...]</real>
			</value>
		</values>
	</categoryValue>
	[...]
</categoriesValues>
			]]></xmcda>
		</output>

		<output id="messages" name="messages" displayName="messages">
			<documentation>
				<description>Messages generated by the program.</description>
			</documentation>
			<xmcda tag="programExecutionResult" />
		</output>

	</parameters>
</program_description>
