from pathlib import Path
from collections import namedtuple, OrderedDict
import matplotlib.pyplot as plt
import numpy as np
from numpy import array
import sys


Plot = namedtuple('Plot', 'performance_table, ax_options, plot_options')


Figure = namedtuple('Figure', 'plots, options')


figure = Figure(
    plots=OrderedDict(
        [
            (
                "cost",
                Plot(
                    performance_table=OrderedDict(
                        [
                            ("Peugot 309 (a1)", 17537.0),
                            ("Toyota Corolla", 13841.0),
                            ("Mitsu Galant", 17219.0),
                            ("Alfa 33", 15335.0),
                            ("Ford Escort", 19800.0),
                            ("Renault 21", 21334.0),
                            ("Nissan Sunny", 16973.0),
                            ("Honda Civic", 18971.0),
                            ("Peugot 309 (a4)", 15980.0),
                            ("Renault 19", 16966.0),
                            ("Opel Astra", 18319.0),
                            ("Mitsu Colt", 15131.0),
                            ("Mazda 323", 15460.0),
                            ("Fiat tipo", 18342.0),
                        ]
                    ),
                    ax_options={"xlabel": "alternatives", "ylabel": "values"},
                    plot_options=[
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                    ],
                ),
            ),
            (
                "acceleration",
                Plot(
                    performance_table=OrderedDict(
                        [
                            ("Peugot 309 (a1)", 28.3),
                            ("Toyota Corolla", 30.8),
                            ("Mitsu Galant", 30.2),
                            ("Alfa 33", 30.2),
                            ("Ford Escort", 29.4),
                            ("Renault 21", 28.9),
                            ("Nissan Sunny", 29.0),
                            ("Honda Civic", 28.0),
                            ("Peugot 309 (a4)", 29.6),
                            ("Renault 19", 30.0),
                            ("Opel Astra", 28.9),
                            ("Mitsu Colt", 29.7),
                            ("Mazda 323", 30.4),
                            ("Fiat tipo", 30.7),
                        ]
                    ),
                    ax_options={"xlabel": "alternatives", "ylabel": "values"},
                    plot_options=[
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                    ],
                ),
            ),
            (
                "pickUp",
                Plot(
                    performance_table=OrderedDict(
                        [
                            ("Peugot 309 (a1)", 34.8),
                            ("Toyota Corolla", 36.5),
                            ("Mitsu Galant", 36.9),
                            ("Alfa 33", 41.6),
                            ("Ford Escort", 34.7),
                            ("Renault 21", 36.7),
                            ("Nissan Sunny", 34.9),
                            ("Honda Civic", 35.6),
                            ("Peugot 309 (a4)", 35.3),
                            ("Renault 19", 37.7),
                            ("Opel Astra", 35.3),
                            ("Mitsu Colt", 35.6),
                            ("Mazda 323", 35.8),
                            ("Fiat tipo", 37.2),
                        ]
                    ),
                    ax_options={"xlabel": "alternatives", "ylabel": "values"},
                    plot_options=[
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                    ],
                ),
            ),
            (
                "brakes",
                Plot(
                    performance_table=OrderedDict(
                        [
                            ("Peugot 309 (a1)", 2.33),
                            ("Toyota Corolla", 1.33),
                            ("Mitsu Galant", 1.66),
                            ("Alfa 33", 2.0),
                            ("Ford Escort", 2.0),
                            ("Renault 21", 2.0),
                            ("Nissan Sunny", 2.66),
                            ("Honda Civic", 2.33),
                            ("Peugot 309 (a4)", 2.33),
                            ("Renault 19", 2.33),
                            ("Opel Astra", 1.66),
                            ("Mitsu Colt", 1.66),
                            ("Mazda 323", 1.66),
                            ("Fiat tipo", 2.33),
                        ]
                    ),
                    ax_options={"xlabel": "alternatives", "ylabel": "values"},
                    plot_options=[
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                    ],
                ),
            ),
            (
                "roadhld",
                Plot(
                    performance_table=OrderedDict(
                        [
                            ("Peugot 309 (a1)", 2.75),
                            ("Toyota Corolla", 2.0),
                            ("Mitsu Galant", 1.25),
                            ("Alfa 33", 2.5),
                            ("Ford Escort", 1.75),
                            ("Renault 21", 2.25),
                            ("Nissan Sunny", 2.5),
                            ("Honda Civic", 2.0),
                            ("Peugot 309 (a4)", 2.75),
                            ("Renault 19", 3.25),
                            ("Opel Astra", 2.0),
                            ("Mitsu Colt", 1.75),
                            ("Mazda 323", 1.5),
                            ("Fiat tipo", 3.0),
                        ]
                    ),
                    ax_options={"xlabel": "alternatives", "ylabel": "values"},
                    plot_options=[
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                        {"color": "#000000", "width": 0.75},
                    ],
                ),
            ),
        ]
    ),
    options={
        "subplot_kw": {"xlabel": "alternatives", "ylabel": "values"},
        "ncols": 2,
        "nrows": 3,
        "squeeze": False,
        "constrained_layout": True,
        "figsize": (10, 9),
    },
)



def plot_plot(plot, ax):
    labels = [*plot.performance_table.keys()]
    x = np.arange(len(labels))
    for xx, yy, options in zip(x, plot.performance_table.values(),
                               plot.plot_options):
        ax.bar(xx, yy, **options)
    ax.set_xticks(x)
    ax.set_xticklabels(labels, rotation=-45, ha="left", rotation_mode="anchor")


def plot_figure(figure):
    fig, axes = plt.subplots(**figure.options)
    for i, k in enumerate(figure.plots.keys()):
        ax = axes[int(i/2), i%2]
        ax.set_title(k)
        plot_plot(figure.plots[k], ax)
    if len(figure.plots) % 2 == 1 and len(figure.plots) != 1:
        axes[-1,1].remove()
    return fig


def save_fig(fig, dirname, filename):
    fig.savefig(Path(dirname, filename))


def main(directory='.'):
    f = plot_figure(figure)
    save_fig(f, directory, "numericPerformanceTable.png")


if __name__ == "__main__":
    sys.exit(main())
