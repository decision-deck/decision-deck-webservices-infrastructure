<?xml version="1.0" encoding="UTF-8"?>
<program_description>
  <program provider="PyXMCDA"
       name="plotNumericPerformanceTable"
       displayName="plotNumericPerformanceTable"
       version="1.4" />

  <documentation>
    <description>
        Generate plots from provided performance tables as well as the scripts generating these plots.

        Colormap can be defined by the user, by giving a list of colors in the parameters.xml file.
        The number of colors is not restrained, and the colormap will linearly distribute the color
        in their provided order and interpolate between them.
        If only one is provided, it will be used for all data plot.
        Each color is either one of the color names predefined in matplotlib
        (See https://matplotlib.org/stable/gallery/color/named_colors.html#sphx-glr-gallery-color-named-colors-py)
        or a RGB color defined in hexadecimal '#RRGGBB'.

        N.B.: This service can handle missing data by not representing them except for starCharts which
        centre values represent missing data.
        Also, when plotting pie charts, negative and null values are not represented!
    </description>
    <contact>Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)</contact>
    <url>https://gitlab.com/nduminy/ws-pyxmcda</url>
  </documentation>

  <parameters>
      <input displayName="alternatives" name="alternatives" id="alternatives" isoptional="1">
        <documentation>
            <description>The alternatives to be plotted. All are plotted if not provided.</description>
        </documentation>
        <xmcda tag="alternatives" />
      </input>

      <input displayName="criteria" name="criteria" id="criteria" isoptional="1">
        <documentation>
            <description>The criteria to be plotted. All are plotted if not provided.</description>
        </documentation>
        <xmcda tag="criteria" />
      </input>

      <input displayName="performance table" name="performanceTable" id="performanceTable" isoptional="0">
        <documentation>
            <description>The performance table.</description>
        </documentation>
        <xmcda tag="performanceTable" />
      </input>

    <input id="parameters" name="parameters" displayName="parameters" isoptional="1">
          <documentation>
            <description>Parameters of the method</description>
          </documentation>
          <xmcda tag="programParameters"><![CDATA[
    <programParameters>
        <programParameter id="x_axis" name="X-axis label">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="y_axis" name="Y-axis label">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="chart_type" name="Chart type">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="colors" name="Colors">
            <!-- colormap: %colormap / nb colors: %nb_colors -->
            <values>%color-mono%color-A%color-B%color-a%color-b%color-c%color-d%color-e%color-f%color-g%color-h%color-i%color-j
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%7a%7b</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="plotter" name="Plotter">
            <values>
                <value>
                    <label>%8</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="order_alternatives_by" name="Order alternatives by">
            <values>
                <value>
                    <label>%9</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="order_criteria_by" name="Order criteria by">
            <values>
                <value>
                    <label>%10</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="reverse_order" name="Reverse order">
            <values>
                <value>
                    <boolean>%11</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="mix_colors" name="Mix colors">
            <values>
                <value>
                    <boolean>%12</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="superposed_plot_title" name="Superposed plot title">
            <values>
                <value>
                    <label>%13</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="naming_conventions" name="Naming conventions">
            <values>
                <value>
                    <label>%14</label>
                </value>
            </values>
        </programParameter>
    </programParameters>
]]></xmcda>
          <gui status="preferGUI">
            <entry id="%1" type="string" displayName="X-axis label">
              <documentation>
                <description>X-axis name to display below each plot.</description>
              </documentation>
              <defaultValue></defaultValue>
            </entry>
            <entry id="%2" type="string" displayName="Y-axis label">
              <documentation>
                <description>Y-axis name to display on the left of each plot.</description>
              </documentation>
              <defaultValue></defaultValue>
            </entry>
            <entry id="%3" type="enum" displayName="Chart type">
              <documentation>
                <description>Type of chart to be plotted.</description>
              </documentation>
              <items>
                  <item id="barChart">
                      <description>bar chart</description>
                      <value>barChart</value>
                  </item>
                  <item id="pieChart">
                      <description>pie chart</description>
                      <value>pieChart</value>
                  </item>
                  <item id="starChart">
                      <description>star chart</description>
                      <value>starChart</value>
                  </item>
              </items>
              <defaultValue>barChart</defaultValue>
            </entry>

            <entry id="%colormap" type="enum" displayName="Colormap">
              <documentation>
                <description>Type of the colormap.</description>
              </documentation>
              <items>
                  <item id="monochrome">
                      <description>Monochrome</description>
                      <value>monochrome</value>
                  </item>
                  <item id="bicolor">
                      <description>Bicolor</description>
                      <value>bicolor</value>
                  </item>
                  <item id="multicolor">
                      <description>Multi-color (advanced)</description>
                      <value>multicolor (advanced)</value>
                  </item>
              </items>
              <defaultValue>monochrome</defaultValue>
            </entry>

            <entry id="%nb_colors" type="int" displayName="Nb of colors">
              <documentation>
                <description>The number of colors supplied to define the colormap.</description>
              </documentation>
               <constraint>
                  <description>The value should be between 1 and 10.</description>
                  <code><![CDATA[%nb_colors > 0 && %nb_colors <= 10]]></code>
               </constraint>
              <defaultValue>2</defaultValue>
              <dependency><![CDATA[%colormap:type="multicolor"]]></dependency>
            </entry>

            <entry id="%color-mono" type="enum" displayName="Color">
              <documentation>
                <description>Color to use.</description>
              </documentation>
              <items>
                  <item id="black">
                      <description>black</description>
                      <value><![CDATA[
                <value>
                    <label>black</label>
                </value>]]></value>
                  </item>
                  <item id="red">
                      <description>red</description>
                      <value><![CDATA[
                <value>
                    <label>red</label>
                </value>]]></value>
                  </item>
                  <item id="green">
                      <description>green</description>
                      <value><![CDATA[
                <value>
                    <label>green</label>
                </value>]]></value>
                  </item>
                  <item id="blue">
                      <description>blue</description>
                      <value><![CDATA[
                <value>
                    <label>blue</label>
                </value>]]></value>
                  </item>
                  <item id="orange">
                      <description>orange</description>
                      <value><![CDATA[
                <value>
                    <label>orange</label>
                </value>]]></value>
                  </item>
                  <item id="green">
                      <description>green</description>
                      <value><![CDATA[
                <value>
                    <label>green</label>
                </value>]]></value>
                  </item>
                  <item id="purple">
                      <description>purple</description>
                      <value><![CDATA[
                <value>
                    <label>purple</label>
                </value>]]></value>
                  </item>
                  <item id="cyan">
                      <description>cyan</description>
                      <value><![CDATA[
                <value>
                    <label>cyan</label>
                </value>]]></value>
                  </item>
                  <item id="magenta">
                      <description>magenta</description>
                      <value><![CDATA[
                <value>
                    <label>magenta</label>
                </value>]]></value>
                  </item>
                  <item id="yellow">
                      <description>yellow</description>
                      <value><![CDATA[
                <value>
                    <label>yellow</label>
                </value>]]></value>
                  </item>
                  <item id="salmon">
                      <description>salmon</description>
                      <value><![CDATA[
                <value>
                    <label>salmon</label>
                </value>]]></value>
                  </item>
                  <item id="orangered">
                      <description>orangered</description>
                      <value><![CDATA[
                <value>
                    <label>orangered</label>
                </value>]]></value>
                  </item>
                  <item id="chocolate">
                      <description>chocolate</description>
                      <value><![CDATA[
                <value>
                    <label>chocolate</label>
                </value>]]></value>
                  </item>
                  <item id="greenyellow">
                      <description>greenyellow</description>
                      <value><![CDATA[
                <value>
                    <label>greenyellow</label>
                </value>]]></value>
                  </item>
                  <item id="aquamarine">
                      <description>aquamarine</description>
                      <value><![CDATA[
                <value>
                    <label>aquamarine</label>
                </value>]]></value>
                  </item>
                  <item id="pink">
                      <description>pink</description>
                      <value><![CDATA[
                <value>
                    <label>pink</label>
                </value>]]></value>
                  </item>
                  <item id="grey">
                      <description>grey</description>
                      <value><![CDATA[
                <value>
                    <label>grey</label>
                </value>]]></value>
                  </item>
                  <item id="whitesmoke">
                      <description>whitesmoke</description>
                      <value><![CDATA[
                <value>
                    <label>whitesmoke</label>
                </value>]]></value>
                  </item>
              </items>
              <defaultValue>black</defaultValue>
              <dependency><![CDATA[%colormap:type="monochrome"]]></dependency>
            </entry>
            <!-- / single color monochrome -->

            <entry id="%color-A" type="enum" displayName="1st color">
              <documentation>
                <description>First color to use for the colormap.</description>
              </documentation>
              <items>
                  <item id="black">
                      <description>black</description>
                      <value><![CDATA[
                <value>
                    <label>black</label>
                </value>]]></value>
                  </item>
                  <item id="red">
                      <description>red</description>
                      <value><![CDATA[
                <value>
                    <label>red</label>
                </value>]]></value>
                  </item>
                  <item id="green">
                      <description>green</description>
                      <value><![CDATA[
                <value>
                    <label>green</label>
                </value>]]></value>
                  </item>
                  <item id="blue">
                      <description>blue</description>
                      <value><![CDATA[
                <value>
                    <label>blue</label>
                </value>]]></value>
                  </item>
                  <item id="orange">
                      <description>orange</description>
                      <value><![CDATA[
                <value>
                    <label>orange</label>
                </value>]]></value>
                  </item>
                  <item id="green">
                      <description>green</description>
                      <value><![CDATA[
                <value>
                    <label>green</label>
                </value>]]></value>
                  </item>
                  <item id="purple">
                      <description>purple</description>
                      <value><![CDATA[
                <value>
                    <label>purple</label>
                </value>]]></value>
                  </item>
                  <item id="cyan">
                      <description>cyan</description>
                      <value><![CDATA[
                <value>
                    <label>cyan</label>
                </value>]]></value>
                  </item>
                  <item id="magenta">
                      <description>magenta</description>
                      <value><![CDATA[
                <value>
                    <label>magenta</label>
                </value>]]></value>
                  </item>
                  <item id="yellow">
                      <description>yellow</description>
                      <value><![CDATA[
                <value>
                    <label>yellow</label>
                </value>]]></value>
                  </item>
                  <item id="salmon">
                      <description>salmon</description>
                      <value><![CDATA[
                <value>
                    <label>salmon</label>
                </value>]]></value>
                  </item>
                  <item id="orangered">
                      <description>orange-red</description>
                      <value><![CDATA[
                <value>
                    <label>orangered</label>
                </value>]]></value>
                  </item>
                  <item id="chocolate">
                      <description>chocolate</description>
                      <value><![CDATA[
                <value>
                    <label>chocolate</label>
                </value>]]></value>
                  </item>
                  <item id="greenyellow">
                      <description>green-yellow</description>
                      <value><![CDATA[
                <value>
                    <label>greenyellow</label>
                </value>]]></value>
                  </item>
                  <item id="aquamarine">
                      <description>aquamarine</description>
                      <value><![CDATA[
                <value>
                    <label>aquamarine</label>
                </value>]]></value>
                  </item>
                  <item id="pink">
                      <description>pink</description>
                      <value><![CDATA[
                <value>
                    <label>pink</label>
                </value>]]></value>
                  </item>
                  <item id="grey">
                      <description>grey</description>
                      <value><![CDATA[
                <value>
                    <label>grey</label>
                </value>]]></value>
                  </item>
                  <item id="whitesmoke">
                      <description>whitesmoke</description>
                      <value><![CDATA[
                <value>
                    <label>whitesmoke</label>
                </value>]]></value>
                  </item>
                  <item id="white">
                      <description>white</description>
                      <value><![CDATA[
                <value>
                    <label>white</label>
                </value>]]></value>
                  </item>
              </items>
              <defaultValue>black</defaultValue>
              <dependency><![CDATA[%colormap:type="bicolor"]]></dependency>
            </entry>
            <!-- / end color 1 -->

            <entry id="%color-B" type="enum" displayName="2nd color">
              <documentation>
                <description>Second color to use for the colormap.</description>
              </documentation>
              <items>
                  <item id="black">
                      <description>black</description>
                      <value><![CDATA[
                <value>
                    <label>black</label>
                </value>]]></value>
                  </item>
                  <item id="red">
                      <description>red</description>
                      <value><![CDATA[
                <value>
                    <label>red</label>
                </value>]]></value>
                  </item>
                  <item id="green">
                      <description>green</description>
                      <value><![CDATA[
                <value>
                    <label>green</label>
                </value>]]></value>
                  </item>
                  <item id="blue">
                      <description>blue</description>
                      <value><![CDATA[
                <value>
                    <label>blue</label>
                </value>]]></value>
                  </item>
                  <item id="orange">
                      <description>orange</description>
                      <value><![CDATA[
                <value>
                    <label>orange</label>
                </value>]]></value>
                  </item>
                  <item id="green">
                      <description>green</description>
                      <value><![CDATA[
                <value>
                    <label>green</label>
                </value>]]></value>
                  </item>
                  <item id="purple">
                      <description>purple</description>
                      <value><![CDATA[
                <value>
                    <label>purple</label>
                </value>]]></value>
                  </item>
                  <item id="cyan">
                      <description>cyan</description>
                      <value><![CDATA[
                <value>
                    <label>cyan</label>
                </value>]]></value>
                  </item>
                  <item id="magenta">
                      <description>magenta</description>
                      <value><![CDATA[
                <value>
                    <label>magenta</label>
                </value>]]></value>
                  </item>
                  <item id="yellow">
                      <description>yellow</description>
                      <value><![CDATA[
                <value>
                    <label>yellow</label>
                </value>]]></value>
                  </item>
                  <item id="salmon">
                      <description>salmon</description>
                      <value><![CDATA[
                <value>
                    <label>salmon</label>
                </value>]]></value>
                  </item>
                  <item id="orangered">
                      <description>orangered</description>
                      <value><![CDATA[
                <value>
                    <label>orangered</label>
                </value>]]></value>
                  </item>
                  <item id="chocolate">
                      <description>chocolate</description>
                      <value><![CDATA[
                <value>
                    <label>chocolate</label>
                </value>]]></value>
                  </item>
                  <item id="greenyellow">
                      <description>greenyellow</description>
                      <value><![CDATA[
                <value>
                    <label>greenyellow</label>
                </value>]]></value>
                  </item>
                  <item id="aquamarine">
                      <description>aquamarine</description>
                      <value><![CDATA[
                <value>
                    <label>aquamarine</label>
                </value>]]></value>
                  </item>
                  <item id="pink">
                      <description>pink</description>
                      <value><![CDATA[
                <value>
                    <label>pink</label>
                </value>]]></value>
                  </item>
                  <item id="grey">
                      <description>grey</description>
                      <value><![CDATA[
                <value>
                    <label>grey</label>
                </value>]]></value>
                  </item>
                  <item id="whitesmoke">
                      <description>whitesmoke</description>
                      <value><![CDATA[
                <value>
                    <label>whitesmoke</label>
                </value>]]></value>
                  </item>
              </items>
              <defaultValue>black</defaultValue>
              <dependency><![CDATA[%colormap:type="bicolor"]]></dependency>
            </entry>
            <!-- end color 2 -->

            <entry id="%color-a" type="string" displayName="Color 1 (ex.: cyan or #00FFFF)">
              <documentation>
                <description>1st color (ex.: cyan or #00FFFF)</description>
              </documentation>
              <xmcda><![CDATA[
                <value>
                    <label>%s</label>
                </value>]]></xmcda>
              <defaultValue>black</defaultValue>
              <dependency><![CDATA[(%colormap:type="multicolor") AND (%nb_colors:value >= 1)]]></dependency>
            </entry>
            <entry id="%color-b" type="string" displayName="Color 2">
              <documentation>
                <description>2nd color</description>
              </documentation>
              <xmcda><![CDATA[
                <value>
                    <label>%s</label>
                </value>]]></xmcda>
              <defaultValue>black</defaultValue>
              <dependency><![CDATA[(%colormap:type="multicolor") AND (%nb_colors:value >= 2)]]></dependency>
            </entry>
            <entry id="%color-c" type="string" displayName="Color 3">
              <documentation>
                <description>3rd color</description>
              </documentation>
              <xmcda><![CDATA[
                <value>
                    <label>%s</label>
                </value>]]></xmcda>
<!--            <constraint>
                   <description>The value cannot be empty.</description>
                  <code>%color-c != ""</code>
                </constraint>
-->             <defaultValue>black</defaultValue>
              <dependency><![CDATA[(%colormap:type="multicolor") AND (%nb_colors:value >= 3)]]></dependency>
            </entry>
            <entry id="%color-d" type="string" displayName="Color 4">
              <documentation>
                <description>4th color</description>
              </documentation>
              <xmcda><![CDATA[
                <value>
                    <label>%s</label>
                </value>]]></xmcda>
              <defaultValue>black</defaultValue>
              <dependency><![CDATA[(%colormap:type="multicolor") AND (%nb_colors:value >= 4)]]></dependency>
            </entry>
            <entry id="%color-e" type="string" displayName="Color 5">
              <documentation>
                <description>5th color</description>
              </documentation>
              <xmcda><![CDATA[
                <value>
                    <label>%s</label>
                </value>]]></xmcda>
              <defaultValue>black</defaultValue>
              <dependency><![CDATA[(%colormap:type="multicolor") AND (%nb_colors:value >= 5)]]></dependency>
            </entry>
            <entry id="%color-f" type="string" displayName="Color 6">
              <documentation>
                <description>6th color</description>
              </documentation>
              <xmcda><![CDATA[
                <value>
                    <label>%s</label>
                </value>]]></xmcda>
              <defaultValue>black</defaultValue>
              <dependency><![CDATA[(%colormap:type="multicolor") AND (%nb_colors:value >= 6)]]></dependency>
            </entry>
            <entry id="%color-g" type="string" displayName="Color 7">
              <documentation>
                <description>7th color</description>
              </documentation>
              <xmcda><![CDATA[
                <value>
                    <label>%s</label>
                </value>]]></xmcda>
              <defaultValue>black</defaultValue>
              <dependency><![CDATA[(%colormap:type="multicolor") AND (%nb_colors:value >= 7)]]></dependency>
            </entry>
            <entry id="%color-h" type="string" displayName="Color 8">
              <documentation>
                <description>8th color</description>
              </documentation>
              <xmcda><![CDATA[
                <value>
                    <label>%s</label>
                </value>]]></xmcda>
              <defaultValue>black</defaultValue>
              <dependency><![CDATA[(%colormap:type="multicolor") AND (%nb_colors:value >= 8)]]></dependency>
            </entry>
            <entry id="%color-i" type="string" displayName="Color 9">
              <documentation>
                <description>9th color</description>
              </documentation>
              <xmcda><![CDATA[
                <value>
                    <label>%s</label>
                </value>]]></xmcda>
              <defaultValue>black</defaultValue>
              <dependency><![CDATA[(%colormap:type="multicolor") AND (%nb_colors:value >= 9)]]></dependency>
            </entry>
            <entry id="%color-j" type="string" displayName="Color 10">
              <documentation>
                <description>10th color</description>
              </documentation>
              <xmcda><![CDATA[
                <value>
                    <label>%s</label>
                </value>]]></xmcda>
              <defaultValue>black</defaultValue>
              <dependency><![CDATA[(%colormap:type="multicolor") AND (%nb_colors:value >= 10)]]></dependency>
            </entry>
            <!-- end multi colors -->

            <entry id="%7a" type="enum" displayName="Image file extension">
              <documentation>
                <description>File extension of generated image figure.</description>
              </documentation>
              <items>
                  <item id="eps">
                      <description>.eps (Encapsulated PostScript)</description>
                      <value>eps</value>
                  </item>
                  <item id="jpg">
                      <description>.jpg (Joint Photographic Experts Group)</description>
                      <value>jpg</value>
                  </item>
                  <item id="pdf">
                      <description>.pdf (Portable Document Format)</description>
                      <value>pdf</value>
                  </item>
                  <item id="pgf">
                      <description>.pgf (Progressive Graphics File)</description>
                      <value>pgf</value>
                  </item>
                  <item id="png">
                      <description>.png (Portable Network Graphics)</description>
                      <value>png</value>
                  </item>
                  <item id="ps">
                      <description>.ps (PostScript)</description>
                      <value>ps</value>
                  </item>
                  <item id="raw">
                      <description>.raw (Raw RGBA bitmap)</description>
                      <value>raw</value>
                  </item>
                  <item id="rgba">
                      <description>.rgba (Silicon Graphics RGB)</description>
                      <value>rgba</value>
                  </item>
                  <item id="svg">
                      <description>.svg (Scalable Vector Graphics)</description>
                      <value>svg</value>
                  </item>
                  <item id="svgz">
                      <description>.svgz (Compressed Scalable Vector Graphics)</description>
                      <value>svgz</value>
                  </item>
                  <item id="tif">
                      <description>.tif (Tagged Image File Format)</description>
                      <value>tif</value>
                  </item>
              </items>
              <defaultValue>png</defaultValue>
              <dependency><![CDATA[%8:type="matplotlib"]]></dependency>
            </entry>
            <entry id="%7b" type="enum" displayName="Image file extension">
              <documentation>
                <description>File extension of generated image figure.</description>
              </documentation>
              <items>
                  <item id="eps">
                      <description>.eps (Encapsulated PostScript)</description>
                      <value>eps</value>
                  </item>
                  <item id="jpg">
                      <description>.jpg (Joint Photographic Experts Group)</description>
                      <value>jpg</value>
                  </item>
                  <item id="pdf">
                      <description>.pdf (Portable Document Format)</description>
                      <value>pdf</value>
                  </item>
                  <item id="png">
                      <description>.png (Portable Network Graphics)</description>
                      <value>png</value>
                  </item>
                  <item id="svg">
                      <description>.svg (Scalable Vector Graphics)</description>
                      <value>svg</value>
                  </item>
              </items>
              <defaultValue>png</defaultValue>
              <dependency><![CDATA[%8:type="gnuplot"]]></dependency>
            </entry>
            <entry id="%8" type="enum" displayName="Plotter">
              <documentation>
                <description>Plotter used to generate image.</description>
              </documentation>
              <items>
                  <item id="matplotlib">
                      <description>Matplotlib</description>
                      <value>matplotlib</value>
                  </item>
                  <item id="gnuplot">
                      <description>Gnuplot</description>
                      <value>gnuplot</value>
                  </item>
              </items>
              <defaultValue>matplotlib</defaultValue>
            </entry>
            <entry id="%9" type="enum" displayName="Order alternatives by">
              <documentation>
                <description>Defines how alternatives will be sorted on the plots.</description>
              </documentation>
              <items>
                  <item id="id">
                      <description>ids</description>
                      <value>id</value>
                  </item>
                  <item id="name">
                      <description>names</description>
                      <value>name</value>
                  </item>
                  <item id="value">
                      <description>values</description>
                      <value>value</value>
                  </item>
              </items>
              <defaultValue>values</defaultValue>
            </entry>
            <entry id="%10" type="enum" displayName="Order criteria by">
              <documentation>
                <description>Defines how criteria will be sorted on the figure.</description>
              </documentation>
              <items>
                  <item id="id">
                      <description>ids</description>
                      <value>id</value>
                  </item>
                  <item id="name">
                      <description>names</description>
                      <value>name</value>
                  </item>
              </items>
              <defaultValue>id</defaultValue>
            </entry>
            <entry id="%11" type="boolean" displayName="Reverse order">
              <documentation>
                <description>Defines if order direction should be reversed (descending order) or not.</description>
              </documentation>
              <defaultValue>false</defaultValue>
            </entry>
            <entry id="%12" type="boolean" displayName="Mix colors">
              <documentation>
                <description>Defines if colors should be mixed for a better contrast (more color-blind friendly) or not.</description>
              </documentation>
              <defaultValue>false</defaultValue>
            </entry>
            <entry id="%13" type="string" displayName="Superposed plot title">
              <documentation>
                <description>
                    Title of the plot superposing all alternatives performances.
                    This plot is not rendered if this parameter is not provided.
                    Only usable for starChart plots.
                </description>
              </documentation>
              <dependency><![CDATA[%3:type="starChart"]]></dependency>
            </entry>
            <entry id="%14" type="enum" displayName="Naming conventions">
              <documentation>
                <description>How categories and alternatives are labelled on the graph.</description>
              </documentation>
              <items>
                  <item id="id">
                      <description>Only ids are shown</description>
                      <value>id</value>
                  </item>
                  <item id="name">
                      <description>Only names are shown (disambiguated by appending the ids, if needed)</description>
                      <value>name</value>
                  </item>
                  <item id="name (id)">
                      <description>Names and ids are shown in that order</description>
                      <value>name (id)</value>
                  </item>
                  <item id="id (name)">
                      <description>Ids and names are shown in that order</description>
                      <value>id (name)</value>
                  </item>
              </items>
              <defaultValue>name</defaultValue>
            </entry>
          </gui>
        </input>

    <output displayName="performance table plot" name="glob:numericPerformanceTable.{bmp,dia,fig,gif,hpgl,ico,jpg,jpeg,jpe,pdf,png,ps,ps2,svg,svgz,tif,tiff}" id="numericPerformanceTablePlot">
        <documentation>
            <description>Image containing all selected values for performance table plots. Format corresponds to the one given in parameters (default is .png).</description>
        </documentation>
    </output>

    <output displayName="performance table plot script" name="glob:plot_numericPerformanceTable.{py,plt}" id="numericPerformanceTablePlotScript">
        <documentation>
            <description>
                Generated Python or Gnuplot script that made the image.
                Given to enable users to later customize the appearance of the plots.
                Extension is .py if matplotlib is used, .plt for gnuplot.
            </description>
        </documentation>
    </output>

    <output displayName="messages" name="messages" id="messages">
        <documentation>
            <description>Status messages.</description>
        </documentation>
        <xmcda tag="programExecutionResult" />
    </output>
  </parameters>
</program_description>
