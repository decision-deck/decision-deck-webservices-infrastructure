#! /bin/bash

# If the script isn't in the PATH, replace the following with its absolute path
call=/home/TB/projects/mcda/decision-deck/infrastructure-webservices/python/genericXMCDAService.py

$call -U http://webservices-test-v3.decision-deck.org/soap/%s.py \
      -n weightedSum-XMCDAv3 \
      --submit-and-wait-solution \
      alternatives:tests/in1/alternatives.xml \
      criteria:tests/in1/criteria.xml \
      criteriaWeights:tests/in1/criteriaWeights.xml \
      performanceTable:tests/in1/performanceTable.xml \
      parameters:tests/in1/parameters.xml
